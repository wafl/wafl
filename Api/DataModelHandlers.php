<?php

namespace Wafl\Api;

class DataModelHandlers
{
    private static $_authenticateWriteAccessMethod = null;
    private static $_authenticateReadAccessMethod = null;

    public static function SetAuthenticateWriteAccessMethod($callbackMethod)
    {
        self::$_authenticateWriteAccessMethod = $callbackMethod;
    }

    public static function SetAuthenticateReadAccessMethod($callbackMethod)
    {
        self::$_authenticateReadAccessMethod = $callbackMethod;
    }

    public static function UpdateDblEjDataModel($requestObject, $headers, $files)
    {
        $writeMethod = self::$_authenticateWriteAccessMethod;
        if ($writeMethod == null || $writeMethod($requestObject))
        {
            if (is_a($requestObject, "\DblEj\Data\PersistableModel"))
            {
                try
                {
                    if ($requestObject->Save())
                    {
                        return $requestObject;
                    }
                    else
                    {
                        return new \DblEj\Communication\Ajax\AjaxError(new \Exception("Object did not persist from client-side save attempt."));
                    }
                } catch (\Exception $ex) {
                    return new \DblEj\Communication\Ajax\AjaxError(new \Exception("Object did not persist from client-side save attempt due to an exception: ".$ex->getMessage()));
                }
            }
            else
            {
                if (is_object($requestObject))
                {
                    return new \DblEj\Communication\Ajax\AjaxError(new \Exception("Object of type " . get_class($requestObject) . " is not persistable"));
                }
                elseif (is_array($requestObject))
                {
                    return new \DblEj\Communication\Ajax\AjaxError(new \Exception("Array is not persistable"));
                }
            }
        }
        else
        {
            return new \DblEj\Communication\Ajax\AjaxError(new \Exception("You do not have permission to update this object (".  get_class($requestObject).")"));
        }
    }

    public static function GetDblEjDataModelByKey($requestObject, $headers, $files)
    {
        if (!isset($requestObject["serverClass"]))
        {
            throw new \Exception("When getting a model using GetDblEjDataModelByKey, you must pass the serverClass parameter.");
        }
        $key       = isset($requestObject["keyValue"])?$requestObject["keyValue"]:null;
        $className = $requestObject["serverClass"];
        $className = \Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Application()->Get_RootNameSpace() . "\\FunctionalModel\\$className";
        $instance  = new $className($key);
        $readMethod  = self::$_authenticateReadAccessMethod;
        if ($readMethod == null || $readMethod($instance))
        {
            return $instance;
        }
        else
        {
            return new \DblEj\Communication\Ajax\AjaxError(new \Exception("You do not have permission to get this object"));
        }
    }

    public static function DeleteDblEjDataModelByKey($requestObject, $headers, $files)
    {
        $writeMethod = self::$_authenticateWriteAccessMethod;
        $readMethod  = self::$_authenticateReadAccessMethod;
        $key       = $requestObject["keyValue"];
        $className = $requestObject["serverClass"];
        $className = \Wafl\Core::$RUNNING_APPLICATION->Get_Namespace() . "\\FunctionalModel\\$className";
        $instance  = new $className($key);
        if ($writeMethod == null || $writeMethod($instance))
        {
            if ($readMethod == null || $readMethod($instance))
            {
                $instance->Delete();
                return new \DblEj\Communication\Ajax\AjaxResult("Object deleted");
            }
            else
            {
                return new \DblEj\Communication\Ajax\AjaxError(new \Exception("You do not have permission to get this object for deletion"));
            }
        }
        else
        {
            return new \DblEj\Communication\Ajax\AjaxError(new \Exception("You do not have permission to delete this object"));
        }
    }
}