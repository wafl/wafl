<?php

namespace Wafl\Api;

class DateTimeHandlers
{
    public static function SetTimezoneOffset($requestObject, $headers, $files, $app)
    {
        if (!isset($requestObject["TzOffset"]))
        {
            throw new \Exception("The SetTimezoneOffset API was called without a timezone argument");
        }
        $tzOffset = intval($requestObject["TzOffset"]);
        $app->Set_ClientGmtOffset($tzOffset * 60);
        return new \DblEj\Communication\Ajax\AjaxResult("Timezone Set to UTC" . (($tzOffset > 0) ? "+" : "-") . $requestObject["TzOffset"]);
    }
}