<?php
namespace Wafl\AppSupport;

use Wafl\Application\AppRunner;
use Wafl\Core;

/**
 * Initialize Wafl and run the app
 */
class Application
{

    public static function BootstrapAndRunApplication($applicationSyrupFile, $environment = null, $applicationClass = "\\Wafl\\Application\\MvcWebApplication", $performanceLogPath = null)
    {
        $application = self::BootstrapApplication($applicationSyrupFile, $environment, $applicationClass, $performanceLogPath);
        return self::Run($application);
    }

    public static function BootstrapAndRunModularApplication($applicationSyrupFile, $environment = null, $applicationClass = "\\Wafl\\Application\\ModularMvcWebApplication", $performanceLogPath = null)
    {
        $application = self::BootstrapModularApplication($applicationSyrupFile, $environment, $applicationClass, $performanceLogPath);
        return self::Run($application);
    }

    public static function BootstrapApplication($applicationSyrupFile, $environment = null, $applicationClass = "\\Wafl\\Application\\MvcWebApplication", $performanceLogPath = null)
    {
        self::_bootstrapCommon($applicationSyrupFile, $environment);
        $app = AppRunner::PrepareAppFromSyrup($applicationSyrupFile, "\\Wafl\\Syrup\\Utils", true, $environment, $applicationClass, $performanceLogPath);
        require_once(WAFL_PATH . DIRECTORY_SEPARATOR . "Bootstrapper.php");
        \Wafl\Bootstrapper::BootstrapEnvironment($app->Get_Settings()->Get_Debug()->Get_DebugMode(), $app);
        \Wafl\Bootstrapper::BootstrapApplication($app);

        //initialize site pages and controls for web apps
        if (is_a($app, "\\DblEj\\Application\\IWebApplication"))
        {
            foreach (\DblEj\SiteStructure\SitePage::GetAll() as $sitePage)
            {
                foreach ($sitePage->GetDeferredControls() as $deferredControl)
                {
                    $control = \Wafl\Util\Controls::GetControl($deferredControl);
                    $sitePage->AddControl($control);
                }
            }
        }
        return $app;
    }

    public static function BootstrapModularApplication($applicationSyrupFile, $environment = null, $applicationClass = "\\Wafl\\Application\\ModularMvcWebApplication", $performanceLogPath = null)
    {
        self::_bootstrapCommon($applicationSyrupFile, $environment);

        $app = AppRunner::PrepareModularAppFromSyrup($applicationSyrupFile, "\\Wafl\\Syrup\\Utils", true, $environment, $applicationClass, $performanceLogPath);
        require_once(WAFL_PATH . DIRECTORY_SEPARATOR . "Bootstrapper.php");
        \Wafl\Bootstrapper::BootstrapEnvironment($app->Get_Settings()->Get_Debug()->Get_DebugMode(), $app);
        \Wafl\Bootstrapper::BootstrapApplication($app);

        //initialize site pages and controls for web apps
        if (is_a($app, "\\DblEj\\Application\\IWebApplication"))
        {
            foreach (\DblEj\SiteStructure\SitePage::GetAll() as $sitePage)
            {
                foreach ($sitePage->GetDeferredControls() as $deferredControl)
                {
                    $control = \Wafl\Util\Controls::GetControl($deferredControl);
                    $sitePage->AddControl($control);
                }
            }
        }
        return $app;
    }

    public static function Run(\DblEj\Application\IApplication $application, &$exitCode = null)
    {
        return $application->Run($exitCode);
    }

    /**
     * Setup temporary error handling for use until the real handlers get setup
     */
    private static function _setupTemporaryErrorHandlers()
    {
        error_reporting(E_ALL);
        ini_set("display_errors", 0);
        ini_set("display_startup_errors", 0);
        ini_set("html_errors", 0);

        set_error_handler
        (
            function($errorNumber, $message, $file, $line, $context)
            {
                if
                (
                    $errorNumber == E_WARNING || $errorNumber == E_USER_WARNING ||
                    $errorNumber == E_NOTICE || $errorNumber == E_USER_NOTICE ||
                    $errorNumber == E_STRICT || $errorNumber == E_DEPRECATED || $errorNumber == E_USER_DEPRECATED
                )
                {
                    //for recoverable errors, dont throw an exception.
                    //let the app continue in case some other component
                    //is testing for the error and wants to throw an informative exception.
                    //If we handle the error, then they dont get a chance to catch it.
                    return false;
                }
                $reptLevel = error_reporting();
                if ($reptLevel > 0)
                {
                    self::_handleException(new \ErrorException($message, $errorNumber, 0, $file, $line));
                }
            },
            E_ALL
        );
        set_exception_handler(
        function($exception)
        {
            self::_handleException($exception);
        }
        );
        register_shutdown_function(
            function()
            {
                if (!defined("EXCEPTION_OUTPUT"))
                {
                    if (!defined("WAFL_ERROR_HANDLERS_LOADED"))
                    {
                        $lastError = error_get_last();
                        if ($lastError !== null)
                        {
                            $reptLevel = error_reporting();
                            if ($reptLevel > 0)
                            {
                                if ((!defined("AM_WEBPAGE") || AM_WEBPAGE))
                                {
                                    if (!headers_sent())
                                    {
                                        header("HTTP/1.0 500 Application fatal error detected during shutdown " . $lastError["type"]);
                                    }
                                    print("<html><head><title>Application fatal error detected during shutdown</title><body style='padding: 0 32px; text-align: center;'><h2 style='color: red;'>Http 500</h2><h3 style='color: red;'>Fatal ActiveWAFL Application Startup Error</h3>"
                                    . "<hr><p style='font-size: 20px;text-align: left;'>" . $lastError["message"] . "</p>"
                                    . "<div style='text-align: left;'>in <b>" . $lastError["file"] . "</b> at <b>line " . $lastError["line"] . "</b></div>"
                                    . "</body></html>");
                                }
                                else
                                {
                                    print("**Application fatal error detected during shutdown**\n"
                                    . "---------------\n"
                                    . $lastError["message"] . "\n"
                                    . "in " . $lastError["file"] . " at line " . $lastError["line"]);
                                }
                            }
                        }
                        elseif (Core::Get_RequestPending())
                        {
                            if ((!defined("AM_WEBPAGE") || AM_WEBPAGE))
                            {
                                if (!headers_sent())
                                {
                                    header("HTTP/1.0 500 ActiveWAFL Application Startup Warning");
                                }
                                print("<html><head><title>ActiveWAFL Application Startup Warning</title><body style='padding: 0 32px; text-align: center;'><h2 style='color: orange;'>Http 500</h2><h3 style='color: orange;'>ActiveWAFL Application Startup Warning</h3>"
                                . "<hr><p style='font-size: 20px;text-align: left;'>ActiveWAFL was successfully initialized and shutdown, however, no Application ran.</p>"
                                . "<div style='text-align: left;'>This indicates that your app never started, or that it exited prematurely *without any errors*.  This could be caused by an error with your Wafl configuration.  Note that if Application Auto Start is turned off, then you must start the application manually.</div>"
                                . "</body></html>");
                            }
                            else
                            {
                                print("**ActiveWAFL Application Startup Warning**\n"
                                . "-----------\n"
                                . "ActiveWAFL was successfully initialized and shutdown, however, no Application ran.\n"
                                . "This indicates that your app never started, or that it exited prematurely *without any errors*.  "
                                . "This could be caused by an error with your Wafl configuration.  "
                                . "Note that if Application Auto Start is turned off, then you must start the application manually.");
                            }
                        }
                        else
                        {
                            if ((!defined("AM_WEBPAGE") || AM_WEBPAGE))
                            {
                                if (!headers_sent())
                                {
                                    header("HTTP/1.0 500 ActiveWAFL Application Startup Warning");
                                }
                                print("<html><head><title>ActiveWAFL Application Startup Warning</title><body style='padding: 0 32px; text-align: center;'><h2 style='color: orange;'>Http 500</h2><h3 style='color: orange;'>ActiveWAFL Application Startup Warning</h3>"
                                . "<hr><p style='font-size: 20px;text-align: left;'>ActiveWAFL was successfully loaded and shutdown, however, the application was never initialized and it did not run.</p>"
                                . "<div style='text-align: left;'>This is usually due to Wafl Application AutoStart being turned off.  If Application Auto Start is turned off, then you must start the application manually.  If Auto Start is already on, then this error indicates that the application was intentionally halted before completion.</div>"
                                . "</body></html>");
                            }
                            else
                            {
                                print("**ActiveWAFL Application Startup Warning**"
                                . "--------------"
                                . "ActiveWAFL was successfully loaded and shutdown, however, the application was never initialized and it did not run.\n"
                                . "This is usually due to Wafl Application AutoStart being turned off.  "
                                . "If Application Auto Start is turned off, then you must start the application manually.  "
                                . "If Auto Start is already on, then this error indicates that the application was intentionally halted before completion.</div>");
                            }
                        }
                        define("EXCEPTION_OUTPUT", true);
                    }
                }
            }
        );
    }

    private static function _handleException($exception)
    {
        $errorColor = "red";
        $errorTypeString = "Error";
        switch ($exception->getCode())
        {
            case E_WARNING:
            case E_USER_WARNING:
            case E_DEPRECATED:
                $errorColor = "orange";
                $errorTypeString = "Warning";
                break;
            case E_NOTICE:
            case E_USER_NOTICE:
            case E_STRICT:
                $errorColor = "blue";
                $errorTypeString = "Notice";
                break;
        }
        if ((!defined("AM_WEBPAGE") || AM_WEBPAGE) && !headers_sent())
        {
            header("HTTP/1.0 500 Application Startup $errorTypeString " . $exception->getCode());
            print("<html><head><title>Application Startup $errorTypeString</title>"
            . "<body style='padding: 32px; text-align: center;'>"
            . "<h2 style='color: $errorColor;'>Http 500</h2>"
            . "<h3 style='color: $errorColor;'>Application Startup $errorTypeString</h3>"
            . "<small>Severity: " . $exception->getCode() . "</small>"
            . "<hr><p style='font-size: 20px;text-align: left;'>" . $exception->getMessage() . "</p>"
            . "<div style='text-align: left;'>in <i>" . $exception->getFile() . "</i> at " . $exception->getLine() . "</div>"
            . "<div style='text-align: left;'>" . nl2br($exception->getTraceAsString()) . "</div>");
            while ($prev = $exception->getPrevious())
            {
                print "<div style='text-align: left;'>" . nl2br($prev->getTraceAsString()) . "</div>";
            }
            print ("</body></html>");
        }
        else
        {
            print("**Application Startup $errorTypeString**\n"
            . "Severity: " . $exception->getCode() . "\n"
            . "-----------------\n");

            while ($exception !== null)
            {
                print ("\n" . $exception->getMessage() . "\n"
                . "in " . $exception->getFile() . " at " . $exception->getLine() . "\n\n"
                . $exception->getTraceAsString() . "\n\n");
                $exception = $exception->getPrevious();
            }
        }
        if (!defined("EXCEPTION_OUTPUT"))
        {
            define("EXCEPTION_OUTPUT", true);
        }
    }
    public static function SetupClassLoader($waflPath, $dblEjPath)
    {
        if (!class_exists("\\Wafl\\Autoloaders\\ClassLoader", false))
        {
            require_once($waflPath . "Autoloaders" . DIRECTORY_SEPARATOR . "ClassLoader.php");
            new \Wafl\Autoloaders\ClassLoader;
        }
    }

    public static function GetWaflPathsFromSyrp($applicationSyrpFile, $env = null)
    {
        //load basic settings from Application.syrp
        $parsedConfig = self::ParseSyrupFileAsArray($applicationSyrpFile);
        if (!isset($parsedConfig["Paths"]))
        {
            throw new \Exception("Application settings file ($applicationSyrpFile) is missing the Paths section");
        }
        if (!isset($parsedConfig["Paths"]["Application"]))
        {
            throw new \Exception("Application settings file ($applicationSyrpFile) is missing the Paths&gt;Application section");
        }
        $appConfigFolder = $parsedConfig["Paths"]["Application"]["ConfigFolder"];

        if (!$env)
        {
            $env = WAFL_ENVIRONMENT;
        }
        $appRoot         = dirname($applicationSyrpFile) . DIRECTORY_SEPARATOR;
        $appConfigFolder = $appRoot . $appConfigFolder;
        $appSettingsFile = $appConfigFolder . "Settings.$env.syrp";
        if (!file_exists($appSettingsFile))
        {
            throw new \Exception("Application settings file is missing for the $env environment.  It was expected at $appSettingsFile");
        }

        $appSettingsIni = self::ParseSyrupFileAsArray($appSettingsFile);
        if (!isset($appSettingsIni["Paths"]))
        {
            throw new \Exception("Application settings file ($appSettingsFile) is missing the Paths section");
        }
        if (!isset($appSettingsIni["Paths"]["Application"]))
        {
            throw new \Exception("Application settings file ($appSettingsFile) is missing the Paths&gt;Application section");
        }
        
        $waflFolder = realpath($appSettingsIni["Paths"]["Wafl"]["WaflFolder"]);
        $dblEjFolder = realpath($appSettingsIni["Paths"]["Wafl"]["DblEjFolder"]);
        
        if ($waflFolder === false)
        {
            throw new \Exception("Invalid WaflFolder");
        }
        
        if ($dblEjFolder === false)
        {
            throw new \Exception("Invalid DblEjFolder");
        }
       
        if (substr($waflFolder, strlen($waflFolder) - 1) != DIRECTORY_SEPARATOR)
        {
            $waflFolder .= DIRECTORY_SEPARATOR;
        }
        if (substr($dblEjFolder, strlen($dblEjFolder) - 1) != DIRECTORY_SEPARATOR)
        {
            $dblEjFolder .= DIRECTORY_SEPARATOR;
        }
        
        return array(
            "Wafl"  => $appSettingsIni["Paths"]["Wafl"]["WaflFolder"],
            "DblEj" => $appSettingsIni["Paths"]["Wafl"]["DblEjFolder"]);
    }

    private static function _bootstrapCommon($applicationSyrupFile, $environment = null)
    {
        /**
         * Load temporary error handlers to catch any startup errors that occur
         * before Wafl registers the main error handlers.
         */
        self::_setupTemporaryErrorHandlers();

        /**
         * Get the wafl path from the settings ini
         */
        $waflPaths = self::GetWaflPathsFromSyrp($applicationSyrupFile, $environment);

        self::SetupClassLoader($waflPaths["Wafl"], $waflPaths["DblEj"]);

        require_once($waflPaths["Wafl"] . "Application" . DIRECTORY_SEPARATOR . "AppRunner.php");
    }

    public static function ParseSyrupFileAsArray($syrupFile, $useCache = true)
    {
        if ($useCache && is_writable(sys_get_temp_dir()))
        {
            $cacheFolder = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "SyrupCache";
            if (!file_exists($cacheFolder))
            {
                mkdir($cacheFolder, 0777, true);
                chmod($cacheFolder, 0777);
            }
        }
        else
        {
            $useCache = false;
        }
        if ($useCache)
        {
            $cacheFile = $cacheFolder . DIRECTORY_SEPARATOR . md5($syrupFile) . ".syrupcache"; //we use a hash because we want to capture the uniqueness based on entire path
            //if the syrup file is newer than the cache, then refresh the cache
            if (file_exists($cacheFile) && (filemtime($syrupFile) > filemtime($cacheFile)))
            {
                unlink($cacheFile);
            }
            if (file_exists($cacheFile))
            {
                $syrup = unserialize(file_get_contents($cacheFile));
            }
            else
            {
                $syrup = \Wafl\Syrup\Utils::ParseFileAsArray($syrupFile);
                file_put_contents($cacheFile, serialize($syrup));
            }
        }
        else
        {
            $syrup = \Wafl\Syrup\Utils::ParseFileAsArray($syrupFile);
        }
        return $syrup;
    }

    public static function InvalidateSyrupCache($syrupFile)
    {
        $cacheFolder = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "SyrupCache";
        $cacheFile   = $cacheFolder . DIRECTORY_SEPARATOR . md5($syrupFile) . ".syrupcache";
        if (file_exists($cacheFile))
        {
            unlink($cacheFile);
        }
    }

    public static function PurgeSyrupCache()
    {
        $cacheFolder = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "SyrupCache";
        if (file_exists($cacheFolder))
        {
            unlink($cacheFolder);
        }
    }
}