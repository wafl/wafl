<?php
namespace Wafl\AppSupport;

use DblEj\Extension\IModularLoader;
use Wafl\Application\Application;
use Wafl\Application\ModularMvcWebApplication;
use Wafl\AppSupport\Application as ApplicationUtil;

class WebIndex
{
    public static $DISK_CACHE_TIMEOUT = 600;
    private static $_TEMP_FOLDER = null;

    private static function _init()
    {
        if (!self::$_TEMP_FOLDER)
        {
            self::$_TEMP_FOLDER = sys_get_temp_dir() . DIRECTORY_SEPARATOR ."Wafl";
            if (!file_exists(self::$_TEMP_FOLDER))
            {
                mkdir(self::$_TEMP_FOLDER, 0777, true);
                chmod(self::$_TEMP_FOLDER, 0777);
            }
        }
    }

    /**
     *
     * @param string $syrupConfigFile
     * @param string $environment
     * @param string $applicationClass
     * @param string $performanceLogPath
     * @return \Wafl\Application\WebApplication
     */
    public static function BootstrapApplication($syrupConfigFile, $environment = null, $applicationClass = "\\Wafl\\Application\\ModularMvcWebApplication", $performanceLogPath = null)
    {
        self::_init();
        $application = ApplicationUtil::BootstrapApplication($syrupConfigFile, $environment, $applicationClass, $performanceLogPath);
        return $application;
    }

    /**
     *
     * @param string $syrupConfigFile
     * @param string $environment
     * @param string $applicationClass
     * @param string $performanceLogPath
     * @return \Wafl\Application\ModularMvcWebApplication
     */
    public static function BootstrapModularApplication($syrupConfigFile, $environment = null, $applicationClass = "\\Wafl\\Application\\ModularMvcWebApplication", $performanceLogPath = null)
    {
        self::_init();
        $application = ApplicationUtil::BootstrapModularApplication($syrupConfigFile, $environment, $applicationClass, $performanceLogPath);
        return $application;
    }

    public static function RunApplication(Application $application, &$exitCode)
    {
        self::_init();
        $localRoot = $application->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
        $appFolder = $application->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR;
        $localAppFolder = $localRoot . $appFolder;

        if (@file_exists($localAppFolder . $application->Get_Settings()->Get_Paths()->Get_Application()->Get_GlobalScript()))
        {
            include_once($localAppFolder . $application->Get_Settings()->Get_Paths()->Get_Application()->Get_GlobalScript());
        }
        return ApplicationUtil::Run($application, $exitCode);
    }

    public static function RunModularApplication(ModularMvcWebApplication $modularApplication, IModularLoader $modularloader, &$exitCode = null)
    {
        self::_init();

        //add modules
        foreach ($modularloader->GetModules($modularApplication) as $waflModule)
        {
            $modularApplication->AddModule($waflModule);
        }

        //load resource permissions
        foreach ($modularloader->GetResources() as $resource)
        {
            $modularApplication->AddRestrictedResource($resource);
        }

        foreach ($modularloader->GetResourcePermissions() as $resourcePermission)
        {
            $modularApplication->AddResourcePermissionObject($resourcePermission);
        }
        return self::RunApplication($modularApplication, $exitCode);
    }

    public static function BootstrapAndRunApplication($syrupConfigFile, $environment = null, $applicationClass = "\\Wafl\\Application\\MvcWebApplication")
    {
        self::_init();

        $application = self::BootstrapWebApplication($syrupConfigFile, $environment, $applicationClass);
        self::RunApplication($application);
    }

    public static function BootstrapAndRunModularApplication($syrupConfigFile, IModularLoader $modularloader, $environment = null, $applicationClass = "\\Wafl\\Application\\ModularMvcWebApplication")
    {
        self::_init();

        $application = self::BootstrapModularApplication($syrupConfigFile, $environment, $applicationClass);
        self::RunModularApplication($application, $modularloader);
    }

    public static function OutputCachedStaticResources($requestUri, $browserTimeout = 86400, $appUid = "Wafl.Application.Generic")
    {
        self::_init();

        $requestFile = basename($requestUri);
        if (stripos($requestFile, "?") > -1)
        {
            $requestFile = substr($requestFile, 0, stripos($requestFile, "?"));
        }
        $fileExtension = substr($requestFile, strlen($requestFile) - 3);
        if ($fileExtension == ".js")
        {
            $fileExtension = "js";
        }

        $savedEtag = null;
        $if_none_match = null;
        if ($fileExtension == "css" || $fileExtension == "png" || $fileExtension == "jpg" || $fileExtension == "js" || $fileExtension == "ttf" || $fileExtension == "otf")
        {
            $uriHash = md5($requestUri);
            $resultDiskCache = self::$_TEMP_FOLDER . DIRECTORY_SEPARATOR . "$appUid.AppCache.$uriHash.$requestFile";
            $resultDiskCacheEtag = $resultDiskCache . ".etag";
            $resultDiskCacheHeaders = $resultDiskCache . ".headers";
            $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) : false;
            if ($if_none_match)
            {
                //apache likes to add "-gzip" to the end of etags that it zips.
                //Which is irrelevant to me because if apache is handling zipping
                //then I will be outputting unzipped content regardless
                $if_none_match = str_replace("-gzip", "", $if_none_match);
            }
            if (file_exists($resultDiskCache) && (time() - filemtime($resultDiskCache) > self::$DISK_CACHE_TIMEOUT))
            {
                unlink($resultDiskCache);
                if (file_exists($resultDiskCacheEtag))
                {
                    unlink($resultDiskCacheEtag);
                }
            }
            if (file_exists($resultDiskCacheEtag))
            {
                $savedEtag = file_get_contents($resultDiskCacheEtag);
            }
        }
        else
        {
            $resultDiskCache = null;
        }
        if ($savedEtag && $if_none_match)
        {
            header("Etag: $savedEtag");
            if ($savedEtag == $if_none_match)
            {
                if (file_exists($resultDiskCacheHeaders))
                {
                    $headers = unserialize(file_get_contents($resultDiskCacheHeaders));
                    foreach ($headers as $header)
                    {
                        $matches = null;

                        if (preg_match("/cache-control:.*max-age=([0-9]+)/i", $header, $matches))
                        {
                            $cacheSeconds = $matches[1];
                            header($header);
                            header("expires: ". substr(date("r", time()-date('Z')+$cacheSeconds), 0, -5) . "GMT");
                        }
                    }
                }

                http_response_code(304);
                return true;
            }
            elseif ($resultDiskCache && file_exists($resultDiskCache))
            {
                unlink($resultDiskCache);
            }
        }
        if ($resultDiskCache && file_exists($resultDiskCache))
        {
            http_response_code(200);
            $headers = unserialize(file_get_contents($resultDiskCacheHeaders));
            foreach ($headers as $header)
            {
                header($header);
            }
            print(file_get_contents($resultDiskCache));
            return true;
        }
        return false;
    }

    public static function CacheStaticResource($requestUri, \DblEj\Communication\Http\IResponse $result, $appUid = "Wafl.Application.Generic")
    {
        self::_init();
        if ($result->Get_RawResponse() && (!$result->Get_ContentIsSymbolic()))
        {
            return self::CacheRawStaticResource($requestUri, $result->Get_RawResponse(), $result->Get_Etag(), $appUid);
        }
        else
        {
            return false;
        }
    }

    public static function CacheRawStaticResource($requestUri, $rawResult, $etag = null, $appUid = "Wafl.Application.Generic")
    {
        self::_init();

        $requestFile = basename($requestUri);
        if (stripos($requestFile, "?") > -1)
        {
            $requestFile = substr($requestFile, 0, stripos($requestFile, "?"));
        }
        $fileExtension = substr($requestFile, strlen($requestFile) - 3);
        if ($fileExtension == ".js")
        {
            $fileExtension = "js";
        }
        if ($fileExtension == "css" || $fileExtension == "png" || $fileExtension == "jpg" || $fileExtension == "js" || $fileExtension == "ttf" || $fileExtension == "otf")
        {
            $uriHash         = md5($requestUri);
            $resultDiskCache = self::$_TEMP_FOLDER . DIRECTORY_SEPARATOR . "$appUid.AppCache.$uriHash.$requestFile";
        }
        else
        {
            $resultDiskCache = null;
        }

        if ($resultDiskCache && $rawResult && ($rawResult != "STREAM_OUT_FILE"))
        {
            if (file_exists($resultDiskCache) && (time() - filemtime($resultDiskCache) > self::$DISK_CACHE_TIMEOUT))
            {
                unlink($resultDiskCache);
            }
            if (!file_exists($resultDiskCache))
            {
                if (!is_writable(dirname($resultDiskCache)))
                {
                    throw new \Exception("Cannot write to temp folder: ".self::$_TEMP_FOLDER);
                }
                file_put_contents($resultDiskCache, $rawResult);
                if ($etag)
                {
                    file_put_contents($resultDiskCache . ".etag", $etag);
                }

                $cleanedHeaders = []; //remove cookies for security
                foreach (headers_list() as $header)
                {
                    if (stristr($header, "Set-Cookie") === false)
                    {
                        $cleanedHeaders[] = $header;
                    }
                }
                file_put_contents($resultDiskCache.".headers", serialize($cleanedHeaders));
            }
        }
        return true;
    }

    public static function UncacheStaticResource($requestUri, $appUid = "Wafl.Application.Generic")
    {
        self::_init();

        $requestFile = basename($requestUri);
        if (stripos($requestFile, "?") > -1)
        {
            $requestFile = substr($requestFile, 0, stripos($requestFile, "?"));
        }
        $fileExtension = substr($requestFile, strlen($requestFile) - 3);
        if ($fileExtension == ".js")
        {
            $fileExtension = "js";
        }
        $uriHash         = md5($requestUri);
        $resultDiskCache = self::$_TEMP_FOLDER . DIRECTORY_SEPARATOR . "$appUid.AppCache.$uriHash.$requestFile";
        if (file_exists($resultDiskCache))
        {
            unlink($resultDiskCache);
        }
        if (file_exists("$resultDiskCache.etag"))
        {
            unlink("$resultDiskCache.etag");
        }
        if (file_exists("$resultDiskCache.headers"))
        {
            unlink("$resultDiskCache.headers");
        }
        return true;
    }

    public static function UncacheStaticResources($fileExtension = null, $expiredOnly = false, $appUid = "Wafl.Application.Generic")
    {
        self::_init();

        if (\DblEj\Util\Strings::StartsWith($fileExtension, "."))
        {
            $fileExtension = substr($fileExtension, 1);
        }
        if ($fileExtension)
        {
            $staticResourceCacheFiles = \DblEj\Util\Folder::GetAllChildObjects(self::$_TEMP_FOLDER, false, "$appUid.AppCache.*.$fileExtension");
            $staticResourceCacheFiles = array_merge($staticResourceCacheFiles, \DblEj\Util\Folder::GetAllChildObjects(self::$_TEMP_FOLDER, false, "$appUid.AppCache.*.$fileExtension.etag"));
            $staticResourceCacheFiles = array_merge($staticResourceCacheFiles, \DblEj\Util\Folder::GetAllChildObjects(self::$_TEMP_FOLDER, false, "$appUid.AppCache.*.$fileExtension.headers"));
        }
        else
        {
            $staticResourceCacheFiles = \DblEj\Util\Folder::GetAllChildObjects(self::$_TEMP_FOLDER, false, "$appUid.AppCache.*");
        }
        foreach ($staticResourceCacheFiles as $staticResourceCacheFile)
        {
            if (file_exists($staticResourceCacheFile))
            {
                if (!$expiredOnly || (time() - filemtime($staticResourceCacheFile) > self::$DISK_CACHE_TIMEOUT))
                {
                    unlink($staticResourceCacheFile);
                }
            }
        }
        return true;
    }

    public static function OutputHttpException(\DblEj\Communication\Http\Exception $e, $exceptionFolder)
    {
        self::_init();

        $requestUri = $_SERVER["REQUEST_URI"]; //@todo do all http servers use this header?  Tested in apache. And what about REDIRECT_URL, I was checking that as well elsewhere
        $completeErrorText = "";
        if (\Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_DebugMode() && !\Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_SuppressErrors())
        {
            throw $e;
        }
        http_response_code($e->Get_HttpErrorCode());
        $errorCode = $e->Get_HttpErrorCode();
        if (class_exists("\\Wafl\\Core") && \Wafl\Core::$CURRENT_USER)
        {
            $currentUserId = \Wafl\Core::$CURRENT_USER->Get_UserId();
        }
        else
        {
            $currentUserId = null;
        }
        try
        {
            $referrer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "DirectLink";
            $serverName = isset($_SERVER["SERVER_NAME"]) ? $_SERVER["SERVER_NAME"] : "localhost";
            $clientIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
            $userAgent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "unknown";

            $completeErrorText = "Server: $serverName\n";
            $completeErrorText .= "Requested Uri: $requestUri\n";
            $completeErrorText .= "Referrer: $referrer\n";
            $completeErrorText .= "Client: $clientIp\n";
            $completeErrorText .= "Agent: $userAgent\n";
            $completeErrorText .= "Time: ".strftime("%Y-%m-%d  %I:%M %p", time())."\n";
            $completeErrorText .= "Error Code: $errorCode\n";
            $completeErrorText .= $e->getMessage() . "\n";

            $innerMostException = $e;
            while ($prevE = $e->getPrevious())
            {
                $innerMostException = $prevE;
            }
            $completeTrace = $innerMostException->getTraceAsString() . "\n";
            $completeErrorText = "\n****PHP ERROR****\n" . $completeErrorText . "\n" . $completeTrace . "\n***************\n";

            if ($errorCode != 404) //404's go in the access log
            {
                error_log($completeErrorText);
            }

            $errorInclude = $exceptionFolder . $e->Get_ErrorOutputCode() . ".php";
            if (!file_exists($errorInclude))
            {
                $errorInclude = $exceptionFolder . $errorCode . ".php";
            }
            if (!file_exists($errorInclude))
            {
                $errorInclude = $exceptionFolder . "500.php";
            }
            $publicDetails = $e->Get_PublicDetails();
            define("PUBLIC_ERROR_DETAILS", $publicDetails?$publicDetails:"Server Error $errorCode");
            require($errorInclude);
        }
        catch (\Exception $ex)
        {
            error_log("Error while trying to record an error.  This error: ".$ex->getMessage()." ".$ex->getTraceAsString());
        }
        return $completeErrorText;
    }

    public static function OutputException(\Throwable $e, $exceptionFolder)
    {
        self::_init();

        $requestUri = $_SERVER["REQUEST_URI"]; //@todo do all http servers use this header?  Tested in apache. And what about REDIRECT_URL, I was checking that as well elsewhere
        $errorCode = 500;
        $completeErrorText = "";
        if (class_exists("\\Wafl\\Core") && \Wafl\Core::$RUNNING_APPLICATION && \Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_DebugMode() && !\Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_SuppressErrors())
        {
            throw $e;
        }
        http_response_code(500);
        if (class_exists("\\Wafl\\Core") && \Wafl\Core::$CURRENT_USER)
        {
            $currentUserId = \Wafl\Core::$CURRENT_USER->Get_UserId();
        }
        else
        {
            $currentUserId = null;
        }
        try
        {
            $referrer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "DirectLink";
            $serverName = isset($_SERVER["SERVER_NAME"]) ? $_SERVER["SERVER_NAME"] : "localhost";
            $clientIp = class_exists("\\DblEj\\Communication\\Http\\Util") ? \DblEj\Communication\Http\Util::GetClientIpAddress() : "LibNotLoaded";

            $completeErrorText = "Server: $serverName\n";
            $completeErrorText .= "Requested Uri: $requestUri\n";
            $completeErrorText .= "Referrer: $referrer\n";
            $completeErrorText .= "Client: $clientIp\n";
            $completeErrorText .= "Time: ".strftime("%Y-%m-%d  %I:%M %p", time())."\n";
            $completeErrorText .= $e->getMessage() . "\n";
            $completeTrace = $e->getTraceAsString() . "\n";

            $completeErrorText = "\n****PHP ERROR****\n" . $completeErrorText . "\n" . $completeTrace . "\n***************\n";
            $completeErrorTextArray = explode("\n", $completeErrorText);
            foreach ($completeErrorTextArray as $completeErrorTextLine)
            {
                error_log($completeErrorTextLine);
            }

            if (is_a($e, "\\DblEj\\System\\Exception"))
            {
                define("PUBLIC_ERROR_DETAILS", ($e->Get_PublicDetails()?$e->Get_PublicDetails():"Server Error $errorCode"));
            } else {
                define("PUBLIC_ERROR_DETAILS", "Server Error $errorCode");
            }
            require($exceptionFolder . "500.php");
        }
        catch (\Exception $ex)
        {
            error_log("Exception while trying to log an error: ".$ex->getMessage());
        }
        if (!defined("EXCEPTION_OUTPUT"))
        {
            define("EXCEPTION_OUTPUT", true);
        }
        return $completeErrorText;
    }
}