<?php

namespace Wafl\Application;

class AppRunner
{

    /**
     *
     * @param string $syrpFile
     * @return \Wafl\MvcWebApplication
     * @throws Exception
     */
    public static function PrepareModularAppFromSyrup($syrpFile, $syrupParserClassname = null, $loadAppConfig = true, $environment = null, $applicationClass = "\\Wafl\\Application\\ModularMvcWebApplication", $performanceLogPath = null)
    {
        return self::PrepareAppFromSyrup($syrpFile, $syrupParserClassname, $loadAppConfig, $environment, $applicationClass, $performanceLogPath);
    }

    /**
     *
     * @param string $syrpFile
     * @return \Wafl\Application\MvcWebApplication
     * @throws Exception
     */
    public static function PrepareAppFromSyrup($syrpFile, $syrupParserClassname, $loadAppConfig = true, $environment = null, $applicationClass = "\\Wafl\\Application\\MvcWebApplication", $performanceLogPath = null)
    {
        \Wafl\Util\Syrup::Initialize($syrupParserClassname);

        //load basic settings from $syrpFile
        $appIni  = \Wafl\Util\Syrup::ParseSyrupFileAsArray($syrpFile);
        $appName = "untitled App";
        if (!key_exists("Application", $appIni))
        {
            throw new AppRunnerException($appName, "Could not run the app because it is missing required settings.  Please correct your App's config file under (missing the Application section).");
        }
        if (!key_exists("Paths", $appIni))
        {
            throw new AppRunnerException($appName, "Could not run the app because it is missing required settings.  Please correct your config file (missing Paths section).");
        }
        if (!key_exists("Application", $appIni["Paths"]))
        {
            throw new AppRunnerException($appName, "Could not run the app because it is missing required settings.  Please correct your config file (missing the Paths>Application section).");
        }
        if (!key_exists("RootNameSpace", $appIni["Application"]))
        {
            throw new AppRunnerException($appName, "Could not run the app because it is missing required settings.  Please correct your config file (missing the Application>RootNameSpace setting).");
        }
        if (!key_exists("Name", $appIni["Application"]))
        {
            throw new AppRunnerException($appName, "Could not run the app because it is missing required settings.  Please correct your config file (missing the Application>Name setting).");
        }
        if (!key_exists("Guid", $appIni["Application"]))
        {
            throw new AppRunnerException($appName, "Could not run the app because it is missing required settings.  Please correct your config file (missing the Application>Guid setting).");
        }
        if (!key_exists("Version", $appIni["Application"]))
        {
            throw new AppRunnerException($appName, "Could not run the app because it is missing required settings.  Please correct your config file (missing the Application>Version setting).");
        }

        $appNamespace    = $appIni["Application"]["RootNameSpace"];
        $appName         = $appIni["Application"]["Name"];
        $appGuid         = $appIni["Application"]["Guid"];
        $appVersion      = $appIni["Application"]["Version"];
        $appConfigFolder = $appIni["Paths"]["Application"]["ConfigFolder"];
        if (!$environment)
        {
            $environment = WAFL_ENVIRONMENT;
        }
        $appRoot         = realpath(dirname($syrpFile)) . DIRECTORY_SEPARATOR;
        if ((substr($appRoot, 1, 1) == ":") && (DIRECTORY_SEPARATOR != "\\"))
        {
            throw new \Exception("The application appears to be misconfigured.  There is a windows path in the configs but the app is not running on Windows.  The config was loaded from $syrpFile.");
        }
        $appConfigPath   = $appRoot . $appConfigFolder;
        if (!substr($appConfigPath, strlen($appConfigPath)-1) != DIRECTORY_SEPARATOR)
        {
            $appConfigPath.=DIRECTORY_SEPARATOR;
        }
        $appSettingsFile = $appConfigPath . "Settings.$environment.syrp";

        //validate folder paths
        if (!file_exists($appConfigPath))
        {
            throw new AppRunnerException($appName, "Application configuration folder is missing (expected in $appConfigPath).");
        }
        if (!file_exists($appSettingsFile))
        {
            throw new AppRunnerException($appName, "Application settings file is missing for the $environment environment.  It was expected at $appSettingsFile");
        }

        $parsedSyrp = \Wafl\Util\Syrup::ParseSyrupFileAsArray($appSettingsFile);
        $localRoot  = $parsedSyrp["Paths"]["Application"]["LocalRoot"];
        if (substr($localRoot, strlen($localRoot) - 1, 1) != "/" && substr($localRoot, strlen($localRoot) - 1, 1) != "\\")
        {
            $localRoot.="/";
        }
        $waflPath  = realpath(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR);
        if (!file_exists($waflPath))
        {
            throw new AppRunnerException($appName, "Cannot find the WAFL installation folder at $waflPath (Environment: $environment).  Are you set to the correct environment?");
        }
        $waflPath .= DIRECTORY_SEPARATOR;
        $dblEjPath = $parsedSyrp["Paths"]["Wafl"]["DblEjFolder"];
        if (!file_exists($dblEjPath))
        {
            throw new AppRunnerException($appName, "Cannot find the DblEj library at $dblEjPath (Environment: $environment).  Are you set to the correct environment?");
        }
        $preparedApp = self::PrepareApp($appName, $appGuid, $appNamespace, $localRoot, $appConfigFolder, $environment, "Settings.$environment.syrp", $dblEjPath, $applicationClass, $performanceLogPath, $appVersion);
        if ($loadAppConfig)
        {
            //load the app configuration
            $preparedApp->LoadConfig();
        }

        return $preparedApp;
    }

    /**
     *
     * @param string $appName
     * @param string $appGuid
     * @param string $appNamespace
     * @param string $appRootPath
     * @param string $configFolder
     * @param string $environment
     * @param string $appSettingsFile
     * @param string $dblEjPath
     * @return \Wafl\MvcWebApplication
     */
    public static function PrepareApp($appName, $appGuid, $appNamespace, $appRootPath, $configFolder = "Config/", $environment = "dev", $appSettingsFile = null, $dblEjPath = null, $applicationClass = "\\Wafl\\Application\\MvcWebApplication", $performanceLogPath = null, $appVersion = null)
    {
        if (!$appSettingsFile)
        {
            $appSettingsFile = "Settings.$environment.syrp";
        }
        if (!$dblEjPath)
        {
            $dblEjPath = __DIR__ . DIRECTORY_SEPARATOR . "/../../DblEj/";
        }
        $appConfigPath = $appRootPath . $configFolder;
        $appConfigPath = str_replace("\\", DIRECTORY_SEPARATOR, $appConfigPath);
        $appConfigPath = str_replace("/", DIRECTORY_SEPARATOR, $appConfigPath);
        if (substr($appConfigPath, strlen($appConfigPath) - 1, 1) != DIRECTORY_SEPARATOR)
        {
            $appConfigPath.=DIRECTORY_SEPARATOR;
        }
        //define wafl application constants
        define("WAFL_APP_ROOT_PATH", $appRootPath);
        define("WAFL_APP_CONFIG_PATH", $appConfigPath);
        define("WAFL_APP_SETTINGS_FILE", $appConfigPath . $appSettingsFile);

        if (!file_exists(DBLEJ_PATH . "Autoloader.php"))
        {
            throw new AppRunnerException("App at $appRootPath", "Halted App startup because the DblEj autoloader cannot be found.  This usually indicates that you set the DblEj path incorrectly, you do not have DblEj installed, or your server does not have read access to the DblEj path (" . DBLEJ_PATH . ".)");
        }
        require_once(DBLEJ_PATH . "Autoloader.php");

        if (is_array($applicationClass))
        {
            if (count($applicationClass) > 1)
            {
                $applicationClassPath = $applicationClass[1];
                if (is_file($applicationClassPath))
                {
                    require_once($applicationClassPath);
                }
            }
            $applicationClass = $applicationClass[0];
        }
        $settingsClassName = $applicationClass::Get_SettingsClassName();
        $appSettings    = new $settingsClassName($appName, $appGuid, $appNamespace, $appVersion);
        $newApplication = new $applicationClass($appSettings, $environment, null, null, $performanceLogPath);
        if (!defined("AM_WEBPAGE") || (AM_WEBPAGE === true))
        {
            $siteStructureFile = $appConfigPath . $appSettings->Get_Paths()->Get_Application()->Get_SiteStructurFilename();
            $siteNavigation = null;
            $siteNavigation = \Wafl\Util\Syrup::ParseSyrupFileAsArray($siteStructureFile);
            if ($siteNavigation)
            {
                $newApplication->Get_SiteMap()->ParseSiteStructureConfig($siteNavigation, null, $newApplication);
            }
        }
        return $newApplication;
    }

    public static function RunApp(MvcWebApplication $application)
    {
        return $application->Run();
    }
}