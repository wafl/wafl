<?php

namespace Wafl\Application;

class AppRunnerException
extends \Exception
{

    public function __construct($appName = "Unnamed App", $message = "", $severity = E_ERROR, \Exception $innerException = null)
    {
        if (!$appName)
        {
            $appName = "Unnamed App";
        }
        parent::__construct("There was an error trying to run the application $appName. $message", $severity, $innerException);
    }
}