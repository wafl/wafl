<?php

namespace Wafl\Application;

use DblEj\Application\IApplication,
    DblEj\Application\RunStats,
    DblEj\Communication\Ajax\AjaxHandler,
    DblEj\Communication\CommunicationException,
    DblEj\Communication\Http\Routing\InvalidRouterRequestException,
    DblEj\Data\IDataStore,
    DblEj\Data\PersistableModel,
    DblEj\EventHandling\EventInfo,
    DblEj\EventHandling\EventTypeCollection,
    DblEj\Internationalization\Locale as Locale2,
    DblEj\Resources\ResourceAccessDeniedException,
    DblEj\Resources\ResourceCollection,
    DblEj\Resources\ResourcePermissionCollection,
    DblEj\Resources\ResourcePermissionContainer,
    DblEj\System\MissingPhpExtensionException,
    DblEj\Authentication\IUser,
    Wafl\UserFeedback,
    DblEj\Util\SystemEvents,
    Locale,
    ReflectionObject,
    Wafl\Application\Settings\All,
    Wafl\Application\Settings\DataStorage\Connection,
    Wafl\Core,
    Wafl\Exceptions\Exception,
    Wafl\Internationalization\ArrayTranslator,
    Wafl\ResponseGates\GateChain,
    Wafl\ResponseGates\GateKeepers\ApiCall,
    Wafl\ResponseGates\GateKeepers\PhpScript,
    Wafl\ResponseGates\GateKeepers\SitePage,
    Wafl\Users\User,
    Wafl\Util\Extensions,
    Wafl\Util\Router,
    Wafl\Util\Syrup,
    Wafl\Util\Template;

class Application
extends ResourcePermissionContainer
implements IApplication
{
    const EVENT_APPLICATION_CREATING            = 1001;
    const EVENT_APPLICATION_CREATED             = 1003;
    const EVENT_APPLICATION_BEFORE_INITIALIZE   = 1006;
    const EVENT_APPLICATION_AFTER_INITIALIZE    = 1007;
    const EVENT_APPLICATION_ACL_LOADED          = 1010;
    const EVENT_APPLICATION_BEFORE_RUN          = 1020;
    const EVENT_APPLICATION_AFTER_RUN           = 1030;
    const EVENT_APPLICATION_CONFIG_LOADING      = 1200;
    const EVENT_APPLICATION_CONFIG_LOADED       = 1210;
    const EVENT_APPLICATION_BEFORE_REQUEST_ROUTED       = 1300;
    const EVENT_APPLICATION_AFTER_REQUEST_ROUTED        = 1310;
    const EVENT_APPLICATION_BEFORE_RESPONSE_SENT    = 1400;

    protected $_reflectionClass;
    protected $_classFilename;
    protected $_classFolder;
    protected $_environment;
    protected $_responseGatekeepers;
    protected $_runStats;
    protected $_tracePath;
    protected $_localUploadFolder;

    /**
     *
     * @var All
     */
    protected $_settings;
    protected $_user;
    protected $memoryStores   = [];
    protected $locales        = array();
    protected $currentLocale  = null;
    protected $storageEngines = array();
    protected $iniMappings;
    protected $_amInitialized = false;
    private $_foldersToBeAddedToTemplateRenderer;
    private $_clientIncludes;
    protected $_localConfigFolder;
    protected $_localAppFolder;
    protected $_localRoot;
    protected $_clientGmtOffset = 0;


    /**
    *
    * @var \DblEj\Authentication\IUserSession
    */
    protected $_session;

    public function __construct(All $settings, $environment = "dev", ResourceCollection $restrictedResources = null, ResourcePermissionCollection $resourcePermissions = null, $tracePath = null)
    {
        $this->_runStats  = new RunStats($this);
        $this->_tracePath = $tracePath;
        if (!$resourcePermissions)
        {
            $resourcePermissions = new ResourcePermissionCollection();
        }
        if (!$restrictedResources)
        {
            $restrictedResources = new ResourceCollection();
        }
        $this->_settings    = $settings;

        Core::$RUNNING_APPLICATION = $this; //lame

        parent::__construct($restrictedResources, $resourcePermissions);
        require(__DIR__ . "/../Autoloaders/Autoload.php");

        $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_CREATING, $this));

        $this->_reflectionClass = new ReflectionObject($this);
        $this->_classFilename   = $this->_reflectionClass->getFilename();
        $this->_classFolder     = dirname($this->_classFilename);
        $this->_environment     = $environment;

        $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_ACL_LOADED, $this));

        //@todo need to be able to inject these
        $this->_responseGatekeepers = new GateChain();
        $this->_responseGatekeepers->AddGateKeeper(new SitePage());
        $this->_responseGatekeepers->AddGateKeeper(new ApiCall());
        $this->_responseGatekeepers->AddGateKeeper(new PhpScript());

        $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_CREATED, $this));

        $this->_foldersToBeAddedToTemplateRenderer = array();

    }

    /**
     * Get the current session object
     * @return \DblEj\Authentication\IUserSession
     */
    public function GetCurrentSession()
    {
        return $this->_session;
    }

    public function StoreSessionData($dataName, $dataValue)
    {
        return $this->_session->SetData($dataName, $dataValue);
    }

    public function GetSessionData($dataName)
    {
        return $this->_session->GetData($dataName);
    }

    public function DeleteSessionData($dataName)
    {
        return $this->_session->DeleteData($dataName);
    }

    public function Get_ReplacementTokens()
    {
        $returnArray = [];
        $localRoot       = realpath($this->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot()) . DIRECTORY_SEPARATOR;
        $confFolder      = $this->Get_Settings()->Get_Paths()->Get_Application()->Get_Config();
        $localConfFolder = $localRoot . $confFolder;
        $returnArray["RUNNING_APPLICATION"] = Core::$RUNNING_APPLICATION;
        $returnArray["LOCAL_ROOT"] = realpath($localRoot);
        $returnArray["DBLEJ_FOLDER"] = $this->Get_Settings()->Get_Paths()->Get_Wafl()->Get_DblEjFolder();
        $returnArray["WAFL_FOLDER"] = $this->Get_Settings()->Get_Paths()->Get_Wafl()->Get_WaflFolder();
        $returnArray["LOCAL_DBLEJ_FOLDER"] = realpath($this->Get_Settings()->Get_Paths()->Get_Wafl()->Get_DblEjFolder());
        $returnArray["LOCAL_WAFL_FOLDER"] = realpath($this->Get_Settings()->Get_Paths()->Get_Wafl()->Get_WaflFolder());
        $returnArray["LOCAL_INCLUDE_PATH_FRAMEWORK"] = realpath($this->_settings->Get_Paths()->Get_Wafl()->Get_DblEjFolder());
        $returnArray["WAFL_ENVIRONMENT"] = WAFL_ENVIRONMENT;

        $returnArray["ENV_CONFIG_FILE"] = realpath($localConfFolder . "Settings." . WAFL_ENVIRONMENT . ".syrp");
        $returnArray["APP_CONFIG_FILE"] = realpath($localRoot . "Application.syrp");

        $returnArray["GLOBAL_ERRORS"] = UserFeedback::GetGlobalErrors();
        $returnArray["GLOBAL_INFO"] = UserFeedback::GetGlobalInfos();
        $returnArray["DEBUG_MODE"] = $this->_settings->Get_Debug()->Get_DebugMode();

        $returnArray["CURRENT_USER"] = Core::$CURRENT_USER;

        return $returnArray;
    }

    public function SetGlobalTemplateData($dataname, $datavalue)
    {
        $templateRenderer = $this->Get_TemplateRenderer();
        if ($templateRenderer)
        {
            $templateRenderer->SetGlobalData($dataname, $datavalue);
        }
    }

    public function ClearTemplateCache($includeCompiles = false)
    {
        $templateRenderer = $this->Get_TemplateRenderer();
        if ($templateRenderer)
        {
            $templateRenderer->DeleteCache($includeCompiles);
        }
    }

    protected function get_TemplateFolders()
    {
        return $this->_foldersToBeAddedToTemplateRenderer;
    }

    protected function registerTemplateFolder($folderPath, $priority)
    {
        if (file_exists($folderPath))
        {
            if (!in_array($folderPath, $this->_foldersToBeAddedToTemplateRenderer))
            {
                if (isset($this->_foldersToBeAddedToTemplateRenderer[$priority]))
                {
                    throw new \Exception("Cannot register the template folder $folderPath at priority $priority because another template folder (" . $folderPath[$priority] . ") is already registered at that priority.");
                }

                $templateRenderer = $this->Get_TemplateRenderer();
                if ($templateRenderer)
                {
                    $templateRenderer->AddTemplateFolder($folderPath, $priority);
                } else {
                    $this->_foldersToBeAddedToTemplateRenderer[$priority] = $folderPath;
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc=" Properties ">
    public function Get_Name()
    {
        return $this->_settings->Get_Application()->Get_AppName();
    }

    public function Get_Guid()
    {
        return $this->_settings->Get_Application()->Get_Guid();
    }

    public function Get_Version()
    {
        return $this->_settings->Get_Application()->Get_Version();
    }

    public function Get_Namespace()
    {
        return $this->_settings->Get_Application()->Get_RootNameSpace();
    }

    public function Get_TemplateRenderer()
    {
        return Extensions::GetExtensionByType("\\DblEj\\Presentation\\Integration\\ITemplateRendererExtension");
    }
    // </editor-fold>

    /**
     * @return All
     *
     */
    public function Get_Settings()
    {
        return $this->_settings;
    }

    public function GetSettingsSection($sectionName)
    {
        return $this->_settings->GetOption($sectionName);
    }

    public function SetSetting($settingName, $settingValue)
    {
        $newOption = $this->_settings->AddOption($settingName);
        $newOption->Set_CurrentValue($settingValue);
    }

    public function GetSettingValue($settingName)
    {
        return $this->_settings->GetOptionValue($settingName);
    }

    public function Run(&$exitCode = null)
    {
        $routerUsed = null;

        //give our subclasses a chance to override (@todo shouldnt onBeforeRun *raise* the event, .Net style?)
        $this->onBeforeRun();

        //get the passed in arg
        global $argv;
        $requestString = isset($argv[1]) ? $argv[1] : "";

        $response = null;
        //ask the router utility to route the request to the appropriate handler
        $route = Router::FromClientToServer($requestString, $this, $routerUsed);
        if ($route)
        {
            $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_BEFORE_REQUEST_ROUTED, $route, $this));

            //check if user is allowed to go to this route
            $gateState = $this->_responseGatekeepers->AttemptRoute($route, Core::$CURRENT_USER);
            if ($gateState->Get_IsRestricted())
            {
                throw new ResourceAccessDeniedException($gateState->Get_RestrictedResourceName(), Core::$CURRENT_USER->Get_DisplayName());
            }


            $preResponse = $route->PrepareRoute($this);
            $response = $route->GotoRoute($this);

            SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::BEFORE_SESSION_CLOSE));
            $this->_session->Close();
            SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_SESSION_CLOSE));

            //validate the reponse
            if (!$response)
            {
                throw new CommunicationException("The route did not return a response.");
            }

            $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_AFTER_REQUEST_ROUTED, $route, $this));

            Core::$CURRENT_USER = User::RegisterNewUser(get_current_user(), "");

            //throw an exception if the user does not have access to the response...
            $gateState = $this->_responseGatekeepers->AttemptEntry($response, Core::$CURRENT_USER);
            if ($gateState->Get_IsRestricted())
            {
                throw new ResourceAccessDeniedException($gateState->Get_RestrictedResourceName(), Core::$CURRENT_USER->Get_DisplayName());
            }
        }
        else
        {
            throw new InvalidRouterRequestException($requestString, null, "Could not find a route for the specified request ($requestString).");
        }

        if (!Router::FromServerToClient($response, $this))
        {
            throw new InvalidRouterRequestException($requestString, $routerUsed, "The destination action was executed, but it does not appear to have done anything.");
        }

        $this->_runStats->SetResponse($response, $route);

        if ($this->_tracePath && file_exists(dirname($this->_tracePath)))
        {
            file_put_contents($this->_tracePath, serialize($this->_runStats->GetStatsArray()), true);
        }
        $this->onAfterRun($response, $route);
        $exitCode = 0;
        return $response;
    }

    protected function onBeforeRun()
    {
        //raise event before run
        $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_BEFORE_RUN, $this));
    }

    protected function onAfterRun($response, $route)
    {
        $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_AFTER_RUN, $this));
        Core::Set_RequestPending(false);
    }

    protected function parseSyrpSection($sectionElements, $iniMappings = null)
    {
        $appConfigPath = WAFL_APP_CONFIG_PATH;
        if ($this->iniMappings === null)
        {
            if ($iniMappings === null)
            {
                $mappingFile = $appConfigPath . "More/SettingMappings.syrp";
                $iniMappings = Syrup::ParseSyrupFileAsArray($mappingFile);
            }
            $this->iniMappings = $iniMappings;
        }
        foreach ($sectionElements as $sectionName => $settings)
        {
            $unmappedSettings = null;
            if ($this->_settings->GetIsOption($sectionName))
            {
                $value = $this->_settings->GetOptionValue($sectionName);
            } else {
                $this->_settings->SetOption($sectionName, new \DblEj\Configuration\OptionList());
                $value = $this->_settings->GetOptionValue($sectionName);
            }
            $this->mapSyrpToObject($sectionName, $settings, $this->iniMappings[$sectionName], $value, $unmappedSettings);
        }
    }

    protected function mapSyrpToObject($sectionName, $settings, $iniMappings, $targetObject, &$unmappedSettings = null)
    {
        if (!is_object($targetObject))
        {
            throw new Exception("Target must be an object instance.");
        }
        foreach ($settings as $settingName => $settingValue)
        {
            if (isset($iniMappings[$settingName]) || ($sectionName == "DataStorage"))
            {
                if (isset($iniMappings[$settingName]))
                {
                    $propertyName = $iniMappings[$settingName];
                }
                elseif ($sectionName == "DataStorage")
                {
                    $propertyName = $iniMappings["ConnectionTemplate"];
                }

                if (is_scalar($propertyName))
                {
                    if (method_exists($targetObject, "Get_" . $propertyName))
                    {
                        if (strtolower($settingValue) == "true")
                        {
                            $settingValue = true;
                        }
                        elseif (strtolower($settingValue) == "false")
                        {
                            $settingValue = false;
                        }
                        elseif ($settingValue === null)
                        {
                            $settingValue = ""; //our option lists treat null values as not being set and replace them with the option's default value.  but in our case, if the setting is in the file, even if the value is blank, we want to use it.
                        }
                        elseif (is_numeric($settingValue))
                        {
                            $settingValue = floatval($settingValue);
                        }
                        call_user_func(array($targetObject, "Set_" . $propertyName), $settingValue);
                    }
                    else
                    {
                        $targetObject->SetOption($settingName, $settingValue);
                        //throw new Exception("Attempting to map to an invalid property. $sectionName: $propertyName", E_ERROR);
                    }
                }
                else
                {
                    foreach ($settings as $settingNameInner => $settingValueInner)
                    {
                        if ($sectionName == "DataStorage" && $settingNameInner != "Common")
                        {
                            if (isset($iniMappings["ConnectionTemplate"]))
                            {
                                $connectionSettings = Connection::CreateFromArray($settingValueInner);
                                $targetObject->AddConnection($settingNameInner, $connectionSettings);
                                //$this->mapSyrpToObject($settingNameInner, $settingValueInner, $iniMappings["ConnectionTemplate"], $targetObject->GetDataStorageOptions($settingNameInner));
                            }
                        }
                        elseif (isset($iniMappings[$settingNameInner]))
                        {
                            if ($targetObject->GetIsOption($settingNameInner))
                            {
                                $this->mapSyrpToObject($settingNameInner, $settingValueInner, $iniMappings[$settingNameInner], $targetObject->GetOptionValue($settingNameInner));
                            } else {
                                throw new \Exception("Mapped setting ($settingNameInner) is not present in the settings file, or the settings file is corrupt.");
                            }
                        }
                    }
                }
            } else {
                $targetObject->SetOption($settingName, $settingValue);
                //throw new Exception("Cannot load application settings because there is an unmapped property: \"$sectionName\": \"$settingName\" (specified in " . WAFL_APP_SETTINGS_FILE . ")", E_ERROR);
            }
        }
    }

    protected function onLoadConfig($appConfigPath, $appDefinitions, $appSettings, $moreSettings)
    {
        $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_CONFIG_LOADED, $this));
        return true;
    }

    final public function LoadConfig()
    {
        $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_CONFIG_LOADING, $this));
        if (!defined("WAFL_APP_CONFIG_PATH"))
        {
            throw new Exception("Application configuration folder is not defined.");
        }
        $appConfigPath  = WAFL_APP_CONFIG_PATH;
        $advancedFolder = $appConfigPath . "More/";
        if (file_exists(WAFL_APP_SETTINGS_FILE))
        {
            $advancedSettingsFile = $advancedFolder . "FolderNames.syrp";
            $appDefinitionFile    = WAFL_APP_ROOT_PATH . "Application.syrp";
            if (!file_exists($appDefinitionFile))
            {
                throw new Exception("Application config file missing at $appDefinitionFile");
            }
            if (!file_exists($advancedSettingsFile))
            {
                throw new Exception("Cannot find advanced settings files at $advancedSettingsFile");
            }

            $appDefinitionIni    = Syrup::ParseSyrupFileAsArray($appDefinitionFile);
            $appSettingsIni      = Syrup::ParseSyrupFileAsArray(WAFL_APP_SETTINGS_FILE);
            $advancedSettingsIni = Syrup::ParseSyrupFileAsArray($advancedSettingsFile);
            $appSettings         = array(
                $appDefinitionFile     => $appDefinitionIni,
                WAFL_APP_SETTINGS_FILE => $appSettingsIni,
                $advancedSettingsFile  => $advancedSettingsIni);
            foreach ($appSettings as $filename => $appSettingsFromFile)
            {
                try
                {
                    $this->parseSyrpSection($appSettingsFromFile);
                }
                catch (\Exception $ex)
                {
                    throw new ApplicationException($this, "There was an error parsing the file $filename (" . ($ex->getPrevious() ? $ex->getPrevious()->getMessage() : $ex->getMessage()) . ")");
                }
            }
        }
        else
        {
            throw new Exception("Application settings file is missing for the $this->_environment environment.  It was expected at " . WAFL_APP_SETTINGS_FILE);
        }

        $includesFile = $advancedFolder . "Includes.syrp";

        if (file_exists($includesFile))
        {
            $includes              = Syrup::ParseSyrupFileAsArray($includesFile);
            $this->_clientIncludes = array();
            foreach ($includes["DblEj"]["JsIncludes"] as $include)
            {
                $this->_clientIncludes[] = $include;
            }
        }

        $localeFiles = \DblEj\Util\Folder::GetFiles($advancedFolder . "Locale/", false, "*.syrp");
        foreach ($localeFiles as $localeFile)
        {
            if (!class_exists("\Locale"))
            {
                throw new MissingPhpExtensionException("php_intl", "You must enable the php_intl php extension in order to use ActiveWafl internationalization features.");
            }
            $localeCode  = Locale::canonicalize(basename($localeFile));
            $localeSyrup = Syrup::ParseSyrupFileAsArray($localeFile);
            if (!isset($localeSyrup["Dictionary"]))
            {
                throw new \Exception("The language file does not contain a dictionary.");
            }
            $translator                 = new ArrayTranslator($localeSyrup["Dictionary"]);
            $this->locales[$localeCode] = New Locale2($localeCode, $translator);
        }

        $this->_localConfigFolder   = $appConfigPath;
        $this->_localRoot           = WAFL_APP_ROOT_PATH;
        $this->_localAppFolder      = $this->Get_LocalIncludeFolder();

        $localTemplatesFolder = $this->_localAppFolder .
            $this->Get_Settings()->Get_Paths()->Get_Application()->Get_Presentation() .
            $this->Get_Settings()->Get_Paths()->Get_Application()->Get_Html();

        $localExtensionsFolder = realpath($this->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder());
        $localControlsFolder = realpath($this->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ControlsFolder());

        $this->registerTemplateFolder($localTemplatesFolder, 100);
		$this->registerTemplateFolder($localExtensionsFolder, 200);
		$this->registerTemplateFolder($localControlsFolder, 300);


        $this->onLoadConfig($appConfigPath, $appDefinitionIni, $appSettingsIni, $advancedSettingsIni);
    }

    public function SetLocale(Locale2 $locale)
    {
        $this->currentLocale = $locale;
    }

    public function SetLocaleByCode($code)
    {
        if (!class_exists("\Locale"))
        {
            throw new MissingPhpExtensionException("php_intl", "You must enable the php_intl php extension in order to use ActiveWafl internationalization features.");
        }
        $code = Locale::canonicalize($code);
        if (!isset($this->locales[$code]))
        {
            throw new \Exception("Invalid locale");
        }
        $this->SetLocale($this->locales[$code]);
    }

    public function GetTranslatedText($text)
    {
        if (isset($this->currentLocale))
        {
            return $this->currentLocale->GetTranslatedText($text);
        }
        else
        {
            return $text;
        }
    }

    public function Get_LocalIncludeFolder()
    {
        return $this->_settings->Get_Paths()->Get_Application()->Get_LocalRoot() . $this->_settings->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR;
    }

    public function Get_LocalLogicFolder()
    {
        return $this->Get_LocalIncludeFolder() . "Logic" . DIRECTORY_SEPARATOR;
    }

    public function GetClientIncludes()
    {
        return $this->_clientIncludes;
    }

    public function GetRaisedEventTypes()
    {
        return new EventTypeCollection
        (
            [\DblEj\Resources\ResourceContainer::EVENT_RESOURCE_ADDED,
            \DblEj\Resources\ResourceContainer::EVENT_RESOURCE_REMOVED,
            self::EVENT_APPLICATION_CREATING,
            self::EVENT_APPLICATION_CREATED,
            self::EVENT_APPLICATION_BEFORE_INITIALIZE,
            self::EVENT_APPLICATION_AFTER_INITIALIZE,
            self::EVENT_APPLICATION_ACL_LOADED,
            self::EVENT_APPLICATION_BEFORE_RUN,
            self::EVENT_APPLICATION_AFTER_RUN,
            self::EVENT_APPLICATION_CONFIG_LOADING,
            self::EVENT_APPLICATION_CONFIG_LOADED,
            self::EVENT_APPLICATION_BEFORE_REQUEST_ROUTED,
            self::EVENT_APPLICATION_AFTER_REQUEST_ROUTED,
            self::EVENT_APPLICATION_BEFORE_RESPONSE_SENT]
        );
    }

    protected function onBeforeInitizalized()
    {
        $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_BEFORE_INITIALIZE));
    }

    protected function onAfterExtensionsInitialized()
    {
        SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_EXTENSIONS_INITIALIZED));
    }

    public function Initialize(IUser $user = null, $uiPage = true, $storageEngineType = "\\Wafl\\Extensions\\Storage\\MySql\\MySql")
    {
        try
        {
            if (!$this->_amInitialized)
            {
                $this->_user = $user;

                //load global event handlers
                if (@file_exists($this->_localAppFolder . $this->Get_Settings()->Get_Paths()->Get_Application()->Get_GlobalEventHandlers()))
                {
                    require_once($this->_localAppFolder . $this->Get_Settings()->Get_Paths()->Get_Application()->Get_GlobalEventHandlers());
                }

                $this->onBeforeInitizalized();

                //LOAD EXTENSIONS FROM SYRUP
                $extensionFile = $this->_localConfigFolder . "More/Extensions." . WAFL_ENVIRONMENT . ".syrp";
                if (!file_exists($extensionFile))
                {
                    $extensionFile = $this->_localConfigFolder . "More/Extensions.syrp";
                }

                Extensions::InitializeExtensionsFromSyrup
                (
                    $this,
                    $extensionFile,
                    function($initializedExtension)
                    {
                        if (is_a($initializedExtension, "\\DblEj\\Data\\Integration\\IDatabaseServerExtension"))
                        {
                            $engine = $initializedExtension;
                            SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::BEFORE_DATA_CONNECT));
                            foreach ($engine->GetConnections() as $dbId=>$dbConnection)
                            {
                                if (!$dbConnection->IsConnected())
                                {
                                    throw new Exception("Cound not connect to the database (".$dbConnection->Get_ConnectionName().").  Please double-check the database settings for this environment.");
                                }

                                if (!Core::$STORAGE_ENGINE)
                                {
                                    Core::$STORAGE_ENGINE = $dbConnection;
                                }
                            }
                            SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_DATA_CONNECT));
                        }
                        elseif (is_a($initializedExtension, "\\DblEj\\Presentation\\Integration\\ITemplateRendererExtension"))
                        {
                            //SETUP TEMPLATE ENGINE
                            SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::BEFORE_TEMPLATE_ENGINE_INITIALIZE));

                            $templateRenderer = $this->Get_TemplateRenderer();
                            if (defined("AM_WEBPAGE") && AM_WEBPAGE && !$templateRenderer)
                            {
                                throw new \DblEj\Mvc\InvalidViewEngineException();
                            }
                            if ($templateRenderer)
                            {
                                $templateRenderer->PostInit();
                                foreach ($this->_foldersToBeAddedToTemplateRenderer as $priority => $templateFolder)
                                {
                                   $templateRenderer->AddTemplateFolder($templateFolder, $priority);
                                }
                                $this->_foldersToBeAddedToTemplateRenderer = [];
                            }
                            SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_TEMPLATE_ENGINE_INITIALIZE));
                        }
                    }
                );
                $this->onAfterExtensionsInitialized();



                //add API handlers
                if (@file_exists($this->_localConfigFolder . "More/ApiCalls.syrp"))
                {
                    $apiMappingFilePath = $this->_localConfigFolder . "More/ApiCalls.syrp";

                    $apiMappings        = \Wafl\Util\Syrup::ParseSyrupFileAsArray($apiMappingFilePath);

                    foreach ($apiMappings as $apiMapping)
                    {
                        if (!isset($apiMapping["ApiCall"]) || !isset($apiMapping["HandlerFunction"]) || !isset($apiMapping["HandlerFile"]) || !isset($apiMapping["HandlerClass"]))
                        {
                            throw new \Exception("Unable to parse the API Calls from the config file ".$this->_localConfigFolder . "More/ApiCalls.syrp.  Please verify the syntax (syntax validator available at http://syrupfile.org/) of the file and try again.");
                        }
                        $reindexModelArrays = isset($apiMapping["ReindexModelArrays"])?$apiMapping["ReindexModelArrays"]:false;
                        $modelsToPreload = isset($apiMappings["PreloadModels"])?$apiMappings["PreloadModels"]:[];
                        AjaxHandler::AddAjaxHandler($apiMapping["ApiCall"], $apiMapping["HandlerFunction"], $this->_localAppFolder . $this->_settings->Get_Paths()->Get_Application()->Get_ApiHandlerFolder() . $apiMapping["HandlerFile"], $apiMapping["HandlerClass"], AjaxHandler::REQUEST_TYPE_GET, 0, $reindexModelArrays, $modelsToPreload, $this);
                    }
                }

                //setup search engine
                $indexer = Extensions::GetExtensionByType("\\DblEj\\Data\\Integration\\IIndexer");
                if ($indexer)
                {
                    PersistableModel::SetSearchIndexer($indexer);
                }

                date_default_timezone_set($this->_settings->Get_Application()->Get_Timezone());

                //add any data stores
                foreach (Extensions::GetExtensions() as $extension)
                {
                    if (is_a($extension, "\\DblEj\\Data\\IDataStore"))
                    {
                        $this->AddMemoryStore($extension->Get_StoreId(), $extension);
                    }
                }

                $sessionExtension = Extensions::GetExtensionByType("\\DblEj\\Authentication\\Integration\\ISessionManagerExtension");
                if ($sessionExtension)
                {
                    SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::BEFORE_SESSION_START));
                    $this->_session = $sessionExtension->OpenSession($this);
                    SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_SESSION_START));
                }

                $this->_amInitialized = true;
                $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_AFTER_INITIALIZE));
            }
        }
        catch (\Exception $err)
        {
            throw $err;
        }
    }

    public function AddMemoryStore($storeName, IDataStore $store)
    {
        $this->memoryStores[$storeName] = $store;
    }

    public function GetMemoryStore($storeName = null)
    {
        if (!$storeName)
        {
            $storeName = key($this->memoryStores);
        }
        return isset($this->memoryStores[$storeName]) ? $this->memoryStores[$storeName] : null;
    }

    public function StoreInMemory($dataName, $dataValue, $storeName = null)
    {
        if (!$storeName)
        {
            $storeName = key($this->memoryStores);
        }
        if (!$storeName)
        {
            throw new \Exception("There are no memory stores available.  Please add a memory store extension, such as Redis or memcached");
        }
        $this->memoryStores[$storeName]->SetData($dataName, $dataValue);
    }

    public function GetFromMemory($dataName, $storeName = null)
    {
        if (!$storeName)
        {
            $storeName = key($this->memoryStores);
        }
        if (!$storeName)
        {
            throw new \Exception("There are no memory stores available.  Please add a memory store extension, such as Redis or memcached");
        }
        return $this->memoryStores[$storeName]->GetData($dataName);
    }

    public function DeleteFromMemory($dataName, $storeName = null)
    {
        if (!$storeName)
        {
            $storeName = key($this->memoryStores);
        }
        if (!$storeName)
        {
            throw new \Exception("There are no memory stores available.  Please add a memory store extension, such as Redis or memcached");
        }
        $this->memoryStores[$storeName]->DeleteData($dataName);
    }

    public function ClearSharedMemory($storeName = null)
    {
        if (!$storeName)
        {
            $storeName = key($this->memoryStores);
        }
        if (!$storeName)
        {
            throw new \Exception("There are no memory stores available.  Please add a memory store extension, such as Redis or memcached");
        }
        $this->memoryStores[$storeName]->FlushAllData();
    }

    public function GetStorageEngines()
    {
        $ext = Extensions::GetExtensionByType("\\DblEj\\Data\\Integration\\IDatabaseServerExtension");
        if ($ext)
        {
            return $ext->GetConnections();
        } else {
            return [];
        }
    }

    public function GetStorageEngine($connectionName)
    {
        $connections = $this->GetStorageEngines();
        return isset($connections[$connectionName]) ? $connections[$connectionName] : null;
    }
    public static function Get_SettingsClassName()
    {
        return "\\Wafl\\Application\\Settings\\All";
    }

    /**
     * Get the current session object
     * @return \DblEj\Authentication\IUserSession
     */
    public function Get_Session()
    {
        return $this->_session;
    }

    public function Set_Session($session)
    {
        $this->_session = $session;
    }
    public function Get_ClientGmtOffset()
    {
        return $this->_clientGmtOffset;
    }

    public function Set_ClientGmtOffset($gmtOffset)
    {
        $this->_clientGmtOffset = $gmtOffset;
    }
}