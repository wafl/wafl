<?php

namespace Wafl\Application;

class ApplicationException
extends \Exception
{

    public function __construct(\DblEj\Application\IApplication $app, $message = "", $severity = E_ERROR, \Exception $innerException = null)
    {
        parent::__construct("There was an error with the application " . $app->Get_Name() . " ($message)", $severity, $innerException);
    }
}