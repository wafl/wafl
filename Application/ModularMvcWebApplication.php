<?php

namespace Wafl\Application;

class ModularMvcWebApplication
extends MvcWebApplication
{
    private $_moduleChainIndex;

    //Keep modeule chains in a static so they dont litter our instance debug dumps
    /**
     *
     * @var type
     */
    private static $_moduleChains = [];

    private static function _addModuleChain($moduleChain)
    {
        $chainIdx = count(self::$_moduleChains);
        self::$_moduleChains[$chainIdx] = $moduleChain;
        return $chainIdx;
    }

    private function _getModuleChain()
    {
        return self::$_moduleChains[$this->_moduleChainIndex];
    }

    public function GetModuleChain()
    {
        return self::$_moduleChains[$this->_moduleChainIndex];
    }
    public function __construct(Settings\AllWeb $settings, $environment = "dev", \DblEj\Resources\ResourceCollection $restrictedResources = null, \DblEj\Resources\ResourcePermissionCollection $resourcePermissions = null, $tracePath = null, \DblEj\Mvc\ViewCollection $views = null)
    {
        parent::__construct($settings, $environment, $restrictedResources, $resourcePermissions, $tracePath, $views);
        $this->_moduleChainIndex = self::_addModuleChain(new \DblEj\Extension\ModuleChain());
    }

    protected function onLoadConfig($appConfigPath, $appDefinitions, $appSettings, $moreSettings)
    {
        parent::onLoadConfig($appConfigPath, $appDefinitions, $appSettings, $moreSettings);
        $this->_getModuleChain()->Set_DefaultPermissionForUnregisteredSiteAreas($this->_settings->Get_Application()->Get_DefaultSiteAreaPermission());
        $this->_getModuleChain()->Set_DefaultPermissionForUnregisteredSitePages($this->_settings->Get_Application()->Get_DefaultSitePagePermission());
        $this->_getModuleChain()->Set_DefaultPermissionForUnregisteredApiCalls($this->_settings->Get_Application()->Get_DefaultApiCallPermission());
    }
    public function AddModule(\DblEj\Extension\IModule $module)
    {
        $this->_getModuleChain()->AddModule($module);
    }

    public function GetModule($moduleId)
    {
        return $this->_getModuleChain()->GetModule($moduleId);
    }

    public function GetModuleScreen($screenId)
    {
        return $this->_getModuleChain()->GetScreen($screenId);
    }

    public function Get_ModuleChain()
    {
        return $this->_getModuleChain();
    }

    public function AddModulePermission(\DblEj\Resources\IActor $actor, \DblEj\Extension\IModule $module, $permission = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $this->AddResourcePermission($actor, $module, $permission);
    }

    public function AddScreenPermission(\DblEj\Resources\IActor $actor, \DblEj\Extension\IScreen $screen, $permission = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $this->AddResourcePermission($actor, $screen, $permission);
    }

    public function AddApiCall(\DblEj\Resources\IActor $actor, \DblEj\Extension\IApiCall $apiCall, $permission = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $this->AddResourcePermission($actor, $apiCall, $permission);
    }

    public function AddWidgetPermission(\DblEj\Resources\IActor $actor, \DblEj\Extension\IWidget $widget, $permission = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $this->AddResourcePermission($actor, $widget, $permission);
    }

    public function IsActorAllowedAccessToSiteArea(\DblEj\Resources\IActor $actor, \DblEj\SiteStructure\SiteArea $siteArea, $permissions = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        return $this->_getModuleChain()->IsActorPermittedToSiteArea($this, $actor, $siteArea, $this, $permissions);
    }

    public function IsActorAllowedAccessToSitePage(\DblEj\SiteStructure\SitePage $sitePage, \DblEj\Resources\IActor $actor = null, $permissions = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $modChain = $this->_getModuleChain();
        $permission = $modChain->IsActorPermittedToSitePage($this, $actor, $sitePage, $this, $permissions);
        return $permission;
    }

    public function IsActorAllowedAccessToApiCall($apiCall, \DblEj\Resources\IActor $actor = null, $permissions = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        return $this->_getModuleChain()->IsActorPermittedToApiCall($this, $actor, $apiCall, $this, $permissions);
    }
}