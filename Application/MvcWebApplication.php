<?php
namespace Wafl\Application;

use DblEj\Application\IMvcWebApplication,
    DblEj\Mvc\IView,
    DblEj\Mvc\SitePage,
    DblEj\Resources\ResourceCollection,
    DblEj\Resources\ResourcePermissionCollection,
    DblEj\SiteStructure\SiteArea,
    DblEj\SiteStructure\SiteAreaCollection,
    DblEj\SiteStructure\SitePage as MvcSitePage,
    DblEj\SiteStructure\SitePageCollection,
    Wafl\Application\Settings\AllWeb,
    DblEj\Mvc\SiteMap as MvcSitemap;

class MvcWebApplication
extends WebApplication
implements IMvcWebApplication
{

    /**
     * Construct an MvcWebApplication
     *
     * @param \Wafl\Application\Settings\AllWeb $settings
     *
     * @param string $environment
     * The name of the runtime environment (usualy one of: dev, test, build, or prod)
     *
     * @param \DblEj\Resources\ResourceCollection $restrictedResources
     * A collection of permission-restricted resources that live within this application.
     *
     * @param \DblEj\Resources\ResourcePermissionCollection $resourcePermissions
     * A collection of permissions for this applications Resources.
     *
     * @param string $tracePath
     *
     * @param \DblEj\SiteStructure\SiteAreaCollection $siteAreas
     * A collection of the all Site Areas in this application.
     *
     * @param \DblEj\SiteStructure\SitePageCollection $sitePages
     * A collection of the all Site Pages in this application.
     */
    public function __construct(AllWeb $settings, $environment = "dev", ResourceCollection $restrictedResources = null, ResourcePermissionCollection $resourcePermissions = null, $tracePath = null, SiteAreaCollection $siteAreas = null, SitePageCollection $sitePages = null)
    {
        if (!$this->_siteMapIndex)
        {
            $this->_siteMapIndex = self::addSitemap(new MvcSitemap($siteAreas, $sitePages));
        }
        parent::__construct($settings, $environment, $restrictedResources, $resourcePermissions, $tracePath);
    }

    protected function getExpectedResponseClass()
    {
        return "\\DblEj\\Mvc\\SitePageResponse";
    }

    /**
     * Get the full absolute local path to the directory that holds the Controllers for this application.
     *
     * @return type
     */
    public function Get_LocalLogicFolder()
    {
        return $this->Get_LocalIncludeFolder() . "Controllers" . DIRECTORY_SEPARATOR;
    }

    /**
     * Add a view to the internal list of views.
     *
     * @param IView $view
     */
    public function AddView(IView $view)
    {
        $this->getSitemap()->AddSitePage($view);
    }

    /**
     * Get an array of all views.
     *
     * @return IView[]
     */
    public function GetAllViews()
    {
        return $this->getSitemap()->GetAllSitePages();
    }

    /**
     * Get a view by it's id.
     *
     * @param int|string $viewid A unique id for this view
     *
     * @return IView
     */
    public function GetView($viewid)
    {
        $view = $this->getSitemap()->GetSitePage($viewid);
        if (!$view)
        {
            $view = new \DblEj\Mvc\View($viewid, new \DblEj\Data\ArrayModel([]), $viewid);
        }
        return $view;
    }

    /**
     *
     * @param string $title
     * @param string $controllerFile
     * @param string $templateName
     * @param int $menuDisplayOrder
     * @param SiteArea $parentArea
     * @param boolean $isAreaDefault
     * @param string $description
     * @param string $fullTitle
     * @param string $keywords
     * @param boolean $preprocessCss
     * @return MvcSitePage
     */
    public function RegisterSitePage($title, $controllerFile, $templateName, $menuDisplayOrder = 0, SiteArea $parentArea = null, $isAreaDefault = false, $description = "", $fullTitle = "", $keywords = "", $preprocessCss = false, $registeredBy = "unknown")
    {
        $uniqueId = $title;
        if (!$this->GetSitePage($uniqueId))
        {
            if (!$this->Get_TemplateRenderer() || $this->Get_TemplateRenderer()->DoesTemplateExist($templateName))
            {
                $newPage = new SitePage($uniqueId, $title, $description, $controllerFile, $templateName, $menuDisplayOrder, $parentArea ? $parentArea->Get_AreaId() : null, $isAreaDefault, $fullTitle, $keywords, $preprocessCss, [], $registeredBy);
                $this->getSitemap()->AddSitePage($newPage);

                if ($parentArea)
                {
                    $this->getSitemap()->AddSiteArea($parentArea);
                    if ($isAreaDefault)
                    {
                        $parentArea->AddDefaultPage($newPage);
                    }
                }
            } else {
                $newPage = null;
            }
            return $newPage;
        }
        else
        {
            return $this->GetSitePage($uniqueId);
        }
    }
}