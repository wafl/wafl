<?php
namespace Wafl\Application\Settings;

class All
extends \DblEj\Configuration\OptionList
{

    public function __construct($appName, $appGuid, $appNamespace, $appVersion)
    {
        parent::__construct(
            array(
                new \DblEj\Configuration\Option("Application", null, new Application($appName, $appGuid, $appNamespace, $appVersion)),
                new \DblEj\Configuration\Option("Database", null, new Database()),
                new \DblEj\Configuration\Option("Debug", null, new Debug()),
                new \DblEj\Configuration\Option("Paths", null, new Path()),
                new \DblEj\Configuration\Option("DataStorage", null, new DataStorageSettings())
            )
        );
    }

    /**
     * @return \Wafl\Application\Settings\Database
     */
    public function Get_Database()
    {
        return $this->GetOptionValue("Database");
    }

    /**
     * @return \Wafl\Application\Settings\Debug
     */
    public function Get_Debug()
    {
        return $this->GetOptionValue("Debug");
    }

    /**
     * @return \Wafl\Application\Settings\Path
     */
    public function Get_Paths()
    {
        return $this->GetOptionValue("Paths");
    }

    public function Get_DataStorage()
    {
        return $this->GetOptionValue("DataStorage");
    }

    public function Set_DataStorage(DataStorage $settings)
    {
        $this->SetOption("DataStorage", $settings);
    }

    public function Set_Database(Database $settings)
    {
        $this->SetOption("Database", $settings);
    }

    public function Set_Debug(Debug $settings)
    {
        $this->SetOption("Debug", $settings);
    }

    public function Set_Paths(Path $paths)
    {
        $this->SetOption("Paths", $paths);
    }

    /**
     *
     * @return \Wafl\Application\Settings\Application
     */
    public function Get_Application()
    {
        return $this->GetOptionValue("Application");
    }

    public function Set_Application(Application $application)
    {
        $this->SetOption("Application", $application);
    }
}