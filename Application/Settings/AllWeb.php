<?php

namespace Wafl\Application\Settings;

class AllWeb
extends All
{

    public function __construct($appName, $appGuid, $appNamespace, $appVersion)
    {
        parent::__construct($appName, $appGuid, $appNamespace, $appVersion);
        $this->AddOption("Web");
        $this->SetOption("Web", new Web());
    }

    /**
     * @return \Wafl\Application\Settings\Web
     */
    public function Get_Web()
    {
        return $this->GetOptionValue("Web");
    }

    public function Set_Web(Web $web)
    {
        $this->SetOption("Web", $web);
    }
}