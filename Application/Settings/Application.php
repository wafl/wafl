<?php
namespace Wafl\Application\Settings;

class Application
extends \DblEj\Configuration\OptionList
{

    public function __construct($appName, $appGuid, $appNamespace, $appVersion)
    {
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("Name", null, $appName),
            new \DblEj\Configuration\Option("Guid", null, $appGuid),
            new \DblEj\Configuration\Option("Version", null, $appVersion),
            new \DblEj\Configuration\Option("RootNameSpace", null, $appNamespace),
            new \DblEj\Configuration\Option("AutoDownloadExtensions", null, true)
        );
        parent::__construct($arrayOfOptions);
    }

    public function Get_AppName()
    {
        return $this->GetOptionValue("Name");
    }

    public function Get_AutoDownloadExtensions()
    {
        return $this->GetOptionValue("AutoDownloadExtensions");
    }

    public function Get_Guid()
    {
        return $this->GetOptionValue("Guid");
    }

    public function Get_Version()
    {
        return $this->GetOptionValue("Version");
    }

    public function Get_RootNameSpace()
    {
        return $this->GetOptionValue("RootNameSpace");
    }

    public function Get_Timezone()
    {
        return $this->GetOptionValue("Timezone");
    }

    public function Get_GuestGroupId()
    {
        return $this->GetOptionValue("GuestGroupId");
    }

    public function Get_CacheStoreId()
    {
        if ($this->GetIsOption("CacheStoreId"))
        {
            return $this->GetOptionValue("CacheStoreId");
        } else {
            return null;
        }
    }

    public function Get_SiteArea()
    {
        return $this->GetOptionValue("SiteArea");
    }

    public function Get_DefaultUserClass()
    {
        return $this->GetOptionValue("DefaultUserClass");
    }

    public function Get_DefaultUserGroupClass()
    {
        return $this->GetOptionValue("DefaultUserGroupClass");
    }

    public function Get_DefaultUsergroupId()
    {
        return $this->GetOptionValue("DefaultUsergroupId");
    }

    public function Get_DefaultSiteAreaPermission()
    {
        if ($this->GetIsOption("DefaultSiteAreaPermission"))
        {
            return $this->GetOptionValue("DefaultSiteAreaPermission");
        } else {
            return false;
        }
    }

    public function Get_DefaultSitePagePermission()
    {
        if ($this->GetIsOption("DefaultSitePagePermission"))
        {
            return $this->GetOptionValue("DefaultSitePagePermission");
        } else {
            return false;
        }
    }

    public function Get_DefaultApiCallPermission()
    {
        if ($this->GetIsOption("DefaultApiCallPermission"))
        {
            return $this->GetOptionValue("DefaultApiCallPermission");
        } else {
            return false;
        }
    }

    public function Set_AppName($newValue)
    {
        $this->AddOption("Name", $newValue);
        return $this;
    }

    public function Set_AutoDownloadExtensions($newValue)
    {
        $this->AddOption("AutoDownloadExtensions", $newValue);
        return $this;
    }

    public function Set_Guid($newValue)
    {
        $this->AddOption("Guid", $newValue);
        return $this;
    }

    public function Set_Version($newValue)
    {
        $this->AddOption("Version", $newValue);
        return $this;
    }

    public function Set_RootNameSpace($newValue)
    {
        $this->AddOption("RootNameSpace", $newValue);
        return $this;
    }

    public function Set_Timezone($newValue)
    {
        $this->AddOption("Timezone", $newValue);
        return $this;
    }

    public function Set_GuestGroupId($newValue)
    {
        $this->AddOption("GuestGroupId", $newValue);
        return $this;
    }

    public function Set_CacheStoreId($newValue)
    {
        $this->AddOption("CacheStoreId", $newValue);
        return $this;
    }

    public function Set_SiteArea($newValue)
    {
        $this->AddOption("SiteArea", $newValue);
        return $this;
    }

    public function Set_DefaultUserClass($newValue)
    {
        $this->AddOption("DefaultUserClass", $newValue);
        return $this;
    }

    public function Set_DefaultUserGroupClass($newValue)
    {
        $this->AddOption("DefaultUserGroupClass", $newValue);
        return $this;
    }

    public function Set_DefaultUsergroupId($newValue)
    {
        $this->AddOption("DefaultUsergroupId", $newValue);
        return $this;
    }

    public function Set_DefaultSiteAreaPermission($newValue)
    {
        $this->AddOption("DefaultSiteAreaPermission", $newValue);
        return $this;
    }

    public function Set_DefaultSitePagePermission($newValue)
    {
        $this->AddOption("DefaultSitePagePermission", $newValue);
        return $this;
    }

    public function GetSessionTagName()
    {
        return "Y".substr(strtolower(md5($this->Get_Guid() . $this->Get_Version())), 2, 8);
    }

}