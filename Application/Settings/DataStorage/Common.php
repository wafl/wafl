<?php

namespace Wafl\Application\Settings\DataStorage;

class Common
extends \DblEj\Configuration\OptionList
{

    public function __construct($loadBalanced = false)
    {
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("LoadBalanced", null, $loadBalanced)
        );
        parent::__construct($arrayOfOptions);
    }

    public function Get_LoadBalanced()
    {
        return $this->GetOptionValue("LoadBalanced");
    }

    public function Set_LoadBalanced($newValue)
    {
        $this->SetOption("LoadBalanced", $newValue);
        return $this;
    }
}