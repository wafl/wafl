<?php

namespace Wafl\Application\Settings\DataStorage;

class Connection
extends \DblEj\Configuration\OptionList
{

    public function __construct($uri = null, $catalog = null, $username = null, $password = null, $createScript = null, $updateScript = null, $replicationRole = null, $persistModels = null, $modelGroup = null, $requiredLocation = null, $accessScope = null, $engineDriverPath = null, $characterEncoding = null)
    {
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("Uri", null, $uri),
            new \DblEj\Configuration\Option("Catalog", null, $catalog),
            new \DblEj\Configuration\Option("Username", null, $username),
            new \DblEj\Configuration\Option("Password", null, $password),
            new \DblEj\Configuration\Option("CreateScript", null, $createScript),
            new \DblEj\Configuration\Option("UpdateScript", null, $updateScript),
            new \DblEj\Configuration\Option("ReplicationRole", null, $replicationRole),
            new \DblEj\Configuration\Option("PersistModels", null, $persistModels),
            new \DblEj\Configuration\Option("ModelGroup", null, $modelGroup),
            new \DblEj\Configuration\Option("RequiredLocation", null, $requiredLocation),
            new \DblEj\Configuration\Option("AccessScope", null, $accessScope),
            new \DblEj\Configuration\Option("EngineDriverPath", null, $engineDriverPath),
            new \DblEj\Configuration\Option("CharacterEncoding", null, $characterEncoding),
        );
        parent::__construct($arrayOfOptions);
    }

    public static function CreateFromArray(array $associativeArray = null)
    {
        $connection = new Connection();
        foreach ($associativeArray as $key => $val)
        {
            $connection->SetOption($key, $val);
        }
        return $connection;
    }

    public function Get_Uri()
    {
        return $this->GetOptionValue("Uri");
    }

    public function Get_Catalog()
    {
        return $this->GetOptionValue("Catalog");
    }

    public function Get_Username()
    {
        return $this->GetOptionValue("Username");
    }

    public function Get_Password()
    {
        return $this->GetOptionValue("Password");
    }

    public function Get_CreateScript()
    {
        return $this->GetOptionValue("CreateScript");
    }

    public function Get_UpdateScript()
    {
        return $this->GetOptionValue("UpdateScript");
    }

    public function Get_ReplicationRole()
    {
        return $this->GetOptionValue("ReplicationRole");
    }

    public function Get_PersistModels()
    {
        return $this->GetOptionValue("PersistModels");
    }

    public function Get_ModelGroup()
    {
        return $this->GetOptionValue("ModelGroup");
    }

    public function Get_RequiredLocation()
    {
        return $this->GetOptionValue("RequiredLocation");
    }

    public function Get_AccessScope()
    {
        return $this->GetOptionValue("AccessScope");
    }

    public function Get_EngineDriverPath()
    {
        return $this->GetOptionValue("EngineDriverPath");
    }

    public function Get_CharacterEncoding()
    {
        return $this->GetOptionValue("CharacterEncoding");
    }

    public function Set_Uri($newValue)
    {
        $this->SetOption("Uri", $newValue);
        return $this;
    }

    public function Set_Catalog($newValue)
    {
        $this->SetOption("Catalog", $newValue);
        return $this;
    }

    public function Set_Username($newValue)
    {
        $this->SetOption("Username", $newValue);
        return $this;
    }

    public function Set_Password($newValue)
    {
        $this->SetOption("Password", $newValue);
        return $this;
    }

    public function Set_CreateScript($newValue)
    {
        $this->SetOption("CreateScript", $newValue);
        return $this;
    }

    public function Set_UpdateScript($newValue)
    {
        $this->SetOption("UpdateScript", $newValue);
        return $this;
    }

    public function Set_ReplicationRole($newValue)
    {
        $this->SetOption("ReplicationRole", $newValue);
        return $this;
    }

    public function Set_PersistModels($newValue)
    {
        $this->SetOption("PersistModels", $newValue);
        return $this;
    }

    public function Set_ModelGroup($newValue)
    {
        $this->SetOption("ModelGroup", $newValue);
        return $this;
    }

    public function Set_RequiredLocation($newValue)
    {
        $this->SetOption("RequiredLocation", $newValue);
        return $this;
    }

    public function Set_AccessScope($newValue)
    {
        $this->SetOption("AccessScope", $newValue);
        return $this;
    }

    public function Set_EngineDriverPath($newValue)
    {
        $this->SetOption("EngineDriverPath", $newValue);
        return $this;
    }

    public function Set_CharacterEncoding($newValue)
    {
        $this->SetOption("CharacterEncoding", $newValue);
        return $this;
    }
}