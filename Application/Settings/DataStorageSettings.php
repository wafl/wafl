<?php

namespace Wafl\Application\Settings;

class DataStorageSettings
extends \DblEj\Configuration\OptionList
{

    public function __construct(DataStorage\Common $common = null)
    {
        if (!$common)
        {
            $common = new \Wafl\Application\Settings\DataStorage\Common();
        }
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("Common", null, $common)
        );
        parent::__construct($arrayOfOptions);
    }

    public function Set_Common(DataStorage\Common $common)
    {
        $this->SetOption("Common", $common);
    }

    public function AddConnection($connectionName, DataStorage\Connection $connection)
    {
        $this->SetOption($connectionName, $connection);
    }

    /**
     * @return \Wafl\Application\Settings\Paths\ApplicationPathSettings
     */
    public function Get_Common()
    {
        return $this->GetOptionValue("Common");
    }

    /**
     * @return \Wafl\Application\Settings\Paths\WaflPathSettings
     */
    public function GetConnection($connectionName)
    {
        return $this->GetOptionValue($connectionName);
    }
}