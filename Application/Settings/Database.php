<?php

namespace Wafl\Application\Settings;

class Database
extends \DblEj\Configuration\OptionList
{

    public function __construct($createScript = null)
    {
        parent::__construct(array(
            new \DblEj\Configuration\Option("CreateScript", null, $createScript)));
    }

    public function Get_CreateScript()
    {
        return $this->GetOptionValue("CreateScript");
    }

    public function Set_CreateScript($createScript)
    {
        $this->SetOption("CreateScript", $createScript);
    }
}