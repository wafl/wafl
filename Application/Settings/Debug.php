<?php

namespace Wafl\Application\Settings;

class Debug
extends \DblEj\Configuration\OptionList
{

    public function __construct($suppressErrors = null, $debugMode = null, $autoupdateDb = null)
    {
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("SuppressErrors", null, $suppressErrors),
            new \DblEj\Configuration\Option("DebugMode", null, $debugMode),
            new \DblEj\Configuration\Option("AutoUpdateDb", null, $autoupdateDb)
        );
        parent::__construct($arrayOfOptions);
    }

    public function Get_SuppressErrors()
    {
        return $this->GetOptionValue("SuppressErrors");
    }

    public function Get_DebugMode()
    {
        return $this->GetOptionValue("DebugMode");
    }

    public function Get_AutoUpdateDb()
    {
        return $this->GetOptionValue("AutoUpdateDb");
    }

    public function Set_SuppressErrors($newValue)
    {
        $this->SetOption("SuppressErrors", $newValue);
        return $this;
    }

    public function Set_DebugMode($newValue)
    {
        $this->SetOption("DebugMode", $newValue);
        return $this;
    }

    public function Set_AutoUpdateDb($newValue)
    {
        $this->SetOption("AutoUpdateDb", $newValue);
        return $this;
    }
}