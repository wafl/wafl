<?php

namespace Wafl\Application\Settings;

class InvalidSettingException
extends \Exception
{

    public function __construct($settingName, $message = "", $severity = E_NOTICE, \Exception $innerException = null)
    {
        parent::__construct("Invalid setting: $settingName.  $message", $severity, $innerException);
    }
}