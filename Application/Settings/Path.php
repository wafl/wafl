<?php

namespace Wafl\Application\Settings;

class Path
extends \DblEj\Configuration\OptionList
{

    public function __construct(Paths\Application $applicationPaths = null, Paths\Wafl $waflPaths = null)
    {
        if (!$applicationPaths)
        {
            $applicationPaths = new Paths\Application();
        }
        if (!$waflPaths)
        {
            $waflPaths = new Paths\Wafl();
        }
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("Application", null, $applicationPaths),
            new \DblEj\Configuration\Option("Wafl", null, $waflPaths)
        );
        parent::__construct($arrayOfOptions);
    }

    public function Set_Application(Paths\Application $paths)
    {
        $this->SetOption("Application", $paths);
    }

    public function Set_Wafl(Paths\Wafl $paths)
    {
        $this->SetOption("Wafl", $paths);
    }

    /**
     * @return \Wafl\Application\Settings\Paths\Application
     */
    public function Get_Application()
    {
        return $this->GetOptionValue("Application");
    }

    /**
     * @return \Wafl\Application\Settings\Paths\Wafl
     */
    public function Get_Wafl()
    {
        return $this->GetOptionValue("Wafl");
    }
}