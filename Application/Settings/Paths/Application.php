<?php
namespace Wafl\Application\Settings\Paths;

class Application
extends \DblEj\Configuration\OptionList
{

    public function __construct($configFolder = null, $presentationFolder = null, $htmlFolder = null, $stylesheetsFolder = null, $localRootFolder = null, $publicWebFolder = "Public/", $defaultPage = null, $functionalModelsClientFolder = null, $functionalModelsServerFolder = null, $templateCompilesFolder = null, $templateCacheFolder = null, $tempFolder = null, $apiHandlerFolder = null, $uploadFolder = null, $globalEventHandlerFile = null, $globalScript = null, $siteStructureSyrupFile = "SiteStructure.syrp", $logs="Logs/")
    {
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("Presentation", null, $presentationFolder),
            new \DblEj\Configuration\Option("Html", null, $htmlFolder),
            new \DblEj\Configuration\Option("Stylesheets", null, $stylesheetsFolder),
            new \DblEj\Configuration\Option("LocalRoot", null, $localRootFolder),
            new \DblEj\Configuration\Option("Logs", "Logs/", $logs),
            new \DblEj\Configuration\Option("PublicWeb", "Public/", $publicWebFolder),
            new \DblEj\Configuration\Option("DefaultPage", null, $defaultPage),
            new \DblEj\Configuration\Option("FuncModelsClient", null, $functionalModelsClientFolder),
            new \DblEj\Configuration\Option("FuncModelsServer", null, $functionalModelsServerFolder),
            new \DblEj\Configuration\Option("TemplateCompiles", null, $templateCompilesFolder),
            new \DblEj\Configuration\Option("TemplateCache", null, $templateCacheFolder),
            new \DblEj\Configuration\Option("Temp", null, $tempFolder),
            new \DblEj\Configuration\Option("ApiHandlerFolder", null, $apiHandlerFolder),
            new \DblEj\Configuration\Option("UploadFolder", null, $uploadFolder),
            new \DblEj\Configuration\Option("GlobalEventHandlers", null, $globalEventHandlerFile),
            new \DblEj\Configuration\Option("GlobalScript", null, $globalScript),
            new \DblEj\Configuration\Option("Config", null, $configFolder),
            new \DblEj\Configuration\Option("SiteStructureFilename", "SiteStructure.syrp", $siteStructureSyrupFile)
        );
        parent::__construct($arrayOfOptions);
    }

    public function Get_Presentation()
    {
        return $this->GetOptionValue("Presentation");
    }

    public function Get_Html()
    {
        return $this->GetOptionValue("Html");
    }

    public function Get_Stylesheets()
    {
        return $this->GetOptionValue("Stylesheets");
    }

    public function Get_LocalRoot()
    {
        return $this->GetOptionValue("LocalRoot");
    }

    public function Get_Logs()
    {
        return $this->GetOptionValue("Logs");
    }

    public function Get_PublicWeb()
    {
        return $this->GetOptionValue("PublicWeb");
    }

    public function Get_DefaultPage()
    {
        return $this->GetOptionValue("DefaultPage");
    }

    public function Get_FunctionalModelsClient()
    {
        return $this->GetOptionValue("FuncModelsClient");
    }

    public function Get_FunctionalModelsServer()
    {
        return $this->GetOptionValue("FuncModelsServer");
    }

    public function Get_TemplateCompiles()
    {
        return $this->GetOptionValue("TemplateCompiles");
    }

    public function Get_TemplateCache()
    {
        return $this->GetOptionValue("TemplateCache");
    }

    public function Get_Temp()
    {
        return $this->GetOptionValue("TempFolder");
    }

    public function Get_ApiHandlerFolder()
    {
        return $this->GetOptionValue("ApiHandlerFolder");
    }

    public function Get_UploadFolder()
    {
        return $this->GetOptionValue("UploadFolder");
    }

    public function Get_GlobalEventHandlers()
    {
        return $this->GetOptionValue("GlobalEventHandlers");
    }

    public function Get_GlobalScript()
    {
        return $this->GetOptionValue("GlobalScript");
    }

    public function Get_Config()
    {
        return $this->GetOptionValue("Config");
    }

    public function Get_SiteStructurFilename()
    {
        return $this->GetOptionValue("SiteStructureFilename");
    }

    public function Set_Presentation($newValue)
    {
        $this->SetOption("Presentation", $newValue);
        return $this;
    }

    public function Set_Html($newValue)
    {
        $this->SetOption("Html", $newValue);
        return $this;
    }

    public function Set_Stylesheets($newValue)
    {
        $this->SetOption("Stylesheets", $newValue);
        return $this;
    }

    public function Set_LocalRoot($newValue)
    {
        $this->SetOption("LocalRoot", $newValue);
        return $this;
    }

    public function Set_Logs($newValue)
    {
        $this->SetOption("Logs", $newValue);
        return $this;
    }

    public function Set_PublicWeb($newValue)
    {
        $this->SetOption("PublicWeb", $newValue);
        return $this;
    }

    public function Set_DefaultPage($newValue)
    {
        $this->SetOption("DefaultPage", $newValue);
        return $this;
    }

    public function Set_FunctionalModelsClient($newValue)
    {
        $this->SetOption("FuncModelsClient", $newValue);
        return $this;
    }

    public function Set_FunctionalModelsServer($newValue)
    {
        $this->SetOption("FuncModelsServer", $newValue);
        return $this;
    }

    public function Set_TemplateCompiles($newValue)
    {
        $this->SetOption("TemplateCompiles", $newValue);
        return $this;
    }

    public function Set_TemplateCache($newValue)
    {
        $this->SetOption("TemplateCache", $newValue);
        return $this;
    }

    public function Set_Temp($newValue)
    {
        $this->SetOption("TempFolder", $newValue);
        return $this;
    }

    public function Set_ApiHandlerFolder($newValue)
    {
        $this->SetOption("ApiHandlerFolder", $newValue);
        return $this;
    }

    public function Set_UploadFolder($newValue)
    {
        $this->SetOption("UploadFolder", $newValue);
        return $this;
    }

    public function Set_GlobalEventHandlers($newValue)
    {
        $this->SetOption("GlobalEventHandlers", $newValue);
        return $this;
    }

    public function Set_GlobalScript($newValue)
    {
        $this->SetOption("GlobalScript", $newValue);
        return $this;
    }

    public function Set_Config($newValue)
    {
        $this->SetOption("Config", $newValue);
        return $this;
    }

    public function Set_SiteStructurFilename($newValue)
    {
        $this->SetOption("SiteStructureFilename", $newValue);
        return $this;
    }
}