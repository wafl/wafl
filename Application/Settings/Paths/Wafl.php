<?php
namespace Wafl\Application\Settings\Paths;

class Wafl
extends \DblEj\Configuration\OptionList
{

    public function __construct($waflFolder = null, $dblEjFolder = null, $controls = null, $extensionsFolder = null, $utilFolder = null)
    {
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("WaflFolder", null, $waflFolder),
            new \DblEj\Configuration\Option("DblEjFolder", null, $dblEjFolder),
            new \DblEj\Configuration\Option("ControlsFolder", null, $controls),
            new \DblEj\Configuration\Option("ExtensionsFolder", null, $extensionsFolder),
            new \DblEj\Configuration\Option("UtilFolder", null, $utilFolder)
        );
        parent::__construct($arrayOfOptions);
    }

    public function Get_WaflFolder()
    {
        return $this->GetOptionValue("WaflFolder");
    }

    public function Get_DblEjFolder()
    {
        return $this->GetOptionValue("DblEjFolder");
    }

    public function Get_ControlsFolder()
    {
        return $this->GetOptionValue("ControlsFolder");
    }

    public function Get_ExtensionsFolder()
    {
        return $this->GetOptionValue("ExtensionsFolder");
    }

    public function Get_UtilFolder()
    {
        return $this->GetOptionValue("UtilFolder");
    }

    public function Set_WaflFolder($newValue)
    {
        $this->SetOption("WaflFolder", $newValue);
        return $this;
    }

    public function Set_DblEjFolder($newValue)
    {
        $this->SetOption("DblEjFolder", $newValue);
        return $this;
    }

    public function Set_ControlsFolder($newValue)
    {
        $this->SetOption("ControlsFolder", $newValue);
        return $this;
    }

    public function Set_ExtensionsFolder($newValue)
    {
        $this->SetOption("ExtensionsFolder", $newValue);
        return $this;
    }

    public function Set_UtilFolder($newValue)
    {
        $this->SetOption("UtilFolder", $newValue);
        return $this;
    }
}