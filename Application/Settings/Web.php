<?php

namespace Wafl\Application\Settings;

class Web
extends \DblEj\Configuration\OptionList
{

    public function __construct($domainName = null, $webUrl = null, $webUrlSecure = null, $webUrlRelative = "/", $siteEmail = null, $cookielifetime = 350000, $cookieDomainPrefix = ".", $siteTitle = "WAFL Web Application", $globalStylesheetHasTokens = false, $webPort = 80, $webPortSecure = 443)
    {
        $arrayOfOptions = array
        (
            new \DblEj\Configuration\Option("DomainName", null, $domainName),
            new \DblEj\Configuration\Option("WebUrl", null, $webUrl),
            new \DblEj\Configuration\Option("WebUrlSecure", null, $webUrlSecure),
            new \DblEj\Configuration\Option("WebUrlRelative", "/", $webUrlRelative),
            new \DblEj\Configuration\Option("WebPort", 80, $webPort),
            new \DblEj\Configuration\Option("WebPortSecure", 443, $webPortSecure),
            new \DblEj\Configuration\Option("SiteEmail", null, $siteEmail),
            new \DblEj\Configuration\Option("CookieLifetime", 350000, $cookielifetime),
            new \DblEj\Configuration\Option("CookieDomainPrefix", ".", $cookieDomainPrefix),
            new \DblEj\Configuration\Option("GlobalStylesheetHasTokens", "0", $globalStylesheetHasTokens),
            new \DblEj\Configuration\Option("SiteDisplayTitle", null, $siteTitle)
        );
        parent::__construct($arrayOfOptions);
    }

    public function Get_DomainName()
    {
        return $this->GetOptionValue("DomainName");
    }

    public function Get_WebUrl()
    {
        return $this->GetOptionValue("WebUrl");
    }

    public function Get_WebUrlSecure()
    {
        return $this->GetOptionValue("WebUrlSecure");
    }

    public function Get_ApiUrl()
    {
        return $this->GetOptionValue("ApiUrl");
    }

    public function Get_ApiUrlSecure()
    {
        return $this->GetOptionValue("ApiUrlSecure");
    }

    public function Get_ApiUrlRelative()
    {
        return $this->GetOptionValue("ApiUrlRelative");
    }

    public function Get_WebUrlRelative()
    {
        return $this->GetOptionValue("WebUrlRelative");
    }

    public function Get_WebPort()
    {
        return $this->GetOptionValue("WebPort");
    }

    public function Get_WebPortSecure()
    {
        return $this->GetOptionValue("WebPortSecure");
    }

    public function Get_SiteEmail()
    {
        return $this->GetOptionValue("SiteEmail");
    }

    public function Get_CookieLifetime()
    {
        return $this->GetOptionValue("CookieLifetime");
    }

    public function Get_CookieDomainPrefix()
    {
        return $this->GetOptionValue("CookieDomainPrefix");
    }

    public function Get_SiteDisplayTitle()
    {
        return $this->GetOptionValue("SiteDisplayTitle");
    }

    public function Get_GlobalStylesheetHasTokens()
    {
        return $this->GetOptionValue("GlobalStylesheetHasTokens");
    }

    public function Set_DomainName($newValue)
    {
        $this->SetOption("DomainName", $newValue);
        return $this;
    }

    public function Set_WebUrl($newValue)
    {
        $this->SetOption("WebUrl", $newValue);
        return $this;
    }

    public function Set_WebUrlSecure($newValue)
    {
        $this->SetOption("WebUrlSecure", $newValue);
        return $this;
    }

    public function Set_ApiUrl($newValue)
    {
        $this->SetOption("ApiUrl", $newValue);
        return $this;
    }

    public function Set_ApiUrlSecure($newValue)
    {
        $this->SetOption("ApiUrlSecure", $newValue);
        return $this;
    }

    public function Set_ApiUrlRelative($newValue)
    {
        $this->SetOption("ApiUrlRelative", $newValue);
        return $this;
    }

    public function Set_WebUrlRelative($newValue)
    {
        $this->SetOption("WebUrlRelative", $newValue);
        return $this;
    }

    public function Set_WebPort($newValue)
    {
        $this->SetOption("WebPort", $newValue);
        return $this;
    }

    public function Set_WebPortSecure($newValue)
    {
        $this->SetOption("WebPortSecure", $newValue);
        return $this;
    }

    public function Set_SiteEmail($newValue)
    {
        $this->SetOption("SiteEmail", $newValue);
        return $this;
    }

    public function Set_CookieLifetime($newValue)
    {
        $this->SetOption("CookieLifetime", $newValue);
        return $this;
    }

    public function Set_CookieDomainPrefix($newValue)
    {
        $this->SetOption("CookieDomainPrefix", $newValue);
        return $this;
    }

    public function Set_SiteDisplayTitle($newValue)
    {
        $this->SetOption("SiteDisplayTitle", $newValue);
        return $this;
    }

    public function Set_GlobalStylesheetHasTokens($newValue)
    {
        $this->AddOption("GlobalStylesheetHasTokens", $newValue);
        return $this;
    }
}