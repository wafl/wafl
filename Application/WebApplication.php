<?php
namespace Wafl\Application;

use DblEj\Application\IWebApplication,
    DblEj\Communication\Http\InvalidResponseException,
    DblEj\Communication\Http\Request as HttpRequest,
    DblEj\Communication\Http\Routing\InvalidRouteException,
    DblEj\Communication\Http\Routing\InvalidRouterRequestException,
    DblEj\Communication\Http\Util,
    DblEj\EventHandling\EventInfo,
    DblEj\Presentation\RenderOptions,
    DblEj\Resources\IActor,
    DblEj\Resources\Resource,
    DblEj\Resources\ResourceAccessDeniedException,
    DblEj\Resources\ResourceCollection,
    DblEj\Resources\ResourcePermission,
    DblEj\Resources\ResourcePermissionCollection,
    DblEj\SiteStructure\SiteArea,
    DblEj\SiteStructure\SitePage,
    DblEj\UI\Skin,
    DblEj\UI\StyledFont,
    DblEj\UI\Stylesheet,
    DblEj\Util\SystemEvents,
    Wafl\Application\Settings\AllWeb,
    Wafl\Core,
    Wafl\Users\UserGroup,
    Wafl\Util\Controls,
    Wafl\Util\HttpRouter,
    DblEj\Mvc\SiteMap;

class WebApplication
extends Application
implements IWebApplication
{
    private $_skins;

    /**
     *
     * @var Skin
     */
    private $_currentSkin;
    private $_webRootPath;

    /**
     *
     * @var SiteMap
     */
    protected $_siteMapIndex;

    public function __construct(AllWeb &$settings, $environment = "dev", ResourceCollection $restrictedResources = null, ResourcePermissionCollection $resourcePermissions = null, $tracePath = null, $siteAreas = null, $sitePages = null)
    {
        if (!$this->_siteMapIndex)
        {
            $this->_siteMapIndex = self::addSitemap(new SiteMap($siteAreas, $sitePages));
        }
        parent::__construct($settings, $environment, $restrictedResources, $resourcePermissions, $tracePath);
    }

    protected function onBeforeInitizalized()
    {
        //PRE REGISTER CONTROLS FROM SYRUP
        Controls::PreRegisterControlsFromSyrup($this->_localConfigFolder . "More/Controls.syrp");

        parent::onBeforeInitizalized();
    }

    protected function onAfterExtensionsInitialized()
    {
        \Wafl\Util\Extensions::RegisterExtensionSitePages($this);
        SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_EXTENSIONS_SITEPAGES_REGISTERED));

        parent::onAfterExtensionsInitialized();
    }


    public function Run(&$exitCode = null)
    {
        $routerUsed = null;
        //give our subclasses a chance to override (@todo shouldnt onBeforeRun *raise* the event, .Net style?)
        $this->onBeforeRun();
        $this->Get_TemplateRenderer()->SetGlobalData("SKINS", $this->_skins);
        $this->Get_TemplateRenderer()->SetGlobalData("CURRENT_SKIN_TITLE", $this->_currentSkin->Get_Title());
        $this->Get_TemplateRenderer()->SetGlobalData("SKIN", $this->_currentSkin);
        $this->Get_TemplateRenderer()->SetGlobalData("APP", $this);
        $this->Get_TemplateRenderer()->SetGlobalData("APP_VERSION", $this->Get_Settings()->Get_Application()->Get_Version());

        //get the passed in uri
        $requestUri   = Util::GetInput("REQUEST_URI", HttpRequest::INPUT_SERVER); //@todo do all http servers use this header?  Tested in apache. And what about REDIRECT_URL, I was checking that as well elsewhere
        $requestMethod = Util::GetInput("REQUEST_METHOD", HttpRequest::INPUT_SERVER); //@todo do all http servers use this header?
        $httpResponse = null;

        //ask the router utility to route the request to the appropriate handler
        $route = HttpRouter::FromClientToServer($requestUri, $this, $routerUsed, $requestMethod);

        if ($route)
        {
            $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_BEFORE_REQUEST_ROUTED, $route, $this));

            //check if user is allowed to go to this route
            $gateState = $this->_responseGatekeepers->AttemptRoute($route, Core::$CURRENT_USER);
            if ($gateState->Get_IsRestricted())
            {
                throw new ResourceAccessDeniedException($gateState->Get_RestrictedResourceName(), Core::$CURRENT_USER->Get_DisplayName(), E_ERROR, null, null, "Gate Keeper routing for user not in a user group");
            }

            $this->Get_TemplateRenderer()->SetGlobalDataReference("APP_RUNSTATS", $this->_runStats);

            //see if we can do preliminary work and send the headers before we actually process and send the response.
            //this allows the client to be more responsive by starting to process our headers and show navigation updates
            //while we process the request for a full reply.
            $httpPreResponse = $route->PrepareRoute($this);
            $headersSent = false;
            if ($httpPreResponse)
            {
                //send headers
                $headersSent = HttpRouter::SendResponseHeaders($httpPreResponse->Get_Headers(), $httpPreResponse->Get_ResponseCode(), $httpPreResponse->Get_ContentLength(), $this);
                if (\Wafl\Core::$_SendOutputAsEarlyAsPossible)
                {
                    print(" ");
                    flush();
                }
                if ($httpPreResponse->Get_ResponseCode() >= 300 && $httpPreResponse->Get_ResponseCode() <= 399)
                {
                    //don't continue after reidrect
                    die();
                }
            }

            $httpResponse = $route->GotoRoute($this);
            $httpResponse->Set_HeadersSent($headersSent);

            if ($this->_session)
            {
                SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::BEFORE_SESSION_CLOSE));
                $this->_session->Close();
                SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_SESSION_CLOSE));
            }

            $this->_runStats->SetResponse($httpResponse, $route);

            //validate the reponse
            if (!$httpResponse)
            {
                throw new InvalidRouteException("The router did not return a response.");
            }
            if (!is_a($httpResponse, "\\DblEj\\Communication\\Http\\Response"))
            {
                throw new InvalidResponseException($requestUri, "Expected a \\DblEj\\Communication\\Http\\Response but got a " . get_class($httpResponse) . " instead.");
            }
            $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_AFTER_REQUEST_ROUTED, $routerUsed, $this));

            //check if user is allowed to view the response
            $gateState = $this->_responseGatekeepers->AttemptEntry($httpResponse, Core::$CURRENT_USER);
            if ($gateState->Get_IsRestricted())
            {
                throw new ResourceAccessDeniedException($gateState->Get_RestrictedResourceName(), $gateState->Get_AttemptedActorNames(), E_ERROR, null, null, "Gatekeeper did not allow current user to see response");
            }

            if (is_a($httpResponse, $this->getExpectedResponseClass()))
            {
                $sitePage = $httpResponse->Get_SitePage();
                $this->PreparePageForDisplay($sitePage);
            }
            $this->raiseEvent(new EventInfo(self::EVENT_APPLICATION_BEFORE_RESPONSE_SENT, $httpResponse, $this));
        }
        else
        {
            throw new InvalidRouterRequestException($requestUri, null, "Could not find a route for the specified request ($requestUri).  If the intended destination is an MVC controller, then the controller file is likely missing, named wrong, or has restricted access.");
        }

        if (!$httpResponse->Get_Options())
        {
            $httpResponse->Set_Options(RenderOptions::GetDefaultInstance());
        }
        if (!HttpRouter::FromServerToClient($httpResponse, $this))
        {
            throw new InvalidRouterRequestException
            (
                $requestUri, $routerUsed,
                "The destination action was executed, but it does not appear to have done anything.  "
                . "If the destination is an MVC Controller, please verify that the Controller is a sub-class of ControllerBase and is set up correctly."
            );
        }
        $this->_runStats->SetResponse($httpResponse, $route);

        if ($this->_tracePath && file_exists(dirname($this->_tracePath)))
        {
            file_put_contents($this->_tracePath, serialize($this->_runStats->GetStatsArray()), true);
        }
        $this->onAfterRun($httpResponse, $route);
        $exitCode = 0; //will be used as exit code
        return $httpResponse;
    }

    protected function getExpectedResponseClass()
    {
        return "\\DblEj\\Communication\\Http\\SitePageResponse";
    }

    public function Get_ReplacementTokens()
    {
        $returnArray = parent::Get_ReplacementTokens();

        $protocol        = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)) ? "https" : "http";
        $returnArray["HTTP_PROTOCOL"] = $protocol;
        $returnArray["WEB_ROOT_RELATIVE"] = $this->Get_Settings()->Get_Web()->Get_WebUrlRelative();
        $returnArray["LOCAL_WEB_ROOT"] = realpath($this->_webRootPath);
        $returnArray["WEB_FOLDER"] = $this->Get_Settings()->Get_Paths()->Get_Application()->Get_PublicWeb();

        //main site areas
        $returnArray["SITE_AREAS"] = $this->getSitemap()->GetAllSiteAreas();

        $returnArray["WEB_ROOT"] = $this->_settings->Get_Web()->Get_WebUrl();
        $returnArray["WEB_ROOT_SECURE"] = $this->_settings->Get_Web()->Get_WebUrlSecure();

        $returnArray["SITE_DISPLAY_TITLE"] = $this->_settings->Get_Web()->Get_SiteDisplayTitle();
        $returnArray["SITE_EMAIL"] = $this->_settings->Get_Web()->Get_SiteEmail();

        $returnArray["IMAGE_FOLDER"] = $this->_settings->Get_Web()->Get_WebUrl() . "Resources/Images/";

        $returnArray["SITE_DOMAIN_NAME"] = $this->_settings->Get_Web()->Get_DomainName();
        $returnArray["APPLICATION_SITE_AREA"] = $this->_settings->Get_Application()->Get_SiteArea();

        return $returnArray;
    }

    public function PreparePageForDisplay(SitePage &$sitePage)
    {
        //in some circumstance this was getting called before Initiazlize, causing problems,
        //so now we call it to be safe (kinda lame).
        //it doesnt do anything if its already done it so its not too bad
        $this->Initialize(\Wafl\Core::$CURRENT_USER, true);

        $localRoot            = realpath($this->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot()) . DIRECTORY_SEPARATOR;
        $extensionsFolder     = realpath($this->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder()) . DIRECTORY_SEPARATOR;
        $localAppFolder       = realpath($this->Get_LocalIncludeFolder()) . DIRECTORY_SEPARATOR;
        $cssPath              = realpath($localAppFolder . $this->_settings->Get_Paths()->Get_Application()->Get_Presentation() . $this->_settings->Get_Paths()->Get_Application()->Get_Stylesheets()) . DIRECTORY_SEPARATOR;
        $logicPath            = realpath($this->Get_LocalLogicFolder()) . DIRECTORY_SEPARATOR;
        $templateFilename     = $sitePage->Get_Template()->Get_Filename();
        $clientJsFile         = $sitePage->GetClientLogicFile();
        $checkAutoCssFilePath = $cssPath . dirname($templateFilename) . DIRECTORY_SEPARATOR . basename($templateFilename, ".tpl") . ".css";
        $checkAutoJsFilePath  = $logicPath . $clientJsFile;
        foreach ($this->_skins as $skinName => $skin)
        {
            $sitePage->AddCss(new Stylesheet($skinName, $this->Get_Settings()->Get_Web()->Get_WebUrlRelative() . Core::GLOBAL_STYLESHEET_FILENAME, $this->_settings->Get_Web()->Get_GlobalStylesheetHasTokens()));

            if (file_exists($checkAutoCssFilePath))
            {

                $sitePage->AddCss(new Stylesheet($skinName, $this->Get_Settings()->Get_Web()->Get_WebUrlRelative() . $templateFilename . ".css", $skin->Get_PreProcessCss()));
            }
            else
            {
                //it maybe an extensions css which is in a different folder.  Lets check
                $checkAutoCssFilePath = $extensionsFolder . str_replace("Templates", "Stylesheets", dirname($templateFilename)) . DIRECTORY_SEPARATOR . basename($templateFilename, ".tpl") . ".css";
                if (file_exists($checkAutoCssFilePath))
                {
                    $sitePage->AddCss(new Stylesheet($skinName, $this->Get_Settings()->Get_Web()->Get_WebUrlRelative() . "Extensions/" . str_replace(".tpl", ".css", str_replace("Presentation/Templates/", "", $templateFilename)), $skin->Get_PreProcessCss()));
                }
            }
        }

        if (file_exists($checkAutoJsFilePath))
        {
            $sitePage->AddJavascript($clientJsFile);
        }

        $preRegistsredControls = [];
        //preregistered controls
        foreach (Controls::$_PREREGISTERED_CONTROLS as $controlInfo)
        {
            $controlName = $controlInfo[0];
            $control = Controls::GetControl($controlName, $controlInfo[1], null, null, null, isset($controlInfo[3])?$controlInfo[3]:"0.0.1"); // Get control also creates the controls.. its poorly named maybe need to break into two
            $preRegistsredControls[$controlName] = $control;
        }

        $pagePlusSitewideControls = [];
        foreach ($sitePage->GetControls() as $controlName => $control)
        {
            $pagePlusSitewideControls[get_class($control)] = $control;
        }
        foreach ($preRegistsredControls as $controlName => $control)
        {
            $pagePlusSitewideControls[get_class($control)] = $control;
        }

        $controlsHtml = "";
        foreach ($pagePlusSitewideControls as $controlName => $control)
        {
            $controlInfo = new \ReflectionClass($controlName);
            //for each control type, add that control's css
            $fullyQualifiedControlName = $controlInfo->getName();
            $controlName               = $controlInfo->getShortName();


            $fullyQualifiedControlNameJs = str_replace("\\", ".", $fullyQualifiedControlName);
            $scripts                     = $fullyQualifiedControlName::Get_Javascripts();
            if (count($scripts) > 0)
            {
                //for each control type, append a call to that control types static initializer
                $controlsHtml .= "<script>
					Start
					(
						function()
						{
							if (IsDefined($fullyQualifiedControlNameJs.Initialize))
							{
								$fullyQualifiedControlNameJs.Initialize();
							}
						}
					)
			 </script>";
            }
        }

        //if we're in debug mode, show the debug panel
        if ($this->_settings->Get_Debug()->Get_DebugMode())
        {
            $currentUrl = $_SERVER["REQUEST_URI"];
            if (stristr($currentUrl, "?"))
            {
                $varStart = "&amp;";
            }
            else
            {
                $varStart = "?";
            }
            $controlsHtml .= "<div class=\"DebugPanel\" id=\"WaflDebugPanel\" style=\"top: auto; position: fixed; right: 0px; bottom: 0px; opacity: .3;\">
                                <button type=\"button\" style=\"float: left; border: none; background: none;\" onclick=\"$('WaflDebugPanel').Hide();\"><i id=\"DeleteDebugPanelIcon\" class=\"IconRemove\"></i></button>
                                <button type=\"button\" style=\"float: right; border: none; background: none;\" onclick=\"$('WaflDebugPanel').ToggleCss('opacity',.3,1);$('DebugPanelExpandedPanel').ToggleVertical();$('CollapseDebugPanelIcon').ToggleClass('IconChevronDown').ToggleClass('IconChevronUp');\"><i id=\"CollapseDebugPanelIcon\" class=\"IconChevronDown\"></i></button>
                                <h1>Application Debug Panel</h1>
                                <div id=\"DebugPanelExpandedPanel\" hidden>
                                    <small>This panel is only visible when DebugMode is turned on.</small>
                                    <dl>
                                        <dt>Application<dd>" . $this->_settings->Get_Application()->Get_AppName() . "
                                        <dt>Site Page<dd>" . $sitePage->Get_FullTitle() . "
                                        <dt>Template<dd>" . $sitePage->Get_Template()->Get_Filename() . "
                                    </dl>
                                    <div style=\"text-align: center;\">
                                        <a class=\"Button\" href=\"{$currentUrl}{$varStart}ClearServerCache=1\">Clear Server Cache</a>&nbsp;
                                        <a class=\"Button\" href=\"{$currentUrl}{$varStart}ClearServerCache=2\">Clear Server Cache &amp; Compiles</a>
                                    </div>
                                </div>
						      </div>";
        }

        $userGroupClass = $this->Get_Settings()->Get_Application()->Get_DefaultUserGroupClass();
        $userClass = $this->Get_Settings()->Get_Application()->Get_DefaultUserClass();
        if (is_a($userGroupClass, "\DblEj\Data\IPersistable"))
        {
            $allUserGroups = $userGroupClass::Filter();
            foreach ($allUserGroups as $group)
            {
                UserGroup::RegisterUserGroup($group);
            }
        }
        if (!Core::$CURRENT_USER)
        {
            Core::$CURRENT_USER = $userClass::GetGuestUser();
        }

        foreach ($this->Get_ReplacementTokens() as $tokenKey=>$tokenVal)
        {
            $this->Get_TemplateRenderer()->SetGlobalData($tokenKey, $tokenVal);
        }
        $this->Get_TemplateRenderer()->SetStaticData("CORE", "\Wafl\Core");
        $this->Get_TemplateRenderer()->SetGlobalData("WAFL_ENVIRONMENT", WAFL_ENVIRONMENT);
        $this->Get_TemplateRenderer()->SetGlobalData("SELECTED_AREA", $sitePage->Get_ParentArea($this));
        $this->Get_TemplateRenderer()->SetGlobalData("CURRENT_SITEPAGE", $sitePage);
        $this->Get_TemplateRenderer()->SetGlobalData("PAGE", $sitePage);
        $this->Get_TemplateRenderer()->SetGlobalData("STYLESHEETS", $sitePage->GetStyleSheets());
        $this->Get_TemplateRenderer()->SetGlobalData("ADDITIONAL_RAW_HEAD_HTML", $sitePage->GetAdditionalRawHeadHtml());
        $this->Get_TemplateRenderer()->SetGlobalData("ADDITIONAL_RAW_FOOT_HTML", $sitePage->GetAdditionalRawFooterHtml() . $controlsHtml);
    }

    public function Get_Skins()
    {
        return $this->_skins;
    }



    public function Initialize(\DblEj\Authentication\IUser $user = null, $uiPage = true, $storageEngineType = "\\Wafl\\Extensions\\Storage\\Mysql")
    {
        if (!$this->_amInitialized)
        {
            parent::Initialize($user, $uiPage, $storageEngineType);

            $this->_skins = array();
            foreach ($this->_lookAndFeelSettings["Skins"] as $skinName => $skinInfo)
            {
                $skin                    = Skin::CreateSkinFromArray($skinName, $skinInfo);
                $this->_skins[$skinName] = $skin;
            }
            if (count($this->_skins) == 0)
            {
                $this->_skins[] = new Skin("Default Skin", "DefaultSkin", new StyledFont());
            }
            if (isset($_REQUEST["WAFLSKIN"]))
            {
                $currentSkinTitle   = escapeshellcmd($_REQUEST["WAFLSKIN"]);
                $this->_currentSkin = $this->_skins[$currentSkinTitle];
            }
            else
            {
                $this->_currentSkin = reset($this->_skins);
            }
            $this->registerTemplateFolder
            (
                $this->_localRoot,
                $this->Get_Settings()->Get_Paths()->Get_Application()->Get_Presentation() .
                $this->_currentSkin->Get_BaseFilename() . "/" .
                $this->Get_Settings()->Get_Paths()->Get_Application()->Get_Html(), 500
            );
        }
    }



    private $_lookAndFeelSettings = null;
    protected function onLoadConfig($appConfigPath, $appDefinitions, $appSettings, $moreSettings)
    {
        $advancedFolder = $appConfigPath . "More/";

        $this->_localUploadFolder = $this->_localRoot . $this->Get_Settings()->Get_Paths()->Get_Application()->Get_UploadFolder();

        $lookAndFeelFile = $advancedFolder . "LookAndFeel.syrp";
        if (file_exists($lookAndFeelFile))
        {
            $lookAndFeelSettings = \Wafl\Util\Syrup::ParseSyrupFileAsArray($lookAndFeelFile);
        }
        else
        {
            $lookAndFeelSettings = array();
        }
        $this->_lookAndFeelSettings = $lookAndFeelSettings;
//        $this->_skins = array();
//        foreach ($lookAndFeelSettings["Skins"] as $skinName => $skinInfo)
//        {
//            $skin                    = Skin::CreateSkinFromArray($skinName, $skinInfo);
//            $this->_skins[$skinName] = $skin;
//        }
//        if (count($this->_skins) == 0)
//        {
//            $this->_skins[] = new Skin("Default Skin", "DefaultSkin", new StyledFont());
//        }
//        if (isset($_REQUEST["WAFLSKIN"]))
//        {
//            $currentSkinTitle   = escapeshellcmd($_REQUEST["WAFLSKIN"]);
//            $this->_currentSkin = $this->_skins[$currentSkinTitle];
//        }
//        else
//        {
//            $this->_currentSkin = reset($this->_skins);
//        }
//
//        $this->registerTemplateFolder
//        (
//            $this->_localRoot,
//            $this->Get_Settings()->Get_Paths()->Get_Application()->Get_Presentation() .
//            $this->_currentSkin->Get_BaseFilename() . "/" .
//            $this->Get_Settings()->Get_Paths()->Get_Application()->Get_Html(), 500
//        );
        $this->_webRootPath = $this->_localRoot . $this->_settings->Get_Paths()->Get_Application()->Get_PublicWeb();

        parent::onLoadConfig($appConfigPath, $appDefinitions, $appSettings, $moreSettings);
    }

    public function GetWebRootPath()
    {
        return $this->_webRootPath;
    }

    public function IsActorAllowedAccessToSiteArea(IActor $actor, SiteArea $siteArea, $permissions = ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $resource = $this->GetRestrictedResource($siteArea->Get_AreaId(), Resource::RESOURCE_TYPE_SITEAREA);
        return $resource ? $this->IsAllowed($resource, $actor, $permissions) : $this->_settings->Get_Application()->Get_DefaultSiteAreaPermission();
    }

    public function IsActorAllowedAccessToSitePage(SitePage $sitePage, IActor $actor = null, $permissions = ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $resource = $this->GetRestrictedResource($sitePage->Get_PageId(), Resource::RESOURCE_TYPE_SITEPAGE);
        return $resource ? ($actor ? $this->IsAllowed($resource, $actor, $permissions) : $this->_settings->Get_Application()->Get_DefaultSitePagePermission()) : $this->_settings->Get_Application()->Get_DefaultSitePagePermission();
    }
    public function IsActorAllowedAccessToApiCall($apiCall, \DblEj\Resources\IActor $actor = null, $permissions = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $resource = $this->GetRestrictedResource($apiCall, Resource::RESOURCE_TYPE_APICALL);
        return $resource ? ($actor ? $this->IsAllowed($resource, $actor, $permissions) : $this->_settings->Get_Application()->Get_DefaultApiCallPermission()) : $this->_settings->Get_Application()->Get_DefaultApiCallPermission();
    }
    public function Get_SiteMap()
    {
        return $this->getSitemap();
    }

    public function Get_AllPages($onlyVisible = false)
    {
        if ($onlyVisible)
        {
            $returnArray = array();
            foreach ($this->getSitemap()->GetAllSitePages() as $pageKey => &$page)
            {
                if ($page->Get_DisplayOrder() > 0)
                {
                    $returnArray[$pageKey] = &$page;
                }
            }
        }
        else
        {
            $returnArray = &$this->_allPages;
        }
        return $returnArray;
    }

    public function GetRegisteredSitePageByUri($requestPath, SiteArea $currentArea = null)
    {
        $filename = $this->getRequestedFilenameFromUri($requestPath);

        if (!$filename)
        {
            throw new \Exception("There is not SitePage registered for $requestPath");
        }
        if (substr($filename, strlen($filename) - 1) == "/")
        {
            $filename .= $this->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage();
        }
        $sitePage = $this->GetSitePageByFilename($filename, $currentArea, false);
        return $sitePage;
    }

    public function GetRegisteredSitePageByRequest(HttpRequest $request, SiteArea $currentArea = null)
    {
        $requestPath = $request->GetInput("REQUEST_URI", INPUT_SERVER);
        if (!$requestPath)
        {
            $requestPath = $request->GetInput("REDIRECT_URL", INPUT_SERVER);
        }
        return $this->GetRegisteredSitePageByUri($requestPath, $currentArea);
    }

    public function GetSitePageByFilename($logicFile, SiteArea $parentArea = null, $autoRegisterifNonExistent = true)
    {
        $returnPage = null;
        $webRootLen = strlen($this->Get_Settings()->Get_Web()->Get_WebUrlRelative());

        if (($logicFile != $this->Get_Settings()->Get_Web()->Get_WebUrlRelative()) && substr($logicFile, 0, $webRootLen) == $this->Get_Settings()->Get_Web()->Get_WebUrlRelative())
        {
            $logicFile = substr($logicFile, $webRootLen);
        }
        $logicFile = str_replace("\\", "/", $logicFile);
        if ($parentArea)
        {
            foreach ($this->getSitemap()->GetSitePages($parentArea) as $page)
            {
                $pagePath = str_replace("\\", "/", $page->Get_LogicPath());
                if (($pagePath == $logicFile) || ($pagePath == "$logicFile.php"))
                {
                    $returnPage = $page;
                    break;
                }
            }
        }
        else
        {
            foreach ($this->getSitemap()->GetAllSitePages() as $page)
            {
                $pagePath = str_replace("\\", "/", $page->Get_LogicPath());
                if (($pagePath == $logicFile) || ($pagePath == "$logicFile.php") || (($pagePath . ".php") == $logicFile))
                {
                    $returnPage = $page;
                    break;
                }
            }
        }
        if ($returnPage == null)
        {
            if ($parentArea)
            {
                $allPages = $this->getSitemap()->GetSitePages($parentArea);
            }
            else
            {
                $allPages = $this->getSitemap()->GetAllSitePages();
            }

            if ($parentArea)
            {
                foreach ($allPages as $page)
                {
                    if ($page->Get_IsAreaDefault())
                    {
                        $testHandler = str_replace("\\", "/", $page->Get_LogicPath());
                        $testHandlerDir = str_replace(".", "", dirname($testHandler));
                        if (($testHandlerDir . "/") == $logicFile)
                        {
                            $returnPage = $page;
                            break;
                        }
                    }
                }
            }
        }
        if (($returnPage == null) && $autoRegisterifNonExistent)
        {
            $pageBase = str_replace(".php", "", $logicFile);
            $pageId = str_replace(DIRECTORY_SEPARATOR, ".", $pageBase);
            if (\DblEj\Util\Strings::StartsWith($pageBase, "Extensions\\") || \DblEj\Util\Strings::StartsWith($pageBase, "Extensions/"))
            {
                //we dont want smarty scanning the whole wafl folder
                //so we have the extensions folder set to template folder instead
                //what this means though is that we need to remove the "extensions" beginning of the template name
                //and add the extension's presentation template folder
                $pagePath = substr(dirname($pageBase), 11);
                $pageName = basename($pageBase);
                $templatePath = $pagePath . DIRECTORY_SEPARATOR . "Presentation/Templates/$pageName.tpl";
                $returnPage = $this->RegisterSitePage($pageId, $pageBase, $templatePath, 0, null, false, "", "", "", false, "Extension logic request not using sitestructure config: $logicFile");
            }
            else
            {
                $returnPage = $this->RegisterSitePage($pageId, $pageBase, $pageBase, 0, null, false, "", "", "", false, "Logic request not using sitestructure config: $logicFile");
            }
        }
        return $returnPage;
    }

    public function GetSitePage($uniqueId)
    {
        return $this->getSitemap()->GetSitePage($uniqueId);
    }

    //Keep sitemaps in a static so they dont litter our instance debug dumps
    private static $_applicationSiteMaps = [];

    protected static function addSitemap($sitemap)
    {
        $sitemapIndex = count(self::$_applicationSiteMaps);
        self::$_applicationSiteMaps[$sitemapIndex] = $sitemap;
        return $sitemapIndex;
    }

    protected function getSitemap()
    {
        return self::$_applicationSiteMaps[$this->_siteMapIndex];
    }

    public function GetSitePagesInArea($areaUniqueId, $includeGrandChildren = false)
    {
        $area = $this->GetSiteArea($areaUniqueId);
        $sitePages = [];
        if ($area)
        {
            $sitePages = $this->getSitemap()->GetSitePages($area);
            if ($includeGrandChildren)
            {
                $subAreas = $this->getSitemap()->GetSiteAreas($area);
                foreach ($subAreas as $subArea)
                {
                    $sitePages = array_merge($sitePages, $this->GetSitePagesInArea($subArea->Get_AreaId(), true));
                }
            }
        }
        return $sitePages;
    }

    public function GetSiteArea($uniqueId)
    {
        return $this->getSitemap()->GetSiteArea($uniqueId);
    }
    /**
     * Returns true if a Site Page with the specified criteria has been pre-registered with the application.
     *
     * @param string $logicFile The name of the server-side request handler for this page.
     * @param SiteArea $parentArea The Site Area to look for the page in.
     */
    public function DoesSitePageExist($logicFile, SiteArea $parentArea = null)
    {
        $doesExist  = false;
        $webRootLen = strlen($this->Get_Settings()->Get_Web()->Get_WebUrlRelative());
        if (($logicFile != $this->Get_Settings()->Get_Web()->Get_WebUrlRelative()) && substr($logicFile, 0, $webRootLen) == $this->Get_Settings()->Get_Web()->Get_WebUrlRelative())
        {
            $logicFile = substr($logicFile, $webRootLen);
        }
        if ($parentArea)
        {
            foreach ($parentArea->GetChildPages() as $page)
            {
                if (($page->Get_LogicPath() == $logicFile) || ($page->Get_LogicPath() == "$logicFile.php"))
                {
                    $doesExist = true;
                    break;
                }
            }
        }
        else
        {
            foreach ($this->_allPages as $page)
            {
                if (($page->Get_LogicPath() == $logicFile) || ($page->Get_LogicPath() == "$logicFile.php"))
                {
                    $doesExist = true;
                    break;
                }
            }
        }
        return $doesExist;
    }

    /**
     * Get a SitePage that should be used for the given request
     *
     * @param Request $request
     * @param SiteArea $currentArea
     *
     * @return SitePage Get the registered SitePage, or create and register a new SitePage, that should be used for the given request
     */
    public function GetSitePageByRequest(HttpRequest $request, SiteArea $currentArea = null)
    {
        $requestPath = $request->Get_RequestUrl();
        return $this->GetSitePageByUri($requestPath, $currentArea);
    }

    /**
     * Get a SitePage that should be used for the given request path
     *
     * @param string $requestPath
     * @param SiteArea $currentArea
     *
     * @return SitePage Get the registered SitePage, or create and register a new SitePage, that should be used for the given request path
     */
    public function GetSitePageByUri($requestPath, SiteArea $currentArea = null)
    {
        $sitePage = null;
        $filename = $this->getRequestedFilenameFromUri($requestPath);
        if ($filename)
        {
            $sitePage = $this->GetSitePageByFilename($filename, $currentArea, true);
        }
        return $sitePage;
    }

    /**
     *
     * @param string $title
     * @param string $handlerFile
     * @param string $templateName
     * @param int $menuDisplayOrder
     * @param SiteArea $parentArea
     * @param boolean $isAreaDefault
     * @param string $description
     * @param string $fullTitle
     * @param string $keywords
     * @param boolean $preprocessCss
     *
     * @return SitePage
     */
    public function RegisterSitePage($title, $handlerFile, $templateName, $menuDisplayOrder = 0, SiteArea $parentArea = null, $isAreaDefault = false, $description = "", $fullTitle = "", $keywords = "", $preprocessCss = false, $registeredBy = "unknown")
    {
        $uniqueId = $title;
        if (!$this->GetSitePage($uniqueId))
        {
            if (!$this->Get_TemplateRenderer() || $this->Get_TemplateRenderer()->DoesTemplateExist($templateName))
            {
                $newPage = new SitePage($uniqueId, $title, $description, $handlerFile, $templateName, $menuDisplayOrder, $parentArea ? $parentArea->Get_AreaId() : null, $isAreaDefault, $fullTitle, $keywords, $preprocessCss, [], $registeredBy);
                $this->getSitemap()->AddSitePage($newPage);

                if ($parentArea)
                {
                    $this->getSitemap()->AddSiteArea($parentArea);
                    if ($isAreaDefault)
                    {
                        $parentArea->AddDefaultPage($newPage);
                    }
                }
            } else {
                $newPage = null;
            }
            return $newPage;
        }
        else
        {
            return $this->GetSitePage($uniqueId);
        }
    }

    private function getRequestedFilenameFromUri($requestPath)
    {
        $webRootRelativeLength = strlen($this->Get_Settings()->Get_Web()->Get_WebUrlRelative());
        $filename              = null;
        if (stristr($requestPath, "?"))
        {
            $requestPath = substr($requestPath, 0, stripos($requestPath, "?"));
        }

        if ($requestPath == $this->Get_Settings()->Get_Web()->Get_WebUrlRelative())
        {
            $filename = $this->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage();
        }
        elseif (substr($requestPath, 0, $webRootRelativeLength) == $this->Get_Settings()->Get_Web()->Get_WebUrlRelative())
        {
            $filename = substr($requestPath, $webRootRelativeLength);
            if (!$filename)
            {
                $filename = $this->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage();
            }
        }
        if (substr($filename, strlen($filename) - 1) == "/")
        {
            $filename .= $this->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage();
        }
        return $filename;
    }
    public static function Get_SettingsClassName()
    {
        return "\\Wafl\\Application\\Settings\\AllWeb";
    }
}