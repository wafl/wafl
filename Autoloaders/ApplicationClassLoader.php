<?php
namespace Wafl\Autoloaders;

use DblEj\Application\IApplication,
    Wafl\Core;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "Autoloader.php");
class ApplicationClassLoader
extends Autoloader
{
    /**
     *
     * @var IApplication
     */
    private $_application;

    public function __construct(IApplication $application)
    {
        parent::__construct();
        $this->_application = $application;
    }

    protected function onAutoload($classname)
    {
        $apiHandlerFolder = "Api"; //@todo this shouldnt be hardcoded
        $classfile        = str_replace("\\", "/", $classname);
        if (substr($classfile, 0, 1) == "/")
        {
            $classfile = substr($classfile, 1);
        }
        $rootNamespace         = $this->_application->Get_Settings()->Get_Application()->Get_RootNameSpace();
        $localRoot             = $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
        $appFolder             = $rootNamespace . DIRECTORY_SEPARATOR;
        $localAppFolder        = $localRoot . $appFolder;
        $serverFuncModelFolder = $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_FunctionalModelsServer();
        if (substr($classfile, 0, strlen($rootNamespace)) == $rootNamespace)
        {
            $modelsPath = $appFolder . Core::MODELS_FOLDER;
            if (substr($serverFuncModelFolder, strlen($serverFuncModelFolder) - 1, 1) == "/")
            {
                $functionalModelFolder = substr($serverFuncModelFolder, 0, strlen($serverFuncModelFolder) - 1);
            }
            else
            {
                $functionalModelFolder = $serverFuncModelFolder;
            }
            $classfile                 = substr($classfile, strlen($rootNamespace) + 1);
            $functionalModelFolderBase = basename($functionalModelFolder);
            if (substr($classfile, 0, 9) == "DataModel")
            {
                $classfile = $modelsPath . $classfile;
            }
            elseif ($functionalModelFolderBase && (substr($classfile, 0, strlen($functionalModelFolderBase)) == $functionalModelFolderBase))
            {
                $classfile = $modelsPath . $classfile;
            }
            elseif (substr($classfile, 0, strlen($apiHandlerFolder)) == $apiHandlerFolder)
            {
                $classfile = $appFolder . $classfile;
            }
            if (substr($classfile, 0, 1) == "/")
            {
                $classfile = substr($classfile, 1);
            }
            $classpath = $localRoot . "$classfile.php";
        }
        else
        {
            $classpath = $localRoot . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "$classfile.php";
        }
        $triedPaths[] = $classpath;
        if (!@file_exists($classpath))
        {
            $classpath    = $localAppFolder . "$classfile.php";
            $triedPaths[] = $classpath;
        }
        if (!@file_exists($classpath))
        {
            $classpath    = $localRoot . "$classfile.php";
            $triedPaths[] = $classpath;
        }
        if (is_a($this->_application, "\\Wafl\\Application\\WebApplication"))
        {
            if (!@file_exists($classpath))
            {
                $classpath    = $this->_application->GetWebRootPath() . "$classfile.php";
                $triedPaths[] = $classpath;
            }
            $triedPaths[] = $classpath;
        }

        if (@file_exists($classpath))
        {
            $isPhpFile = \DblEj\Util\PhpFile::DoesFileHaveOpeningPhpTag($classpath);
            if ($isPhpFile)
            {
                if (\DblEj\Util\PhpFile::DoesFileHavePhpClass($classpath, 30, true, true, true))
                {
                    require_once($classpath);
                    return true;
                }
            }
        }
        return false;
    }
}