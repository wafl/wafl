<?php
use Wafl\Autoloaders\ApplicationClassLoader;
use Wafl\Autoloaders\ClassLoader;
use Wafl\Autoloaders\ComponentLoader;
use Wafl\Core;

if (!class_exists("\\Wafl\\Autoloaders\\ClassLoader", false))
{
    require_once(__DIR__ . DIRECTORY_SEPARATOR . "ClassLoader.php");
    new ClassLoader;
}

if ((!class_exists("\\Wafl\\Autoloaders\\ApplicationClassLoader", false) || !ApplicationClassLoader::$InstanceCreated) && Core::$RUNNING_APPLICATION)
{
    require_once(__DIR__ . DIRECTORY_SEPARATOR . "ApplicationClassLoader.php");
    new ApplicationClassLoader(Core::$RUNNING_APPLICATION);
}

if (!class_exists("\\Wafl\\Autoloaders\\ComponentLoader", false) || !ComponentLoader::$InstanceCreated)
{
    require_once(__DIR__ . DIRECTORY_SEPARATOR . "ComponentLoader.php");
    new ComponentLoader;
}