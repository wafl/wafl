<?php
namespace Wafl\Autoloaders;

abstract class Autoloader
{
    public static $InstanceCreated = false;

    public function __construct()
    {
        $subclass = get_called_class();
        $subclass::$InstanceCreated = true;

        spl_autoload_register(array(
            $this,
            "_autoload"));
    }

    private function _autoload($classname)
    {
        if (!class_exists($classname, false))
        {
            return $this->onAutoload($classname);
        }
        return false;
    }

    abstract protected function onAutoload($classname);
}