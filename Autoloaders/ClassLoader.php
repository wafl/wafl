<?php
namespace Wafl\Autoloaders;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "Autoloader.php"); //the base class
class ClassLoader
extends Autoloader
{
    protected function onAutoload($classname)
    {
        $triedPaths   = array();
        $classpath    = "";
        $triedPaths[] = $classname;
        $classfile    = str_replace("\\", "/", $classname);

        if (substr($classfile, 0, 1) == "/")
        {
            $classfile = substr($classfile, 1);
        }
        if (substr($classfile, 0, 5) == "DblEj")
        {
            $classfile = substr($classfile, 5);
            $classpath = DBLEJ_PATH . "$classfile.php";
        }
        elseif (substr($classfile, 0, 4) == "Wafl")
        {
            $classfile = substr($classfile, 4);
            $classpath = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "$classfile.php";
        }

        $triedPaths[] = $classpath;

        if ($classpath && @file_exists($classpath))
        {
            require_once($classpath);
            return true;
        }
        return false;
    }
}