<?php
namespace Wafl\Autoloaders;

use Wafl\Core;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "Autoloader.php");
class ComponentLoader
extends Autoloader
{
    protected function onAutoload($classname)
    {
        $triedPaths   = array();
        $classpath    = "";
        $triedPaths[] = $classname;
        $classfile    = str_replace("\\", "/", $classname);
        if (substr($classfile, 0, 1) == "/")
        {
            $classfile = substr($classfile, 1);
        }

        if (\DblEj\Util\Strings::StartsWith($classfile, "Wafl/Extensions"))
        {
            $extensionsFolder = Core::$RUNNING_APPLICATION->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder();
            $extensionPath = substr($classfile, 16);
            $classpath    = realpath($extensionsFolder."$extensionPath.php");
        }

        if (\DblEj\Util\Strings::StartsWith($classfile, "Wafl/Controls"))
        {
            $controlsFolder = Core::$RUNNING_APPLICATION->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ControlsFolder();
            $controlPath = substr($classfile, 14);
            $classpath    = realpath($controlsFolder."$controlPath.php");
        }

        if ($classpath && @file_exists($classpath))
        {
            $isPhpFile = \DblEj\Util\PhpFile::DoesFileHaveOpeningPhpTag($classpath);
            if ($isPhpFile)
            {
                $hasPhpClass = \DblEj\Util\PhpFile::DoesFileHavePhpClass($classpath, 30, true, true);
                if ($hasPhpClass)
                {
                    require_once($classpath);
                    return true;
                }
            }
        }
        return false;
    }
}