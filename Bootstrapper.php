<?php

namespace Wafl;

use DblEj\Application\IApplication,
    DblEj\Communication\Http\Request,
    DblEj\Communication\Http\Util;

class Bootstrapper
{
    private static $_bootstrapped = false;
    public static function BootstrapEnvironment($debugMode = true, $app = null)
    {
        if (!self::$_bootstrapped)
        {
            if (!defined("AM_WEBPAGE"))
            {
                if (Util::GetInput("HTTP_USER_AGENT", Request::INPUT_SERVER))
                {
                    define('AM_WEBPAGE', true);
                }
                elseif (php_sapi_name() == "cli")
                {
                    define('AM_WEBPAGE', false);
                }
                elseif (php_sapi_name() == "cgi-fcgi")
                {
                    if (Util::GetInput("HTTP_HOST", Request::INPUT_SERVER))
                    {
                        define('AM_WEBPAGE', true);
                    }
                    elseif (Util::GetInput("REQUEST_SCHEME", Request::INPUT_SERVER))
                    {
                        define('AM_WEBPAGE', true);
                    }
                    elseif (Util::GetInput("REMOTE_ADDR", Request::INPUT_SERVER))
                    {
                        define('AM_WEBPAGE', true);
                    }
                    else
                    {
                        define('AM_WEBPAGE', false);
                    }
                }
                else
                {
                    define('AM_WEBPAGE', true);
                }
            }
            if (AM_WEBPAGE)
            {
                require_once(__DIR__ . "/SystemCallbacks/Errors_http.php");
                require_once(__DIR__ . "/SystemCallbacks/Exceptions_http.php");
                require_once(__DIR__ . "/SystemCallbacks/Shutdown_http.php");
            }
            else
            {
                require_once(__DIR__ . "/SystemCallbacks/Errors.php");
                require_once(__DIR__ . "/SystemCallbacks/Exceptions.php");
                //require_once(__DIR__ . "/SystemCallbacks/Shutdown.php");  dont detect shutdown errors on command line apps
            }
            error_reporting(E_ALL);
            ini_set("display_errors", 0);
            ini_set("display_startup_errors", 0);
            ini_set("html_errors", 0);

            if ($debugMode)
            {
                set_error_handler("__debugActiveWaflErrorHandler", E_ALL);
                set_exception_handler("__activeWaflExceptionHandler2");
                if (AM_WEBPAGE)
                {
                    register_shutdown_function("__debugActiveWaflShutdownHandler");
                }
            }
            else
            {
                set_error_handler("__activeWaflErrorHandler");
                set_exception_handler("__activeWaflExceptionHandler2");
            }
            define("WAFL_ERROR_HANDLERS_LOADED", true);
            Core::InitializeFramework($app);
        }
    }

    public static function BootstrapApplication(IApplication $app)
    {
        $app->Initialize(Core::$CURRENT_USER, AM_WEBPAGE);
    }
}