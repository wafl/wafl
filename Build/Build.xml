<?xml version="1.0" encoding="UTF-8"?>
<project
    name="ActiveWAFL Framework"
    default="Build"
    xmlns:if="ant:if"
    xmlns:unless="ant:unless"
>
    <taskdef resource="net/sf/antcontrib/antlib.xml"/>
    <property environment="env"/>
    <echo message="Getting the repo revision number for ActiveWAFL"/>
    <exec dir="${env.WORKSPACE}/" executable="hg" outputproperty="ActiveWaflRepoSummary">
        <arg line="summary"/>
    </exec>
    <echo>Got ActiveWAFL revision summary ${ActiveWaflRepoSummary}</echo>
    <propertyregex property="ActiveWaflRevision"
                   input="${ActiveWaflRepoSummary}"
                   regexp="parent: ([0-9]*):([a-z0-9]*) tip"
                   select="\1"
                   defaultValue="UnknownVersion"
                   casesensitive="false" />
    <echo>Got ActiveWAFL revision ${ActiveWaflRevision}</echo>
    <echo message="Jenkins workspace: ${env.WORKSPACE}"/>

    <target name="Build"
            depends="Clean,CheckPhpSyntax,MeasureProjectSize,CalculatePhpCodeQuality,DetectPhpMesses,ValidatePhpCodingStandard,DetectDuplicatePhpCode,RunUnitTests,AggregateBuildReports,CopyDist">
    </target>
    <target name="QuickBuild"
            depends="Clean,CopyDist">
    </target>
    <target name="Clean">
        <delete dir="${env.WORKSPACE}/Private" />
        <delete dir="${env.WORKSPACE}/Output" />
        <mkdir dir="${env.WORKSPACE}/Output" />
        <mkdir dir="${env.WORKSPACE}/Output/Dist" />
    </target>
    <target name="CheckPhpSyntax" description="Perform syntax check of sourcecode files">
        <echo>Checking syntax with php -l...</echo>
        <apply executable="php" failonerror="true">
            <arg value="-l" />
            <fileset dir="${env.WORKSPACE}/">
                <include name="**/*.php" />
                <modified />
            </fileset>
        </apply>
    </target>

    <target name="MeasureProjectSize" description="Measure project size using phploc">
        <echo>Running phploc to get project size...</echo>
        <exec executable="phploc">
            <arg value="--log-csv" />
            <arg value="${env.WORKSPACE}/Output/ProjectSizeReport.csv" />
            <arg path="${env.WORKSPACE}/" />
        </exec>
    </target>

    <target name="CalculatePhpCodeQuality" description="Calculate software metrics using PHP_Depend">
        <exec executable="pdepend">
            <arg value="--jdepend-xml=${env.WORKSPACE}/Output/AppMetrics.xml" />
            <arg value="--jdepend-chart=${env.WORKSPACE}/Output/AppMetricsChart.svg" />
            <arg value="--overview-pyramid=${env.WORKSPACE}/Output/OverviewPyramidChart.svg" />
            <arg path="${env.WORKSPACE}" />
        </exec>
    </target>
    <target name="DetectPhpMesses" description="Perform project mess detection using phpmd creating a log file for the continuous integration server">
        <echo>PHPMD is excluding using this argument: --exclude ${env.WORKSPACE}/CodingStandards/,${env.WORKSPACE}/Extensions/,${env.WORKSPACE}/Private/,${env.WORKSPACE}/Tests/,${env.WORKSPACE}/Cli/Utensils.php,${env.WORKSPACE}/Util/Counterpart.php</echo>
        <exec executable="phpmd">
            <arg path="${env.WORKSPACE}" />
            <arg value="xml" />
            <arg path="${env.WORKSPACE}/CodingStandards/WaflMd/ruleset.xml" />
            <arg value="--exclude" />
            <arg value="${env.WORKSPACE}/CodingStandards/,${env.WORKSPACE}/Extensions/,${env.WORKSPACE}/Controls/,${env.WORKSPACE}/SharedSdks/,${env.WORKSPACE}/Private/,${env.WORKSPACE}/Tests/,${env.WORKSPACE}/Cli/Utensils.php,${env.WORKSPACE}/Util/Counterpart.php" />
            <arg value="--reportfile" />
            <arg path="${env.WORKSPACE}/Output/MessDetectionLog.xml" />
        </exec>
    </target>
    <target name="ValidatePhpCodingStandard" description="Find coding standard violations using PHP_CodeSniffer creating a log file for the continuous integration server">
        <exec executable="phpcs">
            <arg value="--ignore=*.js,*.css,.hg*,*.syrp,*Test.php,*/Private/*,*/Extensions/*,*/Controls/*,*/SharedSdks/*,*/CodingStandards/*,Utensils.php,Counterpart.php" />
            <arg prefix="--report-checkstyle=" file="${env.WORKSPACE}/Output/CodeStandardViolations.xml" />
            <arg value="--standard=Wafl" />
            <arg path="${env.WORKSPACE}" />
        </exec>
    </target>

    <target name="DetectDuplicatePhpCode" description="Find duplicate code using phpcpd">
        <exec executable="phpcpd">
            <arg value="-n" />
            <arg value="--names-exclude=${env.WORKSPACE}/Cli/Utensils.php,${env.WORKSPACE}/Util/Counterpart.php,${env.WORKSPACE}/Resources/AppTemplates.phar" />
            <arg value="--exclude=CodingStandards" />
            <arg value="--exclude=Extensions" />
            <arg value="--exclude=Private" />
            <arg value="--exclude=Tests" />
            <arg value="--exclude=Controls" />
            <arg value="--exclude=SharedSdks" />
            <arg prefix="--log-pmd=" file="${env.WORKSPACE}/Output/DuplicateCodeReport.xml" />
            <arg path="${env.WORKSPACE}" />
        </exec>
    </target>

    <target name="RunUnitTests" description="Run unit tests with PHPUnit">
        <exec executable="phpunit" dir="${env.WORKSPACE}/Tests">
            <arg value="-c" />
            <arg path="${basedir}/PhpUnit.xml" />
        </exec>
        <echo message="done" />
    </target>

    <target name="AggregateBuildReports" description="Aggregate tool output with PHP_CodeBrowser">
        <exec executable="phpcb">
            <arg value="--log" />
            <arg path="${env.WORKSPACE}/Output" />
            <arg value="--source" />
            <arg path="${env.WORKSPACE}" />
            <arg value="--output" />
            <arg path="${env.WORKSPACE}/Output/CodeBrowser" />
        </exec>
    </target>
    <target name="CopyDist">


        <tstamp/>
        <loadfile srcfile="${basedir}/LicenseHeader.txt" property="LicenseHeader">
            <filterchain>
                <replacetokens begintoken="{" endtoken="}">
                    <token key="MAJOR" value="0"/>
                    <token key="MINOR" value="4"/>
                    <token key="REVISION" value="${ActiveWaflRevision}"/>
                    <token key="BUILD" value="${env.BUILD_NUMBER}"/>
                    <token key="DATE" value="${TODAY}"/>
                </replacetokens>
            </filterchain>
        </loadfile>

        <copy todir="${env.WORKSPACE}/Output/Dist">
            <fileset dir="${env.WORKSPACE}">
                <include name="**/*.js"/>
                <exclude name="**/Tests/"/>
                <exclude name="**/PackageDocs/"/>
                <exclude name="**/Build/"/>
                <exclude name="**/Output/"/>
            </fileset>
            <filterchain>
                <concatfilter prepend="${env.WORKSPACE}/Build/LicenseHeader.txt"/>
                <replacetokens begintoken="{" endtoken="}">
                    <token key="MAJOR" value="0"/>
                    <token key="MINOR" value="4"/>
                    <token key="REVISION" value="${ActiveWaflRevision}"/>
                    <token key="BUILD" value="${env.BUILD_NUMBER}"/>
                    <token key="DATE" value="${TODAY}"/>
                </replacetokens>
            </filterchain>
        </copy>

        <copy todir="${env.WORKSPACE}/Output/Dist">
            <fileset dir="${env.WORKSPACE}">
                <include name="**/*.php"/>
                <exclude name="**/Tests/"/>
                <exclude name="**/PackageDocs/"/>
                <exclude name="**/Build/"/>
                <exclude name="**/Output/"/>
                <exclude name="**/*.js"/>
            </fileset>
            <filterchain>
                <tokenfilter>
                    <filetokenizer/>
                    <replaceregex pattern="([^\s]&lt;?php)"
                                  flags="s"
                                  replace="\1${line.separator}${line.separator}${LicenseHeader}"/>
                </tokenfilter>
            </filterchain>
        </copy>

        <copy todir="${env.WORKSPACE}/Output/Dist">
            <fileset dir="${env.WORKSPACE}">
                <exclude name="**/*.php"/>
                <exclude name="**/*.js"/>
                <exclude name="**/*.wafl"/>
                <exclude name="**/Tests/"/>
                <exclude name="**/PackageDocs/"/>
                <exclude name="**/Output/"/>
                <exclude name="**/Build/"/>
            </fileset>
        </copy>

        <copy todir="${env.WORKSPACE}/Output/Dist">
            <fileset dir="${env.WORKSPACE}">
                <include name="**/*.wafl/"/>
            </fileset>
            <filterchain>
                <replacetokens begintoken="{" endtoken="}">
                    <token key="MAJOR" value="0"/>
                    <token key="MINOR" value="4"/>
                    <token key="REVISION" value="${ActiveWaflRevision}"/>
                    <token key="BUILD" value="${env.BUILD_NUMBER}"/>
                    <token key="DATE" value="${TODAY}"/>
                </replacetokens>
            </filterchain>
        </copy>

    </target>
    <target name="GenerateStubApps">
        <delete dir="/usr/share/WaflAppTemplates/App" />
        <delete dir="/usr/share/WaflAppTemplates/WebApp" />
        <delete dir="/usr/share/WaflAppTemplates/MvcWebApp" />
        <delete dir="/usr/share/WaflAppTemplates/ModularMvcWebApp" />
        <mkdir dir="/usr/share/WaflAppTemplates" />
        <mkdir dir="/usr/share/WaflAppTemplates/App" />
        <mkdir dir="/usr/share/WaflAppTemplates/WebApp" />
        <mkdir dir="/usr/share/WaflAppTemplates/MvcWebApp" />
        <mkdir dir="/usr/share/WaflAppTemplates/ModularMvcWebApp" />

        <exec executable="php" >
            <arg path="/usr/share/Wafl/Bin/Utensils" />
            <arg value="-a" />
            <arg value="d" />
            <arg value="App" />
            <arg value="null" />
            <arg value="/usr/share/WaflAppTemplates/App" />
        </exec>
        <exec executable="php">
            <arg path="/usr/share/Wafl/Bin/Utensils" />
            <arg value="-a" />
            <arg value="d" />
            <arg value="WebApp" />
            <arg value="null" />
            <arg value="/usr/share/WaflAppTemplates/WebApp" />
        </exec>
        <exec executable="php">
            <arg path="/usr/share/Wafl/Bin/Utensils" />
            <arg value="-a" />
            <arg value="d" />
            <arg value="MvcWebApp" />
            <arg value="null" />
            <arg value="/usr/share/WaflAppTemplates/MvcWebApp" />
        </exec>
        <exec executable="php">
            <arg path="/usr/share/Wafl/Bin/Utensils" />
            <arg value="-a" />
            <arg value="d" />
            <arg value="ModularMvcWebApp" />
            <arg value="null" />
            <arg value="/usr/share/WaflAppTemplates/ModularMvcWebApp" />
        </exec>
    </target>
    <target name="ZipDist">
        <zip destfile="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}.zip">
            <fileset dir="${env.WORKSPACE}/Output/Dist" />
        </zip>
        <zip destfile="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj.zip">
            <zipfileset dir="${env.WORKSPACE}/Output/Dist" prefix="Wafl" />
            <zipfileset dir="${env.WORKSPACE}/../../DblEj/workspace/Output/Dist" prefix="DblEj" />
        </zip>
        <zip destfile="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+ModMvcWebApp.zip">
            <zipfileset dir="${env.WORKSPACE}/Output/Dist" prefix="Wafl" />
            <zipfileset dir="${env.WORKSPACE}/../../DblEj/workspace/Output/Dist" prefix="DblEj" />
            <zipfileset dir="/usr/share/WaflAppTemplates/ModularMvcWebApp" prefix="ModularMvcWebApp" />
        </zip>
        <zip destfile="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+MvcWebApp.zip">
            <zipfileset dir="${env.WORKSPACE}/Output/Dist" prefix="Wafl" />
            <zipfileset dir="${env.WORKSPACE}/../../DblEj/workspace/Output/Dist" prefix="DblEj" />
            <zipfileset dir="/usr/share/WaflAppTemplates/MvcWebApp" prefix="MvcWebApp" />
        </zip>
        <zip destfile="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+WebApp.zip">
            <zipfileset dir="${env.WORKSPACE}/Output/Dist" prefix="Wafl" />
            <zipfileset dir="${env.WORKSPACE}/../../DblEj/workspace/Output/Dist" prefix="DblEj" />
            <zipfileset dir="/usr/share/WaflAppTemplates/WebApp" prefix="WebApp" />
        </zip>
        <zip destfile="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+App.zip">
            <zipfileset dir="${env.WORKSPACE}/Output/Dist" prefix="Wafl" />
            <zipfileset dir="${env.WORKSPACE}/../../DblEj/workspace/Output/Dist" prefix="DblEj" />
            <zipfileset dir="/usr/share/WaflAppTemplates/App" prefix="App" />
        </zip>
    </target>
    <target name="PushDev" description="Push the current version as a dev release.">
        <copy tofile="/var/www/vhosts/activewafl.com/Public/Resources/Releases/ActiveWAFL-DEV-0.4.${ActiveWaflRevision}.zip" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}.zip" />
        <copy tofile="/var/www/vhosts/activewafl.com/Public/Resources/Releases/ActiveWAFL-DEV-0.4.${ActiveWaflRevision}+DblEj.zip" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj.zip" />
        <copy tofile="/var/www/vhosts/activewafl.com/Public/Resources/Releases/ActiveWAFL-DEV-0.4.${ActiveWaflRevision}+DblEj+App.zip" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+App.zip" />
        <copy tofile="/var/www/vhosts/activewafl.com/Public/Resources/Releases/ActiveWAFL-DEV-0.4.${ActiveWaflRevision}+DblEj+WebApp.zip" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+WebApp.zip" />
        <copy tofile="/var/www/vhosts/activewafl.com/Public/Resources/Releases/ActiveWAFL-DEV-0.4.${ActiveWaflRevision}+DblEj+MvcWebApp.zip" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+MvcWebApp.zip" />
        <copy tofile="/var/www/vhosts/activewafl.com/Public/Resources/Releases/ActiveWAFL-DEV-0.4.${ActiveWaflRevision}+DblEj+ModMvcWebApp.zip" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+ModMvcWebApp.zip" />
        <loadresource property="WaflNextBuildNumber">
           <file file="/var/lib/jenkins/jobs/ActiveWAFL/nextBuildNumber"/>
        </loadresource>
        <loadresource property="ActiveWaflChangeLog">
           <file file="${env.WORKSPACE}/Build/Changes.txt"/>
        </loadresource>
        <loadresource property="ActiveWaflBreakingChangeLog">
           <file file="${env.WORKSPACE}/Build/BreakingChanges.txt"/>
        </loadresource>
        <sql
            classpath="/usr/share/java/mysql-connector-java.jar"
            driver="com.mysql.jdbc.Driver"
            url="jdbc:mysql://127.0.0.1:3306/activewaflweb"
            userid="webapp"
            password="71m3c0d3"
            autocommit="true">
            insert into Releases (Title, Filename, Major, Minor, Revision, Build, IsStable, IsRelease, DateReleased)
            values ('ActiveWAFL','ActiveWAFL-DEV-0.4.${ActiveWaflRevision}.zip', 0, 4, ${ActiveWaflRevision}, ${WaflNextBuildNumber}-1, 0, 0, unix_timestamp());

            insert into BlogPosts (PostDate, Title, Contents, UserId, BlogCategoryId, UrlTitle)
            values (unix_timestamp(), concat('ActiveWAFL 0.4.${ActiveWaflRevision} (Dev Build ', ${WaflNextBuildNumber}-1, ') is now available'), concat('ActiveWAFL ${ActiveWaflRevision} (Dev Build ', ${WaflNextBuildNumber}-1, ') is now available for download in the &lt;a href="/Resources/Downloads/"&gt;Download Center&lt;/a&gt;.  &lt;br&gt;NOTE: This is a dev build.  Use at your own risk.  &lt;br&gt;&lt;br&gt;&lt;b&gt;Breaking Changes in this version:&lt;/b&gt;${ActiveWaflBreakingChangeLog}&lt;br&gt;&lt;b&gt;Non-breaking changes in this version:&lt;/b&gt;${ActiveWaflChangeLog}'), 1, 1, '');

        </sql>
    </target>
    <target name="PushStable" description="Push the current version as a stable release.">
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+App.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+WebApp.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+MvcWebApp.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+ModMvcWebApp.zip" />
        <loadresource property="WaflNextBuildNumber">
           <file file="/var/lib/jenkins/jobs/ActiveWAFL/nextBuildNumber"/>
        </loadresource>
        <loadresource property="ActiveWaflChangeLog">
           <file file="${env.WORKSPACE}/Build/Changes.txt"/>
        </loadresource>
        <loadresource property="ActiveWaflBreakingChangeLog">
           <file file="${env.WORKSPACE}/Build/BreakingChanges.txt"/>
        </loadresource>
        <sql
            classpath="/usr/share/java/mysql-connector-java.jar"
            driver="com.mysql.jdbc.Driver"
            url="jdbc:mysql://127.0.0.1:3306/activewaflweb"
            userid="webapp"
            password="71m3c0d3"
            autocommit="true">
            insert into Releases (Title, Filename, Major, Minor, Revision, Build, IsStable, IsRelease, DateReleased)
            values ('ActiveWAFL','ActiveWAFL-0.4.${ActiveWaflRevision}.zip', 0, 4, ${ActiveWaflRevision}, ${WaflNextBuildNumber}-1, 1, 0, unix_timestamp());

            insert into BlogPosts (PostDate, Title, Contents, UserId, BlogCategoryId)
            values (unix_timestamp(), concat('ActiveWAFL 0.4.${ActiveWaflRevision} (Stable Build ', ${WaflNextBuildNumber}-1, ') is now available'), concat('ActiveWAFL ${ActiveWaflRevision} (Stable Build ', ${WaflNextBuildNumber}-1, ') is now available for download in the &lt;a href="/Resources/Downloads/"&gt;Download Center&lt;/a&gt;.  This is a STABLE build.  &lt;br&gt;&lt;b&gt;Breaking Changes in this version:&lt;/b&gt;${ActiveWaflBreakingChangeLog}&lt;br&gt;&lt;b&gt;Non-breaking changes in this version:&lt;/b&gt;${ActiveWaflChangeLog}'), 1, 1);

        </sql>
    </target>
    <target name="PushRelease" description="Push the current version as an official release.">
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+App.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+WebApp.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+MvcWebApp.zip" />
        <copy todir="/var/www/vhosts/activewafl.com/Public/Resources/Releases" file="${env.WORKSPACE}/Output/ActiveWAFL-0.4.${ActiveWaflRevision}+DblEj+ModMvcWebApp.zip" />
        <loadresource property="WaflNextBuildNumber">
           <file file="/var/lib/jenkins/jobs/ActiveWAFL/nextBuildNumber"/>
        </loadresource>
        <loadresource property="ActiveWaflChangeLog">
           <file file="${env.WORKSPACE}/Build/Changes.txt"/>
        </loadresource>
        <loadresource property="ActiveWaflBreakingChangeLog">
           <file file="${env.WORKSPACE}/Build/BreakingChanges.txt"/>
        </loadresource>

        <sql
            classpath="/usr/share/java/mysql-connector-java.jar"
            driver="com.mysql.jdbc.Driver"
            url="jdbc:mysql://127.0.0.1:3306/activewaflweb"
            userid="webapp"
            password="71m3c0d3"
            autocommit="true">
            insert into Releases (Title, Filename, Major, Minor, Revision, Build, IsStable, IsRelease, DateReleased)
            values ('ActiveWAFL','ActiveWAFL-0.4.${ActiveWaflRevision}.zip', 0, 4, ${ActiveWaflRevision}, ${WaflNextBuildNumber}-1, 1, 1, unix_timestamp());

            insert into BlogPosts (PostDate, Title, Contents, UserId, BlogCategoryId)
            values (unix_timestamp(), concat('ActiveWAFL 0.4.${ActiveWaflRevision} (Build ', ${WaflNextBuildNumber}-1, ') has been released'), concat('ActiveWAFL ${ActiveWaflRevision} (Release Build ', ${WaflNextBuildNumber}-1, ') has been released. Download the upgrade or the full install now from the &lt;a href="/Resources/Downloads/"&gt;Download Center&lt;/a&gt;.  This is a RELEASE build.  You should upgrade your applications to this version where possible.&lt;br&gt;&lt;b&gt;Breaking Changes in this version:&lt;/b&gt;${ActiveWaflBreakingChangeLog}&lt;br&gt;&lt;b&gt;Non-Breaking Changes in this version:&lt;/b&gt;${ActiveWaflChangeLog}'), 1, 1);

        </sql>
    </target>
    <target name="DistributeSharedLibs">
        <copy todir="/usr/share/Wafl">
            <fileset dir="${env.WORKSPACE}/Output/Dist" />
        </copy>
    </target>
    <target name="MakeDocs">
        <exec executable="php" dir="/usr/share/Doc2Db">
            <arg value="phpdoc2db2.php" />
            <arg value="dblej.ini" />
            <arg value="${ActiveWaflRevision}" />
        </exec>
        <exec executable="php" dir="/usr/share/Doc2Db">
            <arg value="phpdoc2db2.php" />
            <arg value="wafl.ini" />
            <arg value="${ActiveWaflRevision}" />
            <arg value="append" />
        </exec>
    </target>
</project>