/**
 * ActiveWAFL version {MAJOR}.{MINOR}.{REVISION} Build {BUILD}
 * Built {DATE}
 * Copyright (c) 2008-2014, Wafl.org
 * All rights reserved.
 */
