<?php
namespace Wafl\Cli;

require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Scripts" . DIRECTORY_SEPARATOR . "IScriptRunner.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Scripts" . DIRECTORY_SEPARATOR . "ScriptBase.php");
abstract class CliBase
extends \Wafl\Scripts\ScriptBase
{
    protected $_appRootPath;
    protected $_appClass = "\\Wafl\\Application\\Application";

    /**
     * @var \DblEj\Application\IApplication
     */
    protected $_application;

    protected function getExtensionDependencies()
    {
        return [];
    }

    protected function getUsageString()
    {
        $classInfo = new \ReflectionClass($this);
        return "Usage: wapp ".$classInfo->getShortName();
    }

    protected function onArgParse($argName, $argVal)
    {
        $parsed = false;
        $parsed = parent::onArgParse($argName, $argVal);
        switch ($argName)
        {
            case "AppRoot":
                if (substr($argVal, strlen($argVal) - 1) != DIRECTORY_SEPARATOR)
                {
                    $argVal = $argVal . DIRECTORY_SEPARATOR;
                }
                $argVal = realpath(trim($argVal));
                $this->_appRootPath = $argVal;
                $parsed = true;
                break;
            case "AppClass":
                $this->_appClass = $argVal;
                $parsed = true;
                break;
        }
        return $parsed;
    }

    public function finishRunning()
    {
        if ($this->_appRootPath && !defined("WAFL_APP_CONFIG_PATH"))
        {
            if (substr($this->_appRootPath, strlen($this->_appRootPath) - 1) != "/" && substr($this->_appRootPath, strlen($this->_appRootPath) - 1) != "\\")
            {
                $this->_appRootPath.=DIRECTORY_SEPARATOR;
            }

            if (!defined("WAFL_ENVIRONMENT"))
            {
                throw new \Exception("Cli scripts must be run from wapp");
            }
            $this->_application = \Wafl\AppSupport\WebIndex::BootstrapApplication($this->_appRootPath . "Application.syrp", WAFL_ENVIRONMENT, $this->_appClass);
            $this->printLine(implode(".", \Wafl\Core::GetFrameworkVersion()) . " - " . \DblEj\Util\Reflection::GetBaseClassname(get_class($this)) . " Utility", false, self::VERBOSITY_DEBUG);
            $this->printLine("App Initialized", false, self::VERBOSITY_DEBUG);
        }
        else
        {
            throw new \Exception("AppRoot argument (and possibly also the WaflEnvironment argument) is required");
        }

    }
}