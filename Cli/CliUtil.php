<?php
namespace Wafl\Cli;

if (!defined("AM_WEBPAGE"))
{
    define('AM_WEBPAGE', false);
}
require_once(__DIR__ . DIRECTORY_SEPARATOR . "CliBase.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR ."Scripts" . DIRECTORY_SEPARATOR . "ScriptUtil.php");
final class CliUtil
extends \Wafl\Scripts\ScriptUtil{}