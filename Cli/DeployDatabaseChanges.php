<?php
namespace Wafl\Cli;

class DeployDatabaseChanges
extends CliBase
{
    private $_barsShown = 0;

    protected function onRun($argCount, $args)
    {
        $engine = \Wafl\Core::$STORAGE_ENGINE;

        if ($engine)
        {
            $handler = new \DblEj\EventHandling\DynamicEventHandler
            (
                \DblEj\Data\ScriptExecutedEvent::DATA_SCRIPT_EXECUTED,
                function(\DblEj\Data\ScriptExecutedEvent $eventinfo)
                {
                    $barsToShow = ceil($eventinfo->Get_ScriptOrdinal() / $eventinfo->Get_TotalScriptCount() * 50);
                    $percent    = $barsToShow * 2;
                    $barString  = str_pad("", $barsToShow, "|");
                    print "\r" . str_pad($barString, 50) . "| " . $eventinfo->Get_ScriptOrdinal() . "/" . $eventinfo->Get_TotalScriptCount() . " $percent%";
                }
            );
            $engine->AddHandler($handler);

            $localRoot       = $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
            $confFolder      = $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_Config();
            $localConfFolder = $localRoot . $confFolder;

            print "\nChecking if this is a new install... ";
            if ($engine->Get_CreateScript() && $engine->Get_RequiredStorageLocation())
            {
                $engine->BeginTransaction();
                if (!file_exists($localRoot . "Output" . DIRECTORY_SEPARATOR . "DataStorageUpdates"))
                {
                    mkdir($localRoot . "Output" . DIRECTORY_SEPARATOR . "DataStorageUpdates", 0777, true);
                }
                try
                {
                    if (!$engine->DoesLocationExist($engine->Get_RequiredStorageLocation()))
                    {
                        print "New install detected\n\n";
                        $createSqlScript = $localConfFolder . $engine->Get_CreateScript();
                        if (file_exists($createSqlScript))
                        {
                            print "Running Create script $createSqlScript... \n";
                            $createBackupScript = $localRoot . "Output" . DIRECTORY_SEPARATOR . "DataStorageUpdates" . DIRECTORY_SEPARATOR . \Wafl\Core::$STORAGE_ENGINE->Get_ConnectionName() . "_" . date("Y-m-d_Gi") . "_" . basename($createSqlScript);
                            $this->_barsShown   = 0;
                            $engine->DirectScriptExecute($createSqlScript);
                            print "\n\n";

                            print "Copying backup into history... ";
                            copy($createSqlScript, $createBackupScript);
                            print "done\n\n";
                        }
                        else
                        {
                            throw new \Exception("According to the REQUIRED_STORAGE_LOCATION setting for " . \Wafl\Core::$STORAGE_ENGINE->Get_ConnectionName() . ", the database needs to be created.  However, the database create script ($createSqlScript) cannot be found.");
                        }
                    }
                    else
                    {
                        print "Existing install detected\n\n";
                    }
                    $engine->CommitTransaction();
                }
                catch (\Exception $ex)
                {
                    $engine->RollbackTransaction();
                    throw new \DblEj\Data\DatabaseUpdateException("Error while trying to run the creation script.", E_ERROR, $ex);
                }
            }
            print "Checking for update script... ";
            if ($engine->Get_UpdateScript())
            {
                $updateSqlScript = $localConfFolder . $engine->Get_UpdateScript();
                print "Looking in $updateSqlScript...";
                if (file_exists($updateSqlScript))
                {
                    print "found " . $engine->Get_UpdateScript() . "\n\n";
                    $engine->BeginTransaction();
                    try
                    {
                        print "Running update script " . $engine->Get_UpdateScript() . "...\n";
                        $this->_barsShown = 0;
                        $engine->DirectScriptExecute($updateSqlScript);
                        print "\n\n";
                        $engine->CommitTransaction();
                    }
                    catch (\Exception $ex)
                    {
                        $engine->RollbackTransaction();
                        throw new \DblEj\Data\DatabaseUpdateException("Error while trying to run the UpdateScript. ".$ex->getMessage(), E_ERROR, $ex);
                    }

                    //move the file if successful
                    print "Moving script into history... ";
                    $updateBackupScript = $localRoot . "Output" . DIRECTORY_SEPARATOR . "DataStorageUpdates" . DIRECTORY_SEPARATOR . \Wafl\Core::$STORAGE_ENGINE->Get_ConnectionName() . "_" . date("Y-m-d_Gi") . "_" . basename($updateSqlScript);
                    rename($updateSqlScript, $updateBackupScript);
                    print "done\n\n";
                }
                else
                {
                    print "none found\n\n";
                }
            }
            else
            {
                print "none found\n\n";
            }
        }
        else
        {
            print "no database engine found\n\n";
        }

        return true;
    }
}