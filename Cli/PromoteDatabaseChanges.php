<?php
namespace Wafl\Cli;

class PromoteDatabaseChanges
extends CliBase
{
    protected function onRun($argCount, $args)
    {
        if ($argCount > 0)
        {
            $promoteEnv = $args[0];
        }
        else
        {
            $promoteEnv = "stage";
        }
        if ($argCount > 1 && (stristr($args[1], "=") === false))
        {
            $promoteDataStorageName = $args[1];
        }
        else
        {
            $promoteDataStorageName = null;
        }
        if (!$promoteEnv || (stristr($promoteEnv, " ") !== false))
        {
            throw new \Exception("Invalid environment to promote to.  You should pass the name of the environment as the first argument.");
        }
        $localRoot       = $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
        $localConfFolder = $localRoot . $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_Config();

        $promoteConfigFile = realpath($localConfFolder . "More/Extensions.$promoteEnv.syrp");
        if (!file_exists($promoteConfigFile))
        {
            $promoteConfigFile = realpath($localConfFolder . "More/Extensions.syrp");
        }
        if ($promoteConfigFile && file_exists($promoteConfigFile))
        {
            $promoteDb           = null;
            $promoteConfigArray  = \Wafl\Util\Syrup::ParseSyrupFileAsArray($promoteConfigFile);
            if (!isset($promoteConfigArray["Storage\\MySql"]) || !isset($promoteConfigArray["Storage\\MySql"]["Connections"]))
            {
                throw new \Exception("Cannot promote database because I cannot find the MySql extension setup for the destination");
            }
            $promoteDbs          = $promoteConfigArray["Storage\\MySql"]["Connections"];
            $promoteDataSettings = null;
            foreach ($promoteDbs as $dataStorageName => $dataSettings)
            {
                if ($dataStorageName != "Common")
                {
                    if (($dataStorageName == $promoteDataStorageName) || ($promoteDataStorageName == ""))
                    {
                        $promoteDataSettings = $dataSettings;
                        break;
                    }
                }
            }
            if ($promoteDataSettings)
            {
                $db        = $promoteDataSettings["Catalog"];
                $user      = $promoteDataSettings["Username"];
                $pass      = $promoteDataSettings["Password"];
                $server    = $promoteDataSettings["Uri"];
            }
            if ($argCount > 2 && (stristr($args[2], "=") === false))
            {
                $server = $args[2];
            }
            $promoteDb = new \Wafl\Extensions\Storage\MySql\Connection($server, $db, $user, $pass);
            $promoteDb->Connect();
        }
        else
        {
            throw new \Exception("Can't find destination environment configuration.\n\n");
        }
        if ($promoteDb)
        {
            print "Promoting database updates to $promoteEnv...\n";
            $localRoot        = $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
            $dbUpdatesFolder  = $localRoot . "Output/DataStorageUpdates/";
            $dbPromotedFolder = $localRoot . "Output/DataStorageUpdates/Promoted-$promoteEnv/";
            print "Looking in $dbUpdatesFolder for db updates...\n";
            if (!file_exists($dbUpdatesFolder))
            {
                mkdir($dbUpdatesFolder, 0777, true);
            }
            $updateFiles = \DblEj\Util\Folder::GetFiles($dbUpdatesFolder);

            if (!file_exists($dbPromotedFolder))
            {
                mkdir($dbPromotedFolder, 0777, true);
            }
            $promotedFiles = \DblEj\Util\Folder::GetFiles($dbPromotedFolder);
            foreach ($updateFiles as $updateFile)
            {
                if (!\DblEj\Util\Strings::EndsWith($updateFile, ".md"))
                {
                    $alreadyPromoted = false;
                    foreach ($promotedFiles as $promotedFile)
                    {
                        if (basename($promotedFile) == basename($updateFile))
                        {
                            $alreadyPromoted = true;
                            break;
                        }
                    }
                    if (!$alreadyPromoted)
                    {
                        //promote
                        $promoteDb->BeginTransaction();
                        try
                        {
                            print "found unpromoted script: $updateFile.  promoting...\n";
                            $promoteDb->DirectScriptExecute($updateFile);
                            $promoteDb->CommitTransaction();
                            print "File promoted.\n";

                            //move the file if successful
                            print "Moving script into history... ";
                            $updateBackupScript = $dbPromotedFolder . basename($updateFile);
                            copy($updateFile, $updateBackupScript);
                            print "done\n\n";
                        }
                        catch (\Exception $ex)
                        {
                            $promoteDb->RollbackTransaction();
                            throw new \DblEj\Data\DatabaseUpdateException("Error while trying to promote the script.  See inner-exception for details.", E_ERROR, $ex);
                        }
                    }
                    else
                    {
                        print "Skipping already promoted script: $updateFile\n";
                    }
                }
            }
        }
        else
        {
            throw new \Exception("no database engine found\n\n");
        }
        return true;
    }

    protected function GetRequiredArgCount()
    {
        return 2;
    }

    protected function getUsageString()
    {
        $helpText = "Usage:".PHP_EOL;
        $helpText .= "wapp PromoteDatabaseChanges <DestinationEnvironment> <DestinationConnection> [<DestinationDbHost>]".PHP_EOL.PHP_EOL;
        $helpText .= "DestinationEnvironment       - The name of the environment you want to promote the database changes to".PHP_EOL;
        $helpText .= "DestinationConnection        - The name of the connection in the Extensions.syrp file that contains the settings for the destination database".PHP_EOL;
        $helpText .= "DestinationDbHost (optional) - The hostname of the database server.  This is useful when the settings file contains a hostname that is not accessible from the build machine, such as localhost, for example.  DEFAULT: the value in the application's settings file".PHP_EOL;
        $helpText .= "This script must be run from wapp";
        return $helpText;
    }
}