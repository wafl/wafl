<?php
namespace Wafl\Cli;

class RunUnitTests
extends CliBase
{

    protected function onRun($argCount, $args)
    {
        if (!defined("AM_WEBPAGE"))
        {
            define("AM_WEBPAGE", false);
        }
        print "Clearing some space so you can see...\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n******************************\n" .
        " ActiveWAFL Application\nUNIT TESTING IN PROGRESS...\n" . date("m/d/Y") . "\n";
        //NOTE: expect a folder path to the tests for the first argument

        /* @var \Wafl\Util\UnitTesting\TestResult $testResults */
        $testEngine = \Wafl\Util\Testing::GetUnitTestingEngine();
        if (!$testEngine)
        {
            throw new \DblEj\UnitTesting\NoTestingEngineException("Unit Testing");
        }
        $testResults = $testEngine->RunTest();

        //config for throw exception on fail
        foreach ($testResults->Get_TestSuites() as $testSuiteName => $testSuite)
        {
            print "\n>>>>>$testSuiteName\n\n";
            foreach ($testSuite->Get_TestCaseResults() as $testName => $testCaseResult)
            {
                if (!$testCaseResult->Get_IsFailure() && !$testCaseResult->Get_IsError())
                {
                    print "\n\t$testName: PASSED " . $testCaseResult->Get_Assertions() . " assertions\n";
                }
                elseif ($testCaseResult->Get_IsError())
                {
                    print "\n\t$testName: ERROR after " . $testCaseResult->Get_Assertions() . " assertions\n";
                    print "\tFile: " . $testCaseResult->Get_TestFileName() . "\n";
                    print "\tLine: " . $testCaseResult->Get_TestFileLineNumber() . "\n";
                    print "\tMessage: " . $testCaseResult->Get_Message() . "\n";
                }
                else
                {
                    print "\n\t$testName: FAILED after " . $testCaseResult->Get_Assertions() . " assertions\n";
                    print "\tFile: " . $testCaseResult->Get_TestFileName() . "\n";
                    print "\tLine: " . $testCaseResult->Get_TestFileLineNumber() . "\n";
                    print "\tMessage: " . $testCaseResult->Get_Message() . "\n";
                }
            }
        }
        print "\n\n";

        if ($testResults->Get_ErrorCount())
        {
            echo <<<EOL
         ******** *******   *******     *******   *******
        /**///// /**////** /**////**   **/////** /**////**
        /**      /**   /** /**   /**  **     //**/**   /**
        /******* /*******  /*******  /**      /**/*******
        /**////  /**///**  /**///**  /**      /**/**///**
        /**      /**  //** /**  //** //**     ** /**  //**
        /********/**   //**/**   //** //*******  /**   //**
        //////// //     // //     //   ///////   //     //

EOL;
        }
        elseif ($testResults->Get_FailureCount())
        {
            echo <<<EOL
         ********     **     ** **       **     ** *******   ********
        /**/////     ****   /**/**      /**    /**/**////** /**/////
        /**         **//**  /**/**      /**    /**/**   /** /**
        /*******   **  //** /**/**      /**    /**/*******  /*******
        /**////   **********/**/**      /**    /**/**///**  /**////
        /**      /**//////**/**/**      /**    /**/**  //** /**
        /**      /**     /**/**/********//******* /**   //**/********
        //       //      // // ////////  ///////  //     // ////////

EOL;
        }
        else
        {
            echo <<<EOL
          ******** **     **   ******    ******  ********  ********  ********
         **////// /**    /**  **////**  **////**/**/////  **//////  **//////
        /**       /**    /** **    //  **    // /**      /**       /**
        /*********/**    /**/**       /**       /******* /*********/*********
        ////////**/**    /**/**       /**       /**////  ////////**////////**
               /**/**    /**//**    **//**    **/**             /**       /**
         ******** //*******  //******  //****** /******** ********  ********
        ////////   ///////    //////    //////  //////// ////////  ////////

EOL;
        }
        print "\n\nSummary\n";
        print "******************************\n";
        print "Total assertions: " . $testResults->Get_AssertionCount() . "\n";
        print "Total failures: " . $testResults->Get_FailureCount() . "\n";
        print "Total errors: " . ($testResults->Get_ErrorCount() ? $testResults->Get_ErrorCount() : 0) . "\n";
        print "******************************\n";

        return true;
    }
}