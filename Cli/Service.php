<?php
namespace Wafl\Cli;

abstract class Service
extends \Wafl\Cli\CliBase
{
    private $_displayName = "Wafl Service Master Process"; //keep track of this because depending on the process fork the process kinda has different roles
    private $_isMaster = false;
    private $_pidFile;

    abstract function getProcessName();
    protected function getUsageString()
    {
        return "Usage: wapp " . $this->getDisplayName() ." start|stop";
    }
    protected function getDisplayName()
    {
        return $this->_displayName;
    }
    protected function GetRequiredArgCount()
    {
        return 1;
    }
    protected function onRun($argCount, $args)
    {
        $this->_displayName = $this->getProcessName()." Service Master Process";
        $this->_pidFile = "/var/run/".$this->getProcessName().".pid";
        $command = $args[0];
        if ($command == "stop")
        {
            if (file_exists($this->_pidFile))
            {
                $pid = file_get_contents($this->_pidFile);
                if ($pid)
                {
                    $this->printLine("Stopping pid $pid...", false, self::VERBOSITY_WARN);
                    if (posix_kill($pid, SIGQUIT))
                    {
                        $this->printLine("Server stopped", true, self::VERBOSITY_WARN);
                    } else {
                        throw new \Exception(posix_strerror(posix_get_last_error()));
                        $this->printLine("Cannot stop server", true, self::VERBOSITY_WARN);
                    }
                } else {
                    $this->printLine("Could not stop ".$this->getProcessName().", pid $pid not found", false, self::VERBOSITY_WARN);
                }
                unlink($this->_pidFile);
            } else {
                throw new \Exception("Cannot stop server because it does not appear to be running");
            }
        }
        else if ($command == "start")
        {
            if (!file_exists($this->_pidFile))
            {
                error_reporting(E_ALL);
                session_cache_limiter("nocache");
                set_time_limit(0);
                ini_set('session.gc_maxlifetime', 7776000);
                ini_set("max_execution_time", "0");
                ini_set("max_input_time", "0");
                ob_implicit_flush();
                declare(ticks = 1);

                //write my pid to a text file so linux services can stop me when they want to
                $pid = posix_getpid();
                file_put_contents($this->_pidFile, "$pid");
                chmod($this->_pidFile, 0777);

                $this->startService();
                $this->_displayName = $this->getProcessName()." Service Forked Process";

                if ($this->Get_IsMaster())
                {
                    $this->printLine("".$this->getProcessName()." Master Process gracefully shutting down...");
                    $this->printLine("Goodbye");
                } else {
                    //send a message to prevent zombies?
                }
            } else {
                throw new \Exception("It appears that the service is already running (lock file exists).");
            }
        }
        else
        {
           return false;
        }

        return true;
    }

    public function Get_IsMaster()
    {
        return $this->_isMaster;
    }

    protected function startService()
    {
        $this->_isMaster = ($this->BecomeDaemon()===true);

        if ($this->_isMaster)
        {
            //write my pid to a text file so linux services can stop me when they want to
            //even though we did this earlier, we need to do this because after forking our pid has changed
            $pid = \posix_getpid();
            if ($this->_pidFile)
            {
                file_put_contents($this->_pidFile, "$pid");
            }

            $setSigHandler = pcntl_signal(SIGTERM, array($this, "unixSignal_handler"));
            $setSigHandler = pcntl_signal(SIGQUIT, array($this, "unixSignal_handler"));
            $setSigHandler = pcntl_signal(SIGINT, array($this, "unixSignal_handler"));
            $setSigHandler = pcntl_signal(SIGCHLD, array($this, "unixSignal_handler"));

            try
            {
                //reconnect database since forking will have closed it
                if (\Wafl\Core::$STORAGE_ENGINE)
                {
                    \Wafl\Core::$STORAGE_ENGINE->Connect();
                }

                $this->onStart();
            } catch (\Exception $ex) {
                if ($this->_pidFile && file_exists($this->_pidFile))
                {
                    unlink($this->_pidFile);
                }
                throw $ex;
            }
        }
    }

    abstract function onStart();
    abstract function onStop();

    protected function notify()
    {
        pcntl_signal_dispatch();
    }

    private function BecomeDaemon()
    {
        //create new process, which will be set to seesion leader and then used to create actual daemon process
        $newpid = \pcntl_fork();
        if ($newpid === -1)
        {
            exit(0);
        }
        else if ($newpid>0)
        {
            //end the originating process which created this daemon

            //this is the start thread, pre-daemon.
            //Lets wait a second to allow the daemon thread to write its pid to disk,
            //otherwise when I exit, systemd will think that my PID is the daemon process.
            sleep(1);

            exit(0);
        }
        // Become the session leader
        \posix_setsid();
        \usleep(100000);

        //now as session leader, create the daemon process
        $newpid = \pcntl_fork();
        if ($newpid === -1)
        {
            exit(0);
        }
        else if ($newpid>0)
        {
            //end the session leader process which created this daemon
            exit(0);
        }
        return true;
    }

    public function unixSignal_handler($signal, $pid=null, $status=null)
    {
        switch ($signal)
        {
            case SIGTERM:
            case SIGQUIT:
            case SIGINT:
                $this->printLine("received term signal\n");
                if ($this->Get_IsMaster())
                {
                    $this->onStop();
                    $this->printLine("Shutting down server...\n");
                } else {
                    $this->printLine("".$this->getProcessName()." child recieved kill signal, shutting down...\n");
                    exit(0);
                }
                break;
            case SIGCHLD:
                $this->printLine("received chld signal");

                if(!$pid){
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }

                //Make sure we get all of the exited children
                while($pid > 0){
                    if($pid){
                        $exitCode = pcntl_wexitstatus($status);
                        //unset($this->currentJobs[$pid]);
                        $this->printLine("$pid exited with status ".$exitCode);
                    }

                    $this->printLine("waiting for any additional waiting children to finish exit\n");
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                    if ($pid == "-1")
                    {
                        $this->printLine("No more child processes waiting\n");
                    }
                }
                break;
        }
    }

}
