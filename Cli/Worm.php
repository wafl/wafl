<?php
namespace Wafl\Cli;

use Wafl\Core,
    Wafl\Util\Counterpart;

require_once(__DIR__.DIRECTORY_SEPARATOR."CliBase.php");

class Worm
extends CliBase
{
    protected function onRun($argCount, $args)
    {
        $tableToClassMappings   = array();
        $tableSuperlassMappings = array();
        $convertPluarlTables = true;
        if (count($args) > 0)
        {
            foreach ($args as $argName => $argValue)
            {
                if (!is_numeric($argName))
                {
                    switch ($argName)
                    {
                        case "PluarlConversion":
                            $convertPluarlTables = boolval($argValue);
                            break;
                        default:
                            $dotPos              = strpos($argName, ".");
                            if ($dotPos !== false)
                            {
                                $argSubname = substr($argName, $dotPos + 1);
                                $argName    = substr($argName, 0, $dotPos);
                                switch ($argSubname)
                                {
                                    case "Superclass":
                                        $tableSuperlassMappings[$argName]                     = $argValue;
                                        break;
                                    default:
                                        $tableToClassMappings[$argName . "." . $argSubname]   = $argValue;
                                        $tableSuperlassMappings[$argName . "." . $argSubname] = "\\DblEj\\Data\\PersistableModel";
                                        break;
                                }
                            }
                            else
                            {
                                $tableToClassMappings[$argName]   = $argValue;
                                $tableSuperlassMappings[$argName] = "\\DblEj\\Data\\PersistableModel";
                            }
                            break;
                    }
                }
            }
        }
        $localAppFolder                   = $this->_appRootPath . $this->_application->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR;
        $datamodelFolder                  = $localAppFolder . Core::MODELS_FOLDER . "DataModel" . DIRECTORY_SEPARATOR;
        $localClientFunctionalModelFolder = $localAppFolder . Core::MODELS_FOLDER . $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_FunctionalModelsClient();
        $localServerFunctionalModelFolder = $localAppFolder . Core::MODELS_FOLDER . $this->_application->Get_Settings()->Get_Paths()->Get_Application()->Get_FunctionalModelsServer();

        if (!file_exists($datamodelFolder))
        {
            mkdir($datamodelFolder);
        }
        if (!file_exists($localClientFunctionalModelFolder))
        {
            mkdir($localClientFunctionalModelFolder);
        }
        if (!file_exists($localServerFunctionalModelFolder))
        {
            mkdir($localServerFunctionalModelFolder);
        }
        $filenames = Counterpart::GetDataModel($this->_application, $convertPluarlTables, $this->_application->Get_Settings()->Get_Application()->Get_RootNameSpace() . "DataModel", false, $tableToClassMappings, $tableSuperlassMappings);
        foreach ($filenames as $filename => $fileData)
        {
            print "Saving file $filename..." . PHP_EOL;
            if (!file_exists($filename))
            {
                file_put_contents($filename, $fileData);
                print "Done" . PHP_EOL . PHP_EOL;
            }
            else
            {
                $pathinfo    = pathinfo($filename);
                $passDirName = $pathinfo["dirname"];
                if (substr($passDirName, strlen($passDirName) - 1) != "/")
                {
                    $passDirName .= "/";
                }

                if (realpath($passDirName) == realpath($datamodelFolder))
                {
                    if (file_put_contents($filename, $fileData) > 0)
                    {
                        print "Done (Overwrote Data Model)" . PHP_EOL . PHP_EOL;
                    }
                    else
                    {
                        print "Done (Skipped Data Model because of an error - probably permissions - make sure DataModel folder is writable by web server.)" . PHP_EOL . PHP_EOL;
                    }
                }
                else
                {
                    print "Skipped Functional Model because it already exists" . PHP_EOL . PHP_EOL;
                }
            }
        }
        if (count($filenames) == 0)
        {
            print "No data models were found" . PHP_EOL . PHP_EOL;
        }
        return true;
    }
}