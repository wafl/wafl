<?php

if (class_exists('PHP_CodeSniffer_Standards_AbstractPatternSniff', true) === false) {
    throw new PHP_CodeSniffer_Exception('Class PHP_CodeSniffer_Standards_AbstractPatternSniff not found');
}

class Wafl_Sniffs_ControlStructures_StructureDeclarationSniff extends PHP_CodeSniffer_Standards_AbstractPatternSniff
{

    public $supportedTokenizers = array(
                                   'PHP',
                                   'JS',
                                  );

    protected function getPatterns()
    {
        return array(
                'tryEOL...{EOL...EOL...}EOL...catch (...)EOL...{EOL...EOL...};EOL',
                'doEOL...{EOL...EOL...}EOL...while (...);EOL',
                'while (...)EOL...{EOL',
                'for (...)EOL...{EOL',
                'if (...)EOL...{EOL',
                'ifEOL...(EOL...EOL...)EOL...{EOL',
                'foreach (...)EOL...{EOL...EOL...};EOL',
                'foreachEOL...(EOL...EOL...)EOL...{EOL...EOL...};EOL',
                'EOL...}EOL...elseif (...)EOL...{EOL',
                'EOL...}EOL...elseEOL...{EOL...}EOL',
               );

    }


}

?>