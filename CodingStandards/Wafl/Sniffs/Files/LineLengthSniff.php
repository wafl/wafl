<?php
/**
 * Warn if lines are longer than 130 chars.  Fail with error if longer than 200.  Ignore comments.
 */
class Wafl_Sniffs_Files_LineLengthSniff extends Generic_Sniffs_Files_LineLengthSniff
{
    public $lineLimit = 0;
    public function __construct()
    {
        $this->lineLimit = 400;
        $this->absoluteLineLimit = 600;
    }
    protected function checkLineLength(PHP_CodeSniffer_File $phpcsFile, $stackPtr, $lineContent)
    {
        $fileFolder = dirname($phpcsFile->getFilename());
        if (substr($fileFolder,strlen($fileFolder)-9) == "DataModel")
        {
            //dont check lengths of auto-generated data models.
            return;
        }
        if ((substr(trim($lineContent),0,1) == "*") || (substr(trim($lineContent),0,2) == "//"))
        {
            return;
        }

        return parent::checkLineLength($phpcsFile, $stackPtr, $lineContent);
    }
}