
<?php
/**
 * Wafl_Sniffs_Methods_MethodNameSniff
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   PHP_CodeSniffer
 * @link      http://pear.php.net/package/PHP_CodeSniffer
 */
if (class_exists('PHP_CodeSniffer_Standards_AbstractScopeSniff', true) === false)
{
    throw new PHP_CodeSniffer_Exception('Class PHP_CodeSniffer_Standards_AbstractScopeSniff not found');
}
class Wafl_Sniffs_Methods_MethodNameSniff
extends PHP_CodeSniffer_Standards_AbstractScopeSniff
{
    /**
     * A list of all PHP magic methods.
     *
     * @var array
     */
    protected $magicMethods = array(
        'construct',
        'destruct',
        'call',
        'callstatic',
        'get',
        'set',
        'isset',
        'unset',
        'sleep',
        'wakeup',
        'tostring',
        'set_state',
        'clone',
        'invoke',
        'call',
        'activeWaflErrorHandler',
        'debugActiveWaflErrorHandler',
        'activeWaflExceptionHandler',
        'debugActiveWaflExceptionHandler',
        'debugActiveWaflShutdownHandler',
        'debugActiveWaflShutdownHandler'
    );

    /**
     * A list of all PHP non-magic methods starting with a double underscore.
     *
     * These come from PHP modules such as SOAPClient.
     *
     * @var array
     */
    protected $methodsDoubleUnderscore = array(
        'soapcall',
        'getlastrequest',
        'getlastresponse',
        'getlastrequestheaders',
        'getlastresponseheaders',
        'getfunctions',
        'gettypes',
        'dorequest',
        'setcookie',
        'setlocation',
        'setsoapheaders',
    );

    /**
     * A list of all PHP magic functions.
     *
     * @var array
     */
    protected $magicFunctions = array(
        'autoload');

    /**
     * If TRUE, the string must not have two capital letters next to each other.
     *
     * @var bool
     */
    public $strict = true;

    /**
     * Constructs a Wafl_Sniffs_Methods_MethodNameSniff.
     */
    public function __construct()
    {
        parent::__construct($this->getScopeTokens(), array(T_FUNCTION), true);
    }
//end __construct()

    protected function getScopeTokens()
    {
        return [T_CLASS, T_INTERFACE, T_TRAIT];
    }

    /**
     * Processes the tokens within the scope.
     *
     * @param PHP_CodeSniffer_File $phpcsFile The file being processed.
     * @param int                  $stackPtr  The position where this token was
     *                                        found.
     * @param int                  $currScope The position of the current scope.
     *
     * @return void
     */
    protected function processTokenWithinScope(PHP_CodeSniffer_File $phpcsFile, $stackPtr, $currScope)
    {
        $methodName = $phpcsFile->getDeclarationName($stackPtr);
        if ($methodName === null)
        {
            // Ignore closures.
            return;
        }
        $className = $phpcsFile->getDeclarationName($currScope);
        $errorData = array(
            $className . '::' . $methodName);

        // Ignore magic methods.
        $magicPart = strtolower(substr($methodName, 0, 2));
        if ($magicPart == "__")
        {
            return;
        }

        $methodProps = $phpcsFile->getMethodProperties($stackPtr);
        if (($methodProps['scope_specified'] !== true))
        {
            $error = 'Method visibility scope must be specified for "%s".';
            $phpcsFile->addError($error, $stackPtr, 'VisibilityScope', $errorData);
        }

        if ($methodProps['scope'] == "public")
        {
            if (!self::isValidPublicMethodName($methodName))
            {
                $error = 'public method name "%s" should start with a capital letter and should not be preceded by an underscore.';
                $data  = array($methodName);
                $phpcsFile->addWarning($error, $stackPtr, 'PublicMethodBadName', $data);
            }
        }
        elseif ($methodProps['scope'] == "protected")
        {
            if (!self::isValidProtectedMethodName($methodName))
            {
                $error = 'protected method name "%s" should start with a lower-case letter and should not be preceded by an underscore.';
                $data  = array($methodName);
                $phpcsFile->addWarning($error, $stackPtr, 'ProtectedMethodBadName', $data);
            }
        }
        elseif ($methodProps['scope'] == "private")
        {
            if (!self::isValidPrivateMethodName($methodName))
            {
                $error = 'private method name "%s" should start with a lower-case letter preceded by an underscore.';
                $data  = array($methodName);
                $phpcsFile->addWarning($error, $stackPtr, 'PrivateMethodBadName', $data);
            }
        }

        $visibility = 0;
        $static     = 0;
        $abstract   = 0;
        $final      = 0;

        $methodPrefixes = array(
            T_PRIVATE   => T_PRIVATE,
            T_PUBLIC    => T_PUBLIC,
            T_PROTECTED => T_PROTECTED,
            T_ABSTRACT  => T_ABSTRACT,
            T_STATIC    => T_STATIC,
            T_FINAL     => T_FINAL,
        );

        $find   = $methodPrefixes;
        $find[] = T_WHITESPACE;
        $prev   = $phpcsFile->findPrevious($find, ($stackPtr - 1), null, true);

        $tokens = $phpcsFile->getTokens();
        $prefix = $stackPtr;
        while (($prefix = $phpcsFile->findPrevious($methodPrefixes, ($prefix - 1), $prev)) !== false)
        {
            switch ($tokens[$prefix]['code'])
            {
                case T_STATIC:
                    $static     = $prefix;
                    break;
                case T_ABSTRACT:
                    $abstract   = $prefix;
                    break;
                case T_FINAL:
                    $final      = $prefix;
                    break;
                default:
                    $visibility = $prefix;
                    break;
            }
        }

        if ($static !== 0 && $static < $visibility)
        {
            $error = 'The static declaration must come after the visibility declaration';
            $phpcsFile->addError($error, $static, 'StaticBeforeVisibility');
        }

        if ($visibility !== 0 && $final > $visibility)
        {
            $error = 'The final declaration must precede the visibility declaration';
            $phpcsFile->addError($error, $final, 'FinalAfterVisibility');
        }

        if ($visibility !== 0 && $abstract > $visibility)
        {
            $error = 'The abstract declaration must precede the visibility declaration';
            $phpcsFile->addError($error, $abstract, 'AbstractAfterVisibility');
        }
    }
//end processTokenWithinScope()

    /**
     * Processes the tokens outside the scope.
     *
     * @param PHP_CodeSniffer_File $phpcsFile The file being processed.
     * @param int                  $stackPtr  The position where this token was
     *                                        found.
     *
     * @return void
     */
    protected function processTokenOutsideScope(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $functionName = $phpcsFile->getDeclarationName($stackPtr);
        if ($functionName === null)
        {
            // Ignore closures.
            return;
        }

        $errorData = array(
            $functionName);

        // Is this a magic function. i.e., it is prefixed with "__".
        if (preg_match('|^__|', $functionName) !== 0)
        {
            $magicPart = strtolower(substr($functionName, 2));
            if (in_array($magicPart, $this->magicFunctions) === false)
            {
                $error = 'Function name "%s" is invalid; only PHP magic methods should be prefixed with a double underscore';
                $phpcsFile->addError($error, $stackPtr, 'FunctionDoubleUnderscore', $errorData);
            }

            return;
        }

        if (PHP_CodeSniffer::isCamelCaps($functionName, true, false, false))
        {
            $error = 'Global public function name "%s" should be in StudlyCaps with no preceding underscore';
            $data  = array(
                $functionName);
            $phpcsFile->addWarning($error, $stackPtr, 'PublicMethodNoUnderscore', $data);
        }
    }
//end processTokenOutsideScope()

    private static function isValidPublicMethodName($methodName)
    {
        // Check the first character first.
        $legalFirstChar = '[A-Z]';

        if (preg_match("|^$legalFirstChar|", $methodName) === 0)
        {
            return false;
        }

        // Check that the name only contains legal characters.
        $legalChars = 'a-zA-Z0-9_';
        if (preg_match("|[^$legalChars]|", substr($methodName, 1)) > 0)
        {
            return false;
        }

        return true;
    }

    private static function isValidProtectedMethodName($methodName)
    {
        // Check the first character first.
        $legalFirstChar = '[a-z]';

        if (preg_match("|^$legalFirstChar|", $methodName) === 0)
        {
            return false;
        }

        // Check that the name only contains legal characters.
        $legalChars = 'a-zA-Z0-9_';
        if (preg_match("|[^$legalChars]|", $methodName) > 0)
        {
            return false;
        }

        return true;
    }

    private static function isValidPrivateMethodName($methodName)
    {
        // Check the first character first.
        if (substr($methodName,0,1) != "_")
        {
            return false;
        }

        // Check that the name only contains legal characters.
        $legalChars = 'a-zA-Z0-9_';
        if (preg_match("|[^$legalChars]|", substr($methodName, 1)) > 0)
        {
            return false;
        }

        return true;
    }
}
?>
