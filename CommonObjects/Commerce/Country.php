<?php

namespace Wafl\CommonObjects\Commerce;

class Country
{
    private $_title;
    private $_isoCode;

    public function __construct($title, $isoCode)
    {
        $this->_title = $title;
        $this->_isoCode = $isoCode;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Get_IsoCode()
    {
        return $this->_isoCode;
    }
}
