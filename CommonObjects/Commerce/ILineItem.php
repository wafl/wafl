<?php

namespace Wafl\CommonObjects\Commerce;

interface ILineItem
{
    function Get_Uid();
    function Get_ItemUid();
    function Get_ItemImageUrl();
    function Get_Title();
    function Get_Description();
    function Get_Qty();
    function Get_PriceEach();
    function Get_CreateDate();
    function Get_ShippingCost();
}

