<?php

namespace Wafl\CommonObjects\Commerce;

interface IOrder
{
    function Get_Uid();
    function Get_OrderDate();
    function Get_LastModifiedDate();
    function Get_SubTotal();
    function Get_GrandTotal();
    function Get_ShippingTotal();
    function Get_TaxTotal();
    function Get_CouponTotal();
    function Get_DealTotal();
    function Get_ItemTotal();
    function Get_ShippingStreet1();
    function Get_ShippingStreet2();
    function Get_ShippingCity();
    function Get_ShippingRegion();
    function Get_ShippingCountry();
    function Get_ShippingFirstName();
    function Get_ShippingLastName();
    function Get_ShipDate();
    function Get_PaymentMethod();
    function Get_BuyerNotes();
    function Get_PaymentStatus();
    function Get_IsShipped();
    function Get_BuyerEmail();
    function Get_ShippingRate();
    function Get_ShipmentService();
    function Get_ShippingTrackingCode();
    function Get_LineItems();
    function Get_SourceSystem();
}
