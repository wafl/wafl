<?php

namespace Wafl\CommonObjects\Commerce;

interface IShipment
{
    function Get_TrackingCode();
    function Get_ShipmentService();
}