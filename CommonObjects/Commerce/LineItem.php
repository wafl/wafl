<?php

namespace Wafl\CommonObjects\Commerce;

class LineItem
implements ILineItem
{
    private $_uid;
    private $_itemUid;
    private $_title;
    private $_description;
    private $_qty;
    private $_priceEach;
    private $_createDate;
    private $_shippingCost;
    private $_itemImageUrl;

    public function __construct($uid, $itemUid, $qty, $priceEach, $createDate, $title, $description, $itemImageUrl = null, $shippingCharge = 0)
    {
        $this->_uid = $uid;
        $this->_itemUid = $itemUid;
        $this->_title = $title;
        $this->_description = $description;
        $this->_qty = $qty;
        $this->_priceEach = $priceEach;
        $this->_createDate = $createDate;
        $this->_shippingCost = $shippingCharge;
        $this->_itemImageUrl = $itemImageUrl;
    }

    public function Get_Uid()
    {
        return $this->_uid;
    }

    public function Get_ItemUid()
    {
        return $this->_itemUid;
    }
    public function Get_ItemImageUrl()
    {
        return $this->_itemImageUrl;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Get_Description()
    {
        return $this->_description;
    }

    public function Get_Qty()
    {
        return $this->_qty;
    }

    public function Get_PriceEach()
    {
        return $this->_priceEach;
    }

    public function Get_CreateDate()
    {
        return $this->_createDate;
    }

    public function Get_ShippingCost()
    {
        return $this->_shippingCost;
    }
}

