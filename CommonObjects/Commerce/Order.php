<?php

namespace Wafl\CommonObjects\Commerce;

class Order
implements IOrder
{
    private $_uid;
    private $_orderDate;
    private $_lastModifiedDate;
    private $_subTotal;
    private $_grandTotal;
    private $_shippingTotal;
    private $_taxTotal;
    private $_couponTotal;
    private $_dealTotal;
    private $_itemTotal;
    private $_shippingStreet1;
    private $_shippingStreet2;
    private $_shippingCity;
    private $_shippingRegion;
    private $_shippingTrackingCode;
    private $_shipDate;

    /**
     *
     * @var \Wafl\CommonObjects\Commerce\Country
     */
    private $_shippingCountry;
    private $_shippingPostalCode;
    private $_shippingFirstName;
    private $_shippingLastName;
    private $_paymentMethod;
    private $_buyerNotes;
    private $_paymentStatus;
    private $_isShipped;
    private $_buyerEmail;
    private $_shippingRate;
    private $_lineItems;
    private $_sourceSystem;

    public function __construct
    (
        $sourceSystemId, $uid, $orderDate, $buyerEmail, $subTotal, $grandTotal, $shippingTotal, $taxTotal, $itemTotal,
        $shippingFirstName, $shippingLastName, $shippingStreet1, $shippingStreet2, $shippingCity, $shippingRegion, \Wafl\CommonObjects\Commerce\Country $shippingCountry, $shippingPostalCode,
        $shippingRate, $paymentMethod, $paymentStatus, $lineItems = [], $buyerNotes = "", $couponTotal = 0, $dealTotal = 0, $lastModifiedDate = null, $shippingTrackingCode = null, $shipDate = null)
    {
        if (!$lastModifiedDate)
        {
            $lastModifiedDate = $orderDate;
        }

        $this->_uid = $uid;
        $this->_orderDate = $orderDate;
        $this->_shippingStreet1 = $shippingStreet1;
        $this->_shippingStreet2 = $shippingStreet2;
        $this->_shippingCity = $shippingCity;
        $this->_shippingRegion = $shippingRegion;
        $this->_shippingCountry = $shippingCountry;
        $this->_shippingFirstName = $shippingFirstName;
        $this->_shippingLastName = $shippingLastName;
        $this->_shippingRate = $shippingRate;
        $this->_shipDate = $shipDate;
        $this->_subTotal = $subTotal;
        $this->_shippingTotal = $shippingTotal;
        $this->_grandTotal = $grandTotal;
        $this->_taxTotal = $taxTotal;
        $this->_itemTotal = $itemTotal;
        $this->_paymentMethod = $paymentMethod;
        $this->_paymentStatus = $paymentStatus;
        $this->_buyerNotes = $buyerNotes;
        $this->_buyerEmail = $buyerEmail;
        $this->_couponTotal = $couponTotal;
        $this->_dealTotal = $dealTotal;
        $this->_lastModifiedDate = $lastModifiedDate;
        $this->_shippingPostalCode = $shippingPostalCode;
        $this->_shippingTrackingCode = $shippingTrackingCode;
        $this->_sourceSystem = $sourceSystemId;
        $this->_lineItems = $lineItems;
    }
    public function Get_ShippingFirstName()
    {
        return $this->_shippingFirstName;
    }
    public function Get_ShippingLastName()
    {
        return $this->_shippingLastName;
    }

    public function Get_ShipDate()
    {
        return $this->_shipDate;
    }

    public function Get_SourceSystem()
    {
        return $this->_sourceSystem;
    }
    public function Set_SourceSystem($newSystemId)
    {
        $this->_sourceSystem = $newSystemId;
    }
    public function Get_Uid()
    {
        return $this->_uid;
    }

    public function Get_OrderDate()
    {
        return $this->_orderDate;

    }

    public function Get_LastModifiedDate()
    {
        return $this->_lastModifiedDate;
    }

    public function Get_ShippingTrackingCode()
    {
        return $this->_shippingTrackingCode;
    }

    public function Get_SubTotal()
    {
        return $this->_subTotal;
    }

    public function Get_GrandTotal()
    {
        return $this->_grandTotal;
    }

    public function Get_ShippingTotal()
    {
        return $this->_shippingTotal;
    }

    public function Get_TaxTotal()
    {
        return $this->_taxTotal;
    }

    public function Get_CouponTotal()
    {
        return $this->_couponTotal;
    }

    public function Get_DealTotal()
    {
        return $this->_dealTotal;
    }

    public function Get_ItemTotal()
    {
        return $this->_itemTotal;

    }

    public function Get_ShippingStreet1()
    {
        return $this->_shippingStreet1;

    }

    public function Get_ShippingStreet2()
    {
        return $this->_shippingStreet2;

    }

    public function Get_ShippingCity()
    {
        return $this->_shippingCity;

    }
    public function Get_ShippingRegion()
    {
        return $this->_shippingRegion;

    }

    /**
     *
     * @return Wafl\CommonObjects\Commerce\Country
     */
    public function Get_ShippingCountry()
    {
        return $this->_shippingCountry;

    }
    public function Get_ShippingPostalCode()
    {
        return $this->_shippingPostalCode;

    }
    public function Get_PaymentMethod()
    {
        return $this->_paymentMethod;

    }
    public function Get_BuyerNotes()
    {
        return $this->_buyerNotes;

    }
    public function Get_PaymentStatus()
    {
        return $this->_paymentStatus;

    }
    public function Get_IsShipped()
    {
        return $this->_isShipped;

    }
    public function Get_BuyerEmail()
    {
        return $this->_buyerEmail;

    }
    public function Get_ShippingRate()
    {
         return $this->_shippingRate;

    }
    public function Get_ShipmentService()
    {
        return $this->_shipmentService;

    }
    public function Get_LineItems()
    {
         return $this->_lineItems;
    }
}
