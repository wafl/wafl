<?php

namespace Wafl\CommonObjects\Commerce;

class Shipment
implements IShipment
{
    private $_trackingCode;
    private $_shipmentService;

    public function __construct($trackingCode, $shipmentService)
    {
        $this->_trackingCode = $trackingCode;
        $this->_shipmentService = $shipmentService;
    }
    public function Get_TrackingCode()
    {
        return $this->_trackingCode;
    }

    public function Get_ShipmentService()
    {
        return $this->_shipmentService;
    }
}