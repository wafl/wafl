/*
 *@namespace Wafl
 */
Namespace("Wafl");

Wafl.Core = function ()
{
};

Wafl.Core.UnixTime = function () {
    return Math.round(new Date().getTime() / 1000);
};

Wafl.Core.GetObjectPropertyCount = function (obj)
{
    var propCt = 0;
    for (var propIdx in obj)
    {
        if (obj.hasOwnProperty(propIdx))
        {
            propCt++;
        }
    }
    return propCt;

};


Wafl.Core.GetErrorLog_handler = function (responseText, token)
{
    if (token == "ErrorLog")
    {
        var logDiv = $("DebugWindow");
        if (logDiv)
        {
            logDiv.innerHTML = responseText;
        }
    }
};

Wafl.Core.DisplayHttpErrorLog = function ()
{
    if (typeof (AjaxClient) != "undefined" && typeof AjaxClient != undefined && AjaxClient != null)
    {
        AjaxClient.SendRequestAsync("GetHttpErrorLog", "", "ErrorLog", "GetErrorLog_handler", Core);
    } else {
        var logDiv = $("DebugWindow");
        if (logDiv)
        {
            logDiv.innerHTML = "Loading Ajax client...";
        }
        Wafl.Core.DebugTimerHandler = setTimeout("Wafl.Core.DisplayHttpErrorLog()", 1000);
    }
};

if ("registerElement" in document)
{
    var waflGridLayoutProto = Object.create(HTMLElement.prototype);
    var waflFlowLayoutProto = Object.create(HTMLElement.prototype);
    var waflLayoutRowProto = Object.create(HTMLElement.prototype);
    var waflLayoutCellProto = Object.create(HTMLElement.prototype);
    Object.defineProperty(waflGridLayoutProto, 'auto', {
        value: false,
        writable : true
    });
    Object.defineProperty(waflLayoutCellProto, 'spans', {
        value: null,
        writable : true
    });
    Object.defineProperty(waflLayoutCellProto, 'skips', {
        value: null,
        writable : true
    });
    Wafl.Core.GridLayoutElement = document.registerElement('grid-layout', {
      prototype: waflGridLayoutProto
    });
    Wafl.Core.FlowLayoutElement = document.registerElement('flow-layout', {
      prototype: waflFlowLayoutProto
    });
    Wafl.Core.FlowLayoutElement = document.registerElement('layout-row', {
      prototype: waflLayoutRowProto
    });
    Wafl.Core.FlowLayoutElement = document.registerElement('layout-cell', {
      prototype: waflLayoutCellProto
    });
}
Element
    .AddBlockSelector(".Title")
    .AddBlockSelector(".Block")
    .AddBlockSelector(".Panel")
    .AddBlockSelector(".Alert")
    .AddBlockSelector(".Console")
    .AddBlockSelector("menu>li a")
    .AddBlockSelector("form label")
    .AddBlockSelector(".Tiles > li")
    .AddBlockSelector("li.Button a")
    .AddBlockSelector(".Grid.Layout")
    .AddBlockSelector(".Flow.Layout")
    .AddBlockSelector(".HeaderLabel")
    .AddBlockSelector("ul.Menu>li>a")
    .AddBlockSelector(".Tiles>li>img")
    .AddBlockSelector(".Notification")
    .AddBlockSelector(".CodeContainer")
    .AddBlockSelector("menu>li button")
    .AddBlockSelector("menu>li.Header")
    .AddBlockSelector("blockquote small")
    .AddBlockSelector(".Tiles > li > img")
    .AddBlockSelector(".Grid.Layout .Row")
    .AddBlockSelector("ul.Menu>li>button")
    .AddBlockSelector("ul.Menu>li.Header")
    .AddBlockSelector("menu.Horizontal>li")
    .AddBlockSelector("ul.Horizontal.Menu>li")
    .AddBlockSelector("grid-layout layout-row")
    .AddBlockSelector(".Dialog .Header .Close")
    .AddBlockSelector(".Toolbar > ul.ButtonLinks li > a")
    .AddBlockSelector("grid-layout layout-row layout-cell")
    .AddBlockSelector(".Toolbar .Padding > ul.ButtonLinks li > a")
    .AddBlockTag("dl")
    .AddBlockTag("dd")
    .AddBlockTag("dt")
    .AddBlockTag("pre")
    .AddBlockTag("legend")
    .AddBlockTag("address")
    .AddBlockTag("fieldset")
    .AddBlockTag("grid-layout")
    .AddBlockTag("flow-layout");

Start
    (
        function ()
        {
            var userTz = DblEj.Util.Time.GetTimezoneOffset();
            _("Wafl.SetTimezoneOffset", {"TzOffset": userTz}, null);
        }
    );