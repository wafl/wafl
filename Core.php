<?php

namespace Wafl;

use DblEj\EventHandling\EventInfo,
    DblEj\Logging\NullTraceHandler,
    DblEj\Logging\Tracing,
    DblEj\System\MissingPhpExtensionException,
    DblEj\Util\SystemEvents,
    DblEj\Communication\Ajax\AjaxHandler,
    Wafl\Users\User;

require_once("Core_Deprecated.php");

class Core
{
    use Core_Deprecated;
    // <editor-fold defaultstate="collapsed" desc="Apps-wide Constants">
    const WAFL_API_HANDLER_URL = "/api.php";

    /**
     * The location of the models folder, relative to LOCAL_ROOT
     */
    const MODELS_FOLDER              = "Models/";
    const GLOBAL_STYLESHEET_FILENAME = "Global.css";

    // </editor-fold>

    public static function InitializeFramework($app)
    {
        if (!self::$_AM_INITIALIZED)
        {
            require_once(__DIR__ . DIRECTORY_SEPARATOR . "Global.php");

            //BEGIN INITIALIZATION
            SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::BEFORE_INITIALIZE));
            self::$_AM_INITIALIZED = true;

            //verify environment is good
            self::_checkForSuitableEnvironment();

            //SETUP TRACE HANDLING
            Tracing::SetTraceHandler(new NullTraceHandler()); //@todo put in configs
            //CHECK WAFL VERSION FILE
            if (!self::GetFrameworkVersion())
            {
                throw new \Exception("WAFL version file missing.  Installation seems corrupted.");
            }

            //SETUP DEFAULT USER
            self::_setupDefaultUser($app);

            //SETUP FRAMEWORK AJAX HANDLERS
            self::_addFrameworkApiFolders($app);

            //FINISH INITIALIZE
            SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_INITIALIZE));
        }
    }

    private static function _checkForSuitableEnvironment()
    {
    if ((PHP_MAJOR_VERSION == 5 && PHP_MINOR_VERSION < 5) || PHP_MAJOR_VERSION < 5)
        {
            throw new \Exception("ActiveWAFL " . implode(".", self::GetFrameworkVersion()) . " requires PHP version 5.5 or higher.  You have ".PHP_MAJOR_VERSION.".".PHP_MINOR_VERSION.".");
        }

        if (!function_exists("curl_exec"))
        {
            throw new MissingPhpExtensionException("Curl", "ActiveWAFL " . implode(".", self::GetFrameworkVersion()) . " requires Curl");
        }
    }

    /**
     * Get ActiveWAFL's version.
     * @param int $versionPart
     * The part of the version to be returned. 0 for Major, 1 for Minor, and 2 for Revision.
     * Pass null to get an array with all of the version parts.
     *
     * @return array|int
     */
    public static function GetFrameworkVersion($versionPart = null)
    {
        static $version = null;
        if (!$version)
        {
            $versionFile   = __DIR__ . DIRECTORY_SEPARATOR . ".wafl";
            if (file_exists($versionFile))
            {
                $waflMetaFile = file($versionFile);
                $version      = explode(".", trim($waflMetaFile[3]));
            }
        }
        return $versionPart!==null?$version[$versionPart]:$version;
    }

    protected static function _setupDefaultUser($app)
    {
        SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::BEFORE_USER_INSTANTIATION));
        if ($app)
        {
            $userClass = $app->Get_Settings()->Get_Application()->Get_DefaultUserClass();
        } else {
            $userClass = "\Wafl\Users\User";
        }
        self::$CURRENT_USER = User::GetGuestUser();
        SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_USER_INSTANTIATION));
    }

    protected static function _addFrameworkApiFolders($app)
    {
        if (defined("AM_WEBPAGE") && AM_WEBPAGE)
        {
            AjaxHandler::AddAjaxHandler("DblEj.Data.Model.UpdateModel", "UpdateDblEjDataModel", __DIR__ . DIRECTORY_SEPARATOR . "Api" . DIRECTORY_SEPARATOR . "DataModelHandlers.php", "\\Wafl\\Api\\DataModelHandlers", AjaxHandler::REQUEST_TYPE_GET, 0, false, null, $app);
            AjaxHandler::AddAjaxHandler("DblEj.Data.Model.GetModelByKey", "GetDblEjDataModelByKey", __DIR__ . DIRECTORY_SEPARATOR . "Api" . DIRECTORY_SEPARATOR . "DataModelHandlers.php", "\\Wafl\\Api\\DataModelHandlers", AjaxHandler::REQUEST_TYPE_GET, 0, false, null, $app);
            AjaxHandler::AddAjaxHandler("DblEj.Data.Model.DeleteModelByKey", "DeleteDblEjDataModelByKey", __DIR__ . DIRECTORY_SEPARATOR . "Api" . DIRECTORY_SEPARATOR . "DataModelHandlers.php", "\\Wafl\\Api\\DataModelHandlers", AjaxHandler::REQUEST_TYPE_GET, 0, false, null, $app);
            AjaxHandler::AddAjaxHandler("Wafl.SetTimezoneOffset", "SetTimezoneOffset", __DIR__ . DIRECTORY_SEPARATOR . "Api" . DIRECTORY_SEPARATOR . "DateTimeHandlers.php", "\\Wafl\\Api\\DateTimeHandlers", AjaxHandler::REQUEST_TYPE_GET, 0, false, null, $app);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Master Run State (Checked Clean)">
    private static $_AM_INITIALIZED = false;

    /**
     * Has a request come in that is yet to be handled?
     *
     * When a request comes in $_REQUEST_PENDING will be true until
     * it is set to false by some process that completely handles the request.
     *
     * It is used on shutdown to determine if the request was handled or not (usually due to an exception)
     *
     * @var boolean
     */
    private static $_REQUEST_PENDING = false;

    public static $_SendOutputAsEarlyAsPossible = true;

    public static function Get_RequestPending()
    {
        return self::$_REQUEST_PENDING;
    }

    public static function Set_RequestPending($requestIsPending)
    {
        self::$_REQUEST_PENDING = $requestIsPending;
    }
    /**
     * Has any output been sent to the client?
     * @var boolean
     */
    private static $_OUTPUT_HAS_BEEN_SENT = false;

    public static function Get_HasOutputBeenSent()
    {
        return self::$_OUTPUT_HAS_BEEN_SENT;
    }

    public static function Set_HasOutputBeenSent($hasBeenSent)
    {
        self::$_OUTPUT_HAS_BEEN_SENT = $hasBeenSent;
    }
    // </editor-fold>
}