<?php

namespace Wafl;

trait Core_Deprecated
{

    /**
     * The user that is currently using the application
     * @var \DblEj\Authentication\IUser
     */
    public static $CURRENT_USER;

    /**
     * The currently running application
     *
     * @todo Most of the statics in this file should be moved to be properties of the application
     * @todo Instead of having this global, Id like to pass the running application into controller actions and api handlers,
     * but right now the Application lives in Wafl not DblEj, so some thought needed still.
     *
     * @var \Wafl\Application\Application The currently running application
     */
    public static $RUNNING_APPLICATION;

    /**
     * The primary storage engine for this application
     * @var IStorageEngine
     */
    public static $STORAGE_ENGINE;

    public static function EscapeInput($string)
    {
        return self::$STORAGE_ENGINE->EscapeString($string);
    }

    public static function AssignHtmlVar($name, $value)
    {
        if (isset(self::$_TEMPLATE_RENDERER))
        {
            $smarty = self::$_TEMPLATE_RENDERER->GetUnderlyingEngine();
            $smarty->assign($name, $value);
        }
    }

    public static function AssignHtmlStatic($staticAlias, $fullyQualifiedStaticName)
    {
        if (!class_exists($fullyQualifiedStaticName))
        {
            __activeWaflClassLoader($fullyQualifiedStaticName);
        }
        if (!class_exists($fullyQualifiedStaticName))
        {
            __activeWaflAppSupportLoader($fullyQualifiedStaticName);
        }
        if (isset(self::$_TEMPLATE_RENDERER))
        {
            $smarty = self::$_TEMPLATE_RENDERER->GetUnderlyingEngine();
            $smarty->registerClass($staticAlias, $fullyQualifiedStaticName);
        }
    }

    public static function GetHtmlVar($name)
    {
        if (isset(self::$_TEMPLATE_RENDERER))
        {
            $smarty = self::$_TEMPLATE_RENDERER->GetUnderlyingEngine();
            return $smarty->get_template_vars($name);
        }
    }

    public static function GetHtml($templateName, $localData = null, $forceUtf8 = false)
    {
        if (substr($templateName, strlen($templateName) - 4) != ".tpl")
        {
            $templateName = "$templateName.tpl";
        }
        $app = static::$RUNNING_APPLICATION;
        $smarty = $app->Get_TemplateRenderer()->GetUnderlyingEngine();
        $html   = $smarty->fetch($templateName, null, null, $localData);
        if ($forceUtf8)
        {
            if (!mb_check_encoding($html, "UTF-8"))
            {
                $html = utf8_encode($html);
            }
        }
        return $html;
    }
}