<?php

namespace Wafl\DynamicResources;

class AppConfigJs
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_TEXT;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    function GetLastModifiedTime(\DblEj\Application\IApplication $app = null)
    {
        $localRoot       = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
        $localConfFolder = $localRoot . $app->Get_Settings()->Get_Paths()->Get_Application()->Get_Config();

        $appSettingsFile = $localConfFolder . "Settings." . WAFL_ENVIRONMENT . ".syrp";
        if (file_exists($appSettingsFile))
        {
            return filemtime($appSettingsFile);
        }
        else
        {
            return 0;
        }
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        $filenames      = array(
            "Settings/Settings.js");
        $confContents   = array();
        $waflFolder     = $app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_WaflFolder();
        $appFolder      = $app->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR;
        $siteDomainName = $app->Get_Settings()->Get_Web()->Get_DomainName();

        $portStartPos = stripos($siteDomainName, ":");
        if ($portStartPos !== false)
        {
            $siteDomainName = substr($siteDomainName, 0, $portStartPos);
        }
        foreach ($filenames as $fileName)
        {
            $contents       = file_get_contents($waflFolder . $fileName);
            $contents       = str_replace("{\$CORE::\$API_HANDLER_URL}", $app->Get_Settings()->Get_Web()->Get_ApiUrlSecure(), $contents);
            $contents       = str_replace("{\$CORE::\$API_HANDLER_URL_RELATIVE}", $app->Get_Settings()->Get_Web()->Get_ApiUrlRelative(), $contents);
            $contents       = str_replace("{\$CORE::\$WEB_ROOT_RELATIVE}", $app->Get_Settings()->Get_Web()->Get_WebUrlRelative(), $contents);
            $contents       = str_replace("{\$CORE::\$APP_FOLDER}", $appFolder, $contents);
            $contents       = str_replace("{\$CORE::\$MODELS_FOLDER}", \Wafl\Core::MODELS_FOLDER, $contents);
            $contents       = str_replace("{\$CORE::\$CLIENT_FUNCTIONAL_MODEL_FOLDER}", $app->Get_Settings()->Get_Paths()->Get_Application()->Get_FunctionalModelsClient(), $contents);
            $contents       = str_replace("{\$CORE::\$SITE_DOMAIN_NAME}", $siteDomainName, $contents);
            $confContents[] = $contents;
        }
        return $confContents;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }
}