<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication;
use Wafl\Util\Captcha;

class CaptchaImage
extends ResourceBase
{

    public function GetContents(IApplication $app = null)
    {
        ob_start();
        $waflFolder = realpath(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $fonts      = array();
        $fonts[0]   = $waflFolder . "Fonts/UglyQua.ttf";
        $fonts[1]   = $waflFolder . "Fonts/Walkway_Black.ttf";
        $fonts[2]   = $waflFolder . "Fonts/UglyQua.ttf";
        $fonts[3]   = $waflFolder . "Fonts/Airstream.ttf";
        Captcha::OutputCurrentCaptchaImage(3, $fonts);
        $imageBytes = ob_get_contents();
        ob_end_clean();
        return $imageBytes;
    }

    public function Get_IsBinary()
    {
        return true;
    }

    public function Get_MimeType()
    {
        return "image/gif";
    }

    public function Get_OutputModificationMethod()
    {
        return null;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return true;
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 0;
    }

    public function Get_ContentsType()
    {
        return ResourceBase::CONTENTS_TYPE_BINARY;
    }

    public function Get_Filename()
    {
        return "Captcha.gif";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function GetLastModifiedTime(IApplication $app = null)
    {
        return time();
    }

    public function Get_MinifyOutput()
    {
        return false;
    }

    public function Get_RenderKey1()
    {
        return null;
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return false;
    }
}