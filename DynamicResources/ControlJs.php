<?php

namespace Wafl\DynamicResources;

class ControlJs
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return isset($_REQUEST["PreProcess"]) ? $_REQUEST["PreProcess"] : false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        $className = "\\Wafl\\Controls\\".$this->Get_InstanceName()."\\".$this->Get_InstanceName();
        $templateRenderer = \Wafl\Util\Extensions::GetExtensionByType("\\DblEj\\Presentation\\Integration\\ITemplateRendererExtension");
        $control = new $className($templateRenderer, $this->Get_InstanceName());
        $files = [];
        foreach ($control->Get_Javascripts() as $controlJsFilenameInfo)
        {
            if (is_array($controlJsFilenameInfo))
            {
                $js = $controlJsFilenameInfo["File"];
                $minify = isset($controlJsFilenameInfo["Minify"])?($controlJsFilenameInfo["Minify"]?true:false):true;
            } else {
                $js = $controlJsFilenameInfo;
                $minify = true;
            }

            $localControlsFolder = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ControlsFolder()) . DIRECTORY_SEPARATOR;
            $controlJsFilename = $localControlsFolder . $this->Get_InstanceName() . DIRECTORY_SEPARATOR . $js;
            if (file_exists($controlJsFilename))
            {
                $files[] = ["File"=>$controlJsFilename, "Minify"=>$minify];
            }
        }
        return $files;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return "Control" . $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}