<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication;
use Wafl\Core;

class DataModelJs
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(IApplication $app = null)
    {
        $filenames = array();
        if ($app !== null)
        {
            if (is_a($app, "\\Wafl\\Application\\MvcWebApplication"))
            {
                if ($this->Get_InstanceName())
                {
                    $localRoot        = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
                    $appRootNamespace = $app->Get_Settings()->Get_Application()->Get_RootNameSpace();
                    if (\DblEj\Util\Strings::StartsWith($this->Get_InstanceName(), "CombinedModel"))
                    {
                        $dataModelFilename = $localRoot . $appRootNamespace . DIRECTORY_SEPARATOR . "Models" . DIRECTORY_SEPARATOR . "DataModel" . DIRECTORY_SEPARATOR . basename($this->Get_Filename());
                        $funcModelFilename = $localRoot . $appRootNamespace . DIRECTORY_SEPARATOR . "Models" . DIRECTORY_SEPARATOR . "FunctionalModel" . DIRECTORY_SEPARATOR . basename($this->Get_Filename());
                        if (file_exists($dataModelFilename))
                        {
                            $filenames[] = $dataModelFilename;
                        }
                        if (file_exists($funcModelFilename))
                        {
                            $filenames[] = $funcModelFilename;
                        }
                    }
                    else
                    {
                        $modelFilename = $localRoot . $appRootNamespace . DIRECTORY_SEPARATOR . "Models" . DIRECTORY_SEPARATOR . $this->Get_Filename();
                        if (file_exists($modelFilename))
                        {
                            $filenames[] = $modelFilename;
                        }
                    }
                }
            }
        }
        if (count($filenames) == 0)
        {
            $filenames = null;
        }
        return $filenames;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}