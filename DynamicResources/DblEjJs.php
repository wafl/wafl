<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication;

class DblEjJs
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(IApplication $app = NULL)
    {
        if ($app !== null)
        {
            $filenames   = array();
            $dblEjFolder = $app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_DblEjFolder();

            foreach ($app->GetClientIncludes() as $fileName)
            {
                $resolvedFilename = realpath($dblEjFolder . $fileName);
                if ($resolvedFilename)
                {
                    $filenames[] = $resolvedFilename;
                }
                else
                {
                    throw new \Exception("Could not load all of the specified includes.  Please double-check the contents of the Includes.syrp file.");
                }
            }
            return $filenames;
        }
        else
        {
            return null;
        }
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return "DblEjJs.".$this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}