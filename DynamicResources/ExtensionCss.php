<?php

namespace Wafl\DynamicResources;

class ExtensionCss
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/css";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Css";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return isset($_REQUEST["PreProcess"]) ? $_REQUEST["PreProcess"] : false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".css";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        $localExtensionsFolder = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder()) . DIRECTORY_SEPARATOR;
        $controlJsFilename = $localExtensionsFolder . $this->Get_InstanceName() . DIRECTORY_SEPARATOR . "Presentation" . DIRECTORY_SEPARATOR . "Stylesheets" . DIRECTORY_SEPARATOR . $this->Get_ContentReference();
        if (file_exists($controlJsFilename))
        {
            return $controlJsFilename;
        }
        else
        {
            return null;
        }
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return "Extension" . $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}