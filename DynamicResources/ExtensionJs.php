<?php

namespace Wafl\DynamicResources;

class ExtensionJs
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return isset($_REQUEST["PreProcess"]) ? $_REQUEST["PreProcess"] : false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        $localExtensionsFolder = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder()) . DIRECTORY_SEPARATOR;
        if (\DblEj\Util\Strings::StartsWith($this->Get_ContentReference(), "DataModel".DIRECTORY_SEPARATOR))
        {
            //extension data model
            $jsFilename = $localExtensionsFolder . $this->Get_InstanceName() . DIRECTORY_SEPARATOR . $this->Get_ContentReference();
        }
        elseif (\DblEj\Util\Strings::StartsWith($this->Get_ContentReference(), "FunctionalModel".DIRECTORY_SEPARATOR))
        {
            //extension functional model
            $jsFilename = $localExtensionsFolder . $this->Get_InstanceName() . DIRECTORY_SEPARATOR . $this->Get_ContentReference();
        }
        else
        {
            //extension controller
            $jsFilename = $localExtensionsFolder . $this->Get_InstanceName() . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . $this->Get_ContentReference();
        }
        if (file_exists($jsFilename))
        {
            return realpath($jsFilename);
        }
        else
        {
            return null;
        }
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        $contentRef = $this->Get_ContentReference();
        if (strlen($contentRef) > 200)
        {
            $contentRef = substr($contentRef, 0, 200);
        }
        $contentRef = str_replace("\\", "-", $contentRef);
        $contentRef = str_replace("/", "-", $contentRef);
        return "Extension" . $this->Get_InstanceName() . $contentRef;
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}