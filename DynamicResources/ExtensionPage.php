<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication,
    DblEj\Communication\Http\Headers,
    DblEj\Extension\ExtensionException,
    DblEj\SiteStructure\SitePage,
    DblEj\UI\Stylesheet,
    ReflectionClass,
    Wafl\Core,
    Wafl\Util\Extensions;

class ExtensionPage
extends ResourceBase
{

    public function __construct($extensionName, $contentReference, $cachePoint = Headers::CACHEPOINT_BROWSER)
    {
        parent::__construct($extensionName, $contentReference, $cachePoint);
    }

    public function GetContents(IApplication $app = null)
    {
        $ext = Extensions::GetExtension($this->Get_InstanceName());
        if ($ext)
        {
            $extClass       = new ReflectionClass($ext);
            $extClassName   = $extClass->getName();
            $extensionPages = $extClassName::Get_SitePages();
            $extPage        = $extensionPages[$this->Get_ContentReference()];
            if ($extPage)
            {
                $ext->PrepareSitePage($this->Get_ContentReference());
                $sitePage = new \DblEj\Mvc\SitePage($this->Get_InstanceName(), $this->Get_InstanceName(), $this->Get_InstanceName(), $this->Get_ContentReference(), $extPage->Get_TemplateName(), 1, $ext->Get_SiteAreaId(), false, $this->Get_InstanceName(), null, false, [], "Extension: ".$this->Get_InstanceName());
                foreach ($ext->Get_GlobalStylesheets() as $stylesheet)
                {
                    $templateStyleSheet = new Stylesheet($stylesheet->Get_SkinName(), Core::$WEB_ROOT_RELATIVE . "Extensions/" . $this->Get_InstanceName() . "/" . $stylesheet->Get_Filename());
                    $sitePage->AddCss($templateStyleSheet);
                }
                foreach ($ext->Get_GlobalScripts() as $script)
                {
                    $sitePage->AddJavascript(realpath(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . $this->_settings->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder() . $this->Get_InstanceName() . DIRECTORY_SEPARATOR . $script));
                }
                foreach ($extPage->Get_Stylesheets() as $stylesheet)
                {
                    $templateStyleSheet = new Stylesheet($stylesheet->Get_SkinName(), Core::$WEB_ROOT_RELATIVE . "Extensions/" . $this->Get_InstanceName() . "/" . $stylesheet->Get_Filename());
                    $sitePage->AddCss($templateStyleSheet);
                }
                foreach ($extPage->Get_Scripts() as $script)
                {
                    $sitePage->AddJavascript(realpath(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . $this->_settings->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder() . $this->Get_InstanceName() . DIRECTORY_SEPARATOR . $script));
                }

                $app->PreparePageForDisplay($sitePage);
                $templateRenderer = \Wafl\Util\Extensions::GetExtensionByType("\\DblEj\\Presentation\\Integration\\ITemplateRendererExtension");
                return $sitePage->Render($templateRenderer, \DblEj\Presentation\RenderOptions::GetDefaultInstance());
            }
            else
            {
                throw new ExtensionException("Trying to display a non-existent extension site page", 0, E_WARNING);
            }
        }
        else
        {
            throw new ExtensionException("Trying to display a site page for non-existent extension", 0, E_WARNING);
        }
    }

    public function Get_BrowserCacheRevalidate()
    {

    }

    public function Get_BrowserCacheTimeoutSeconds()
    {

    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function GetLastModifiedTime(IApplication $app = null)
    {
        return time();
    }

    public function Get_Filename()
    {

    }

    public function Get_IsBinary()
    {

    }

    public function Get_MimeType()
    {

    }

    public function Get_MinifyOutput()
    {

    }

    public function Get_OutputModificationMethod()
    {

    }

    public function Get_PreprocessWithViewRenderer()
    {

    }

    public function Get_RenderKey1()
    {

    }

    public function Get_RenderKey2()
    {

    }

    public function Get_UseServerSideCache()
    {

    }
}