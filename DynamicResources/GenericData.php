<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication,
    DblEj\Communication\Http\Headers;

class GenericData
extends ResourceBase
{
    private $_isBinary;
    private $_contentType; //from ResourceBase::CONTENTS_TYPE_...
    private $_mimeType;
    private $_revalidateCache;
    private $_cacheTimeout;
    private $_modifiedTime;
    private $_bytes;
    public function __construct($instanceName, $bytes, $mimeType, $cachePoint = Headers::CACHEPOINT_BROWSER, $contentType = ResourceBase::CONTENTS_TYPE_BINARY, $isBinary = true, $revalidateCache = false, $cacheTimeout = 0, \DblEj\Communication\Http\HeaderCollection $additionalHeaders = null)
    {
        parent::__construct($instanceName, $instanceName, $cachePoint, $additionalHeaders);
        $this->_isBinary        = $isBinary;
        $this->_contentType     = $contentType;
        $this->_mimeType        = $mimeType;
        $this->_revalidateCache = $revalidateCache;
        $this->_cacheTimeout    = $cacheTimeout;
        $this->_modifiedTime = time();
        $this->_bytes = $bytes;
    }

    public function GetHttpResponseCode($request)
    {
        if (isset($_SERVER["HTTP_RANGE"]))
        {
            $range = $_SERVER["HTTP_RANGE"];
            $range = str_replace("bytes=", "", $range);
            $rangeArray = explode("-", $range);
            $startByte = $rangeArray[0];
            $endByte = trim(isset($rangeArray[1])?$rangeArray[1]:null);
            if (!$startByte && !$endByte)
            {
                $responseCode = \DblEj\Communication\Http\Response::HTTP_OK_200;
            } else {
                $responseCode = \DblEj\Communication\Http\Response::HTTP_PARTIAL_206;
            }
        } else {
            $responseCode = \DblEj\Communication\Http\Response::HTTP_OK_200;
        }
        return $responseCode;
    }
    public function GetContents(IApplication $app = null)
    {
        return $this->_bytes;
    }

    public function Get_IsBinary()
    {
        return $this->_isBinary;
    }

    public function Get_MimeType()
    {
        return $this->_mimeType;
    }

    public function Get_OutputModificationMethod()
    {
        return null;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return $this->_revalidateCache;
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return $this->_cacheTimeout;
    }

    public function Get_ContentsType()
    {
        return $this->_contentType;
    }

    public function Get_Filename()
    {
        return $this->Get_ContentReference();
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    function GetLastModifiedTime(IApplication $app = null)
    {
       return $this->_modifiedTime;
    }

    public function Get_MinifyOutput()
    {
        return false;
    }

    public function Get_RenderKey1()
    {
        return null;
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return false;
    }
}