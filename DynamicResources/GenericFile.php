<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication,
    DblEj\Communication\Http\Headers;

class GenericFile
extends ResourceBase
{
    private $_isBinary;
    private $_contentType; //from ResourceBase::CONTENTS_TYPE_...
    private $_mimeType;
    private $_revalidateCache;
    private $_cacheTimeout;

    public function __construct($filename, $mimeType, $cachePoint = Headers::CACHEPOINT_BROWSER, $contentType = ResourceBase::CONTENTS_TYPE_BINARY, $isBinary = true, $revalidateCache = false, $cacheTimeout = 0, \DblEj\Communication\Http\HeaderCollection $additionalHeaders = null)
    {
        parent::__construct($filename, basename($filename), $cachePoint, $additionalHeaders);
        $this->_isBinary        = $isBinary;
        $this->_contentType     = $contentType;
        $this->_mimeType        = $mimeType;
        $this->_revalidateCache = $revalidateCache;
        $this->_cacheTimeout    = $cacheTimeout;

        //if file is too big and being treated as BINARY/TEXT, convert the type to FILE so it can be streamed out instead of being loaded fully into memory
        if (($this->_contentType != ResourceBase::CONTENTS_TYPE_FILE) && file_exists($filename) && (filesize($filename) > 64000))
        {
            $this->_contentType = ResourceBase::CONTENTS_TYPE_FILE;
        }
    }

    public function GetContents(IApplication $app = null)
    {
        $contents = null;
        if ($this->_contentType == ResourceBase::CONTENTS_TYPE_FILE)
        {
            $contents = $this->Get_InstanceName();
        }
        elseif (file_exists($this->Get_InstanceName()))
        {
            $contents = file_get_contents($this->Get_InstanceName());
        }
        return $contents;
    }

    public function Get_IsBinary()
    {
        return $this->_isBinary;
    }

    public function Get_MimeType()
    {
        return $this->_mimeType;
    }

    public function Get_OutputModificationMethod()
    {
        return null;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return $this->_revalidateCache;
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return $this->_cacheTimeout;
    }

    public function Get_ContentsType()
    {
        return $this->_contentType;
    }

    public function Get_Filename()
    {
        return $this->Get_ContentReference();
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function GetLastModifiedTime(IApplication $app = null)
    {
        if (is_file($this->Get_InstanceName()))
        {
            return filemtime($this->Get_InstanceName());
        }
        else
        {
            return 0;
        }
    }

    public function Get_MinifyOutput()
    {
        return false;
    }

    public function Get_RenderKey1()
    {
        return null;
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return false;
    }
}