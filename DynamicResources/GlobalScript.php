<?php

namespace Wafl\DynamicResources;

class GlobalScript
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return isset($_REQUEST["PreProcess"]) ? $_REQUEST["PreProcess"] : false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        if ($app !== null)
        {
            $localRoot        = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
            $appRootNamespace = $app->Get_Settings()->Get_Application()->Get_RootNameSpace();

            $globalScriptFile = $localRoot . $appRootNamespace . DIRECTORY_SEPARATOR . "GlobalScript.js";
            return file_exists($globalScriptFile) ? $globalScriptFile : null;
        }
        else
        {
            return null;
        }
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}