<?php

namespace Wafl\DynamicResources;

interface IResource
{

    public function Get_MimeType();

    public function Get_IsBinary();

    public function Get_OutputModificationMethod();

    public function Get_PreprocessWithViewRenderer();

    public function Get_ContentsType();

    public function Get_ContentReference();

    public function GetContents(\DblEj\Application\IApplication $app = null);

    public function Get_Filename();

    public function Get_CachePoint();

    public function Get_BrowserCacheTimeoutSeconds();

    public function Get_BrowserCacheRevalidate();

    public function Get_InstanceName();
}