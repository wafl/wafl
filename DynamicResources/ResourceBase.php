<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication,
    DblEj\Communication\Http\Headers,
    DblEj\Communication\Http\Response,
    DblEj\Communication\Http\Routing\IDestination,
    DblEj\Communication\Http\Util,
    DblEj\Minification\FileServer,
    DblEj\Presentation\RenderOptions,
    Exception,
    Wafl\Util\Template;

abstract class ResourceBase
implements IResource,
           IDestination
{
    const CONTENTS_TYPE_TEXT   = "CONTENTS_TYPE_TEXT";
    const CONTENTS_TYPE_FILE   = "CONTENTS_TYPE_FILE";
    const CONTENTS_TYPE_BINARY = "CONTENTS_TYPE_BINARY";

    private $_instanceName;
    private $_cachePoint;
    private $_contentReference;
    private $_headers;
    private $_preparedDestination = null;

    protected $renderingOptions;

    /**
     *
     * @param string $instanceName
     * @param mixed $contentReference a reference to the content (depends on Get_ContentsType)
     * @param int $cachePoint
     * @param Headers $additionalHeaders
     */
    public function __construct($instanceName, $contentReference, $cachePoint = Headers::CACHEPOINT_BROWSER, \DblEj\Communication\Http\HeaderCollection $additionalHeaders = null)
    {
        $this->_instanceName     = $instanceName;
        $this->_cachePoint       = $cachePoint;
        $this->_contentReference = $contentReference;
        $this->_headers = new Headers();
        if ($additionalHeaders)
        {
            foreach ($additionalHeaders as $additionalHeaderName=>$additionalHeaderValue)
            {
                $this->_headers->SetHeader($additionalHeaderName, $additionalHeaderValue);
            }
        }
    }

    public function PrepareDestination($request, IApplication $app)
    {
        $etag            = md5($this->GetLastModifiedTime($app));
        $response = null;

        if (Util::DoesClientAlreadyHaveTheLatest($this->GetLastModifiedTime($app), $etag))
        {
            $responseCode = Response::HTTP_FILE_NOT_CHANGED_304;
        }
        else
        {
            $responseCode = $this->GetHttpResponseCode($request);
        }
        $response = new Response(null, $responseCode, $this->_headers, Response::CONTENT_TYPE_PRINTABLE_OUTPUT);
        $response->Set_IsBinaryFile($this->Get_IsBinary());
        $response->Set_BrowserCacheTimeout($this->Get_CachePoint(), $this->Get_BrowserCacheTimeoutSeconds(), $this->Get_BrowserCacheRevalidate());
        $response->Set_MimeType($this->Get_MimeType());

        //setting the filename used to override Content-Disposition force download directives, so now we only set the file name if there is not a Content-Disposition header already set
        if (!$this->_headers->IsHeaderSet("Content-Disposition"))
        {
            $response->Set_Filename($this->Get_Filename());
        }
        $response->Set_LastModifiedTime($this->GetLastModifiedTime($app));
        $response->Set_ETag($etag);
        $this->_preparedDestination = $response;
        return $response;
    }
    public function GetRenderingOptions()
    {
        if (!$this->renderingOptions)
        {
            $this->renderingOptions = new RenderOptions($this->Get_MinifyOutput(), $this->Get_UseServerSideCache(), $this->Get_RenderKey1(), $this->Get_RenderKey2());
        }

        return $this->renderingOptions;
    }

    public abstract function Get_MinifyOutput();

    public abstract function Get_UseServerSideCache();

    public abstract function Get_RenderKey1();

    public abstract function Get_RenderKey2();

    public function GetHttpResponseCode($request)
    {
        return Response::HTTP_OK_200;
    }
    public function GotoDestination($request, IApplication $app = null)
    {
        $responseContent = "";
        if (!$this->_preparedDestination)
        {
            $this->_preparedDestination = $this->PrepareDestination($request, $app);
        }
        $response   = $this->_preparedDestination;

        if ($response->Get_ResponseCode() != Response::HTTP_FILE_NOT_CHANGED_304)
        {
            if ($this->Get_UseServerSideCache() && $app->Get_Settings()->Get_Application()->Get_CacheStoreId())
            {
                $store = $app->GetMemoryStore($app->Get_Settings()->Get_Application()->Get_CacheStoreId());
            } else {
                $store = null;
            }

            $storedResponse = null;
            if ($store)
            {
                $storedResponse = $store->GetData($this->Get_RenderKey1());
            }
            if (!$storedResponse)
            {
                $content      = $this->GetContents($app);
                if (!is_array($content) && isset($content))
                {
                    $content = array($content);
                }
                if (!is_array($content))
                {
                    $content = array();
                }
                if ($this->Get_ContentsType() == self::CONTENTS_TYPE_FILE)
                {
                    $minifyAnyFile = $this->Get_MinifyOutput();
                    foreach ($content as $fileInfo)
                    {
                        $fileName = is_array($fileInfo)?$fileInfo["File"]:$fileInfo; //support for file to be an array with filename + options
                        if (!$minifyAnyFile)
                        {
                            $minifyAnyFile = is_array($fileInfo) && isset($fileInfo["Minify"])?$fileInfo["Minify"]:false;
                        }
                        if (!file_exists($fileName))
                        {
                            throw new \Wafl\Exceptions\Exception("trying to get non-existent file resource: $fileName", E_ERROR, null, "Cannot return resource content because the file does not exist: " . basename($fileName));
                        }
                    }
                    if ($this->Get_PreprocessWithViewRenderer() || $minifyAnyFile)
                    {
                        $templateRenderer = \Wafl\Util\Extensions::GetExtensionByType("\\DblEj\\Presentation\\Integration\\ITemplateRendererExtension");
                        $responseContent = FileServer::GetMinifiedFileContent($content, $this->GetRenderingOptions(), $this->Get_MimeType(), $this->Get_Filename(), $this->GetLastModifiedTime($app), $this->Get_BrowserCacheTimeoutSeconds() > 0, $this->Get_BrowserCacheTimeoutSeconds(), $this->Get_OutputModificationMethod(), $this->Get_PreprocessWithViewRenderer(), $templateRenderer, $store);
                        $response->Set_ContentType(Response::CONTENT_TYPE_PRINTABLE_OUTPUT);
                    } else {
                        foreach ($content as $fileInfo)
                        {
                            if ($responseContent)
                            {
                                $responseContent .= ";";
                            }
                            $responseContent .= is_array($fileInfo)?$fileInfo["File"]:$fileInfo;
                        }
                        $response->Set_ContentType(Response::CONTENT_TYPE_FILE);
                    }
                }
                elseif ($this->Get_ContentsType() == self::CONTENTS_TYPE_TEXT)
                {
                    $templateRenderer = \Wafl\Util\Extensions::GetExtensionByType("\\DblEj\\Presentation\\Integration\\ITemplateRendererExtension");
                    $responseContent = FileServer::GetMinifiedContent($content, $this->GetRenderingOptions(), $this->Get_OutputModificationMethod(), $this->Get_PreprocessWithViewRenderer(), $templateRenderer);
                    $response->Set_ContentType(Response::CONTENT_TYPE_PRINTABLE_OUTPUT);
                }
                elseif ($this->Get_ContentsType() == self::CONTENTS_TYPE_BINARY)
                {
                    $combinedContents = "";
                    foreach ($content as $contentFragment)
                    {
                        $combinedContents.=$contentFragment;
                    }
                    $responseContent = $combinedContents;
                    $response->Set_ContentType(Response::CONTENT_TYPE_PRINTABLE_OUTPUT);
                }
                $response->Set_Content($responseContent);
            } else {
                $response = $storedResponse;
            }
        }
        if ($response->Get_ResponseCode() == Response::HTTP_OK_200)
        {
            if ($this->Get_UseServerSideCache())
            {
                $store = $app->GetMemoryStore($app->Get_Settings()->Get_Application()->Get_CacheStoreId());
                if ($store)
                {
                    $store->SetData($this->Get_RenderKey1(), $response);
                }
            }
        }
        return $response;
    }

    public function Get_CachePoint()
    {
        return $this->_cachePoint;
    }

    public function Set_CachePoint($cachePoint)
    {
        $this->_cachepoint = $cachePoint;
    }

    public function GetLastModifiedTime(IApplication $app = null)
    {
        if ($this->Get_ContentsType() == self::CONTENTS_TYPE_FILE)
        {
            $contents           = $this->GetContents($app);
            $newestModification = 0;
            if (is_array($contents))
            {
                foreach ($contents as $fileInfo)
                {
                    $fileName = is_array($fileInfo)?$fileInfo["File"]:$fileInfo; //support for file to be an array with filename + options
                    $fileModTime = filemtime($fileName);
                    if ($fileModTime > $newestModification)
                    {
                        $newestModification = $fileModTime;
                    }
                }
            }
            else
            {
                $newestModification = filemtime($contents);
            }
            return $newestModification;
        }
        else
        {
            throw new \Exception("Subclasses of ResourceBase (in this case: " . get_called_class() . ") who return non-file-types must override GetLastModifiedTime()");
        }
    }

    public function Get_InstanceName()
    {
        return $this->_instanceName;
    }

    public function Get_ContentReference()
    {
        return $this->_contentReference;
    }
}