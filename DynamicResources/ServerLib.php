<?php
/* copyright(c) 2009 dlc Software Solutions, LLC
 * WAFL_VERSION_PLACEHOLDER
 * http://www.activewafl.com
 * LICENSE_TEXT_PLACEHOLDER
 */
define('AM_WEBPAGE', false);
$zipFileName = \Wafl\Util\Counterpart::GetDataModel(true, \Wafl\Core::$ROOT_NAMESPACE . "Data");
\DblEj\Communication\Http\Util::ServeFile($zipFileName, "application/zip", true);
?>