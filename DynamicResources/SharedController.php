<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication;
use Wafl\Core;

class SharedController
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(IApplication $app = null)
    {
        $filenames = array();
        if ($app !== null)
        {
            if (is_a($app, "\\Wafl\\Application\\WebApplication") || is_subclass_of($app, "\\Wafl\\Application\\WebApplication"))
            {
                if ($this->Get_InstanceName())
                {
                    $filenames        = array();
                    $localRoot        = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
                    $serverFile       = $this->Get_ContentReference();
                    $fileinfo         = pathinfo($serverFile);
                    $appRootNamespace = $app->Get_Settings()->Get_Application()->Get_RootNameSpace();

                    if ($fileinfo['extension'] == "js" || $fileinfo['extension'] == "php")
                    {
                        $serverFile = $fileinfo['filename'].".".$fileinfo['extension'];
                        if (\DblEj\Util\Strings::StartsWith($serverFile, "."))
                        {
                            $serverFile = substr($serverFile, 1);
                        }
                        if (\DblEj\Util\Strings::StartsWith($serverFile, DIRECTORY_SEPARATOR))
                        {
                            $serverFile = substr($serverFile, 1);
                        }
                        $jsFile = $localRoot.$appRootNamespace.DIRECTORY_SEPARATOR."Controllers".DIRECTORY_SEPARATOR."Shared".DIRECTORY_SEPARATOR.$serverFile;
                        if (file_exists($jsFile))
                        {
                            $filenames[] = $jsFile;
                        }
                    }
                }
            }
        }
        if (count($filenames) == 0)
        {
            $filenames = null;
        }
        return $filenames;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}