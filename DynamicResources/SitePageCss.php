<?php

namespace Wafl\DynamicResources;

class SitePageCss
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/css";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Css";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".css";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        $filenames = null;
        if ($app !== null)
        {
            $localRoot = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
            $cssFolder = $app->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR . $app->Get_Settings()->Get_Paths()->Get_Application()->Get_Presentation() . $app->Get_Settings()->Get_Paths()->Get_Application()->Get_Stylesheets();

            $filenames = array();
            if ($this->Get_InstanceName())
            {
                $serverFile = $this->Get_ContentReference();
                if (\DblEj\Util\Strings::StartsWith($serverFile, "/"))
                {
                    $serverFile = substr($serverFile, 1);
                }
                $cssFile = $localRoot . $cssFolder . $serverFile;
                if (file_exists($cssFile))
                {
                    $filenames[] = $cssFile;
                }
            }
        }
        if (count($filenames) == 0)
        {
            $filenames = null;
        };
        return $filenames;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}