<?php

namespace Wafl\DynamicResources;

use DblEj\Application\IApplication;
use Wafl\Core;

class SitePageJs
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return $this->Get_InstanceName() . ".js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(IApplication $app = null)
    {
        $filenames = array();
        if ($app !== null)
        {
            if (is_a($app, "\\Wafl\\Application\\WebApplication") || is_subclass_of($app, "\\Wafl\\Application\\WebApplication"))
            {
                if ($this->Get_InstanceName())
                {
                    $filenames        = array();
                    $localRoot        = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
                    $dblEjFolder = $app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_DblEjFolder();
                    $serverFile       = $this->Get_ContentReference();
                    $fileinfo         = pathinfo($serverFile);
                    $serverFile = $fileinfo['dirname'] . DIRECTORY_SEPARATOR . $fileinfo['filename'];
                    if (\DblEj\Util\Strings::StartsWith($serverFile, "."))
                    {
                        $serverFile = substr($serverFile, 1);
                    }
                    if (\DblEj\Util\Strings::StartsWith($serverFile, DIRECTORY_SEPARATOR))
                    {
                        $serverFile = substr($serverFile, 1);
                    }
                    $sitePage = $app->GetSitePageByFilename($serverFile);
                    $app->PreparePageForDisplay($sitePage);
                    foreach ($sitePage->Get_JavascriptIncludesLib() as $fileName)
                    {
                        if (file_exists($app->Get_LocalLogicFolder() . $fileName))
                        {
                            $filenames[] = $app->Get_LocalLogicFolder() . $fileName;
                        }
                    }

                    foreach ($sitePage->GetJavascriptIncludesDblEj() as $fileName)
                    {
                        $resolvedFilename = realpath($localRoot . $dblEjFolder . $fileName);
                        if ($resolvedFilename)
                        {
                            $filenames[] = $resolvedFilename;
                        }
                        else
                        {
                            throw new \Exception("Could not load all of the specified includes.  Please double-check the contents of the Includes.syrp file.");
                        }
                    }
                }
            }
        }
        if (count($filenames) == 0)
        {
            $filenames = null;
        }
        return $filenames;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }
}