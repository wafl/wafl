<?php

namespace Wafl\DynamicResources;

class SitepageControlsCss
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/css";
    }

    public function Get_OutputModificationMethod()
    {
        return null;
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return "SitewideControls.css";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        $fileArray = array();
        $localControlsPath = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ControlsFolder()) . DIRECTORY_SEPARATOR;
        $sitePage = $this->Get_ContentReference();
        foreach ($sitePage->GetControls() as $usedControl)
        {
            $fullyQualifiedControlName = get_class($usedControl);
            $controlName = basename(str_replace("\\", DIRECTORY_SEPARATOR, $fullyQualifiedControlName));

            $controlStylesheet         = $fullyQualifiedControlName::Get_MainStylesheet();

            if ($controlStylesheet)
            {
                $fullFilename      = $localControlsPath . $controlName . DIRECTORY_SEPARATOR . $controlStylesheet->Get_Filename();
                if (file_exists($fullFilename))
                {
                    $fileArray[] = $fullFilename;
                }
            }
        }
        return $fileArray;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }
}