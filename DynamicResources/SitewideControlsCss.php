<?php

namespace Wafl\DynamicResources;

class SitewideControlsCss
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/css";
    }

    public function Get_OutputModificationMethod()
    {
        return null;
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return "SitewideControls.css";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        $fileArray = array();
        foreach (\Wafl\Util\Controls::GetPreregisteredControls() as $usedControl)
        {
            $fullyQualifiedControlName = $usedControl[1] . "\\" . $usedControl[0] . "\\" . $usedControl[0];
            $controlStylesheet         = $fullyQualifiedControlName::Get_MainStylesheet();

            if ($controlStylesheet)
            {
                $serverFile = $this->Get_ContentReference();
                if (\DblEj\Util\Strings::StartsWith($serverFile, "/"))
                {
                    $serverFile = substr($serverFile, 1);
                }
                $localControlsPath = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ControlsFolder()) . DIRECTORY_SEPARATOR;
                $fullFilename      = $localControlsPath . $usedControl[0] . DIRECTORY_SEPARATOR . $controlStylesheet->Get_Filename();
                if (file_exists($fullFilename))
                {
                    $fileArray[] = $fullFilename;
                }
            }
        }
        return $fileArray;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }
}