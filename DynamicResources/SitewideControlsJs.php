<?php

namespace Wafl\DynamicResources;

class SitewideControlsJs
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/javascript";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Javascript";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return isset($_REQUEST["PreProcess"]) ? $_REQUEST["PreProcess"] : false;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return "SitewideControls.js";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        $fileArray         = array();
        $localControlsPath = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ControlsFolder()) . DIRECTORY_SEPARATOR;
        foreach (\Wafl\Util\Controls::GetPreregisteredControls() as $usedControl)
        {
            $fullyQualifiedControlName = $usedControl[1] . "\\" . $usedControl[0] . "\\" . $usedControl[0];

            $controlJsFilenames        = $fullyQualifiedControlName::Get_Javascripts();
            foreach ($controlJsFilenames as $controlJsFilenameInfo)
            {
                if (is_array($controlJsFilenameInfo))
                {
                    $controlJsFilename = $controlJsFilenameInfo["File"];
                    $minify = isset($controlJsFilenameInfo["Minify"])?($controlJsFilenameInfo["Minify"]?true:false):(isset($usedControl[2])?($usedControl[2]?true:false):true);
                } else {
                    $controlJsFilename = $controlJsFilenameInfo;
                    $minify = isset($usedControl[2])?($usedControl[2]?true:false):true;
                }
                $fullFilename = $localControlsPath . $usedControl[0] . DIRECTORY_SEPARATOR . $controlJsFilename;
                if (file_exists($fullFilename))
                {
                    $fileArray[] = ["File"=>$fullFilename, "Minify"=>$minify];
                }
            }
        }
        return $fileArray;
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }
}