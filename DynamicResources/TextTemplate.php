<?php
namespace Wafl\DynamicResources;
use Wafl\DynamicResources\GenericFile;

class TextTemplate extends GenericFile
{
    public function Get_PreprocessWithViewRenderer()
    {
        return true;
    }

    public function GetRenderingOptions()
    {
        $options = parent::GetRenderingOptions();
        $app = \Wafl\Core::$RUNNING_APPLICATION;
        $options->AddToken($app->Get_ReplacementTokens());
        return $options;
    }
}