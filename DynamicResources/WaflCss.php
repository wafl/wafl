<?php

namespace Wafl\DynamicResources;

class WaflCss
extends ResourceBase
{

    public function Get_IsBinary()
    {
        return false;
    }

    public function Get_MimeType()
    {
        return "text/css";
    }

    public function Get_OutputModificationMethod()
    {
        return "\\DblEj\\Minification\\Css";
    }

    public function Get_PreprocessWithViewRenderer()
    {
        return true;
    }

    public function Get_ContentsType()
    {
        return self::CONTENTS_TYPE_FILE;
    }

    public function Get_Filename()
    {
        return "Wafl.css";
    }

    public function Get_BrowserCacheTimeoutSeconds()
    {
        return 2592000;
    }

    public function GetContents(\DblEj\Application\IApplication $app = null)
    {
        if ($app !== null)
        {
            $waflFolder = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;
            $rootPath   = $waflFolder . "Css" . DIRECTORY_SEPARATOR;



            $fileArray = array(
                $rootPath . "-Base.css",
                $rootPath . "Base-Abbr.css",
                $rootPath . "Base-Address.css",
                $rootPath . "Base-Anchor.css",
                $rootPath . "Base-Button.css",
                $rootPath . "Base-Citation.css",
                $rootPath . "Base-Code.css",
                $rootPath . "Base-DefinitionList.css",
                $rootPath . "Base-Figure.css",
                $rootPath . "Base-FormElements.css",
                $rootPath . "Base-HRule.css",
                $rootPath . "Base-List.css",
                $rootPath . "Base-Multimedia.css",
                $rootPath . "Base-Pre.css",
                $rootPath . "Base-Table.css",
                $rootPath . "Base-Type-Blockquote.css",
                $rootPath . "Base-Type-Headings.css",
                $rootPath . "Base-Type-Paragraph.css",
                $rootPath . "Base-Type-Quote.css",
                $rootPath . "Base-Type-Text.css",
                $rootPath . "Container-Actions.css",
                $rootPath . "Container-ControlGroup.css",
                $rootPath . "Container-Controls.css",
                $rootPath . "Container-FloatContainers.css",
                $rootPath . "Container-Sidebar.css",
                $rootPath . "Indication-Capability.css",
                $rootPath . "Indication-Context.css",
                $rootPath . "Indication-States.css",
                $rootPath . "Indication-Visibility.css",
                $rootPath . "Layout-Modifiers.css",
                $rootPath . "Layout-Structure.css",
                $rootPath . "Mark-Badges.css",
                $rootPath . "Mark-Glyphs.css",
                $rootPath . "TemplatePlugin-CodeContainer.css",
                $rootPath . "TemplatePlugin-Console.css",
                $rootPath . "Ui-Alert.css",
                $rootPath . "Ui-Breadcrumb.css",
                $rootPath . "Ui-Dialog.css",
                $rootPath . "Ui-DebugPanel.css",
                $rootPath . "Ui-HeaderLabel.css",
                $rootPath . "Ui-Inset.css",
                $rootPath . "Ui-Menu.css",
                $rootPath . "Ui-Notification.css",
                $rootPath . "Ui-Pagination.css",
                $rootPath . "Ui-Panel.css",
                $rootPath . "Ui-PrependAppend.css",
                $rootPath . "Ui-ProgressBar.css",
                $rootPath . "Ui-Section.css",
                $rootPath . "Ui-Tile.css",
                $rootPath . "Ui-Toolbar.css",
                $rootPath . "Ui-Tooltip.css"
            );


            foreach (\Wafl\Util\Controls::GetPreregisteredControls() as $usedControl)
            {
                $fullyQualifiedControlName = $usedControl[1] . "\\" . $usedControl[0] . "\\" . $usedControl[0];
                $controlStylesheet         = $fullyQualifiedControlName::Get_MainStylesheet();

                if ($controlStylesheet)
                {
                    $serverFile = $this->Get_ContentReference();
                    if (\DblEj\Util\Strings::StartsWith($serverFile, "/"))
                    {
                        $serverFile = substr($serverFile, 1);
                    }
                    $localControlsPath = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ControlsFolder()) . DIRECTORY_SEPARATOR;
                    $fullFilename      = $localControlsPath . $usedControl[0] . DIRECTORY_SEPARATOR . $controlStylesheet->Get_Filename();
                    if (file_exists($fullFilename))
                    {
                        $fileArray[] = $fullFilename;
                    }
                }
            }

            return $fileArray;
        }
        else
        {
            return null;
        }
    }

    public function Get_BrowserCacheRevalidate()
    {
        return false;
    }

    public function Get_MinifyOutput()
    {
        return true;
    }

    public function Get_RenderKey1()
    {
        return $this->Get_Filename();
    }

    public function Get_RenderKey2()
    {
        return null;
    }

    public function Get_UseServerSideCache()
    {
        return true;
    }
}