<?php
namespace Wafl\Exceptions;

class Exception
extends \DblEj\System\Exception
implements IException
{
    function __construct($message, $severity = E_WARNING, \Exception $innerException = null, $publicDetails = null)
    {
        parent::__construct($message, $severity, $innerException, $publicDetails);
    }
}