<?php

namespace Wafl\Exceptions;

class FormValidatorException
extends Exception
{

    function __construct($message, $severity = E_WARNING, \Exception $innerException = null)
    {
        $completeMessage = "There was an error validating the form&emsp;$message";
        parent::__construct($completeMessage, $severity, $innerException);
    }
}