<?php

namespace Wafl\Exceptions;

class FormValidatorFieldException
extends FormValidatorException
{

    function __construct($formFieldName, $message, $severity = E_WARNING, \Exception $innerException = null)
    {
        $completeMessage = "There was an error validating the field \"$formFieldName\".&emsp;$message";
        parent::__construct($completeMessage, $severity, $innerException);
    }
}