Namespace("Wafl.Exceptions");

Wafl.Exceptions.FriendlyException = DblEj.Data.Model.extend(
{
    init: function()
    {
        this._super();
        this.ServerObjectName = "\\Wafl\\Exceptions\\FriendlyException";
        this.ErrorMessage = null;
    },

    Get_ClientId: function()
    {
        return 1;
    },

    Get_ErrorMessage: function()
    {
        return this.ErrorMessage;
    },
    Set_ErrorMessage: function(newValue)
    {
        if (this.ErrorMessage !== newValue)
        {
            this.ErrorMessage = newValue;
            this.ModelChanged("ErrorMessage");
        }
        return this;
    }
});