<?php
namespace Wafl\Exceptions;

class FriendlyException
extends \Wafl\Exceptions\Exception
implements \DblEj\Communication\IJsonInterfacable
{
    private $_externalMessage = null;

    public function __construct($internalMessage, $externalMessage, $severity = E_WARNING, \Exception $innerException = null)
    {
        parent::__construct($internalMessage, $severity, $innerException, $externalMessage);
        $this->_externalMessage = $externalMessage;
    }

    public function Get_ExternalMessage()
    {
        return $this->_externalMessage;
    }

    public function Get_ClientObjectName()
    {
        return "Wafl.Exceptions.FriendlyException";
    }

    public function Get_SerializableFieldValues($forceUtf8Strings = false, $reindexObjectArrays = false)
    {
        return ["ErrorMessage"=>$this->_externalMessage];
    }

    public function Set_SerializableFieldValues(array $fieldValues)
    {
        $this->_externalMessage = $fieldValues["ErrorMessage"];
    }

    public function ToJson($reindexObjectArrays = true)
    {
        return \DblEj\Communication\JsonUtil::EncodeJson($this, true, $reindexObjectArrays);
    }

    public static function CreateInstanceFromJsonArray(array $jsonArray)
    {
        $ex = new FriendlyException();
        $ex->Set_SerializableFieldValues($jsonArray);
        return $ex;
    }

    public static function Get_SerializableFieldNames()
    {
        return ["ErrorMessage"];
    }

    public function Get_ClientObjectVersion()
    {
        return "0.3";
    }
}

