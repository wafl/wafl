<?php
namespace Wafl\Exceptions;

interface IException
{
    function Get_ErrorMessage();
    function Get_PublicDetails();
}