<?php
require_once(__DIR__ . DIRECTORY_SEPARATOR . "GlobalStatic_Deprecated.php");

function DieR($expression, $maxDepth = 10, $asHtml = null, $hidePrivateMembers = true, $includeBacktrace = false)
{
    if ($asHtml === null)
    {
        $asHtml = stristr(php_sapi_name(), "cli") === false;
    }
    return \DblEj\Util\Debug::DieR($expression, $maxDepth, $asHtml, $hidePrivateMembers, $includeBacktrace);
}
function PrintR($expression, $maxDepth = 10, $asHtml = null, $hidePrivateMembers = true, $includeBacktrace = false)
{
    if ($asHtml === null)
    {
        $asHtml = stristr(php_sapi_name(), "cli") === false;
    }
    return \DblEj\Util\Debug::PrintR($expression, $maxDepth, $asHtml, $hidePrivateMembers, $includeBacktrace);
}
if (!function_exists("shm_attach"))
{
    function shm_attach($key, $size=32767)
    {
        $memId = shmop_open($key, "c", 0664, $size);
        return $memId;
    }

    function shm_get_var($memid, $key)
    {
        $memData = shmop_read($memid, 0, shmop_size($memid));
        $dataTermLoc = strpos($memData, "\0");
        if (!$dataTermLoc)
        {
            $dataTermLoc = strlen($memData);
        }
        $memData = substr($memData, 0, $dataTermLoc);
        if ($memData)
        {
            $memDataObject = unserialize($memData);
        } else {
            $memDataObject = [];
        }
        return isset($memDataObject[$key])?$memDataObject[$key]:null;
    }

    function shm_has_var($memid, $key)
    {
        $memData = shmop_read($memid, 0, shmop_size($memid));
        $dataTermLoc = strpos($memData, "\0");
        if (!$dataTermLoc)
        {
            $dataTermLoc = strlen($memData);
        }
        $memData = substr($memData, 0, $dataTermLoc);
        if ($memData)
        {
            $memDataObject = unserialize($memData);
        } else {
            $memDataObject = [];
        }
        return isset($memDataObject[$key]);
    }

    function shm_put_var($memid, $variable_key, $variable)
    {
        $memData = shmop_read($memid, 0, shmop_size($memid));
        $dataTermLoc = strpos($memData, "\0");
        if (!$dataTermLoc)
        {
            $dataTermLoc = strlen($memData);
        }
        $memData = substr($memData, 0, $dataTermLoc);
        if ($memData)
        {
            $memDataObject = unserialize($memData);
        } else {
            $memDataObject = [];
        }
        $memDataObject[$variable_key] = $variable;
        return (shmop_write($memid, serialize($memDataObject)."\0", 0)) !== false;
    }
    function shm_remove_var($memid, $variable_key)
    {
        $memData = shmop_read($memid, 0, shmop_size($memid));
        $dataTermLoc = strpos($memData, "\0");
        if (!$dataTermLoc)
        {
            $dataTermLoc = strlen($memData);
        }
        $memData = substr($memData, 0, $dataTermLoc);
        if ($memData)
        {
            $memDataObject = unserialize($memData);
        } else {
            $memDataObject = [];
        }
        if (isset($memDataObject[$variable_key]))
        {
            unset($memDataObject[$variable_key]);
        }
        return shmop_write($memid, serialize($memDataObject)."\0", 0) !== false;
    }
    function shm_detach($shm_identifier)
    {
        shmop_close($shm_identifier);
        return true;
    }
    function shm_remove($shm_identifier)
    {
        return shmop_delete($shm_identifier);
    }
}