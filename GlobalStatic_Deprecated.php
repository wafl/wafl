<?php
namespace Wafl;

/**
 * Get the value of a "request" variable (get, post, session, cookie, server)
 * @deprecated since revision 499 (use the HttpRequest object->GetInput() in the controller action instead.  Or use Http\Util::GetInput to access directly.)
 *
 * @param string $varName
 * @param mixed $noExistValue
 * @param boolean $escapeInput
 * @return string
 */
function RequestVar($varName, $noExistValue = null, $escapeInput = false)
{
    if ($escapeInput)
    {
        return isset($_REQUEST[$varName]) ? Core::EscapeInput($_REQUEST[$varName]) : $noExistValue;
    }
    else
    {
        return isset($_REQUEST[$varName]) ? $_REQUEST[$varName] : $noExistValue;
    }
}

/**
 * Get the value of an http GET variable
 * @deprecated since revision 499
 *  (use the HttpRequest object->GetInput() in the controller action instead.
 *  Or use Http\Util::GetInput to access directly.)
 *
 * @param string $varName
 * @param mixed $noExistValue
 * @param boolean $escapeInput
 * @return string
 *
 * @param string $varName
 * @param mixed $noExistValue
 * @return string
 */
function GetVar($varName, $noExistValue = null)
{
    return isset($_GET[$varName]) ? $_REQUEST[$varName] : $noExistValue;
}

/**
 * Get the value of an http POST variable
 * @deprecated since revision 499
 *  (use the HttpRequest object->GetInput() in the controller action instead.
 *  Or use Http\Util::GetInput to access directly.)
 *
 * @param string $varName
 * @param mixed $noExistValue
 * @param boolean $escapeInput
 * @return string
 *
 * @param string $varName
 * @param mixed $noExistValue
 * @return string
 */
function PostVar($varName, $noExistValue = null)
{
    return isset($_POST[$varName]) ? $_REQUEST[$varName] : $noExistValue;
}