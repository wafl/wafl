<?php

namespace Wafl\Internationalization;

class ArrayTranslator
implements \DblEj\Internationalization\ITranslator
{
    private $_lookupTable;

    public function __construct($array)
    {
        $this->_lookupTable = $array;
    }

    public function GetTranslatedText($textName)
    {
        foreach ($this->_lookupTable as $vals)
        {
            if ($vals[0] == $textName)
            {
                return $vals[1];
            }
        }
        return $textName;
    }
}