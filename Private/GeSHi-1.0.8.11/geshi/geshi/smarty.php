<?php
/*************************************************************************************
 * smarty.php
 * ----------
 * Author: Alan Juden (alan@judenware.org)
 * Copyright: (c) 2004 Alan Juden, Nigel McNie (http://qbnz.com/highlighter/)
 * Release Version: 1.0.8.11
 * Date Started: 2004/07/10
 * 
 *
 * CHANGES
 * -------
 * 2004/11/27 (1.0.0)
 *  -  Initial Release
 *
 * Modified: 2014/08/09 to remove deemphasis of the html in the result wafl.org
 * Smarty template language file for GeSHi.
 *
 * TODO
 * ----
 *
 *************************************************************************************
 *
 *     This file is part of GeSHi.
 *
 *   GeSHi is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   GeSHi is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with GeSHi; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ************************************************************************************/

$language_data = array (
    'LANG_NAME' => 'Smarty',
    'COMMENT_SINGLE' => array(),
    'COMMENT_MULTI' => array('{*' => '*}'),
    'CASE_KEYWORDS' => GESHI_CAPS_NO_CHANGE,
    'QUOTEMARKS' => array("'", '"'),
    'ESCAPE_CHAR' => '\\',
    'KEYWORDS' => array(
        1 => array(
            '$smarty', 'now', 'const', 'capture', 'config', 'section', 'foreach', 'template', 'version', 'ldelim', 'rdelim',
            'foreachelse', 'include', 'include_php', 'insert', 'if', 'elseif', 'else', 'php',
            'sectionelse', 'is_cached',
            ),
        2 => array(
            'capitalize', 'count_characters', 'cat', 'count_paragraphs', 'count_sentences', 'count_words', 'date_format',
            'default', 'escape', 'indent', 'lower', 'nl2br', 'regex_replace', 'replace', 'spacify', 'string_format',
            'strip', 'strip_tags', 'truncate', 'upper', 'wordwrap',
            ),
        3 => array(
            'counter', 'cycle', 'debug', 'eval', 'html_checkboxes', 'html_image', 'html_options',
            'html_radios', 'html_select_date', 'html_select_time', 'html_table', 'math', 'mailto', 'popup_init',
            'popup', 'textformat'
            ),
        4 => array(
            '$template_dir', '$compile_dir', '$config_dir', '$plugins_dir', '$debugging', '$debug_tpl',
            '$debugging_ctrl', '$autoload_filters', '$compile_check', '$force_compile', '$caching', '$cache_dir',
            '$cache_lifetime', '$cache_handler_func', '$cache_modified_check', '$config_overwrite',
            '$config_booleanize', '$config_read_hidden', '$config_fix_newlines', '$default_template_handler_func',
            '$php_handling', '$security', '$secure_dir', '$security_settings', '$trusted_dir', '$left_delimiter',
            '$right_delimiter', '$compiler_class', '$request_vars_order', '$request_use_auto_globals',
            '$error_reporting', '$compile_id', '$use_sub_dirs', '$default_modifiers', '$default_resource_type'
            ),
        5 => array(
            'append', 'append_by_ref', 'assign', 'assign_by_ref', 'clear_all_assign', 'clear_all_cache',
            'clear_assign', 'clear_cache', 'clear_compiled_tpl', 'clear_config', 'config_load', 'display',
            'fetch', 'get_config_vars', 'get_registered_object', 'get_template_vars',
            'load_filter', 'register_block', 'register_compiler_function', 'register_function',
            'register_modifier', 'register_object', 'register_outputfilter', 'register_postfilter',
            'register_prefilter', 'register_resource', 'trigger_error', 'template_exists', 'unregister_block',
            'unregister_compiler_function', 'unregister_function', 'unregister_modifier', 'unregister_object',
            'unregister_outputfilter', 'unregister_postfilter', 'unregister_prefilter', 'unregister_resource'
            ),
        6 => array(
            'name', 'file', 'scope', 'global', 'key', 'once', 'script',
            'loop', 'start', 'step', 'max', 'show', 'values', 'value', 'from', 'item'
            ),
        7 => array(
            'eq', 'neq', 'ne', 'lte', 'gte', 'ge', 'le', 'not', 'mod'
            ),
        8 => array(
            // php functions
            'isset', 'is_array', 'empty', 'count', 'sizeof'
            ),
        9 => array(
            //html elements
            'a', 'abbr', 'address', 'article', 'area', 'aside', 'audio',
            'base', 'bdo', 'blockquote', 'body', 'br', 'button', 'b',
            'caption', 'cite', 'code', 'colgroup', 'col', 'canvas', 'command', 'datalist', 'details',
            'dd', 'del', 'dfn', 'div', 'dl', 'dt',
            'em', 'embed',
            'fieldset', 'form', 'figcaption', 'figure', 'footer',
            'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'hr', 'html', 'header', 'hgroup',
            'iframe', 'ilayer', 'img', 'input', 'ins', 'isindex', 'i',
            'kbd', 'keygen',
            'label', 'legend', 'link', 'li',
            'map', 'meta', 'mark', 'meter',
            'noscript', 'nav',
            'object', 'ol', 'optgroup', 'option', 'output',
            'param', 'pre', 'p', 'progress',
            'q',
            'rp', 'rt', 'ruby',
            'samp', 'script', 'select', 'small', 'span', 'strong', 'style', 'sub', 'sup', 's', 'section', 'source', 'summary',
            'table', 'tbody', 'td', 'textarea', 'text', 'tfoot', 'thead', 'th', 'title', 'tr', 'time',
            'ul',
            'var', 'video',
            'wbr','$smarty', 'now', 'const', 'capture', 'config', 'section', 'foreach', 'template', 'version', 'ldelim', 'rdelim',
            'foreachelse', 'include', 'include_php', 'insert', 'if', 'elseif', 'else', 'php',
            'sectionelse', 'is_cached',
            ),
        10 => array(
            //html attributes
            'abbr', 'accept-charset', 'accept', 'accesskey', 'action', 'align', 'alink', 'alt', 'archive', 'axis', 'autocomplete', 'autofocus',
            'background', 'bgcolor', 'border',
            'cellpadding', 'cellspacing', 'char', 'charoff', 'charset', 'checked', 'cite', 'class', 'classid', 'clear', 'code', 'codebase', 'codetype', 'color', 'cols', 'colspan', 'compact', 'content', 'coords', 'contenteditable', 'contextmenu',
            'data', 'datetime', 'declare', 'defer', 'dir', 'disabled', 'draggable', 'dropzone',
            'enctype',
            'face', 'for', 'frame', 'frameborder', 'form', 'formaction', 'formenctype', 'formmethod', 'formnovalidate', 'formtarget',
            'headers', 'height', 'href', 'hreflang', 'hspace', 'http-equiv', 'hidden',
            'id', 'ismap',
            'label', 'lang', 'language', 'link', 'longdesc',
            'marginheight', 'marginwidth', 'maxlength', 'media', 'method', 'multiple', 'min', 'max',
            'name', 'nohref', 'noresize', 'noshade', 'nowrap', 'novalidate',
            'object', 'onblur', 'onchange', 'onclick', 'ondblclick', 'onfocus', 'onkeydown', 'onkeypress', 'onkeyup', 'onload', 'onmousedown', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onselect', 'onsubmit', 'onunload', 'onafterprint', 'onbeforeprint', 'onbeforeonload', 'onerror', 'onhaschange', 'onmessage', 'onoffline', 'ononline', 'onpagehide', 'onpageshow', 'onpopstate', 'onredo', 'onresize', 'onstorage', 'onundo', 'oncontextmenu', 'onformchange', 'onforminput', 'oninput', 'oninvalid', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onmousewheel', 'onscroll', 'oncanplay', 'oncanplaythrough', 'ondurationchange', 'onemptied', 'onended', 'onloadeddata', 'onloadedmetadata', 'onloadstart', 'onpause', 'onplay', 'onplaying', 'onprogress', 'onratechange', 'onreadystatechange', 'onseeked', 'onseeking', 'onstalled', 'onsuspend', 'ontimeupdate', 'onvolumechange', 'onwaiting',
            'profile', 'prompt', 'pattern', 'placeholder',
            'readonly', 'rel', 'rev', 'rowspan', 'rows', 'rules', 'required',
            'scheme', 'scope', 'scrolling', 'selected', 'shape', 'size', 'span', 'src', 'standby', 'start', 'style', 'summary', 'spellcheck', 'step',
            'tabindex', 'target', 'text', 'title', 'type',
            'usemap',
            'valign', 'value', 'valuetype', 'version', 'vlink', 'vspace',
            'width',
            )
        ),
    'SYMBOLS' => array(
        '/', '=', '==', '!=', '>', '<', '>=', '<=', '!', '%'
        ),
    'CASE_SENSITIVE' => array(
        GESHI_COMMENTS => false,
        1 => false,
        2 => false,
        3 => false,
        4 => false,
        5 => false,
        6 => false,
        7 => false,
        8 => false,
        9 => false,
        10 => false,
        ),
    'STYLES' => array(
        'KEYWORDS' => array(
            1 => 'color: #0600FF;',        //Functions
            2 => 'color: #008000;',        //Modifiers
            3 => 'color: #0600FF;',        //Custom Functions
            4 => 'color: #804040;',        //Variables
            5 => 'color: #008000;',        //Methods
            6 => 'color: #6A0A0A;',        //Attributes
            7 => 'color: #D36900;',        //Text-based symbols
            8 => 'color: #0600dd;',        //php functions
            9 => 'color: #0000aa;',        //html tags
            10 => 'color: #02c10f;'),       //html attributes
        'COMMENTS' => array(
            'MULTI' => 'color: #008080; font-style: italic;'
            ),
        'ESCAPE_CHAR' => array(
            0 => 'color: #000099; font-weight: bold;'
            ),
        'BRACKETS' => array(
            0 => 'color: #D36900;'
            ),
        'STRINGS' => array(
            0 => 'color: #ff7777;'
            ),
        'NUMBERS' => array(
            0 => 'color: #cc66cc;'
            ),
        'METHODS' => array(
            1 => 'color: #006600;'
            ),
        'SYMBOLS' => array(
            0 => 'color: #D36900;'
            ),
        'SCRIPT' => array(
            0 => '',
            1 => 'color: #808080; font-style: italic;',
            2 => 'color: #009000;'
            ),
        'REGEXPS' => array(
            0 => 'color: #00aaff;'
            )
        ),
    'URLS' => array(
        1 => 'http://smarty.php.net/{FNAMEL}',
        2 => 'http://smarty.php.net/{FNAMEL}',
        3 => 'http://smarty.php.net/{FNAMEL}',
        4 => 'http://smarty.php.net/{FNAMEL}',
        5 => 'http://smarty.php.net/{FNAMEL}',
        6 => '',
        7 => 'http://smarty.php.net/{FNAMEL}',
        8 => '',
        9 => 'http://www.w3.org/TR/html-markup/{FNAMEL}.html',
        10=> ''
        ),
    'OOLANG' => true,
    'OBJECT_SPLITTERS' => array(
        1 => '.',
        2 => '-&gt;'),
    'REGEXPS' => array(
        // variables
        0 => '\$[a-zA-Z][a-zA-Z0-9_]*',
        ),
    'STRICT_MODE_APPLIES' => GESHI_ALWAYS,
    'SCRIPT_DELIMITERS' => array(
        -2 => array(
            '<![CDATA[' => ']]>'
            ),
        0 => array(
            '{' => '}'
            ),
        1 => array(
            '<!--' => '-->',
        ),
        2 => array(
            '<' => '>'
            )
    ),
    'HIGHLIGHT_STRICT_BLOCK' => array(
        -2 => false,
        0 => true,
        1 => false,
        2 => true
        
    ),
    'PARSER_CONTROL' => array(
        'KEYWORDS' => array(
            1 => array(
            'DISALLOWED_BEFORE' => "(?<![a-zA-Z0-9\$_\|\#;>|^])",
            'DISALLOWED_AFTER' => "(?![a-zA-Z0-9_<\|%\\-&])"
            ),
            2 => array(
            'DISALLOWED_BEFORE' => "(?<![a-zA-Z0-9\$_\|\#;>|^])",
            'DISALLOWED_AFTER' => "(?![a-zA-Z0-9_<\|%\\-&])"
            ),
            3 => array(
            'DISALLOWED_BEFORE' => "(?<![a-zA-Z0-9\$_\|\#;>|^])",
            'DISALLOWED_AFTER' => "(?![a-zA-Z0-9_<\|%\\-&])"
            ),
            4 => array(
            'DISALLOWED_BEFORE' => "(?<![a-zA-Z0-9\$_\|\#;>|^])",
            'DISALLOWED_AFTER' => "(?![a-zA-Z0-9_<\|%\\-&])"
            ),
            5 => array(
            'DISALLOWED_BEFORE' => "(?<![a-zA-Z0-9\$_\|\#;>|^])",
            'DISALLOWED_AFTER' => "(?![a-zA-Z0-9_<\|%\\-&])"
            ),
            6 => array(
            'DISALLOWED_BEFORE' => "(?<![a-zA-Z0-9\$_\|\#;>|^])",
            'DISALLOWED_AFTER' => "(?![a-zA-Z0-9_<\|%\\-&])"
            ),
            7 => array(
            'DISALLOWED_BEFORE' => "(?<![a-zA-Z0-9\$_\|\#;>|^])",
            'DISALLOWED_AFTER' => "(?![a-zA-Z0-9_<\|%\\-&])"
            ),
            8 => array(
            'DISALLOWED_BEFORE' => "(?<![a-zA-Z0-9\$_\|\#;>|^])",
            'DISALLOWED_AFTER' => "(?![a-zA-Z0-9_<\|%\\-&])"
            ),
            9 => array(
            'DISALLOWED_BEFORE' => '(?<=&lt;|&lt;\/)',
            'DISALLOWED_AFTER' => '(?=\s|\/|&gt;)',
            )
        )
    )
);

?>
