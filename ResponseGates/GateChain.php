<?php

namespace Wafl\ResponseGates;

use DblEj\Communication\Http\Response;
use DblEj\Resources\IActor;
use DblEj\Resources\ResourcePermission;

final class GateChain
implements IGateKeeper
{
    private $_gateKeepers;

    public function AddGateKeeper(IGateKeeper $gateKeeper)
    {
        $this->_gateKeepers[] = $gateKeeper;
    }

    public function AttemptEntry(\DblEj\Communication\IResponse $response, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $returnGateState = new GateState();
        foreach ($this->_gateKeepers as $gateKeeper)
        {
            $gateState = $gateKeeper->AttemptEntry($response, $actor, $accessType);
            if ($gateState->Get_IsRestricted())
            {
                $returnGateState = $gateState;
                break;
            }
        }
        return $returnGateState;
    }
    public function AttemptRoute(\DblEj\Communication\IRoute $route, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $returnGateState = new GateState();
        foreach ($this->_gateKeepers as $gateKeeper)
        {
            $gateState = $gateKeeper->AttemptRoute($route, $actor, $accessType);
            if ($gateState->Get_IsRestricted())
            {
                $returnGateState = $gateState;
                break;
            }
        }
        return $returnGateState;
    }
}