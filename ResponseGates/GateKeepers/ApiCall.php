<?php

namespace Wafl\ResponseGates\GateKeepers;

use DblEj\Communication\Http\Response;
use DblEj\Resources\IActor;
use DblEj\Resources\ResourcePermission;
use Wafl\Core;

class ApiCall
implements \Wafl\ResponseGates\IGateKeeper
{

    public function AttemptEntry(\DblEj\Communication\IResponse $httpResponse, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $restrictedResourceName = null;
        $actorNames = [];
        $viewerRolesString = "";
        if (is_a($httpResponse, "\\DblEj\\Communication\\Http\\ApiResponse"))
        {
            $viewerRoles = $httpResponse->Get_ViewerRoles();
            $viewerRolesString = implode(", ", $viewerRoles);

            foreach ($httpResponse->Get_ViewerRoles() as $viewerRolename)
            {
                $actorsInContext = $actor->GetContextualActor($viewerRolename);
                if (!is_array($actorsInContext))
                {
                    $actorsInContext = [$actorsInContext];
                }
                foreach ($actorsInContext as $actorInContext)
                {
                    if (Core::$RUNNING_APPLICATION->IsActorAllowedAccessToApiCall($httpResponse->Get_ApiCall(), $actorInContext, $accessType))
                    {
                        $restrictedResourceName = null;
                        $actorNames = [];
                        break;
                    } else {
                        $actorNames[] = $actorInContext->Get_DisplayName();
                        $restrictedResourceName = "Api Call Entry ($accessType): " . $httpResponse->Get_ApiCall();
                    }
                }
            }
        }
        return new \Wafl\ResponseGates\GateState($restrictedResourceName, implode(", ", $actorNames), $viewerRolesString);
    }

    public function AttemptRoute(\DblEj\Communication\IRoute $route, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $restrictedResourceName = null;
        $destination = $route->Get_Destination();
        $actorNames = [];
        $viewerRolesString = "";
        if (is_array($destination) && isset($destination[0]) && is_a($destination[0], "\\DblEj\\Communication\\Ajax\\AjaxHandler"))
        {
            $viewerRoles = $route->Get_TravelerRoles();
            $viewerRolesString = implode(", ", $viewerRoles);

            foreach ($route->Get_TravelerRoles() as $travelerRoleName)
            {
                $actorsInContext = $actor->GetContextualActor($travelerRoleName);
                if (!is_array($actorsInContext))
                {
                    $actorsInContext = [$actorsInContext];
                }
                foreach ($actorsInContext as $actorInContext)
                {
                    if (Core::$RUNNING_APPLICATION->IsActorAllowedAccessToApiCall($destination[0]->Get_AjaxFunctionName(), $actorInContext, $accessType))
                    {
                        $restrictedResourceName = null;
                        $actorNames = [];
                        break;
                    } else {
                        $actorNames[] = $actorInContext->Get_DisplayName();
                        $restrictedResourceName = "Api Call: " . $destination[0]->Get_AjaxFunctionName();
                    }
                }
            }
        }
        return new \Wafl\ResponseGates\GateState($restrictedResourceName, implode(", ", $actorNames), $viewerRolesString);
    }
}