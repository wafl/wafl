<?php

namespace Wafl\ResponseGates\GateKeepers;

use DblEj\Communication\Http\Response;
use DblEj\Resources\IActor;
use DblEj\Resources\Resource;
use Wafl\Core;
use Wafl\ResponseGates\GateState;
use Wafl\ResponseGates\IGateKeeper;

class PhpScript
implements IGateKeeper
{

    public function AttemptEntry(\DblEj\Communication\IResponse $response, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $restrictedResourceName = null;
        $viewerRolesString = "";
        $actorNames = [];
        if ($response->Get_ResponseType() == Response::CONTENT_TYPE_PHP_SCRIPT)
        {
            $scriptResource = Core::$RUNNING_APPLICATION->GetRestrictedResource($response->Get_Content(), Resource::RESOURCE_TYPE_SCRIPT);
            if ($scriptResource)
            {
                $viewerRoles = $response->Get_ViewerRoles();
                $viewerRolesString = implode(", ", $viewerRoles);

                foreach ($response->Get_ViewerRoles() as $viewerRolename)
                {
                    $actorsInContext = $actor->GetContextualActor($viewerRolename);
                    if (!is_array($actorsInContext))
                    {
                        $actorsInContext = [$actorsInContext];
                    }
                    foreach ($actorsInContext as $actorInContext)
                    {
                        if (Core::$RUNNING_APPLICATION->IsAllowed($scriptResource, $actorInContext, $accessType))
                        {
                            $restrictedResourceName = null;
                            $actorNames = [];
                            break;
                        } else {
                            $restrictedResourceName = "Script: " . $response->Get_Content();
                            $actorNames[] = $actorInContext->Get_DisplayName();
                        }
                    }
                }
            }
        }
        return new GateState($restrictedResourceName, implode(", ", $actorNames), $viewerRolesString);
    }
    public function AttemptRoute(\DblEj\Communication\IRoute $route, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        //we dont know what the script is until it is routed, so always allow this
        $restrictedResourceName = null;
        return new \Wafl\ResponseGates\GateState($restrictedResourceName);
    }
}