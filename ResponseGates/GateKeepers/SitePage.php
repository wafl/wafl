<?php

namespace Wafl\ResponseGates\GateKeepers;

use DblEj\Resources\IActor;
use Wafl\Core;
use Wafl\ResponseGates\IGateKeeper;

class SitePage
implements IGateKeeper
{

    public function AttemptEntry(\DblEj\Communication\IResponse $httpResponse, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $restrictedResourceName = null;
        $actorNames = [];
        $viewerRolesString = "";
        if (is_subclass_of($httpResponse, "\\DblEj\\Communication\\Http\\ISitePageResponse"))
        {
            $currentSitePage = $httpResponse->Get_SitePage();
            $viewerRoles = $httpResponse->Get_ViewerRoles();
            $viewerRolesString = implode(", ", $viewerRoles);
            foreach ($viewerRoles as $viewerRolename)
            {
                $actorsInContext = $actor->GetContextualActor($viewerRolename);
                if (!is_array($actorsInContext))
                {
                    $actorsInContext = [$actorsInContext];
                }

                foreach ($actorsInContext as $actorInContext)
                {
                    if (Core::$RUNNING_APPLICATION->IsActorAllowedAccessToSitePage($currentSitePage, $actorInContext, $accessType))
                    {
                        $restrictedResourceName = null;
                        $actorNames = [];
                        break 2;
                    } else {
                        $actorNames[] = $actorInContext->Get_DisplayName();
                        $restrictedResourceName = "Site Page: " . $currentSitePage->Get_PageId().", Site Id registered by ".$currentSitePage->Get_RegisteredBy();
                    }
                }
            }
        }
        return new \Wafl\ResponseGates\GateState($restrictedResourceName, implode(", ", $actorNames), $viewerRolesString);
    }
    public function AttemptRoute(\DblEj\Communication\IRoute $route, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        //we dont know what the site page is until it is routed, so always allow this
        $restrictedResourceName = null;
        return new \Wafl\ResponseGates\GateState($restrictedResourceName);
    }
}