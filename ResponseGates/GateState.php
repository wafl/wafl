<?php
namespace Wafl\ResponseGates;

/**
 * The result of a IHandler->TryAccess()
 */
class GateState
{
    private $_restrictedResourceName;
    private $_isRestricted;
    private $_attemptedActorNames;
    private $_attemptedRoles;

    /**
     * Create a result to a IHandler->TryAccess() call
     *
     * @param string $restrictedResourceName The name of the resource that was restricted, if any.
     * Otherwise it is null.
     */
    public function __construct($restrictedResourceName = null, $attemptedActorNames = null, $attemptedRoles = null)
    {
        $this->_restrictedResourceName = $restrictedResourceName;
        $this->_isRestricted           = $this->_restrictedResourceName !== null;
        $this->_attemptedActorNames = $attemptedActorNames;
        $this->_attemptedRoles = $attemptedRoles;
    }

    /**
     * If a resource was restricted from the actor, it's name will be returned.
     *
     * @return string
     */
    public function Get_RestrictedResourceName()
    {
        return $this->_restrictedResourceName;
    }

    public function Get_IsRestricted()
    {
        return $this->_isRestricted;
    }

    public function Get_AttemptedActorNames()
    {
        return $this->_attemptedActorNames;
    }

    public function Get_AttemptedRoles()
    {
        return $this->_attemptedRoles;
    }

    public function __toString()
    {
        return "Gate State: $this->_restrictedResourceName, ".($this->_isRestricted?"restrict":"no restrict");
    }
}