<?php

namespace Wafl\ResponseGates;

use DblEj\Resources\IActor;
use DblEj\Resources\ResourcePermission;

interface IGateKeeper
{

    /**
     * Try to gain the given access type to the given resource by the given actor.
     *
     * @param \DblEj\Communication\IResponse $response
     * @param IActor $actor
     * @param int $accessType
     *
     * @return \Wafl\ResponseGates\GateState The state of the gate after attempted entry
     */
    public function AttemptEntry(\DblEj\Communication\IResponse $response, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ);

    public function AttemptRoute(\DblEj\Communication\IRoute $route, IActor $actor = null, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ);
}