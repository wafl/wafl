<?php

namespace Wafl\Routers;

use DblEj\Communication\Ajax\AjaxHandler;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\Communication\Http\Routing\Route;
use DblEj\System\NotYetImplementedException;
use \DblEj\Util\Strings;
use Wafl\Core;

final class Api
implements IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute = null;
        $requestPage = parse_url($request->Get_RequestUrl(), PHP_URL_PATH);
        if (Strings::EndsWith($requestPage, \Wafl\Core::WAFL_API_HANDLER_URL))
        {
            $reqMethod = $request->GetInput("REQUEST_METHOD", Request::INPUT_SERVER);
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            header("Access-Control-Allow-Headers: Content-Type, Content-Length, Cookie, Pragma, x-requested-with, WAFLACTION, user-agent");
            header("Access-Control-Max-Age: 600");

            if ($request->GetInput("initxdomain"))
            {
                header_remove("Set-Cookie");
                print "{\"CrossDomainApi\": \"ok\" }";
                exit(0);
            }
            if (strtoupper($reqMethod) == "OPTIONS")
            {
                header("Cache-Control: public, max-age=600");
                header("Expires: ".substr(date("r", time()-date('Z')+600), 0, -5) . "GMT");
                header_remove("Pragma");
                header_remove("Set-Cookie");
                exit(0);
            }
            $usedRouter = $this;
            //"Call" is deprecated, instead use ACTION http headers
            $call       = isset($_REQUEST["Call"]) ? $_REQUEST["Call"] : null;
            //this replaces the old "Call" get variable
            $action     = isset($_SERVER["HTTP_WAFLACTION"]) ? $_SERVER["HTTP_WAFLACTION"] : $call;

            if ($action)
            {
                $am = AjaxHandler::LoadByAjaxFunctionName($action, Core::$STORAGE_ENGINE, $app);
                if ($am)
                {
                    $returnRoute = new Route($request, array($am, "HandleCall"), array($request, $_REQUEST));
                }
                else
                {
                    throw new \Exception("Could not run api function \"$action\" because its handler cannot be found.");
                }
            }
            else
            {
                throw new \Exception("You must specify the api action");
            }
        }
        return $returnRoute;
    }

    public function GetUrl(IRoute $route, \DblEj\Application\IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        throw new NotYetImplementedException();
    }
}