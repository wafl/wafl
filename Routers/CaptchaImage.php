<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\Headers;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\Communication\Http\Routing\Route;
use Wafl\Core;

final class CaptchaImage
implements IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {

        $returnRoute                = false;
        $appRelativeRequestUriParts = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
        $appRelativeRequestUri      = "/" . end($appRelativeRequestUriParts);
        if (strstr($appRelativeRequestUri, "/CaptchaImage.gif"))
        {
            if ($request->GetInput("ResetCaptcha"))
            {
                \Wafl\Util\Captcha::ResetCaptcha();
            }
            $usedRouter   = $this;
            $captchaImage = new \Wafl\DynamicResources\CaptchaImage("Captcha.gif", "Captcha.gif", Headers::CACHEPOINT_NONE);
            $returnRoute  = new Route($request, $captchaImage);
        }
        return $returnRoute;
    }

    public function GetUrl(IRoute $route, IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        $url = false;
        if ($route->Get_Destination() == Core::$WAFL_FOLDER . "DynamicResources/CaptchaImage.php")
        {
            if ($absoluteUrl)
            {
                if ($useHttps)
                {
                    $url = "http://" . Core::$WEB_ROOT_SECURE . "/CaptchaImage.gif";
                }
                else
                {
                    $url = "http://" . Core::$WEB_ROOT . "/CaptchaImage.gif";
                }
            }
            else
            {
                $url = Core::$WEB_ROOT_RELATIVE . "/CaptchaImage.gif";
            }
        }
        return $url;
    }
}