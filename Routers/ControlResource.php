<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\Headers;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\Communication\Http\Routing\Route;
use Wafl\Core;

final class ControlResource
implements \DblEj\Communication\Http\Routing\IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute                = null;
        $appRelativeRequestUriParts = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
        $controlsIdx                = null;
        for ($urlIdx = 0; $urlIdx < count($appRelativeRequestUriParts); $urlIdx++)
        {
            if ($appRelativeRequestUriParts[$urlIdx] == "ControlResource")
            {
                $controlsIdx = $urlIdx;
                break;
            }
        }
        if ($controlsIdx)
        {
            $resourceName = "";
            $controlName  = $appRelativeRequestUriParts[$controlsIdx + 1];
            for ($urlIdx = $controlsIdx + 2; $urlIdx < count($appRelativeRequestUriParts); $urlIdx++)
            {
                $resourceName .= "/" . $appRelativeRequestUriParts[$urlIdx];
            }
            $waflFolder = $app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_WaflFolder();

            $resourcePath = $waflFolder . "Controls/" . $controlName . "/Resources$resourceName";
            if (file_exists($resourcePath))
            {
                $usedRouter  = $this;
                if (\DblEj\Util\Strings::EndsWith($resourcePath, ".css"))
                {
                    $mimeType = "text/css";
                } else {
                    $mimeInfo = finfo_open(FILEINFO_MIME_TYPE);
                    $mimeType = finfo_file($mimeInfo, $resourcePath);
                }
                
                $returnRoute = new NonExecutableRoute($request, $resourcePath, $mimeType);
            }
        }
        return $returnRoute;
    }

    public function GetUrl(\DblEj\Communication\Http\Routing\IRoute $route, \DblEj\Application\IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        $resourceTokenPos = stripos($route->Get_Destination(), "/Resources");
        $url              = false;
        if ($resourceTokenPos)
        {
            $resourceName = substr($route->Get_Destination(), $resourceTokenPos);
            if ($absoluteUrl)
            {
                if ($useHttps)
                {
                    $url = "http://" . Core::$WEB_ROOT_SECURE . "/ControlResource?Resource=$resourceName";
                }
                else
                {
                    $url = "http://" . Core::$WEB_ROOT . "/ControlResource?Resource=$resourceName";
                }
            }
            else
            {
                $url = Core::$WEB_ROOT_RELATIVE . "/ControlResource?Resource=$resourceName";
            }
            return $url;
        }
    }
}