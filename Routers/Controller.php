<?php
//default controller router uses the filename as the controller and a GET var for the action

namespace Wafl\Routers;

use DblEj\Application\IApplication,
    DblEj\Application\IWebApplication,
    DblEj\Communication\Http\Request,
    DblEj\Communication\Http\Routing\IInternalRouter,
    DblEj\Communication\Http\Routing\InvalidRouterRequestException,
    DblEj\Communication\Http\Routing\IRoute,
    DblEj\Communication\Http\Routing\IRouter as IHttpRouter,
    DblEj\Communication\Http\Routing\Route,
    DblEj\Communication\Http\Routing\RouteNotFoundException,
    DblEj\Communication\IRequest,
    DblEj\Communication\IRouter,
    DblEj\Data\Validator,
    DblEj\Util\File,
    \DblEj\Util\Strings;

final class Controller
implements IInternalRouter
{
    private $_actionVarName = "Action";
    private $_defaultAction = "DefaultAction";
    private $_actionVarType = Request::INPUT_REQUEST;

    public function GetRoute(IRequest $request, IApplication $app = null, IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, IWebApplication $app = null, IHttpRouter &$usedRouter = null)
    {
        $returnRoute = null;
        if ($app !== null)
        {
            $requestFile = $request->Get_RequestUrl();

            //prevent exception if user goes to malformed url like https://activewafl.com//MyPage... which was hapenning due to a bad template
            if (strlen($requestFile) < 2 || substr($requestFile, 0, 2) != "//")
            {
                if (strpos($requestFile, "?"))
                {
                    $requestFile = substr($requestFile, 0, strpos($requestFile, "?"));
                }
                $controllerFullPath = "";

                $mainAreaName  = $app->Get_Settings()->Get_Application()->Get_SiteArea();
                $rootNamespace = $app->Get_Settings()->Get_Application()->Get_RootNameSpace();

                if (substr($requestFile, strlen($requestFile) - 1, 1) == "/")
                {
                    $defaultPage = null;
                    $mainArea    = $app->Get_SiteMap()->GetSiteArea($mainAreaName);
                    if (!$mainArea)
                    {
                        throw new InvalidRouterRequestException($request->Get_RequestUrl(), $this, "The application does not specify a valid top level Site Area.");
                    }
                    foreach ($app->Get_SiteMap()->GetSitePages($mainArea) as $sitePage)
                    {
                        if ($sitePage->Get_IsAreaDefault())
                        {
                            $controllerFolder = dirname($sitePage->Get_ControllerPath());
                            if ($controllerFolder == ".")
                            {
                                $sitePageAreaDefaultPath = "/";
                            }
                            else
                            {
                                $sitePageAreaDefaultPath = "/" . $controllerFolder . "/";
                            }
                            if ($sitePageAreaDefaultPath === $requestFile)
                            {
                                $defaultPage = $sitePage;
                                break;
                            }
                        }
                    }

                    $requestFileWithNoPrecedingSlash = $requestFile;
                    if (substr($requestFileWithNoPrecedingSlash, 0, 1) == "/" || substr($requestFileWithNoPrecedingSlash, 0, 1) == "\\")
                    {
                        $requestFileWithNoPrecedingSlash = substr($requestFileWithNoPrecedingSlash, 1);
                    }
                    if (!$defaultPage)
                    {
                        $controllerFullPath = $app->Get_LocalLogicFolder() . $requestFileWithNoPrecedingSlash . $app->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage();
                        $defaultPage        = $app->GetSitePageByFilename($requestFileWithNoPrecedingSlash . $app->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage());
                    }
                    if ($defaultPage)
                    {
                        $controllerFullPath  = $app->Get_LocalLogicFolder() . $defaultPage->GetServerController();
                        $controllerClassName = "\\" . $rootNamespace . "\\Controllers" . str_replace("/", "\\", $requestFile) . substr(basename($controllerFullPath), 0, strlen(basename($controllerFullPath)) - 4);
                    }
                    elseif (file_exists($controllerFullPath))
                    {
                        throw new RouteNotFoundException($request->Get_RequestUrl(), "The default page for this area (" . basename($controllerFullPath) . ") cannot be loaded, possibly due to a missing or inaccessible presentation template");
                    }
                    else
                    {
                        throw new RouteNotFoundException($request->Get_RequestUrl(), "The default controller file for this area (" . basename($controllerFullPath) . ") does not seem to exist in the requested folder (" . $requestFile . ") or is not accessible");
                    }
                }
                if (!$controllerFullPath)
                {
                    $controllerFullPath  = $app->Get_LocalLogicFolder() . $requestFile;
                    $controllerClassName = "\\" . $rootNamespace . "\\Controllers" . str_replace("/", "\\", $requestFile);
                    $fileExtensionStart  = stripos($controllerClassName, ".");
                    if ($fileExtensionStart !== false)
                    {
                        $controllerClassName = substr($controllerClassName, 0, $fileExtensionStart);
                    }
                    if (!file_exists($controllerFullPath))
                    {
                        $controllerFullPath = $controllerFullPath . ".php";
                    }
                }
                $queryArgs = array();
            }
            if ($controllerFullPath && file_exists($controllerFullPath) && is_file($controllerFullPath))
            {
                $usedRouter     = $this;
                $urlParts       = parse_url($request->Get_RequestUrl());
                $urlQueryString = isset($urlParts["query"]) ? $urlParts["query"] : "";
                if ($urlQueryString)
                {
                    foreach (explode("&", $urlQueryString) as $keyValPairString)
                    {
                        $keyValPair = explode("=", $keyValPairString);
                        if (count($keyValPair) == 2)
                        {
                            $queryArgs[$keyValPair[0]] = $keyValPair[1];
                        }
                    }
                }
                $action    = $request->GetInput($this->_actionVarName, $this->_actionVarType, Validator::VALIDATE_NONE, Validator::SANITIZE_NONE, $this->_defaultAction);
                $isPhpFile = \DblEj\Util\PhpFile::DoesFileHaveOpeningPhpTag($controllerFullPath);
                if ($isPhpFile)
                {
                    $hasPhpClass = \DblEj\Util\PhpFile::DoesFileHavePhpClass($controllerFullPath, 30, true, true);
                    if ($hasPhpClass)
                    {
                        require_once($controllerFullPath);
                        $controllerClass = new $controllerClassName();
                        $returnRoute     = new Route
                        (
                            $request,
                            array
                            (
                                $controllerClass,
                                "HandleRequest"
                            ),
                            array
                            (
                                $action,
                                $request,
                                $app
                            ),
                            array
                            (
                                $controllerClass,
                                "PrepareResponse"
                            )
                        );
                    }
                    else
                    {
                        $returnRoute = new Route($request, $controllerFullPath, $queryArgs);
                    }
                }
                else if (Strings::EndsWith($controllerFullPath, ".js"))
                {
                    $requestFilenameArray = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
                    if (!isset($requestFilenameArray[0]) || !$requestFilenameArray[0] && count($requestFilenameArray) > 1)
                    {
                        array_shift($requestFilenameArray);
                    }
                    $requestFilename = implode("/", $requestFilenameArray);
                    $pagename        = substr($requestFilename, 0, strlen($requestFilename) - 3);

                    if (strlen($pagename) > 7 && substr($pagename, 0, 7) == "Shared/")
                    {
                        $jsResource = new \Wafl\DynamicResources\SharedController($pagename, $requestFilename);
                    } else {
                        $jsResource = new \Wafl\DynamicResources\SitePageJs($pagename, $requestFilename);
                    }
                    $serverFile = $jsResource->Get_ContentReference();
                    if
                    (
                        file_exists
                        (
                            $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot() .
                            $app->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR .
                            "Controllers" . DIRECTORY_SEPARATOR . $serverFile
                        )
                    )
                    {
                        $returnRoute = new Route($request, $jsResource, null, $jsResource);
                    }
                }
            }
        }
        return $returnRoute;
    }

    /**
     * @param IRoute $route
     * @param string $absoluteUrl
     * @param boolean $useHttps
     * @return string
     */
    public function GetUrl(IRoute $route, IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        $localRoot      = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
        $appFolder      = $app->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR;
        $localAppFolder = $localRoot . $appFolder;

        $url             = false;
        $controllerName  = "";
        $actionName      = "";
        $controllersPath = $localAppFolder . "Controllers";
        $destination     = $route->Get_Destination();

        if (is_array($route->Get_Destination()) && class_exists($destination[0]) && in_array("\\DblEj\\Mvc\\ControllerBase", class_parents($destination[0])))
        {
            $controllerName = $destination[0];
            $actionName     = $destination[1];
        }
        elseif (Strings::StartsWith($route->Get_Destination(), $controllersPath))
        {
            $controllerName  = Strings::ReplaceBeginning($destination, $controllersPath, "");
            $destinationArgs = $route->Get_DestinationArguments();
            $actionName      = $destinationArgs[$this->_actionVarName];
        }

        if ($absoluteUrl && $useHttps)
        {
            $url = "http://" . $app->Get_Settings()->Get_Web()->Get_WebUrlSecure() . "/$controllerName?" . $this->_actionVarName . "=$actionName";
        }
        elseif ($absoluteUrl)
        {
            $url = "http://" . $app->Get_Settings()->Get_Web()->Get_WebUrl() . "/$controllerName?" . $this->_actionVarName . "=$actionName";
        }
        else
        {
            $url = $app->Get_Settings()->Get_Web()->Get_WebUrlRelative() . "/$controllerName?" . $this->_actionVarName . "=$actionName";
        }
        return $url;
    }

    final public function GetActionVarType()
    {
        return $this->_actionVarType;
    }

    public function Get_DefaultActionName()
    {
        return $this->_defaultAction;
    }

    final public function GetActionVarName()
    {
        return $this->_actionVarName;
    }
}