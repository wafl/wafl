<?php

namespace Wafl\Routers;

use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\Communication\Http\Routing\Route;
use \DblEj\Util\Strings;

final class Controls
implements IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute          = null;
        $requestFilenameArray = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
        if (!isset($requestFilenameArray[0]) || !$requestFilenameArray[0] && count($requestFilenameArray) > 1)
        {
            array_shift($requestFilenameArray);
        }
        if ($requestFilenameArray[0] == "Controls")
        {
            $controlName = $requestFilenameArray[1];
            array_shift($requestFilenameArray);
            array_shift($requestFilenameArray);
            $requestFilename = implode("/", $requestFilenameArray);
            if ($app)
            {
                if (Strings::StartsWith($requestFilename, $app->Get_Settings()->Get_Web()->Get_WebUrlRelative()))
                {
                    $requestFilename = substr($requestFilename, strlen($app->Get_Settings()->Get_Web()->Get_WebUrlRelative()));
                }
            }
            $controlClass = "\\Wafl\\Controls\\$controlName\\$controlName";
            $templateRenderer = \Wafl\Util\Extensions::GetExtensionByType("\\DblEj\\Presentation\\Integration\\ITemplateRendererExtension");
            $control = new $controlClass($templateRenderer, $controlName);
            $mainStylesheet = $control->Get_MainStylesheet();

            if ($requestFilename == $mainStylesheet->Get_Filename())
            {
                $resourceClassName = "\\Wafl\\DynamicResources\\ControlCss";
                $returnRoute       = new Route($request, new $resourceClassName($controlName, $requestFilename));
            }
            else
            {
                foreach ($control->Get_Javascripts() as $js)
                {
                    if ($requestFilename == is_array($js)?$js["File"]:$js)
                    {
                        $resourceClassName = "\\Wafl\\DynamicResources\\ControlJs";
                        $returnRoute       = new Route($request, new $resourceClassName($controlName, $requestFilename));
                    }
                }
            }
            if ($returnRoute)
            {
                $usedRouter = $this;
            }
        }
        return $returnRoute;
    }

    public function GetUrl(IRoute $route, \DblEj\Application\IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        throw new \DblEj\System\NotYetImplementedException();
    }
}