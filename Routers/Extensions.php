<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\Communication\Http\Routing\Route;
use \DblEj\Util\Strings;
use Wafl\Core;

final class Extensions
implements IInternalRouter
{
    private $_actionVarName = "Action";
    private $_defaultAction = "DefaultAction";
    private $_actionVarType = Request::INPUT_REQUEST;

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute = null;
        $extensionIncludeFile = null;
        if ($app)
        {
            $requestFilenameArray = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
            if (!isset($requestFilenameArray[0]) || !$requestFilenameArray[0] && count($requestFilenameArray) > 1)
            {
                array_shift($requestFilenameArray);
            }
            $requestFilename = implode("/", $requestFilenameArray);
            if ($app)
            {
                if (Strings::StartsWith($requestFilename, $app->Get_Settings()->Get_Web()->Get_WebUrlRelative()))
                {
                    $requestFilename = substr($requestFilename, strlen($app->Get_Settings()->Get_Web()->Get_WebUrlRelative()));
                }
            }
            if (count($requestFilenameArray) > 1 && $requestFilenameArray[0] == "Extensions")
            {
                $extension = \Wafl\Util\Extensions::FindExtensionByPath($requestFilename);
                if ($extension)
                {
                    $extensionclass = get_class($extension);
                    $extensionname = dirname(substr(str_replace("\\", DIRECTORY_SEPARATOR, $extensionclass), 16));
                    $request = $extensionclass::TranslateUrl($request);


                    //reparse since extension may have modified url
                    $requestFilenameArray = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
                    if (!isset($requestFilenameArray[0]) || !$requestFilenameArray[0] && count($requestFilenameArray) > 1)
                    {
                        array_shift($requestFilenameArray);
                    }
                    $requestFilename = implode("/", $requestFilenameArray);
                    if ($app)
                    {
                        if (Strings::StartsWith($requestFilename, $app->Get_Settings()->Get_Web()->Get_WebUrlRelative()))
                        {
                            $requestFilename = substr($requestFilename, strlen($app->Get_Settings()->Get_Web()->Get_WebUrlRelative()));
                        }
                    }

                    if ($requestFilenameArray[count($requestFilenameArray)-2] == "DataModel")
                    {
                        $requestFilenameArray = array_slice($requestFilenameArray, count($requestFilenameArray)-2);
                        $extensionIncludeFile = implode(DIRECTORY_SEPARATOR, $requestFilenameArray);
                    }
                    elseif ($requestFilenameArray[count($requestFilenameArray)-2] == "FunctionalModel")
                    {
                        $requestFilenameArray = array_slice($requestFilenameArray, count($requestFilenameArray)-2);
                        $extensionIncludeFile = implode(DIRECTORY_SEPARATOR, $requestFilenameArray);
                    } else {
                        $extensionIncludeFile = substr($requestFilename, 12+strlen($extensionname));
                    }
                    if (!$extensionIncludeFile)
                    {
                        $extensionIncludeFile = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage();
                        if (strlen($extensionIncludeFile) > 4)
                        {
                            $extensionIncludeFile = substr($extensionIncludeFile, 0, strlen($extensionIncludeFile) - 4);
                        }
                    }

                    if (Strings::EndsWith($extensionIncludeFile, ".css"))
                    {
                        $resourceClassName = "\\Wafl\\DynamicResources\\ExtensionCss";
                        $returnRoute       = new Route($request, new $resourceClassName($extensionname, $extensionIncludeFile));
                    }
                    elseif (Strings::EndsWith($extensionIncludeFile, ".js"))
                    {
                        $resourceClassName = "\\Wafl\\DynamicResources\\ExtensionJs";
                        $returnRoute       = new Route($request, new $resourceClassName($extensionname, $extensionIncludeFile));
                    }
                    else
                    {
                        $queryArgs                   = array();
                        $localExtensionsFolder = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder()) . DIRECTORY_SEPARATOR;
                        $fullExtensionControllerPath = $localExtensionsFolder . $extensionname . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . $extensionIncludeFile . ".php";
                        if (!file_exists($fullExtensionControllerPath))
                        {
                            $fullExtensionControllerPath = $localExtensionsFolder . $extensionname . DIRECTORY_SEPARATOR . $extensionIncludeFile . ".php";
                        }
                        if ($extensionIncludeFile && file_exists($fullExtensionControllerPath))
                        {
                            $usedRouter     = $this;
                            $urlParts       = parse_url($request->Get_RequestUrl());
                            $urlQueryString = isset($urlParts["query"]) ? $urlParts["query"] : "";
                            foreach (explode("&", $urlQueryString) as $keyValPairString)
                            {
                                $keyValPair = explode("=", $keyValPairString);
                                if (count($keyValPair) == 2)
                                {
                                    $queryArgs[$keyValPair[0]] = $keyValPair[1];
                                }
                            }
                            $action    = $request->GetInput($this->_actionVarName, $this->_actionVarType, \DblEj\Data\Validator::VALIDATE_NONE, \DblEj\Data\Validator::SANITIZE_NONE, $this->_defaultAction);
                            $isPhpFile = \DblEj\Util\PhpFile::DoesFileHaveOpeningPhpTag($fullExtensionControllerPath);
                            if ($isPhpFile)
                            {
                                $hasPhpClass = \DblEj\Util\PhpFile::DoesFileHavePhpClass($fullExtensionControllerPath, 30, true, true);
                                if ($hasPhpClass)
                                {
                                    require_once($fullExtensionControllerPath);
                                    $controllerClassName = "\\Wafl\\Extensions\\" . str_replace("/", "\\", $extensionname) . "\\Controllers\\" . str_replace("/", "\\", $extensionIncludeFile);
                                    if (class_exists($controllerClassName))
                                    {
                                        $controllerClass     = new $controllerClassName($extension);
                                        $returnRoute         = new Route(
                                            $request,
                                            array(
                                                $controllerClass,
                                                $action
                                            ),
                                            array($request,$app)
                                        );
                                    } else {
                                        throw new \DblEj\Communication\Http\Exception($request->Get_RequestUrl(), "Invalid extension controller requested", 404);
                                    }
                                }
                                else
                                {
                                    $returnRoute = new Route($request, $fullExtensionControllerPath, $queryArgs);
                                }
                            }
                            else
                            {
                                throw new \DblEj\Communication\Http\Exception($request->Get_RequestUrl(), "Invalid extension page requested", 404);
                            }
                        }
                    }
                }
            }
            if ($returnRoute)
            {
                $usedRouter = $this;
            }
        }
        return $returnRoute;
    }
}