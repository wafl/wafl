<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\System\NotYetImplementedException;

final class Fonts
implements IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute                = null;
        $css = "";
        $appRelativeRequestUriParts = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
        $appRelativeRequestFilename      = end($appRelativeRequestUriParts);
        $appRelativeRequestFilenameParts = explode("?", $appRelativeRequestFilename);
        $appRelativeRequestFilename = $appRelativeRequestFilenameParts[0];
        if ($request->Get_RequestUrl() == "/AppFonts.css")
        {
            $skins = $app->Get_Skins();
            foreach ($skins as $skin)
            {
                if ($skin->Get_MainFont() && $skin->Get_MainFont()->Get_Provider() && $skin->Get_MainFont()->Get_Provider()->Get_StylesheetBaseUrl())
                {
                    $css .= "@font-face {
    font-family: \"".$skin->Get_MainFont()->Get_Family()."\";
    src: url('/Resources/Fonts/".$skin->Get_MainFont()->Get_ProviderArgString().".otf');
    font-weight: ".$skin->Get_MainFont()->Get_Style()->Get_Weight().";
    font-style: ".$skin->Get_MainFont()->Get_Style()->Get_Styling()."; }
    ";
                }
                if ($skin->Get_HeadingFont() && $skin->Get_HeadingFont()->Get_Provider() && $skin->Get_HeadingFont()->Get_Provider()->Get_StylesheetBaseUrl())
                {
                    $css .= "@font-face {
    font-family: \"".$skin->Get_HeadingFont()->Get_Family()."\";
    src: url('/Resources/Fonts/".$skin->Get_HeadingFont()->Get_ProviderArgString().".otf');
    font-weight: ".$skin->Get_HeadingFont()->Get_Style()->Get_Weight().";
    font-style: ".$skin->Get_HeadingFont()->Get_Style()->Get_Styling()."; }
    ";
                }
                if ($skin->Get_SubFont() && $skin->Get_SubFont()->Get_Provider() && $skin->Get_SubFont()->Get_Provider()->Get_StylesheetBaseUrl())
                {
                    $css .= "@font-face {
    font-family: \"".$skin->Get_SubFont()->Get_Family()."\";
    src: url('/Resources/Fonts/".$skin->Get_SubFont()->Get_ProviderArgString().".otf');
    font-weight: ".$skin->Get_SubFont()->Get_Style()->Get_Weight().";
    font-style: ".$skin->Get_SubFont()->Get_Style()->Get_Styling()."; }
    ";
                }
                if ($skin->Get_AccentFont() && $skin->Get_AccentFont()->Get_Provider() && $skin->Get_AccentFont()->Get_Provider()->Get_StylesheetBaseUrl())
                {
                    $css .= "@font-face {
    font-family: \"".$skin->Get_AccentFont()->Get_Family()."\";
    src: url('/Resources/Fonts/".$skin->Get_AccentFont()->Get_ProviderArgString().".otf');
    font-weight: ".$skin->Get_AccentFont()->Get_Style()->Get_Weight().";
    font-style: ".$skin->Get_AccentFont()->Get_Style()->Get_Styling()."; }
    ";
                }
            }
            $returnRoute = new NonExecutableRoute($request, $css, "text/css", "AppFonts.css", 2592000, 1427333346);
            $usedRouter = $this;
        }

        return $returnRoute;
    }

    public function GetUrl(IRoute $route, IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        throw new NotYetImplementedException();
    }
}