<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\System\NotYetImplementedException;

final class Glyphs
implements IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute                = null;
        $appRelativeRequestUriParts = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
        $appRelativeRequestUri      = "/" . end($appRelativeRequestUriParts);
        $waflFolder                 = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;

        if (strstr($appRelativeRequestUri, "/activewafl-glyphs.eot"))
        {
            $filePath    = $waflFolder . "Css/Glyphs/activewafl-glyphs.eot";
            $returnRoute = new NonExecutableRoute($request, $filePath, "application/vnd.ms-fontobject", null, 2592000);
            if (file_exists($filePath))
            {
                $returnRoute = new NonExecutableRoute($request, $filePath, "application/vnd.ms-fontobject", null, 2592000);
            }
        }
        elseif (strstr($appRelativeRequestUri, "/activewafl-glyphs.woff"))
        {
            $filePath = $waflFolder . "Css/Glyphs/activewafl-glyphs.woff";
            if (file_exists($filePath))
            {
                $returnRoute = new NonExecutableRoute($request, $filePath, "application/font-woff", null, 2592000);
            }
        }
        elseif (strstr($appRelativeRequestUri, "/activewafl-glyphs.ttf"))
        {
            $filePath = $waflFolder . "Css/Glyphs/activewafl-glyphs.ttf";
            if (file_exists($filePath))
            {
                $returnRoute = new NonExecutableRoute($request, $filePath, "application/x-font-ttf", null, 2592000);
            }
        }
        elseif (strstr($appRelativeRequestUri, "/activewafl-glyphs.svg"))
        {
            $filePath = $waflFolder . "Css/Glyphs/activewafl-glyphs.svg";
            if (file_exists($filePath))
            {
                $returnRoute = new NonExecutableRoute($request, $filePath, "image/svg+xml", null, 2592000);
            }
        }

        if ($returnRoute)
        {
            $usedRouter = $this;
        }
        return $returnRoute;
    }

    public function GetUrl(IRoute $route, IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        throw new NotYetImplementedException();
    }
}