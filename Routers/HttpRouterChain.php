<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication,
    DblEj\Application\IWebApplication,
    DblEj\Communication\Http\Request,
    DblEj\Communication\Http\Routing\IRoute,
    DblEj\Communication\Http\Routing\IHttpRouter,
    DblEj\Communication\IRequest,
    DblEj\Communication\IRouter as IRouter;

final class HttpRouterChain
extends RouterChain
{

    public function GetRoute(IRequest $request, IApplication $app = null, IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \Wafl\Application\WebApplication $app = null, IHttpRouter &$usedRouter = null)
    {
        if ($app)
        {
            if ($request->GetInput("ClearServerCache") == 1)
            {
                //clear cache
                $app->ClearTemplateCache();
            }
            elseif ($request->GetInput("ClearServerCache") == 2)
            {
                //clear cache and compiles
                $app->ClearTemplateCache(true);
            }
        }
        $route = null;
        if (is_a($request, "\\DblEj\\Communication\\Http\\Request"))
        {
            $route = parent::GetRoute($request, $app, $usedRouter);
        }
        return $route;
    }

    public function GetUrl(IRoute $route, IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        $url = null;
        foreach ($this->_routers as $router)
        {
            $url = $router->GetUrl($route, $app, $absoluteUrl, $useHttps);
            if ($url)
            {
                break;
            }
        }
        return $url;
    }
}