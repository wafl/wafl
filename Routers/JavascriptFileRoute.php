<?php
namespace Wafl\Routers;

use DblEj\Communication\Http\Request;

final class JavascriptFile
extends NonExecutableRoute
{
    /**
     *
     * @param Request $request
     * @param callable|string $destination Either a callback to the function that the request should be routed to,
     * or the name of a script file to hand the request off to if the destination is not a function
     * @param array $destinationArguments
     */
    public function __construct(Request $request, $destination, $destinationArguments = null)
    {
        parent::__construct($request, $destination, "text/javascript");
        $this->_destinationArgs = $destinationArguments;
    }
}