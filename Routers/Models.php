<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\Communication\Http\Routing\Route;

final class Models
implements IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute = null;
        if ($app !== null)
        {
            $requestFilenameArray = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
            if (!isset($requestFilenameArray[0]) || !$requestFilenameArray[0] && count($requestFilenameArray) > 1)
            {
                array_shift($requestFilenameArray);
            }
            if ($requestFilenameArray[0] == "DataModel" || $requestFilenameArray[0] == "FunctionalModel" || $requestFilenameArray[0] == "CombinedModel")
            {
                $requestFilename = implode("/", $requestFilenameArray);
                $modelName       = substr($requestFilename, 0, strlen($requestFilename) - 3);
                $jsResource      = new \Wafl\DynamicResources\DataModelJs($modelName, $requestFilename);
                if ($jsResource->GetContents($app))
                {
                    $returnRoute = new Route($request, $jsResource, null, $jsResource);
                    $usedRouter  = $this;
                }
            }
        }
        return $returnRoute;
    }

    public function GetUrl(IRoute $route, \DblEj\Application\IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        throw new \DblEj\System\NotYetImplementedException();
    }
}