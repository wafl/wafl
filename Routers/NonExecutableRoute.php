<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\Headers;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Response;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\IO\File;
use DblEj\System\NotYetImplementedException;

class NonExecutableRoute
extends \DblEj\Communication\Route
{
    private $_request;
    private $_mimeType;
    private $_fileName;
    private $_clientCacheTimeout;
    private $_lastChangeTime;
    private $_hashTag;

    /**
     *
     * @param Request $request
     * @param callable|string $destination Either a callback to the function that the request should be routed to,
     * or the name of a script file to hand the request off to if the destination is not a function
     */
    public function __construct(Request $request, $destination, $mimeType = null, $fileName = null, $clientCacheTimeout = 86400, $lastChangeTime = null, $hashTag = null)
    {
        parent::__construct($destination);

        $this->_destination = $destination;
        $this->_request     = $request;
        $this->_mimeType    = $mimeType;
        $this->_fileName    = $fileName;
        $this->_clientCacheTimeout = $clientCacheTimeout;
        $this->_lastChangeTime = $lastChangeTime;
        $this->_hashTag = $hashTag;
    }

    public function Get_Destination()
    {
        return $this->_destination;
    }

    public function Get_MimeType()
    {
        return $this->_mimeType;
    }

    public function Get_ClientCacheTimeout()
    {
        return $this->_clientCacheTimeout;
    }

    public function GotoRoute(IApplication $app = null)
    {
        $returnString = null;
        if (is_file($this->_destination))
        {
            $returnString = file_get_contents($this->_destination);
            $fileChangeTime = $this->_lastChangeTime?$this->_lastChangeTime:filemtime($this->_destination);
            if (!$this->_mimeType)
            {
                $this->_mimeType = File::GetMimeType($this->_destination);
            }
            if ($this->_fileName)
            {
                $fileName = $this->_fileName;
            } else {
                $fileName = basename($this->_destination);
            }
        }
        else
        {
            $returnString = $this->_destination;
            $fileChangeTime = $this->_lastChangeTime?$this->_lastChangeTime:time();
            if (!$this->_mimeType)
            {
                throw new \Exception("MIME type argument required when serving raw text content");
            }
            $fileName = $this->_fileName;
        }
        $etag = $this->_hashTag?$this->_hashTag:md5($this->_fileName.$fileChangeTime);
        $response       = new Response($returnString);
        $response->Set_MimeType($this->_mimeType);
        $response->Set_Filename($fileName);
        $response->Set_LastModifiedTime($fileChangeTime);
        $response->Set_ETag($etag);
        $response->Set_BrowserCacheTimeout(Headers::CACHEPOINT_BROWSER, $this->_clientCacheTimeout);
        return $response;
    }

    public function GetDestinationClassname()
    {
        if (class_exists($this->_destination))
        {
            return $this->_destination;
        }
        elseif (is_array($this->_destination) && class_exists($this->_destination[0]))
        {
            return $this->_destination[0];
        }
        else
        {
            return null;
        }
    }

    public function GetUrl(IRoute $route, $absoluteUrl = false, $useHttps = false)
    {
        throw new NotYetImplementedException();
    }
}