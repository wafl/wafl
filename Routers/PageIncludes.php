<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\Communication\Http\Routing\Route;
use \DblEj\Util\Strings;
use Wafl\Core;

final class PageIncludes
implements IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute          = null;
        $requestFilenameArray = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
        if (!isset($requestFilenameArray[0]) || !$requestFilenameArray[0] && count($requestFilenameArray) > 1)
        {
            array_shift($requestFilenameArray);
        }
        $requestFilename = implode("/", $requestFilenameArray);
        if ($app)
        {
            if (Strings::StartsWith($requestFilename, $app->Get_Settings()->Get_Web()->Get_WebUrlRelative()))
            {
                $requestFilename = substr($requestFilename, strlen($app->Get_Settings()->Get_Web()->Get_WebUrlRelative()));
            }
        }
        if ((count($requestFilenameArray) > 2 && $requestFilenameArray[0] == "Wafl" && $requestFilenameArray[1] == "DblEj") || ($requestFilenameArray[0] == "DblEj.js")) //either /Wafl/DblEj/PageName.js (old style) or /DblEj.js (new style)
        {
            if ($app !== null)
            {
                $dbjEjResource  = new \Wafl\DynamicResources\DblEjJs("DblEjJs", $requestFilename);
                $returnRoute    = new Route($request, $dbjEjResource, null, $dbjEjResource);
            }
        }
        elseif (Strings::EndsWith($requestFilename, ".css"))
        {
            if ($app !== null) //dont combine this with the outer elseif above like you're tempted to.  I want to be sure this is the only handling block for this request.
            {
                $pagename          = substr($requestFilename, 0, strlen($requestFilename) - 4);
                $resourceClassName = "\\Wafl\\DynamicResources\\SitePageCss";
                $cssResource       = new $resourceClassName($pagename, $requestFilename);
                $serverFile        = $cssResource->Get_ContentReference();
                if
                (
                    file_exists
                    (
                        $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot() .
                        $app->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR . "Presentation" .
                        DIRECTORY_SEPARATOR . "Stylesheets" . DIRECTORY_SEPARATOR . $serverFile
                    )
                )
                {
                    $returnRoute = new Route($request, $cssResource);
                }
            }
        }
        if ($returnRoute)
        {
            $usedRouter = $this;
        }
        return $returnRoute;
    }
}