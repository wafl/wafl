<?php

namespace Wafl\Routers;

use DblEj\Application\IApplication,
    DblEj\Communication\IRequest,
    DblEj\Communication\IRouter,
    DblEj\Communication\Request,
    DblEj\Communication\Route;

class RouterChain
{
    protected $_routers;

    public function __construct()
    {
        $this->_routers = array();
    }

    public function AddRouter(IRouter $router)
    {
        $this->_routers[] = $router;
    }

    public function IsValidRouter($routerName)
    {
        $filename = __DIR__ . DIRECTORY_SEPARATOR . $routerName;
        return file_exists($filename);
    }

    /**
     *
     * @param Request $request
     * @param IRouter optionally pass a veriable for this argument and it will be populated with the router that got the route
     * @return Route
     */
    public function GetRoute(IRequest $request, IApplication $app = null, IRouter &$usedRouter = null)
    {
        $route = null;
        foreach ($this->_routers as $router)
        {
            $route = $router->GetRoute($request, $app, $usedRouter);
            if ($usedRouter)
            {
                break;
            }
        }
        return $route;
    }
}