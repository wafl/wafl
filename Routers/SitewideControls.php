<?php

namespace Wafl\Routers;

use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Routing\IInternalRouter;
use DblEj\Communication\Http\Routing\IRoute;
use DblEj\Communication\Http\Routing\IRouter;
use DblEj\Communication\Http\Routing\Route;
use \DblEj\Util\Strings;

final class SitewideControls
implements IInternalRouter
{

    public function GetRoute(\DblEj\Communication\IRequest $request, \DblEj\Application\IApplication $app = null, \DblEj\Communication\IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, \DblEj\Application\IWebApplication $app = null, IRouter &$usedRouter = null)
    {
        $returnRoute          = null;
        $requestFilenameArray = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
        if (!isset($requestFilenameArray[0]) || !$requestFilenameArray[0] && count($requestFilenameArray) > 1)
        {
            array_shift($requestFilenameArray);
        }
        $requestFilename = implode("/", $requestFilenameArray);
        if ($app)
        {
            if (Strings::StartsWith($requestFilename, $app->Get_Settings()->Get_Web()->Get_WebUrlRelative()))
            {
                $requestFilename = substr($requestFilename, strlen($app->Get_Settings()->Get_Web()->Get_WebUrlRelative()));
            }
        }
        if ($requestFilename == "SitewideControls.js")
        {
            $controlJs = new \Wafl\DynamicResources\SitewideControlsJs("SitewideControlsJs", $requestFilename);
            $returnRoute       = new Route($request, $controlJs, null, $controlJs);
        }
        elseif ($requestFilename == "SitewideControls.css")
        {
            $returnRoute       = new Route($request, new \Wafl\DynamicResources\SitewideControlsCss("SitewideControlsCss", $requestFilename));
        }
        if ($returnRoute)
        {
            $usedRouter = $this;
        }
        return $returnRoute;
    }

    public function GetUrl(IRoute $route, \DblEj\Application\IApplication $app = null, $absoluteUrl = false, $useHttps = false)
    {
        throw new \DblEj\System\NotYetImplementedException();
    }
}