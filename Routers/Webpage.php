<?php
//default webpage router uses the filename as the logic

namespace Wafl\Routers;

use DblEj\Application\IApplication,
    DblEj\Application\IWebApplication,
    DblEj\Communication\Http\Request,
    DblEj\Communication\Http\Routing\IInternalRouter,
    DblEj\Communication\Http\Routing\InvalidRouterRequestException,
    DblEj\Communication\Http\Routing\IRouter as HttpRouter,
    DblEj\Communication\Http\Routing\Route,
    DblEj\Communication\Http\Routing\RouteNotFoundException,
    DblEj\Communication\IRequest,
    DblEj\Communication\IRouter as IRouter,
    DblEj\Util\File,
    \DblEj\Util\Strings;

final class Webpage
implements IInternalRouter
{
    private $_actionVarName = "Action";
    private $_defaultAction = "DefaultAction";
    private $_actionVarType = Request::INPUT_REQUEST;

    public function GetRoute(IRequest $request, IApplication $app = null, IRouter &$usedRouter = null)
    {
        return $this->GetHttpRoute($request, $app, $usedRouter);
    }

    public function GetHttpRoute(Request $request, IWebApplication $app = null, HttpRouter &$usedRouter = null)
    {
        $returnRoute = null;
        if ($app !== null)
        {
            $requestFile = $request->Get_RequestUrl();
            if (strpos($requestFile, "?"))
            {
                $requestFile = substr($requestFile, 0, strpos($requestFile, "?"));
            }
            $handlerFullPath = "";

            $mainAreaName  = $app->Get_Settings()->Get_Application()->Get_SiteArea();
            $rootNamespace = $app->Get_Settings()->Get_Application()->Get_RootNameSpace();
            if (substr($requestFile, strlen($requestFile) - 1, 1) == "/")
            {
                $defaultPage = null;
                $mainArea    = $app->Get_SiteMap()->GetSiteArea($mainAreaName);
                if (!$mainArea)
                {
                    throw new InvalidRouterRequestException($request->Get_RequestUrl(), $this, "The application does not specify a valid top level Site Area.");
                }
                foreach ($app->Get_SiteMap()->GetSitePages($mainArea) as $sitePage)
                {
                    if ($sitePage->Get_IsAreaDefault())
                    {
                        $handlerFolder = dirname($sitePage->Get_LogicPath());
                        if ($handlerFolder == ".")
                        {
                            $sitePageAreaDefaultPath = "/";
                        }
                        else
                        {
                            $sitePageAreaDefaultPath = "/" . $handlerFolder . "/";
                        }
                        if ($sitePageAreaDefaultPath === $requestFile)
                        {
                            $defaultPage = $sitePage;
                            break;
                        }
                    }
                }
                $localRoot      = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();
                $appFolder      = $app->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR;
                $localAppFolder = $localRoot . $appFolder;
                if (!$defaultPage)
                {
                    $handlerFullPath = $localAppFolder . "Logic/" . $app->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage();
                    $defaultPage     = $app->GetSitePageByFilename($app->Get_Settings()->Get_Paths()->Get_Application()->Get_DefaultPage());
                }
                if ($defaultPage)
                {
                    $handlerFullPath = $localAppFolder . "Logic/" . $defaultPage->Get_LogicPath();
                    if (Strings::EndsWith($handlerFullPath, ".php"))
                    {
                        $logicClassName = "\\" . $rootNamespace . "\\Logic" . str_replace("/", "\\", $requestFile) . substr(basename($handlerFullPath), 0, strlen(basename($handlerFullPath)) - 4);
                    }
                    else
                    {
                        $logicClassName = "\\" . $rootNamespace . "\\Logic" . str_replace("/", "\\", $requestFile) . basename($handlerFullPath);
                    }
                }
                else
                {
                    throw new RouteNotFoundException($request->Get_RequestUrl(), $this, "The default logic file for this area (" . basename($handlerFullPath) . ") does not seem to exist in the requested folder (" . $requestFile . ") or is not accessible");
                }
                if (!file_exists($handlerFullPath))
                {
                    $handlerFullPath = $handlerFullPath . ".php";
                }

                if (!file_exists($handlerFullPath))
                {
                    throw new RouteNotFoundException($request->Get_RequestUrl(), "The default logic file (" . ($defaultPage ? $defaultPage->Get_LogicPath() : "Unknown") . ") does not seem to exist in the requested folder (" . $requestFile . ") or is not accessible");
                }
            }
            if (!$handlerFullPath)
            {
                $localRoot = $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot();

                $handlerFullPath    = $localRoot . $rootNamespace . DIRECTORY_SEPARATOR . "Logic" . $requestFile;
                $logicClassName     = "\\" . $rootNamespace . "\\Logic" . str_replace("/", "\\", $requestFile);
                $fileExtensionStart = stripos($logicClassName, ".");
                if ($fileExtensionStart !== false)
                {
                    $logicClassName = substr($logicClassName, 0, $fileExtensionStart);
                }
                if (!file_exists($handlerFullPath))
                {
                    $handlerFullPath = $handlerFullPath . ".php";
                }
            }
            $queryArgs = array();
            if ($handlerFullPath && file_exists($handlerFullPath))
            {
                $usedRouter     = $this;
                $urlParts       = parse_url($request->Get_RequestUrl());
                $urlQueryString = isset($urlParts["query"]) ? $urlParts["query"] : "";
                foreach (explode("&", $urlQueryString) as $keyValPairString)
                {
                    $keyValPair = explode("=", $keyValPairString);
                    if (count($keyValPair = 2))
                    {
                        $queryArgs[$keyValPair[0]] = $keyValPair[1];
                    }
                }
                $isPhpFile = \DblEj\Util\PhpFile::DoesFileHaveOpeningPhpTag($handlerFullPath);
                if ($isPhpFile)
                {
                    $hasPhpClass = \DblEj\Util\PhpFile::DoesFileHavePhpClass($handlerFullPath, 30, true, true);
                    if ($hasPhpClass)
                    {
                        require_once($handlerFullPath);
                        $logicClass  = new $logicClassName();
                        $returnRoute = new Route
                        (
                            $request, array
                            (
                                $logicClass,
                                "HandleRequest"
                            ),
                            array
                            (
                                $request->Get_RequestUrl(),
                                $request,
                                $app
                            ),
                            array
                            (
                                $logicClass,
                                "PrepareResponse"
                            )
                        );
                    }
                    else
                    {
                        $returnRoute = new Route($request, $handlerFullPath, $queryArgs);
                    }
                }
                else if (Strings::EndsWith($handlerFullPath, ".js"))
                {
                    $requestFilenameArray = explode("/", parse_url($request->Get_RequestUrl(), PHP_URL_PATH));
                    if (!isset($requestFilenameArray[0]) || !$requestFilenameArray[0] && count($requestFilenameArray) > 1)
                    {
                        array_shift($requestFilenameArray);
                    }
                    $requestFilename = implode("/", $requestFilenameArray);
                    $pagename        = substr($requestFilename, 0, strlen($requestFilename) - 3);
                    if (strlen($pagename) < 7 || substr($pagename, 0, 7) != "Shared/")
                    {
                        $jsResource      = new \Wafl\DynamicResources\SitePageJs($pagename, $requestFilename);
                        $serverFile      = $jsResource->Get_ContentReference();
                        if
                        (
                        file_exists
                        (
                        $app->Get_Settings()->Get_Paths()->Get_Application()->Get_LocalRoot() .
                        $app->Get_Settings()->Get_Application()->Get_RootNameSpace() . DIRECTORY_SEPARATOR .
                        "Logic" . DIRECTORY_SEPARATOR . $serverFile
                        )
                        )
                        {
                            $returnRoute = new Route($request, $jsResource);
                        }
                    }
                }
            }
        }
        return $returnRoute;
    }

    final public function GetActionVarType()
    {
        return $this->_actionVarType;
    }

    public function Get_DefaultActionName()
    {
        return $this->_defaultAction;
    }

    final public function GetActionVarName()
    {
        return $this->_actionVarName;
    }
}