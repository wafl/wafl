<?php
namespace Wafl\Scripts;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "ScriptBase.php");
class CompileControl
extends \Wafl\Scripts\ScriptBase
{
    protected function getUsageString()
    {
        return "CompileControl is used to compile a control from source code to a .wctl file.\n"
        . "Usage:\n"
        . "CompileControl <ControlName> <SourceFolder> <DestinationFolder> [<Version>]\n\n"
        . "ControlName        The globally unique name for the control. (Letters, numbers, and hyphens only and must start with a capital letter)\n\n"
        . "SourceFolder         The directory that the control source code is in\n\n"
        . "DestinationFolder    The directory that the resulting .wctl file will be saved in\n\n"
        . "Version              Optionally specify the Controls's version, which is used only to make the output filename unique per version";
    }

    protected function onRun($argCount, $args)
    {
        $didRun = false;
        if ($argCount >= 3)
        {
            $controlName        = $args[0];
            $sourceFolder       = $args[1];
            $destFolder         = $args[2];
            $version            = isset($args[3])?$args[3]:"0.0.1";

            if (!file_exists($sourceFolder))
            {
                throw new \DblEj\System\InvalidArgumentException("Invalid source folder specified ($sourceFolder)");
            }
            if (!file_exists($destFolder))
            {
                mkdir($destFolder);
            }
            $patternMatches = null;
            preg_match("/[A-Z]+[a-zA-Z0-9\-]*/", $controlName, $patternMatches);
            if (!$patternMatches || $patternMatches[0] != $controlName)
            {
                throw new \DblEj\System\InvalidArgumentException("Invalid control name specified ($controlName)");
            }
            $tempZipPath        = tempnam(sys_get_temp_dir(), "waflctl");
            $finalExtPath       = realpath($destFolder).DIRECTORY_SEPARATOR.$controlName."-$version.wctl";

            if (file_exists($tempZipPath))
            {
                unlink($tempZipPath);
            }
            $tempZip = new \DblEj\Util\ZipArchive();
            $tempZip->open($tempZipPath, \DblEj\Util\ZipArchive::CREATE);
            foreach (\DblEj\Util\Folder::GetAllChildObjects($sourceFolder) as $childObject)
            {
                if (is_dir($childObject) && (basename($childObject) != "Private"))
                {
                    $tempZip->addDir($childObject, basename($childObject), []);
                }
                elseif (is_file($childObject))
                {
                    $tempZip->addFile($childObject, basename($childObject));
                }
            }
            $tempZip->addFromString("WaflControl", "$controlName\n$version\n".strftime("%c"));
            $tempZip->close();
            if (copy($tempZipPath, $finalExtPath))
            {
                unlink($tempZipPath);
            } else {
                throw new \Exception("There was an error compiling the control");
            }
            $didRun = true;
        }
        return $didRun;
    }

    protected function getExtensionDependencies()
    {
        return array();
    }
}