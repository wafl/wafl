<?php
namespace Wafl\Scripts;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "ScriptBase.php");
class CompileExtension
extends \Wafl\Scripts\ScriptBase
{
    protected function getUsageString()
    {
        return "CompileExtension is used to compile an extension from source code to a .wext file.\n"
        . "Usage:\n"
        . "CompileExtension <ExtensionName> <SourceFolder> <DestinationFolder> [<Version>]\n\n"
        . "ExtensionName        The globally unique name for the extension. (Letters, numbers, and hyphens only and must start with a capital letter)\n\n"
        . "SourceFolder         The directory that the extension source code is in\n\n"
        . "DestinationFolder    The directory that the resulting .wext file will be saved in\n\n"
        . "Version              Optionally specify the Extension's version, which is used only to make the output filename unique per version";
    }

    protected function onRun($argCount, $args)
    {
        $didRun = false;
        if ($argCount >= 3)
        {
            $extensionName      = $args[0];
            $sourceFolder       = $args[1];
            $destFolder         = $args[2];
            $version            = isset($args[3])?$args[3]:"0.0.1";

            if (!file_exists($sourceFolder))
            {
                throw new \DblEj\System\InvalidArgumentException("Invalid source folder specified ($sourceFolder)");
            }
            if (!file_exists($destFolder))
            {
                mkdir($destFolder);
            }
            $patternMatches = null;
            preg_match("/[A-Z]+[a-zA-Z0-9\-]*/", $extensionName, $patternMatches);
            if (!$patternMatches || $patternMatches[0] != $extensionName)
            {
                throw new \DblEj\System\InvalidArgumentException("Invalid extension name specified ($extensionName)");
            }
            $tempZipPath        = tempnam(sys_get_temp_dir(), "waflext");
            $finalExtPath       = realpath($destFolder).DIRECTORY_SEPARATOR.$extensionName."-$version.wext";
            if (file_exists($tempZipPath))
            {
                unlink($tempZipPath);
            }
            $tempZip = new \DblEj\Util\ZipArchive();
            $tempZip->open($tempZipPath, \DblEj\Util\ZipArchive::CREATE);
            foreach (\DblEj\Util\Folder::GetAllChildObjects($sourceFolder) as $childObject)
            {
                if (is_dir($childObject) && (basename($childObject) != "Private"))
                {
                    $tempZip->addDir($childObject, basename($childObject), []);
                }
                elseif (is_file($childObject) && (basename($childObject) != "build.xml") && (!\DblEj\Util\Strings::EndsWith(basename($childObject), ".wext")))
                {
                    $tempZip->addFile($childObject, basename($childObject));
                }
            }
            $tempZip->addFromString("WaflExtension", "$extensionName\n$version\n".strftime("%c"));
            $tempZip->close();
            if (copy($tempZipPath, $finalExtPath))
            {
                unlink($tempZipPath);
            } else {
                throw new \Exception("There was an error compiling the extension");
            }
            $didRun = true;
        }
        return $didRun;
    }

    protected function getExtensionDependencies()
    {
        return array();
    }
}