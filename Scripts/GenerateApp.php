<?php
namespace Wafl\Scripts;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "ScriptBase.php");
class GenerateApp
extends \Wafl\Scripts\ScriptBase
{
    protected function getUsageString()
    {
        return "GenerateApp is used to generate a clean ActiveWAFL application using settings in an .ini file.\n"
        . "You can also use GenerateApp to create a stub .ini file that you can modify and then use to generate the app.\n\n"
        . "There are four different application types that GenerateApp can create: A basic app, a web app, an mvc web app, or a modular mvc web app.\n\n"
        . "Usage:\n"
        . "GenerateApp <ApplicationType> <SourceIni> <Destination>\n\n"
        . "ApplicationType must be one of the following:\n"
        . "App                Generate a basic application\n"
        . "WebApp             Generate a web application\n"
        . "MvcWebApp          Generate an mvc web application\n"
        . "ModularMvcWebApp   Generate a modular mvc web application\n"
        . "Ini                Generate an .ini stub file that can then be used as the SourceIni\n\n"
        . "SourceIni          The location of the .ini file that has the preconfiguration\n"
        . "                   settings used when generating the application\n\n"
        . "Destination        The directory that the generated application files,\n"
        . "                   or the generated ini file, will be saved"
        ;
    }

    protected function onRun($argCount, $args)
    {
        $didRun = false;

        if ($argCount == 3)
        {
            $appType         = $args[0];
            $settingsIniFile = $args[1];
            $outFolder       = $args[2];
            $zipOutput       = isset($args[3]) ? ($args[3] == 1) : false;

            $settings      = array();
            $settingsIni   = file_get_contents($settingsIniFile);
            $settingsIni   = str_replace("\r\n", "\n", $settingsIni);
            $settingsIni   = str_replace("\r", "\n", $settingsIni);
            $settingsArray = explode("\n", $settingsIni);

            foreach ($settingsArray as $setting)
            {
                if (stristr($setting,"="))
                {
                    $settingArray                     = explode("=", $setting);
                    $settings[trim($settingArray[0])] = trim($settingArray[1]);
                }
            }
            \Wafl\Util\AppTemplates::MakeAppTemplate($appType, $settings, $outFolder, $zipOutput);
            $didRun = true;
        }
        elseif ($argCount == 2 && $args[0] == "ini")
        {
            \Wafl\Util\AppTemplates::MakeIni($args[1]);
            $didRun = true;
        }
        return $didRun;
    }

    protected function getExtensionDependencies()
    {
        return array();
    }
}