<?php
namespace Wafl\Scripts;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "ScriptBase.php");
class GeneratePhar
extends \Wafl\Scripts\ScriptBase
{

    protected function getUsageString()
    {
        return "GeneratePhar APPNAME SOURCEFOLDER DESTINATIONFOLDER [COMPRESS] [ENTRYPOINTFILENAME] [ADD_EXTERNAL_FILE] [ADD_EXTERNAL_FILE]...";
    }

    protected function onRun($argCount, $args)
    {
        $didRun = false;
        if ($argCount > 2)
        {
            ini_set("phar.readonly", "Off");
            $appName        = $args[0];
            $sourceFolder   = $args[1];
            $destFolder     = $args[2];
            $externalFiles  = array();
            $compressPhar   = false;
            $entryPointFile = "";
            if ($argCount > 3)
            {
                $compressPhar = $args[3] != 0;
            }
            if ($argCount > 4)
            {
                $entryPointFile = $args[4];
            }
            for ($argIdx = 5; $argIdx < $argCount; $argIdx++)
            {
                $externalFiles[] = $args[$argIdx];
            }
            if (!file_exists($sourceFolder))
            {
                throw new \Exception("Invalid source folder: $sourceFolder");
            }
            if (!file_exists($destFolder))
            {
                throw new \Exception("Invalid destination folder: $destFolder");
            }
            if (!\DblEj\Util\Strings::EndsWith($sourceFolder, "/") && !\DblEj\Util\Strings::EndsWith($sourceFolder, "\\"))
            {
                $sourceFolder .= DIRECTORY_SEPARATOR;
            }
            if (!\DblEj\Util\Strings::EndsWith($destFolder, "/") && !\DblEj\Util\Strings::EndsWith($destFolder, "\\"))
            {
                $destFolder .= DIRECTORY_SEPARATOR;
            }
            print "\nGenerating Phar...\n\n\tApp Name: $appName\n\n\tSource Folder: $sourceFolder\n\n\tDestination Folder: $destFolder\n\n\tEntry Point File:\n\t$entryPointFile\n";
            \Wafl\Util\Phar::CreatePhar($appName, $sourceFolder, $destFolder, $entryPointFile, $externalFiles, true, $compressPhar);
            print "\nPhar file successfully created.\n";
            $didRun = true;
        }
        return $didRun;
    }

    protected function getExtensionDependencies()
    {
        return array();
    }
}