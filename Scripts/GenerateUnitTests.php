<?php
namespace Wafl\Scripts;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "ScriptBase.php");
class GenerateUnitTests
extends \Wafl\Scripts\ScriptBase
{

    protected function getUsageString()
    {
        return "GenerateUnitTests APPNAME SOURCEFOLDER DESTINATIONFOLDER";
    }

    protected function getExtensionDependencies()
    {
        return array(
            "\\DblEj\\UnitTesting\\ITestGenerator");
    }

    protected function onRun($argCount, $args)
    {
        $didRun = false;

        if ($argCount > 2)
        {
            $extensionClass = \Wafl\Util\Extensions::FindExtensionClassByType("\\DblEj\\UnitTesting\\ITestGenerator");
            $extension      = \Wafl\Util\Extensions::AddExtension(\DblEj\Util\Reflection::GetBaseClassname($extensionClass));
            if ($extension)
            {
                $appName      = $args[0];
                $sourceFolder = str_replace(array("/","\\"), DIRECTORY_SEPARATOR, $args[1]);
                $destFolder   = str_replace(array("/","\\"), DIRECTORY_SEPARATOR, $args[2]);
                $ignorePaths  = str_replace(array("/","\\"), DIRECTORY_SEPARATOR, (isset($args[3]) ? $args[3] : ""));

                if (!\DblEj\Util\Strings::EndsWith($sourceFolder, DIRECTORY_SEPARATOR))
                {
                    $sourceFolder.=DIRECTORY_SEPARATOR;
                }
                if (!\DblEj\Util\Strings::EndsWith($destFolder, DIRECTORY_SEPARATOR))
                {
                    $destFolder.=DIRECTORY_SEPARATOR;
                }
                $ignorePaths = explode(",", $ignorePaths);
                $extension->Configure("AppName", $appName);
                $extension->Configure("SourceFolder", $sourceFolder);
                $extension->Configure("DestinationFolder", $destFolder);
                $extension->Configure("IgnorePaths", $ignorePaths);

                $extension->AddHandler
                (
                    new \DblEj\EventHandling\DynamicEventHandler
                    (
                        \Wafl\Extensions\UnitTestGenerator\UnitTestGenerator::EVENT_GENERATE_UNIT_TEST_BEGIN,
                        function(\DblEj\EventHandling\EventInfo $event) use ($appName, $sourceFolder, $destFolder)
                        {
                            print "\nGenerating Unit Tests...\n\n\tApp Name: $appName\n\n\tSource Folder: $sourceFolder\n\n\tDestination Folder: $destFolder\n";
                        }
                    )
                );
                $extension->AddHandler
                (
                    new \DblEj\EventHandling\DynamicEventHandler
                    (
                        \Wafl\Extensions\UnitTestGenerator\UnitTestGenerator::EVENT_GENERATE_UNIT_TEST_FILE,
                        function(\DblEj\EventHandling\EventInfo $event)
                        {
                            print "\tGenerating tests for file: " . $event->Get_AppliesTo() . "...\n";
                        }
                    )
                );
                $extension->AddHandler
                (
                    new \DblEj\EventHandling\DynamicEventHandler
                    (
                        \Wafl\Extensions\UnitTestGenerator\UnitTestGenerator::EVENT_FOUND_CLASS,
                        function(\DblEj\EventHandling\EventInfo $event)
                        {
                            print $event->Get_Message() . PHP_EOL;
                        }
                    )
                );
                $extension->AddHandler
                (
                    new \DblEj\EventHandling\DynamicEventHandler
                    (
                        \Wafl\Extensions\UnitTestGenerator\UnitTestGenerator::EVENT_CLASS_NOT_FOUND,
                        function(\DblEj\EventHandling\EventInfo $event)
                        {
                            print $event->Get_Message() . PHP_EOL;
                        }
                    )
                );
                $extension->AddHandler
                (
                    new \DblEj\EventHandling\DynamicEventHandler
                    (
                        \Wafl\Extensions\UnitTestGenerator\UnitTestGenerator::EVENT_SAVING_TEST_FILE,
                        function(\DblEj\EventHandling\EventInfo $event)
                        {
                            print $event->Get_Message() . PHP_EOL;
                        }
                    )
                );
                $extension->AddHandler
                (
                    new \DblEj\EventHandling\DynamicEventHandler
                    (
                        \Wafl\Extensions\UnitTestGenerator\UnitTestGenerator::EVENT_GENERATE_UNIT_TEST_COMPLETED,
                        function(\DblEj\EventHandling\EventInfo $event)
                        {
                            print $event->Get_Message() . PHP_EOL;
                        }
                    )
                );


                $extension->GenerateTests();
                $didRun = true;
            }
            else
            {
                throw new \Exception("Could not find a suitable extension for generating unit tests.  Please install PHP Unit or a similar extension.");
            }
        }
        return $didRun;
    }
}