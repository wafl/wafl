<?php

namespace Wafl\Scripts;

interface IScriptRunner
{
    public function Run($clArgs);
}