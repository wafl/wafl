<?php

namespace Wafl\Scripts;

require_once(__DIR__ . DIRECTORY_SEPARATOR . "IScriptRunner.php");
abstract class ScriptBase
implements IScriptRunner
{
    const VERBOSITY_DEBUG = 100;
    const VERBOSITY_INFO = 200;
    const VERBOSITY_WARN = 300;
    const VERBOSITY_ERROR = 400;
    const VERBOSITY_PROMPT = 500;
    const VERBOSITY_QUIET = 1000;

    private $_verbosityLevel = self::VERBOSITY_INFO;
    private $_logVerbosity = self::VERBOSITY_WARN;
    private $_scriptHasSentOutput = false;

    abstract protected function getUsageString();
    abstract protected function onRun($argCount, $args);

    /**
     * This method is called whenever a CLI arg is parsed.
     * This method should *not* be called by subclasses.
     * Subclasses should override this method to be notified when the args are parsed.
     *
     * @param string $argName
     * @param string $argVal
     *
     * @return boolean Returns true if the argument was parsed/handled.
     */
    protected function onArgParse($argName, $argVal)
    {
        $parsed = false;
        switch ($argName)
        {
            case "WaflEnvironment":
                //I dont think this is needed anymore.
                //Wafl now always expects this to be in the environment variable ahead of time
                putenv("WAFL_ENVIRONMENT=$argVal");
                $parsed = true;
                break;
            case "DblEjPath":
                //I dont think this is needed anymore.
                //This gets resolve in the app's Application.php file
                putenv("DBLEJ_PATH=$argVal");
                $parsed = true;
                break;
        }
        return $parsed;
    }

    protected function GetRequiredArgCount()
    {
        return 0;
    }

    public function SetLoggingVerbosityLevel($verbosity = self::VERBOSITY_WARN)
    {
        $this->_logVerbosity = $verbosity;
    }
    public function SetDefaultVerbosityLevel($verbosity = self::VERBOSITY_WARN)
    {
        $this->_verbosityLevel = $verbosity;
    }
    protected function repeatUserInputUntilConditionIsMet(callable $conditionEvaluator, callable $sanitizer, $evaluatedObjectTypePrintable, $promptString = null, $valueRequired = true)
    {
        $this->printLine($promptString, false, self::VERBOSITY_PROMPT);
        $userInput = $this->getUserInput();
        while (!$conditionEvaluator($userInput) && (!$valueRequired || $userInput))
        {
            $cleanedInput = $sanitizer($userInput);
            $this->printLine("$userInput is not a valid $evaluatedObjectTypePrintable.  Would you like to use " . $cleanedInput . "? (Y/N)", false, self::VERBOSITY_PROMPT);
            $userInput    = $this->getUserInput();
            if (strtolower($userInput) == "y")
            {
                $userInput = $cleanedInput;
            }
            else
            {
                $this->printLine(PHP_EOL . "Please try again by entering a valid $evaluatedObjectTypePrintable", false, self::VERBOSITY_PROMPT);
                $userInput = $this->getUserInput();
            }
        }
        return $userInput;
    }

    protected function printLine($line, $writeOverPreviousLine = false, $verbosity = null)
    {
        if (!$verbosity)
        {
            $verbosity = $this->_verbosityLevel;
        }
        if ($verbosity >= $this->_logVerbosity)
        {
            if ($this->_scriptHasSentOutput)
            {
                if ($writeOverPreviousLine)
                {
                    print "\r                                                                                                             \r";
                }
                else
                {
                    print PHP_EOL;
                }
            }
            print $line;
            $this->_scriptHasSentOutput = true;
        }
    }

    protected function clearConsole()
    {
        for ($i = 1; $i < 100; $i++)
        {
            print "\n";
        }
        print "\r";
    }

    protected function getUserInput($prompt = null, $overwritePreviousLine = false)
    {
        if ($prompt)
        {
            $this->printLine($prompt, $overwritePreviousLine, self::VERBOSITY_PROMPT);
        }
        $handle = fopen("php://stdin", "r");
        $input  = trim(fgets($handle), "\n\r");
        fclose($handle);
        return $input;
    }

    protected function getExtensionDependencies()
    {
        return [];
    }

    public function Run($clArgs)
    {
        try
        {
            if (!defined("AM_WEBPAGE"))
            {
                define('AM_WEBPAGE', false);
            }
            if (!defined("WAFL_SCRIPT_ID"))
            {
                define('WAFL_SCRIPT_ID', $this->getDisplayName());
            }

            $cleanedArgs = array();

            if (count($clArgs) > 1)
            {
                $clArgs = array_slice($clArgs, 1);
                for ($argIdx = 0; $argIdx < count($clArgs); $argIdx++)
                {
                    $argPair = explode("=", $clArgs[$argIdx]);
                    if (count($argPair) > 1)
                    {
                        $argName = $argPair[0];
                        $argVal  = $argPair[1];
                    }
                    else
                    {
                        $argName = strval(count($cleanedArgs));
                        $argVal  = $argPair[0];
                    }

                    if (!$this->onArgParse($argName, $argVal))
                    {
                        $cleanedArgs[$argName] = $argVal;
                    }
                }
            }
            if (count($cleanedArgs) < $this->GetRequiredArgCount())
            {
                throw new \Exception("Missing required arguments");
            }
            require_once(__DIR__ . "/../Core.php");
            require_once(__DIR__ . "/../Autoloaders/Autoload.php");
            if (count($this->getExtensionDependencies()) > 0)
            {
                $localExtensionsFolder = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder()) . DIRECTORY_SEPARATOR;
                if (!file_exists($localExtensionFolder))
                {
                    throw new \Exception("Script cannot run because it has Extension dependencies, but the Extensions folder is not in the specifed or default location (" . $localExtensionFolder . ").  Please pass in the ExtensionsFolder argument to use this script.");
                }
            }

            $this->finishRunning();

            if (!$this->onRun(count($cleanedArgs), $cleanedArgs))
            {
                $this->printLine($this->getUsageString().PHP_EOL, false, self::VERBOSITY_PROMPT);
            }
            else
            {
                $this->printLine($this->getDisplayName() . ": Sucessfully finished running", false);
            }
            \Wafl\Core::Set_RequestPending(false);
            print PHP_EOL.PHP_EOL;
            exit(0);
        }
        catch (\Exception $e)
        {
            $this->printLine($e->getMessage(). PHP_EOL . $this->getUsageString(), false, self::VERBOSITY_ERROR);
            print PHP_EOL.PHP_EOL;
            exit(1);
        }
    }

    protected function getDisplayName()
    {
        return \DblEj\Util\Reflection::GetBaseClassname(get_called_class());
    }

    protected function finishRunning()
    {
        $localDblEjFolder = realpath(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "DblEj");
        if (!$localDblEjFolder || !file_exists($localDblEjFolder))
        {
            if (defined("DBLEJ_PATH"))
            {
                $localDblEjFolder = DBLEJ_PATH;
            }
            else
            {
                throw new \Exception("WAFL Scripts that are not run in the context of an application require that you either pass the DblEjPath argument, or set the environment variable DBLEJ_PATH to tell me where to find DblEj.  I tried to find it at $localDblEjFolder, but it isn't there.  To run in an application's context, pass the AppRoot argument (you may also need to pass WaflEnvironment).");
            }
        }

        if (substr($localDblEjFolder, strlen($localDblEjFolder) - 1) != DIRECTORY_SEPARATOR)
        {
            $localDblEjFolder .= DIRECTORY_SEPARATOR;
        }
        
        require_once($localDblEjFolder . "Autoloader.php");
        require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Bootstrapper.php");
        \Wafl\Bootstrapper::BootstrapEnvironment();

        $this->printLine(implode(".", \Wafl\Core::GetFrameworkVersion()) . " - " . $this->getDisplayName() . " Utility", false, self::VERBOSITY_DEBUG);
        $this->printLine($this->getDisplayName()." Initialized", false, self::VERBOSITY_DEBUG);
    }
}