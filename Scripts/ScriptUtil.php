<?php

namespace Wafl\Scripts;

if (!defined("AM_WEBPAGE"))
{
    define('AM_WEBPAGE', false);
}
class ScriptUtil
{
    public static function RunAppScript($appScriptClass, $cliArgs)
    {
        if (!class_exists($appScriptClass))
        {
            if (strrpos($appScriptClass, "\\") > -1)
            {
                $appScriptClassBase = substr($appScriptClass, strrpos($appScriptClass, "\\") + 1);
            }
            throw new \Exception("Cannot find the specified application script class: $appScriptClassBase");
        }
        $appScript = new $appScriptClass();
        $appScript->Run($cliArgs);
    }

    public static function ResolveAndRunScriptOrCliApp($scriptClass, $appArgs)
    {
        if (!class_exists("\\Wafl\\Cli\\$scriptClass") && !class_exists("\\Wafl\\Scripts\\$scriptClass"))
        {
            $scriptFile = realpath(__DIR__.DIRECTORY_SEPARATOR.$scriptClass.".php");
            if (!$scriptFile || !file_exists($scriptFile))
            {
                $scriptFile = realpath(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Cli".DIRECTORY_SEPARATOR.$scriptClass.".php");
                if (!$scriptFile || !file_exists($scriptFile))
                {
                    die("ERROR: Cannot find the specified application script: $scriptClass\n");
                } else {
                    require_once($scriptFile);
                }
            } else {
                require_once($scriptFile);
            }
        }

        if (class_exists("\\Wafl\\Cli\\$scriptClass"))
        {
            $isCliBased = true;
        }
        elseif (class_exists("\\Wafl\\Scripts\\$scriptClass"))
        {
            $isCliBased = false;
        }
        if ($isCliBased)
        {
            require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Cli".DIRECTORY_SEPARATOR."CliUtil.php");
            \Wafl\Cli\CliUtil::RunAppScript("\\Wafl\\Cli\\$scriptClass", $appArgs);
        } else {
            ScriptUtil::RunAppScript("\\Wafl\\Scripts\\$scriptClass", $appArgs);
        }
    }
}
