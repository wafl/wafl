<?php

namespace Wafl\Scripts;

use \DblEj\Util\Strings;

require_once(__DIR__ . "/ScriptBase.php");
class Utensils extends \Wafl\Scripts\ScriptBase
{
    private $_amInteractive;

	protected function requiresApp()
	{
		return false;
	}

	protected function onRun($argCount, $args)
	{
        try
        {
            $this->clearConsole();
            $this->SetDefaultVerbosityLevel(self::VERBOSITY_PROMPT);
            print <<<EOL
,-.  .   .                .
  |  |   |- ,-. ,-. ,-. . |  ,-.
  |  | . |  |-' | | `-. | |  `-.
  `--^-' `' `-' ' ' `-' ' `' `-'

EOL;
            print "for ActiveWAFL ".implode(".",\Wafl\Core::GetFrameworkVersion())."\n\n";

            if ($argCount > 0 && isset($args[0]))
            {
                $this->_amInteractive = false;
                $this->runCommand($args[0], $args);
            }
            else
            {
                $this->_amInteractive = true;
                $this->showMainMenu();
            }
        }
        catch (\Exception $err)
        {
            print "There was an unexpected error.".PHP_EOL;
            print $err->getMessage().PHP_EOL;
            print $err->getFile().PHP_EOL;
            print $err->getLine().PHP_EOL;
            while ($err = $err->getPrevious())
            {
                print $err->getMessage().PHP_EOL;
                print $err->getFile().PHP_EOL;
                print $err->getLine().PHP_EOL;
            }
        }

        return true;
	}

    protected function getUsageString()
    {
        return "Usage: Utensils [switches] [arguments]\nFor help type: Utensils -h [-a|-b|-c|-e|-i|-p|-h]\n";
    }

	private function showHelp($helpCommand=null)
	{
        $this->printLine("Utensils Help");
        if ($helpCommand)
        {
            $this->printLine("Command: $helpCommand\n");
            switch ($helpCommand)
            {
                case "-a":
                    $this->printLine("Generate a new application", false);
                    $this->printLine("Usage: utensils -a [ActionType] [AppType] [SettingsIniPath] [DestinationFolder]\n", false);
                    $this->printLine("ActionType        Specify what you want to generate (P|D|I)", false);
                    $this->printLine("                  P = Generate preconfigured application using", false);
                    $this->printLine("                      the settings in SettingsIniPath", false);
                    $this->printLine("                  D = Generate an application with dummy settings", false);
                    $this->printLine("                  I = Generate a stub Settings ini file that can then be", false);
                    $this->printLine("                      used as the SettingsIniPath to generate an application\n", false);
                    $this->printLine("AppType           The type of application to create", false);
                    $this->printLine("                  (App|WebApp|MvcWebApp|ModularMvcWebApp)\n", false);
                    $this->printLine("SettingsIniPath   An .ini file that has the new application's basic settings\n", false);
                    $this->printLine("DestinationFolder The location to store the generated application files\n", false);
                    break;
                case "-b":
                    break;
                case "-c":
                    $this->printLine("Generate a new control",false);
                    $this->printLine("Usage: utensils -c [ControlName] [ControlsNamespace] [DestinationFolder]\n", false);
                    break;
                case "-e":
                    $this->printLine("Generate a new extension",false);
                    $this->printLine("Usage: utensils -e [ExtensionName] [ExtensionsNamespace] [DestinationFolder]\n", false);
                    break;
                case "-h":
                    $this->printLine("Show help",false);
                    $this->printLine("Usage: utensils -h [-a|-b|-c|-e|-i|-p|-h]\n", false);
                    break;
                case "-i":
                    $this->printLine("Print system information",false);
                    break;
                case "-p":
                    $this->printLine("Generate Pharchive\n", false);
                    $this->printLine("Usage:\nutensils -p [SourceFolder] [PharName] [EntryPointFile] [DestinationFolder]\n", false);
                    $this->printLine("SourceFolder          The contents of this folder will be added to the phar\n", false);
                    $this->printLine("PharName              The name of new phar (do not include a file extension)\n", false);
                    $this->printLine("EntryPointFile        The file inside of the phar to run automatically", false);
                    $this->printLine("                      when the phar is executed directly\n", false);
                    $this->printLine("Destination Folder    The folder that the new phar will be written to\n", false);
                    break;
                case "-u":
                    $this->printLine("Update ActiveWAFL",false);
                    $this->printLine("Usage: utensils -u [stable|dev]\n", false);
                    break;
                case "-v":
                    $this->printLine("Verify the ActiveWAFL installation",false);
                    break;
                default:
                    $this->printLine("Cannot show help for invalid command: $helpCommand");
            }
            if ($this->_amInteractive)
            {
                $this->getUserInput("Press [enter] to continue...");
                $this->showMainMenu();
            }
        } else {
            $this->printLine("Utensils is a command-line utility that provides an easy (interactive or automated) way to run common ActiveWAFL utilities.");
            $this->printLine("");
            $this->printLine("To show the interactive menu, run Utensils with no command-line switches.");
            $this->printLine("or");
            $this->printLine("Use one of the following switches to run a specific command non-interactively.");
            $this->printLine("");
            $this->printLine("Available switches:");
            $this->printLine("-a Create a new application");
            $this->printLine("");
            $this->printLine("-b Submit an ActiveWAFL bug");
            $this->printLine("");
            $this->printLine("-e Create a new extension");
            $this->printLine("");
            $this->printLine("-h Show this help screen");
            $this->printLine("");
            $this->printLine("-i Show system information");
            $this->printLine("");
            $this->printLine("-p Create a phar file");
            $this->printLine("");
            $this->printLine("-u Update ActiveWAFL to the latest release version");
            if ($this->_amInteractive)
            {
                $this->printLine("");
                $helpCommand = $this->getUserInput("Which command would you like help with? (a, b, e, i, p, or u): ",true);
                $this->showHelp("-$helpCommand");
            } else {
            $this->printLine("");
            $this->printLine("Run Utensils -h <switch> for help with a specific switch.");
            }
        }
	}

	private function runCommand($command, $args)
	{
		switch ($command)
		{
			case "-h":
				$helpCommand = isset($args[1]) ? $args[1] : null;
				$this->showHelp($helpCommand);
				break;
			case "-a":
				$this->createNewApplication(isset($args[1]) ? $args[1] : null, isset($args[2]) ? $args[2] : null, isset($args[3]) ? $args[3] : null, isset($args[4]) ? $args[4] : null);
				break;
			case "-b":
                //generate a bug report
				break;
			case "-c":
				$this->createNewControl(isset($args[1]) ? $args[1] : null, isset($args[2]) ? $args[2] : null, isset($args[3]) ? $args[3] : null);
				break;
			case "-e":
				$this->createNewExtension(isset($args[1]) ? $args[1] : null, isset($args[2]) ? $args[2] : null, isset($args[3]) ? $args[3] : null);
				break;
			case "-i":
                $this->printSystemInfo();
				break;
			case "-p":
                                if (ini_get("phar.readonly") == "1")
                                {
                                    $this->printLine("Cannot generate phar files because phar.readonly is set to true in the php configuration.");
                                    $this->printLine("Please try again.\n\n");
                                    $this->showMainMenu();
                                } else {
                                    $this->generatePhar(isset($args[1]) ? $args[1] : null, isset($args[2]) ? $args[2] : null, isset($args[3]) ? $args[3] : null, isset($args[4]) ? $args[4] : null);
                                }
                            
				break;
            case "-u":
                $this->updateActiveWafl();
				break;
            case "-v":
                $this->verifyInstall();
				break;
			case "-x":
				//exit
				break;
            default:
                $this->printLine("Invalid seletion.");
                $this->printLine("Please try again.\n\n");
                $this->showMainMenu();
                break;
		}
	}

	private function showMainMenu()
	{
		print <<<EOL
    a. Generate a new (a)application
    c. Generate a new (c)ontrol
    e. Generate a new (e)xtension
    h. (H)elp
    i. Show system (i)nformation
    p. Make a (p)har
    u. (U)pdate ActiveWafl
    x. E(x)it
EOL;

        $this->printLine("Please choose a letter from the menu:");
		$userInput = $this->getUserInput();
		$this->runCommand("-" . $userInput, array());
	}

    private function verifyInstall()
    {
        if (!file_exists(DBLEJ_PATH))
        {
            $this->printLine("Invalid or inaccessible DblEj folder: ".DBLEJ_PATH);
        }

        $waflPath = __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR;
        $waflInfoFile = $waflPath.".wafl";
        if (file_exists($waflPath) && file_exists($waflInfoFile))
        {
            $this->printLine(file_get_contents($waflInfoFile));
            $this->printLine("Your waffle iron is hot and ready!");
        } else {
            $this->printLine("The ActiveWAFL installation seems corrupt.");
        }
    }
    private function updateActiveWafl()
    {
        $proceed = $this->getUserInput("This will overwrite your existing ActiveWAFL and DblEj installations on this system.  Are you sure you wish to proceed (Y/N)?");
        if ($proceed == "y" || $proceed == "Y")
        {
            $waflPath = __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR;
            $waflPath = realpath($waflPath);
            if (file_exists($waflPath.DIRECTORY_SEPARATOR.".wafl") && !is_writable($waflPath.DIRECTORY_SEPARATOR.".wafl"))
            {
                $this->printLine("You do not have permissions to update the ActiveWAFL files.  You probably need to re-run this with administrator permissions.");
            } else {
                $tempZipFile = sys_get_temp_dir().DIRECTORY_SEPARATOR."Temp_ActiveWaflUpdater".uniqid();
                $destinationFileStream = fopen($tempZipFile, "wb");
                $downloadDomain = "activewafl.com";
                $downloadUrl = "http://activewafl.com/DownloadFiles/ActiveWaflWithDblEj";
                $this->printLine("Updating ActiveWAFL to the latest release...");
                if (function_exists("curl_init"))
                {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $downloadUrl);
                    curl_setopt($ch, CURLOPT_FILE, $destinationFileStream);
                    curl_exec($ch);
                    curl_close($ch);
                } else {
                    $parts = parse_url($downloadUrl);
                    $request = "GET {$parts['path']} HTTP/1.1\r\n";
                    $request .= "Host: {$parts['host']}\r\n";
                    $request .= "User-Agent: Mozilla/5.0\r\n\r\n";

                    $errno=null;
                    $errstr=null;
                    $remote = fsockopen($downloadDomain, 80, $errno, $errstr, 5);
                    $pastHeaders = false;
                    $bufferedInput="";
                    $bytesDownloaded=0;
                    if (!stream_set_timeout($remote, 1))
                    {
                        throw new \Exception("Could not set timeout on stream");
                    }
                    if ($remote !== false)
                    {
                        $this->printLine("Downloading, might take a minute...");
                        $this->printLine(" ", false);
                        fwrite($remote, $request);
                        $readCount=0;
                        while (!feof($remote))
                        {
                            $readData = fread($remote, 8192);
                            $readCount++;
                            $bytesDownloaded+=strlen($readData);
                            if ($pastHeaders)
                            {
                                fwrite($destinationFileStream, $readData);
                            } else {
                                $bufferedInput.=$readData;
                                $endPos = strpos($bufferedInput,"\r\n\r\n");
                                if ($endPos===false)
                                {
                                    $endPos = strpos($bufferedInput,"\n\n");
                                    if ($endPos!==false)
                                    {
                                        $endPos+=2;
                                    } else {
                                        $endPos = strpos($bufferedInput,"\r\r");
                                        if ($endPos!==false)
                                        {
                                            $endPos+=2;
                                        }
                                    }
                                } else {
                                    $endPos+=4;
                                }
                                if ($endPos!==false)
                                {
                                    $bufferedInput = substr($bufferedInput, $endPos);
                                    $pastHeaders = true;
                                    fwrite($destinationFileStream, $bufferedInput);
                                    $bufferedInput="";
                                }
                            }
                        }
                    } else {
                       $this->printLine("Cannot connect to the upgrade server");
                    }
                    fclose($remote);
                }
                fclose($destinationFileStream);

                if (file_exists($tempZipFile))
                {
                    $zip = new \ZipArchive();
                    $openResult = $zip->open($tempZipFile);
                    if ($openResult === true)
                    {
                        $dest = realpath($waflPath.DIRECTORY_SEPARATOR."..");
                        if ($dest)
                        {
                            print ".";
                            $zip->extractTo($dest);
                            $zip->close();

                            $newWaflInfoFile = $dest.DIRECTORY_SEPARATOR."Wafl".DIRECTORY_SEPARATOR.".wafl";
                            if (file_exists($newWaflInfoFile))
                            {
                                $this->printLine("ActiveWAFL has been updated to the latest release version.");
                                $this->printLine(file_get_contents($newWaflInfoFile));
                            } else {
                                $this->printLine("The update does not appear to be successful.");
                            }
                        } else {
                            $this->printLine("Invalid ActiveWAFL folder: $waflPath");
                        }
                    } elseif ($openResult == \ZipArchive::ER_INCONS) {
                        print("Install Failed.  Update package is bad: $tempZipFile ".$openResult.PHP_EOL.PHP_EOL);
                        die();
                    } else {
                        print("Install Failed.  Cannot open update package: $tempZipFile ".$openResult.PHP_EOL.PHP_EOL);
                        die();
                    }
                } else {
                    $this->printLine("The update was not successful", false);
                }

            }
        } else {
            $this->printLine("The update was cancelled.  Please choose a menu option.", false);
            $this->printLine("", false);
            $this->showMainMenu();
        }
    }
    private function printSystemInfo()
    {
        $this->printLine("ActiveWAFL ".implode(".",\Wafl\Core::GetFrameworkVersion()));
        $this->printLine(realpath(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR));
        $this->printLine("");
        $this->printLine("DblEj ".implode(".",  \DblEj\Util\System::GetLibraryVersion()));
        $this->printLine(realpath(DBLEJ_PATH));
        $this->printLine("");
        $this->printLine("PHP ".PHP_VERSION);
        $this->printLine(realpath(PHP_BINARY));
        $this->printLine(realpath(php_ini_loaded_file()));
        $this->printLine("");
        $this->printLine("OS ".PHP_OS);
        $this->printLine(php_uname());
    }

	private function generatePhar($srcFolder = null, $appName = null, $entryPointFile = null, $destFolder = null)
	{
		if (!$srcFolder)
		{
			$this->printLine("What is the path to the folder you want to pharchive?");
			$srcFolder = $this->getUserInput("Folder path: ", false);
		}
		else
		{
			$this->printLine("Source folder: $srcFolder", false);
		}
		if ($srcFolder)
		{
			if (file_exists($srcFolder))
			{
				if (is_dir($srcFolder))
				{
                    if (!Strings::EndsWith($srcFolder, "/") && !Strings::EndsWith($srcFolder, "\\"))
                    {
                        $srcFolder.=DIRECTORY_SEPARATOR;
                    }
					if (!$appName)
					{
						$this->printLine("What is the app/module name you want to give to this pharchive?");
						$appName = $this->getUserInput("App/Module Name: ", false);
					}
					else
					{
						$this->printLine("App/Module: $appName", false);
					}

					if ($appName)
					{
						if (!$entryPointFile)
						{
							$this->printLine("What entry point file do you want to use for the phar?");
							$entryPointFile = $this->getUserInput("Entry point file (leave blank for null): ", false);
						}
						else
						{
							$this->printLine("Entry Point: $entryPointFile", false);
						}

						if (!$destFolder)
						{
							$this->printLine("Where do you want to save the generated file?");
							$destFolder = realpath($this->getUserInput("Destination folder (leave blank for current folder): ", false));
						}
						else
						{
							$this->printLine("Destination folder: $destFolder", false);
						}

						if (!$destFolder)
						{
							$destFolder = getcwd();
						}
                        if (!Strings::EndsWith($destFolder, "/") && !Strings::EndsWith($destFolder, "\\"))
                        {
                            $destFolder.=DIRECTORY_SEPARATOR;
                        }
						$this->printLine("Generating phar in $destFolder...", false);
						\Wafl\Util\Phar::CreatePhar($appName, $srcFolder, $destFolder, $entryPointFile, array(), true);

						$this->printLine("The pharchive has been created at $destFolder");
					}
					else
					{
						$this->printLine("You must specify an app/module name.");
						$this->getUserInput();
						$this->showMainMenu();
					}
				}
				else
				{
					$this->printLine("The specified path is not a valid folder.");
					$this->getUserInput();
					$this->showMainMenu();
				}
			}
			else
			{
				$this->printLine("The specified folder does not exist, or you do not have the privileges to access it.");
				$this->getUserInput();
				$this->showMainMenu();
			}
		}
		else
		{
			$this->printLine("You must specify a folder to generate a phar.  Try again? (y/n)", false);
			if ($this->getUserInput() == "y")
			{
				$this->generatePhar();
			}
			else
			{
				$this->printLine("Phar not generated.");
				$this->showMainMenu();
			}
		}
	}

	private function createNewApplication($actionType=null, $appType=null, $templateIniPath=null, $outFolder=null)
	{
		$this->printLine("Create a New Application...", false);
        $this->printLine("");
        if (!$actionType)
        {
            $this->printLine("You can generate a default application with dummy settings, or you can generate a preconfigured stub using settings that you specify in an ini file.", false);
            $this->printLine("");
            $this->printLine("P = Generate preconfigured application using an ini file that I supply. (use option \"I\" to generate an ini file that you can customize)", false);
            $this->printLine("D = Use dummy settings.", false);
            $this->printLine("I = Generate an ini stub that I can customize and use to create a preconfigured application.", false);
            $this->printLine("");
            $actionType = strtolower(trim($this->getUserInput("Which would you like to do? (P/D/I)")));
        }

        $pharPath = realpath(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Resources/AppTemplates.phar");
        $pharPath = str_replace("\\","/",$pharPath);

        switch (strtolower($actionType))
        {
            case "p":
                if (!$appType)
                {
                    $appType = $this->getUserInput("What type of app are you making (App|WebApp|MvcWebApp|ModularMvcWebApp)?",true);
                }
                if (!$templateIniPath)
                {
                    $templateIniPath = $this->getUserInput("What is the path to the ini file?\n",true);
                }
                $compiledSettings = \Wafl\Util\AppTemplates::GetArrayFromIniFile($templateIniPath);
                if (!$outFolder)
                {
                    $outFolder = $this->getUserInput("What directory should I save the generated application in?\n",true);
                }
                break;
            case "d":
                if (!$appType)
                {
                    $appType = $this->getUserInput("What type of app are you making (App|WebApp|MvcWebApp|ModularMvcWebApp)?",true);
                }
                $templateIniPath = "phar://$pharPath/Settings.ini";
                $compiledSettings = \Wafl\Util\AppTemplates::GetArrayFromIniFile($templateIniPath);
                if (!$outFolder)
                {
                    $outFolder = $this->getUserInput("What directory should I save the generated application in?\n",true);
                }
                break;
            case "i":
                if (!$outFolder)
                {
                    $outFolder = $this->getUserInput("What directory would you like to save the ini file in?\n",true);
                }
                \Wafl\Util\AppTemplates::MakeIni($outFolder);
                $settingsFilePath = realpath($outFolder).DIRECTORY_SEPARATOR."Settings.ini";
                if (file_exists($settingsFilePath))
                {
                    $this->printLine($settingsFilePath . " has been created.");
                    $this->printLine("I am going to try to open the INI file in your text editor for you to review.");
                    $this->printLine("You will need to close your text editor before I can continue...");
                    if (stristr(strtolower(php_uname("s")), "windows"))
                    {
                        shell_exec("notepad \"$settingsFilePath\"");
                    }
                    else
                    {
                        shell_exec("vim \"$settingsFilePath\"");
                    }
                } else {
                    $this->printLine("WARNING: ". $settingsFilePath . " does not look like it was created.");
                }
                break;
            default:
                $this->printLine("Invalid app type specified.",false);
                $this->showMainMenu();
        }

        if ($actionType=="p" || $actionType=="d")
        {
            \Wafl\Util\AppTemplates::MakeAppTemplate($appType, $compiledSettings, $outFolder);
            $this->printLine("The new application has been created in ".realpath($outFolder).".");
        }
	}

    private function createNewControl($controlName=null, $controlsNamespace=null, $outFolder=null)
    {
		$this->printLine("Create a New Control...", false);
        $this->printLine("");

        if (!$controlName)
        {
            $controlName = $this->getUserInput("What is the name of the new control?",true);
        }
        if (!$controlsNamespace)
        {
            $controlsNamespace = $this->getUserInput("What is the namespace for your custom controls (do not include this control name at the end)?\n",true);
        }
        if (stristr($controlsNamespace,"\\"))
        {
            $phpNamespace = $controlsNamespace;
            $jsNamespace = str_replace("\\", ".", $phpNamespace);
        } else {
            $jsNamespace = $controlsNamespace;
            $phpNamespace = str_replace(".", "\\", $jsNamespace);
        }
        if (!$outFolder)
        {
            $outFolder = $this->getUserInput("What directory should I save the generated control in?\n",true);
        }

        \Wafl\Util\AppTemplates::MakeControlTemplate($controlName, $phpNamespace, $jsNamespace, $outFolder);
        $this->printLine("The new control has been created in ".realpath($outFolder).".");
    }
    private function createNewExtension($extensionName=null, $extensionsNamespace=null, $outFolder=null)
    {
		$this->printLine("Create a New Extension...", false);
        $this->printLine("");

        if (!$extensionName)
        {
            $extensionName = $this->getUserInput("What is the name of the new extension?",true);
        }
        if (!$extensionsNamespace)
        {
            $extensionsNamespace = $this->getUserInput("What is the namespace for your extensions (do not include this extension name at the end)?\n",true);
        }
        if (stristr($extensionsNamespace,"\\"))
        {
            $phpNamespace = $extensionsNamespace;
            $jsNamespace = str_replace("\\", ".", $phpNamespace);
        } else {
            $jsNamespace = $extensionsNamespace;
            $phpNamespace = str_replace(".", "\\", $jsNamespace);
        }
        if (!$outFolder)
        {
            $outFolder = $this->getUserInput("What directory should I save the generated extension in?\n",true);
        }

        \Wafl\Util\AppTemplates::MakeExtensionTemplate($extensionName, $phpNamespace, $jsNamespace, $outFolder);
        $this->printLine("The new extension has been created in ".realpath($outFolder).".");
    }
}