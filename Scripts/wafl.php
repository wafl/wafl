<?php
namespace Wafl\Scripts;

$goodUsage = true;
if (!isset($argv[1]))
{
    $goodUsage = false;
}

if (!$goodUsage)
{
    die("USAGE: wafl <command|script> [<arg>, <arg>...]\n");
}

$dblEjPath = getenv("DBLEJ_PATH") ? getenv("DBLEJ_PATH") : null;
$waflPath = realpath(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
if (!$dblEjPath)
{
    $dblEjPath = $waflPath."..".DIRECTORY_SEPARATOR."DblEj".DIRECTORY_SEPARATOR;
}
if (!file_exists($dblEjPath))
{
    if (DIRECTORY_SEPARATOR == "\\")
    {
        $dblEjPath = "c:\\Program Files\\Common\\DblEj\\";
    } else {
        $dblEjPath = "/usr/share/DblEj/";
    }
}
if (!file_exists($dblEjPath))
{
    throw new \Exception("Cannot run wafl because I cannot find the path to the DblEj library.");
}
define("WAFL_PATH", $waflPath);
define("DBLEJ_PATH", $dblEjPath);

require($waflPath."Autoloaders".DIRECTORY_SEPARATOR."Autoload.php");
require($dblEjPath."Autoloader.php");
require_once(__DIR__.DIRECTORY_SEPARATOR."ScriptUtil.php");
require_once(__DIR__.DIRECTORY_SEPARATOR."ScriptBase.php");
require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Cli".DIRECTORY_SEPARATOR."CliUtil.php");
require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Cli".DIRECTORY_SEPARATOR."CliBase.php");
$scriptClass = $argv[1];
$appArgs = [];
foreach ($argv as $argIdx=>$argVal)
{
    if ($argIdx != 1)
    {
        $appArgs[] = $argVal;
    }
}
ScriptUtil::ResolveAndRunScriptOrCliApp($scriptClass, $appArgs);