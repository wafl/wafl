<?php

namespace Wafl\Settings;

class InvalidSettingsFileException
extends \Exception
{
    const SETTINGS          = 1;
    const ADVANCED_SETTINGS = 2;

    public function __construct($settingsFileType, $settingsFile, $severity = E_ERROR)
    {
        parent::__construct("Invalid $settingsFileType settings file:$settingsFile", $severity, null);
    }
}