Namespace("Wafl");
Wafl.Settings =
    {
        AjaxHandlerUrl: "{$CORE::$API_HANDLER_URL}",
        AjaxHandlerUrlRelative: "{$CORE::$API_HANDLER_URL_RELATIVE}",
        DataModelFolder: "{$CORE::$WEB_ROOT_RELATIVE}DataModel/",
        FunctionaModelFolder: "{$CORE::$WEB_ROOT_RELATIVE}FunctionalModel/",
        LibFolder: "{$CORE::$WEB_ROOT_RELATIVE}FunctionalModel/"
    };
document.domain = "{$CORE::$SITE_DOMAIN_NAME}";
DblEj.Communication.JsonUtil.Set_DecoderLibBasePath(Wafl.Settings.DataModelFolder);
DblEj.Communication.JsonUtil.Set_DecoderLibPath(Wafl.Settings.LibFolder);
DblEj.Communication.Ajax.Utils.Set_HandlerUrl(Wafl.Settings.AjaxHandlerUrlRelative);