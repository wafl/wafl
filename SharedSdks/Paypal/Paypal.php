<?php
namespace Wafl\SharedSdks\Paypal;

class Paypal {

    private static $_username;
    private static $_password;
    private static $_signature;
    private static $_applicationId;
    private static $_apiVersion = "121.0";
    private static $_classicApiUrl = "https://api-3t.sandbox.paypal.com/nvp";
    private static $_expressUrl = "https://www.sandbox.paypal.com";
    private static $_restApiUrl = "https://api.sandbox.paypal.com/v1/";
    private static $_restAppClientId;
    private static $_restAppSecret;
    private static $_oauthTokenType;
    private static $_oauthToken;
    private static $_adaptiveApiUrl = "https://svcs.sandbox.paypal.com/AdaptivePayments/";

    private static $_environment = "sandbox"; //sandbox, beta-sandbox, or live

    public static function Set_Username($val) {
        Paypal::$_username = $val;
    }

    public static function Set_Password($val) {
        Paypal::$_password = $val;
    }

	 public static function Set_ClientAppId($val)
	 {
		 self::$_restAppClientId=$val;
	 }
	 public static function Set_ClientAppSecret($val)
	 {
		 self::$_restAppSecret=$val;
	 }
    public static function Set_Signature($val) {
        Paypal::$_signature = $val;
    }

    public static function Set_Environment($val) {
        Paypal::$_environment = $val;
        switch ($val) {
            case "sandbox":
            case "sandbox-beta":
                Paypal::$_classicApiUrl = "https://api-3t.$val.paypal.com/nvp";
                Paypal::$_expressUrl = "https://www.sandbox.paypal.com";
                Paypal::$_restApiUrl = "https://api.$val.paypal.com/v1/";
                Paypal::$_adaptiveApiUrl = "https://svcs.sandbox.paypal.com/AdaptivePayments/";
                break;
            case "live":
                Paypal::$_classicApiUrl = "https://api-3t.paypal.com/nvp";
                Paypal::$_expressUrl = "https://www.paypal.com";
                Paypal::$_restApiUrl = "https://api.paypal.com/v1/";
                Paypal::$_adaptiveApiUrl = "https://svcs.paypal.com/AdaptivePayments/";
                break;
        }
    }

    public static function Get_Environment() {
        return Paypal::$_environment;
    }
	public static function Set_ApplicationId($applicationId)
	{
		self::$_applicationId = $applicationId;
	}
	 public static function GetExpressPaymentDetails($authToken)
	 {
        $postVars = "METHOD=GetExpressCheckoutDetails&VERSION=" . urlencode(Paypal::$_apiVersion) .
                  "&PWD=" . urlencode(Paypal::$_password) .
                  "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                  "&TOKEN=" . urlencode($authToken);
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
        return $httpParsedResponseAr;
	 }


    /**
     * Process a Paypal Express payment.
     * NOTE: This method will set HTTP headers redirecting the client to paypal's servers.  Once the user finishes their transaction at Paypal, they will return to the URL specified by the <em>$returnUrl</em> parameter.
     *
     * @param string $paymentType "Authorization" or "Sale"
     *
     * @param string $paypalUserId
     *
     * @param decimal $totalAmount The total amount of the payment
     *
     * @param string $returnUrl
     * The URL that the user will be sent to after they finish their transaction at PayPal.
     * Paypal will include (via HTTP GET parameters) the <em>PayerID</em> and <em>token</em> variables.
     * These can be used to call ProcessExpressPayment and finalize a payment.
     *
     * @param string $cancelUrl
     * The URL that the user will be sent to if they cancel their transaction at PayPal.
     *
     * @param string $authToken The token provided by PayPal that authorizes this transaction.
     * To obtain a token, pass <em>null</em> for this parameter.
     * The return value for this method will be a token from Paypal.
     * You can then call this method again, passing the token.
     * This will redirect HTTP clients to Paypal's servers.
     * Additionally, after the user is returned to $returnUrl, you will need to
     * pass the <em>token</em> provided in the HTTP GET parameters to this method to finalize the transaction.
     *
     * @param string $payerId
     * When Paypal redirects the user back to the <em>$returnUrl</em>,
     * it will include a <em>PayerID</em> variable.
     * That value should be passed to this method to finalize the transaction.
     *
     * @param string $currencyId "USD" by default
     *
     * @param string $recipientData
     * If the recipient of the money should be someone other than the person processing the payment,
     * set their Paypal user id in this parameter.
     */
    public static function ProcessExpressPayment($paymentType, $paypalUserId, $totalAmount, $returnUrl, $cancelUrl, $authToken=null, $payerId="", $currencyId="USD", $recipientData = null, $paidForItems = null, $shippingAmount = null, $invoiceId = null, &$rawSentToApi = null)
    {
        if (!$authToken)
        {
            $postVars = "METHOD=SetExpressCheckout&VERSION=" . urlencode(Paypal::$_apiVersion) .
                      "&PWD=" . urlencode(Paypal::$_password) .
                      "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                      "&PAYMENTREQUEST_0_PAYMENTACTION=" . urlencode($paymentType) .
                      "&RETURNURL=" . urlencode($returnUrl) .
                      "&CANCELURL=" . urlencode($cancelUrl) .
                      "&SOLUTIONTYPE=Sole" .
                      "&PAYMENTREQUEST_0_AMT=" . urlencode($totalAmount) .
                      "&EMAIL=" . urlencode($paypalUserId);
            if ($invoiceId)
            {
                $postVars .= "&PAYMENTREQUEST_0_INVNUM=" . urlencode($invoiceId);
            }
            if ($recipientData && is_array($recipientData) && isset($recipientData["Settings"]) && isset($recipientData["Settings"]["Secondary Recipient Account Number"]))
            {
                $postVars .= "&PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID=" . urlencode($recipientData["Settings"]["Secondary Recipient Account Number"]);
            }
            if ($paidForItems)
            {
                $itemTotal = 0;
                $itemTax = 0;
                $itemIdx = 0;
                foreach ($paidForItems as $paidForItem)
                {
                    if (intval($paidForItem[2]) != 0 && floatval($paidForItem[3]) != 0)
                    {
                        $postVars .= "&L_PAYMENTREQUEST_0_NUMBER$itemIdx=".urlencode($paidForItem[0]);
                        $postVars .= "&L_PAYMENTREQUEST_0_NAME$itemIdx=".urlencode($paidForItem[1]);
                        $postVars .= "&L_PAYMENTREQUEST_0_QTY$itemIdx=".urlencode($paidForItem[2]);
                        $postVars .= "&L_PAYMENTREQUEST_0_AMT$itemIdx=".urlencode(number_format(round($paidForItem[3], 2), 2));
                        if (isset($paidForItem[4]))
                        {
                            $itemTax += $paidForItem[4];
                        }
                        if (isset($paidForItem[5]))
                        {
                            $postVars .= "&L_PAYMENTREQUEST_0_DESC$itemIdx=".urlencode(strlen($paidForItem[5])>127?substr($paidForItem[5], 0, 127):$paidForItem[5]);
                        }
                        $postVars .= "&L_PAYMENTREQUEST_0_ITEMCATEGORY$itemIdx=Physical";
                        $itemTotal += $paidForItem[2] * $paidForItem[3];
                        $itemIdx++;
                    }
                }
                $postVars .= "&PAYMENTREQUEST_0_ITEMAMT=".urlencode(number_format(round($itemTotal, 2), 2));
                $postVars .= "&PAYMENTREQUEST_0_TAXAMT=".urlencode(number_format(round($itemTax, 2), 2));
                if ($shippingAmount)
                {
                    $postVars .= "&PAYMENTREQUEST_0_SHIPPINGAMT=".urlencode(number_format(round($shippingAmount, 2), 2));
                }
            }
            $rawSentToApi = $postVars;
            $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
            $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
            if (strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESSWITHWARNING") {
                switch ($httpParsedResponseAr["L_ERRORCODE0"])
                {
                    case 10002 :
                        throw new \Exception("Invalid Paypal Express API credentials: ".urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
                    default:
                        throw new \Exception("Paypal Express Payment failed (".$httpParsedResponseAr["L_ERRORCODE0"]."): " . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
                }
            }
            $authToken = $httpParsedResponseAr["TOKEN"];
            return $authToken;
        }
        if (!$payerId)
        {
            \DblEj\Communication\Http\Util::HeaderRedirect(Paypal::$_expressUrl."/webscr?cmd=_express-checkout&token=$authToken", true);
        }
        else
        {
            $postVars = "METHOD=DoExpressCheckoutPayment&VERSION=" . urlencode(Paypal::$_apiVersion) .
                      "&TOKEN=$authToken" .
                      "&PWD=" . urlencode(Paypal::$_password) .
                      "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                      "&PAYMENTREQUEST_0_PAYMENTACTION=" . urlencode($paymentType) .
                      "&PAYERID=" . urlencode($payerId) .
                      "&PAYMENTREQUEST_0_AMT=" . urlencode($totalAmount) .
                      "&PAYMENTREQUEST_0_CURRENCYCODE=" . urlencode($currencyId);

            if ($recipientData && is_array($recipientData) && isset($recipientData["Settings"]) && isset($recipientData["Settings"]["Secondary Recipient Account Number"]))
            {
                $postVars .= "&PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID=" . urlencode($recipientData["Settings"]["Secondary Recipient Account Number"]);
            }
            if ($paidForItems)
            {
                $itemTotal = 0;
                $itemTax = 0;
                $itemIdx = 0;
                foreach ($paidForItems as $paidForItem)
                {
                    $postVars .= "&L_PAYMENTREQUEST_0_NUMBER$itemIdx=".urlencode($paidForItem[0]);
                    $postVars .= "&L_PAYMENTREQUEST_0_NAME$itemIdx=".urlencode($paidForItem[1]);
                    $postVars .= "&L_PAYMENTREQUEST_0_QTY$itemIdx=".urlencode($paidForItem[2]);
                    $postVars .= "&L_PAYMENTREQUEST_0_AMT$itemIdx=".urlencode(number_format($paidForItem[3], 2));
                    if (isset($paidForItem[4]))
                    {
                        $itemTax += $paidForItem[4];
                    }
                    if (isset($paidForItem[5]))
                    {
                        $postVars .= "&L_PAYMENTREQUEST_0_DESC$itemIdx=".urlencode(strlen($paidForItem[5])>127?substr($paidForItem[5], 0, 127):$paidForItem[5]);
                    }
                    $postVars .= "&L_PAYMENTREQUEST_0_ITEMCATEGORY$itemIdx=Physical";
                    $itemTotal += $paidForItem[2] * $paidForItem[3];
                    $itemIdx++;
                }
                $postVars .= "&PAYMENTREQUEST_0_ITEMAMT=".urlencode(number_format($itemTotal, 2));
                $postVars .= "&PAYMENTREQUEST_0_TAXAMT=".urlencode(number_format($itemTax, 2));
                if ($shippingAmount)
                {
                    $postVars .= "&PAYMENTREQUEST_0_SHIPPINGAMT=".urlencode(number_format($shippingAmount, 2));
                }
            }
            // Execute the API operation; see the PPHttpPost function above.
            $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
            $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);

            //force simulate pending echeck
            //$httpParsedResponseAr["PAYMENTSTATUS"] = "Pending";
            //$httpParsedResponseAr["PENDINGREASON"] = "echeck";

            if (strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESSWITHWARNING") {
                if (isset($httpParsedResponseAr["L_ERRORCODE0"]) && (urldecode($httpParsedResponseAr["L_ERRORCODE0"]) == "10486"))
                {
                    \DblEj\Communication\Http\Util::HeaderRedirect(Paypal::$_expressUrl."/webscr?cmd=_express-checkout&token=$authToken", false);
                } else {
                    throw new \Exception("Payment failed: " . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
                }
            }
            return new PaypalDirectPayResponse($httpParsedResponseAr, $rawResponse);
        }
    }

    public static function ProcessChainedPayment($paymentType, $paypalUserId, $totalAmount, $returnUrl, $cancelUrl, $authToken=null, $currencyId="USD", $recipientData = null, $paidForItems = null, $shippingAmount = null, $invoiceId = null, &$rawSentToApi = null)
    {
        $headers = [];
        $headers[] = "X-PAYPAL-SECURITY-USERID: ". Paypal::$_username;
        $headers[] = "X-PAYPAL-SECURITY-PASSWORD: ". Paypal::$_password;
        $headers[] = "X-PAYPAL-SECURITY-SIGNATURE: ". Paypal::$_signature;
        $headers[] = "X-PAYPAL-REQUEST-DATA-FORMAT: nv";
        $headers[] = "X-PAYPAL-RESPONSE-DATA-FORMAT: nv";
        $headers[] = "X-PAYPAL-DEVICE-IPADDRESS: ".\DblEj\Communication\Http\Util::GetClientIpAddress();
        $headers[] = "X-PAYPAL-APPLICATION-ID: ". Paypal::$_applicationId;

        $clientIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
        if (!$clientIp)
        {
            $clientIp = "127.0.0.1";
        }

        $commission = $recipientData["Settings"]["Commission"];
        if (!$authToken)
        {
            $totalMinusComission = $totalAmount - $commission;
            $postVars = "actionType=CREATE" .
                      "&clientDetails.applicationId=" . urlencode(Paypal::$_applicationId) .
                      "&clientDetails.ipAddress=" . urlencode($clientIp) .
                      "&currencyCode=".urlencode($currencyId) .
                      "&feesPayer=" . urlencode("PRIMARYRECEIVER") .
                      "&receiverList.receiver(0).email=" . urlencode($recipientData["Settings"]["Primary Recipient Account Number"]) .
                      "&receiverList.receiver(0).amount=" . urlencode(round($totalAmount, 2)) .
                      "&receiverList.receiver(0).primary=true".
                      "&receiverList.receiver(1).email=" . urlencode($recipientData["Settings"]["Secondary Recipient Account Number"]) .
                      "&receiverList.receiver(1).amount=" . urlencode(round($totalMinusComission, 2)) .
                      "&receiverList.receiver(1).primary=false".
                      "&requestEnvelope.errorLanguage=en_US".
                      "&senderEmail=".urlencode($paypalUserId) .
                      "&returnUrl=" . urlencode($returnUrl) .
                      "&cancelUrl=" . urlencode($cancelUrl);
            if ($invoiceId)
            {
                $postVars .= "&receiverList.receiver(0).invoiceId=" . urlencode($invoiceId);
                $postVars .= "&receiverList.receiver(1).invoiceId=" . urlencode($invoiceId);
            }
            $rawSentToApi = $postVars;
            $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_adaptiveApiUrl."Pay", true, $postVars, false, false, "", "", false, null, null, $headers);
            $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse, false);
            if (strtoupper($httpParsedResponseAr["responseEnvelope.ack"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["responseEnvelope.ack"]) != "SUCCESSWITHWARNING") {
                if (isset($httpParsedResponseAr["payErrorList.payError[0].error"]))
                {
                    throw new \Exception("Paypal Express create chained failed (".$httpParsedResponseAr["payErrorList.payError[0].error"].")");
                }
                elseif (isset($httpParsedResponseAr["error(0).message"]))
                {
                    throw new \Exception("Paypal Express create chained failed (".$httpParsedResponseAr["error(0).message"].")");
                }
                else
                {
                    throw new \Exception("Paypal Express create chained failed");
                }
            }
            if (strtoupper($httpParsedResponseAr["paymentExecStatus"] != "CREATED"))
            {
                throw new \Exception("Paypal Express create chained payment failed");
            }
            $authToken = $httpParsedResponseAr["payKey"];

            return $authToken;
        }
        else
        {
            $postVars = "payKey=$authToken" .
                      "&requestEnvelope.errorLanguage=en_US".
                      "&receiverOptions[0].receiver.email=" . urlencode($recipientData["Settings"]["Primary Recipient Account Number"]) .
                      "&receiverOptions[1].receiver.email=".urlencode($recipientData["Settings"]["Secondary Recipient Account Number"]) .
                      "&returnUrl=" . urlencode($returnUrl) .
                      "&cancelUrl=" . urlencode($cancelUrl);

            if ($paidForItems)
            {
                $itemTotal = 0;
                $itemTax = 0;
                $itemIdx = 0;
                foreach ($paidForItems as $paidForItem)
                {
                    if (intval($paidForItem[2]) != 0 && floatval($paidForItem[3]) != 0)
                    {
                        $postVars .= "&receiverOptions[0].invoiceData.item[$itemIdx].identifier=".urlencode($paidForItem[0]);
                        $postVars .= "&receiverOptions[0].invoiceData.item[$itemIdx].name=".urlencode($paidForItem[1]);
                        $postVars .= "&receiverOptions[0].invoiceData.item[$itemIdx].itemCount=".urlencode($paidForItem[2]);
                        $postVars .= "&receiverOptions[0].invoiceData.item[$itemIdx].price=".urlencode(number_format(round($paidForItem[2] * $paidForItem[3], 2), 2));

                        $postVars .= "&receiverOptions[1].invoiceData.item[$itemIdx].identifier=".urlencode($paidForItem[0]);
                        $postVars .= "&receiverOptions[1].invoiceData.item[$itemIdx].name=".urlencode($paidForItem[1]);
                        $postVars .= "&receiverOptions[1].invoiceData.item[$itemIdx].itemCount=".urlencode($paidForItem[2]);
                        $postVars .= "&receiverOptions[1].invoiceData.item[$itemIdx].price=".urlencode(number_format(round($paidForItem[2] * $paidForItem[3], 2), 2));
                        if (isset($paidForItem[4]))
                        {
                            $itemTax += $paidForItem[4];
                        }
                        $itemTotal += $paidForItem[2] * $paidForItem[3];
                        $itemIdx++;
                    }
                }
                $postVars .= "&receiverOptions[1].invoiceData.item[$itemIdx].identifier=SHOPFEE";
                $postVars .= "&receiverOptions[1].invoiceData.item[$itemIdx].name=Shop Seller Fees";
                $postVars .= "&receiverOptions[1].invoiceData.item[$itemIdx].itemCount=1";
                $postVars .= "&receiverOptions[1].invoiceData.item[$itemIdx].price=".urlencode(number_format(round(-$commission, 2), 2));

                $postVars .= "&receiverOptions[1].invoiceData.totalTax=".urlencode(number_format(round($itemTax, 2), 2));
                $postVars .= "&receiverOptions[0].invoiceData.totalTax=".urlencode(number_format(round($itemTax, 2), 2));
                if ($shippingAmount)
                {
                    $postVars .= "&receiverOptions[1].invoiceData.totalShipping=".urlencode(number_format(round($shippingAmount, 2), 2));
                    $postVars .= "&receiverOptions[0].invoiceData.totalShipping=".urlencode(number_format(round($shippingAmount, 2), 2));
                }
            }

            $rawSentToApi = $postVars;
            $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_adaptiveApiUrl."SetPaymentOptions", true, $postVars, false, false, "", "", false, null, null, $headers);
            $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse, false);
            if (strtoupper($httpParsedResponseAr["responseEnvelope.ack"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["responseEnvelope.ack"]) != "SUCCESSWITHWARNING") {
                if (isset($httpParsedResponseAr["payErrorList.payError[0].error"]))
                {
                    throw new \Exception("Paypal Express set chained payment options failed (".$httpParsedResponseAr["payErrorList.payError[0].error"].")");
                }
                elseif (isset($httpParsedResponseAr["error(0).message"]))
                {
                    throw new \Exception("Paypal Express set chained payment options failed (".$httpParsedResponseAr["error(0).message"].")");
                }
                else
                {
                    throw new \Exception("Paypal Express set chained payment options failed");
                }
            }
            \DblEj\Communication\Http\Util::HeaderRedirect(Paypal::$_expressUrl."/webscr?cmd=_ap-payment&paykey=$authToken", false);
        }
    }

    public static function GetChainedPaymentDetails($paymentId)
    {
        $headers = [];
        $headers[] = "X-PAYPAL-SECURITY-USERID: ". Paypal::$_username;
        $headers[] = "X-PAYPAL-SECURITY-PASSWORD: ". Paypal::$_password;
        $headers[] = "X-PAYPAL-SECURITY-SIGNATURE: ". Paypal::$_signature;
        $headers[] = "X-PAYPAL-REQUEST-DATA-FORMAT: nv";
        $headers[] = "X-PAYPAL-RESPONSE-DATA-FORMAT: nv";
        $headers[] = "X-PAYPAL-DEVICE-IPADDRESS: ".\DblEj\Communication\Http\Util::GetClientIpAddress();
        $headers[] = "X-PAYPAL-APPLICATION-ID: ". Paypal::$_applicationId;

        $clientIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
        if (!$clientIp)
        {
            $clientIp = "127.0.0.1";
        }

        $postVars = "payKey=$paymentId" .
                  "&requestEnvelope.errorLanguage=en_US";

        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_adaptiveApiUrl."PaymentDetails", true, $postVars, false, false, "", "", false, null, null, $headers);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse, false);
        if (strtoupper($httpParsedResponseAr["responseEnvelope.ack"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["responseEnvelope.ack"]) != "SUCCESSWITHWARNING") {
            if (isset($httpParsedResponseAr["payErrorList.payError[0].error"]))
            {
                throw new \Exception("Paypal Express lookup chained payment failed(".$httpParsedResponseAr["payErrorList.payError[0].error"].")");
            }
            elseif (isset($httpParsedResponseAr["error(0).message"]))
            {
                throw new \Exception("Paypal Express lookup chained payment failed ".$httpParsedResponseAr["error(0).message"].")");
            }
            else
            {
                throw new \Exception("Paypal Express lookup chained payment failed");
            }
        }
        return $httpParsedResponseAr;
    }

    /**
	  * @param type $firstName
	  * @param type $lastName
	  * @param type $cardType
	  * @param type $cardNumber
	  * @param type $expMonth
	  * @param type $expYear
	  * @param type $cvv2
	  * @param type $streetAddress
	  * @param type $city
	  * @param type $state
	  * @param type $zip
	  * @param type $country
	  * @param type $amount
	  * @param type $billingPeriod Day,Week,SemiMonth,Month,Year
	  * @param type $billingFrequency
	  * @param type $currencyId
	  */
    public static function Subscribe($firstName, $lastName, $cardType, $cardNumber, $expMonth, $expYear, $cvv2, $streetAddress, $city, $state, $zip, $country, $amount, $billingPeriod, $billingFrequency, $currencyId = "USD")
    {
        $expMonth = str_pad($expMonth, 2, '0', STR_PAD_LEFT);

        $remoteIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
        if (!$remoteIp)
        {
            $remoteIp = "127.0.0.1";
        }
        // Add request-specific fields to the request string.
        $postVars = "METHOD=CreateRecurringPaymentsProfile&VERSION=" . urlencode(Paypal::$_apiVersion) . "&PWD=" . urlencode(Paypal::$_password) .
                "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                "&AMT=" . urlencode($amount) .
                "&IPADDRESS=" . $remoteIp . "&CREDITCARDTYPE=" . urlencode($cardType) . "&ACCT=" . urlencode($cardNumber) .
                "&EXPDATE=" . urlencode($expMonth . $expYear) . "&CVV2=" . urlencode($cvv2) .
                "&FIRSTNAME=" . urlencode($firstName) . "&LASTNAME=" . urlencode($lastName) .
                "&STREET=" . urlencode($streetAddress) . "&CITY=" . urlencode($city) . "&STATE=" . urlencode($state) .
                "&BILLINGPERIOD=" . urlencode($billingPeriod) . "& BILLINGFREQUENCY =" . urlencode($billingFrequency) .
                "&ZIP=" . urlencode($zip) . "&COUNTRYCODE=" . urlencode($country) . "&CURRENCYCODE=" . urlencode($currencyId);
        // Execute the API operation; see the PPHttpPost function above.
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
        return $httpParsedResponseAr;
	 }
    public static function CancelSubscription($paypalProfileId)
    {
        $expMonth = str_pad($expMonth, 2, '0', STR_PAD_LEFT);

        $remoteIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
        if (!$remoteIp)
        {
            $remoteIp = "127.0.0.1";
        }
        // Add request-specific fields to the request string.
        $postVars = "METHOD=ManageRecurringPaymentsProfileStatus&VERSION=" . urlencode(Paypal::$_apiVersion) . "&PWD=" . urlencode(Paypal::$_password) .
                "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                "&PROFILEID=" . urlencode($paypalProfileId) .
                "&IPADDRESS=" . $remoteIp . "&ACTION=" . urlencode("Cancel");
        // Execute the API operation; see the PPHttpPost function above.
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
        return $httpParsedResponseAr;
	 }

    /**
	  *
     * @param string $paymentType "Autohroization" or "Sale"
     * @param string $firstName
     * @param string $lastName
     * @param string $cardType "Visa", "MasterCard", "Discover", "Amex"
     * @param string $cardNumber
     * @param string $expMonth "mm"
     * @param string $expYear "yyyy"
     * @param string $cvv2
     * @param string $streetAddress
     * @param string $city
     * @param string $state
     * @param string $zip
	  * @param string $country
	  * @param decimal $totalAmount
     * @param string $currencyId "US" by default
	  * @return PaypalDirectPayResponse
	  * @throws \Exception
	  */
    public static function ProcessWebProPayment($paymentType, $firstName, $lastName, $cardType, $cardNumber, $expMonth, $expYear, $cvv2, $streetAddress, $city, $state, $zip, $country, $totalAmount, $invoiceId = null, $currencyId = "USD", $recipientData = null, $paidForItems = null, $shippingTotal = null, &$rawSentToApi = null) {
        $expMonth = str_pad($expMonth, 2, '0', STR_PAD_LEFT);
		  if (strlen($expYear)==2)
		  {
			  $expYear = "20".$expYear;
		  }
        $remoteIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
        if (!$remoteIp)
        {
            $remoteIp = "127.0.0.1";
        }
        // Add request-specific fields to the request string.
        $postVars = "METHOD=DoDirectPayment&VERSION=" . urlencode(Paypal::$_apiVersion) . "&PWD=" . urlencode(Paypal::$_password) .
                "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                "&PAYMENTACTION=" . urlencode($paymentType) . "&AMT=" . urlencode($totalAmount) .
                "&IPADDRESS=" . $remoteIp . "&CREDITCARDTYPE=" . urlencode($cardType) . "&ACCT=" . urlencode($cardNumber) .
                "&EXPDATE=" . urlencode($expMonth . $expYear) . "&CVV2=" . urlencode($cvv2) .
                "&FIRSTNAME=" . urlencode($firstName) . "&LASTNAME=" . urlencode($lastName) .
                "&STREET=" . urlencode($streetAddress) . "&CITY=" . urlencode($city) . "&STATE=" . urlencode($state) .
                "&ZIP=" . urlencode($zip) . "&COUNTRYCODE=" . urlencode($country) . "&CURRENCYCODE=" . urlencode($currencyId);

        if ($invoiceId)
        {
            $postVars .= "&INVNUM=".urlencode($invoiceId);
        }

        if ($recipientData && is_array($recipientData) && isset($recipientData["Settings"]) && isset($recipientData["Settings"]["Secondary Recipient Account Number"]))
        {
            $postVars .= "&SUBJECT=".urlencode($recipientData["Settings"]["Secondary Recipient Account Number"]);
        }

        if ($paidForItems)
        {
            $itemTotal = 0;
            $itemTax = 0;
            $itemIdx = 0;
            foreach ($paidForItems as $paidForItem)
            {
                if (intval($paidForItem[2]) != 0 && floatval($paidForItem[3]) != 0)
                {
                    $postVars .= "&L_NUMBER$itemIdx=".urlencode($paidForItem[0]);
                    $postVars .= "&L_NAME$itemIdx=".urlencode($paidForItem[1]);
                    $postVars .= "&L_QTY$itemIdx=".urlencode($paidForItem[2]);
                    $postVars .= "&L_AMT$itemIdx=".urlencode(number_format(round($paidForItem[3], 2), 2));
                    if (isset($paidForItem[4]))
                    {
                        $postVars .= "&L_TAXAMT$itemIdx=".urlencode(number_format(round($paidForItem[4], 2), 2));
                        $itemTax += $paidForItem[4];
                    }
                    if (isset($paidForItem[5]))
                    {
                        $postVars .= "&L_DESC$itemIdx=".urlencode($paidForItem[5]);
                    }
                    $itemTotal += $paidForItem[2] * $paidForItem[3];
                    $itemIdx++;
                }
            }
            $postVars .= "&ITEMAMT=".urlencode(number_format(round($itemTotal, 2), 2));
            $postVars .= "&TAXAMT=".urlencode(number_format(round($itemTax, 2), 2));
            if ($shippingTotal)
            {
                $postVars .= "&SHIPPINGAMT=".urlencode(number_format(round($shippingTotal, 2), 2));
            }
        }
        $rawSentToApi = $postVars;
        // Execute the API operation; see the PPHttpPost function above.
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
        if (strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESSWITHWARNING") {
				switch ($httpParsedResponseAr["L_ERRORCODE0"])
				{
					 case 10752:
						  throw new \Exception("The transaction was denied by the financial institution.&nbsp;&nbsp;Please check your card number, expiration, and security code and try again.");
						  break;
					 case 15005:
						  throw new \Exception("The transaction was denied by the financial institution.&nbsp;&nbsp;Please check your card number, expiration, and security code and try again.");
						  break;
					 default:
						  throw new \Exception("Payment failed (".$httpParsedResponseAr["L_ERRORCODE0"]."): " . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
						  break;
				}
        }
        return new PaypalDirectPayResponse($httpParsedResponseAr, $rawResponse);
    }

    public static function CapturePreauthorizesWebProPayment($authorizationCode,$totalAmount, $currencyId = "USD",$completeTransaction=false) {
        if ($completeTransaction)
        {
            $completeType="Complete";
        } else {
            $completeType="NotComplete";
        }
        $remoteIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
        if (!$remoteIp)
        {
            $remoteIp = "127.0.0.1";
        }

        // Add request-specific fields to the request string.
        $postVars = "METHOD=DoCapture&VERSION=" . urlencode(Paypal::$_apiVersion) . "&PWD=" . urlencode(Paypal::$_password) .
                "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                "&AMT=" . urlencode($totalAmount) .
                "&IPADDRESS=" . $remoteIp .
                "&COMPLETETYPE=" . 'COMPLETE' .
                "&AUTHORIZATIONID=" . urlencode($authorizationCode);
        // Execute the API operation; see the PPHttpPost function above.
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
        if (strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESSWITHWARNING") {
            throw new \Exception("Capture failed: " . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
        }
        return new PaypalDirectPayResponse($httpParsedResponseAr, $rawResponse);
    }

    public static function VoidPreauthorizesWebProPayment($authorizationCode,$totalAmount, $currencyId = "USD") {

        // Add request-specific fields to the request string.
        $postVars = "METHOD=DoVoid&VERSION=" . urlencode(Paypal::$_apiVersion) . "&PWD=" . urlencode(Paypal::$_password) .
                "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                "&AMT=" . urlencode($totalAmount) .
                "&IPADDRESS=" . $_SERVER['REMOTE_ADDR'] .
                "&AUTHORIZATIONID=" . urlencode($authorizationCode);
        // Execute the API operation; see the PPHttpPost function above.
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
        if (strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESSWITHWARNING") {
            throw new \Exception("Void failed: u: " . Paypal::$_username . ", " . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
        }
        return new PaypalDirectPayResponse($httpParsedResponseAr, $rawResponse);
    }

    public static function VoidAuthorizesWebProPayment($transactionid,$totalAmount, $currencyId = "USD") {

        // Add request-specific fields to the request string.
        $postVars = "METHOD=DoVoid&VERSION=" . urlencode(Paypal::$_apiVersion) . "&PWD=" . urlencode(Paypal::$_password) .
                "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                "&AMT=" . urlencode($totalAmount) .
                "&IPADDRESS=" . $_SERVER['REMOTE_ADDR'] .
                "&AUTHORIZATIONID=" . urlencode($transactionid);
        // Execute the API operation; see the PPHttpPost function above.
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
        if (strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESSWITHWARNING") {
            throw new \Exception("Void failed: u: " . Paypal::$_username . ", " . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
        }
        return new PaypalDirectPayResponse($httpParsedResponseAr, $rawResponse);
    }

    public static function RefundTransaction($transactionid, $totalAmount, $partialRefund=false, $currencyId = "USD") {

        // Add request-specific fields to the request string.
        $postVars = "METHOD=RefundTransaction&VERSION=" . urlencode(Paypal::$_apiVersion) . "&PWD=" . urlencode(Paypal::$_password) .
                "&USER=" . urlencode(Paypal::$_username) . "&SIGNATURE=" . urlencode(Paypal::$_signature) .
                "&AMT=" . urlencode($totalAmount) .
                "&REFUNDTYPE=" . (($partialRefund) ? 'Partial' : 'Full') .
                "&IPADDRESS=" . $_SERVER['REMOTE_ADDR'] .
                "&TRANSACTIONID=" . urlencode($transactionid);
        // Execute the API operation; see the PPHttpPost function above.
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_classicApiUrl, true, $postVars, false, false);
        $httpParsedResponseAr = Paypal::ParseProcessResponse($rawResponse);
        if (strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESS" && strtoupper($httpParsedResponseAr["ACK"]) != "SUCCESSWITHWARNING")
        {
            $errorMessage = urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]);
            throw new \Exception($errorMessage);
            return false;
        } else {
            return new PaypalRefundResponse($httpParsedResponseAr);
        }
    }

    /**
     * Store a credit card in paypal's vault
     *
     * @param int|string $userId Any unique id identfiying the buyer
     * @param string $firstName
     * @param string $lastName
     * @param type $cardType
     * @param string $cardNumber
     * @param int $expMonth
     * @param int $expYear
     * @param string $cvv2
     * @param string $streetAddress
     * @param string $city
     * @param string $state
     * @param string $zip
     * @param string $country
     * @return object
     */
    public static function SaveCardInVault($userId, $firstName, $lastName, $cardType, $cardNumber, $expMonth, $expYear, $cvv2 = null, $streetAddress = null, $city = null, $state = null, $zip = null, $country = null) {
        $expMonth = str_pad($expMonth, 2, '0', STR_PAD_LEFT);
		  if (strlen($expYear)==2)
		  {
			  $expYear = "20".$expYear;
		  }
        $remoteIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
        if (!$remoteIp)
        {
            $remoteIp = "127.0.0.1";
        }
		  self::GetOAuthAccess(Paypal::$_restAppClientId,  Paypal::$_restAppSecret);
		  $params = array(
			"number"=>$cardNumber,
			"expire_month"=>$expMonth,
			"expire_year"=>$expYear,
			"cvv2"=>$cvv2,
			"type"=>strtolower($cardType),
			"payer_id"=>$userId);
            if ($firstName)
            {
                $params["first_name"] = $firstName;
            }
            if ($lastName)
            {
                $params["last_name"] = $lastName;
            }
            if ($streetAddress)
            {
                $params["billing_address"]=["line1"=>$streetAddress, "city"=>$city, "state"=>$state, "country_code"=>$country, "postal_code"=>$zip];
            }
			$paramsJson = \DblEj\Communication\JsonUtil::EncodeJson($params);
			$headers = array(
				 "Content-Type:application/json",
				 "Authorization:".self::$_oauthTokenType." ".self::$_oauthToken);
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_restApiUrl."vault/credit-card", true, $paramsJson, false, false, "", "", false, null, null, $headers);
        return \DblEj\Communication\JsonUtil::DecodeJson($rawResponse);
    }

    public static function DeleteCardFromVault($cardKey) {
		  self::GetOAuthAccess(Paypal::$_restAppClientId,  Paypal::$_restAppSecret);
			$headers = array(
				 "Content-Type:application/json",
				 "Authorization:".self::$_oauthTokenType." ".self::$_oauthToken);
			$returnStatus=null;
         $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_restApiUrl."vault/credit-card/$cardKey",false,null,false,false,"","",false,null,null,$headers,true,$returnStatus);
			if ($returnStatus==204)
			{
				return true;
			} else {
				return \DblEj\Communication\JsonUtil::DecodeJson($rawResponse);
			}
    }

    public static function ProcessVaultPayment($cardKey, $amount, $description = "", $payerId = null, $recipientData = null, $invoiceId = null, $currencyId = "USD", $paidForItems = null, $shippingTotal = null, $billingEmail = null, $billingPhone = null, &$rawSentToApi = null) {
		  self::GetOAuthAccess(Paypal::$_restAppClientId,  Paypal::$_restAppSecret);
		  $params = array(
			"intent"=>"sale",
			"payer"=>
				array("payment_method"=>"credit_card",
					   "funding_instruments"=>array(array("credit_card_token"=>array("credit_card_id"=>$cardKey,"payer_id"=>$payerId)))),
            "transactions"=>array(array("amount"=>array("total"=>$amount, "currency"=>$currencyId), "description"=>$description)));
            if ($billingEmail)
            {
                if (!isset($params["payer"]["payer_info"]))
                {
                    $params["payer"]["payer_info"] = [];
                }
                $params["payer"]["payer_info"]["email"] = $billingEmail;
            }
            if ($billingPhone)
            {
                if (!isset($params["payer"]["payer_info"]))
                {
                    $params["payer"]["payer_info"] = [];
                }
                $params["payer"]["payer_info"]["phone"] = $billingPhone;
            }
            if ($recipientData && is_array($recipientData) && isset($recipientData["Settings"]) && isset($recipientData["Settings"]["Secondary Recipient Account Number"]))
            {
                $params["transactions"][0]["payee"] = array("email"=>$recipientData["Settings"]["Secondary Recipient Account Number"]);
            }

            if ($invoiceId)
            {
                $params["transactions"][0]["description"] = "Invoice $invoiceId";
            }

            if ($paidForItems)
            {
                $params["transactions"][0]["amount"]["details"] = [];
                $params["transactions"][0]["item_list"] = ["items"=>[]];
                $itemTotal = 0;
                $itemIdx = 0;
                $taxTotal = 0;
                foreach ($paidForItems as $paidForItem)
                {
                    if (intval($paidForItem[2]) != 0 && floatval($paidForItem[3]) != 0)
                    {
                        $params["transactions"][0]["item_list"]["items"][$itemIdx] =
                        [
                            "quantity"=>$paidForItem[2],
                            "name"=>$paidForItem[1],
                            "price"=>number_format(round($paidForItem[3], 2), 2),
                            "currency"=>$currencyId,
                            "sku"=>substr($paidForItem[0], 0, 60)
                        ];
                        if (isset($paidForItem[4]))
                        {
                            $taxTotal += $paidForItem[4];
                        }
                        $itemTotal += $paidForItem[2] * $paidForItem[3];
                        $itemIdx++;
                    }
                }
                $params["transactions"][0]["amount"]["details"]["subtotal"] = number_format(round($itemTotal, 2), 2);
                $params["transactions"][0]["amount"]["details"]["tax"] = number_format(round($taxTotal, 2), 2);
            }
            if ($shippingTotal)
            {
                $params["transactions"][0]["amount"]["details"]["shipping"] = number_format(round($shippingTotal, 2), 2);
            }
			$paramsJson = \DblEj\Communication\JsonUtil::EncodeJson($params);
            $rawSentToApi = $paramsJson;
			$headers = array(
				 "Content-Type:application/json",
				 "Authorization:".self::$_oauthTokenType." ".self::$_oauthToken);
        $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_restApiUrl."payments/payment",true,$paramsJson,false,false,"","",false,null,null,$headers);
        return \DblEj\Communication\JsonUtil::DecodeJson($rawResponse);
     }

    public static function SendPayment($recipientEmail, $amount, $returnUrl, $cancelUrl) {
        $headers[] = "X-PAYPAL-SECURITY-USERID: ". Paypal::$_username;
        $headers[] = "X-PAYPAL-SECURITY-PASSWORD: ". Paypal::$_password;
        $headers[] = "X-PAYPAL-SECURITY-SIGNATURE: ". Paypal::$_signature;
        $headers[] = "X-PAYPAL-REQUEST-DATA-FORMAT: json";
        $headers[] = "X-PAYPAL-RESPONSE-DATA-FORMAT: json";
        $headers[] = "X-PAYPAL-DEVICE-IPADDRESS: ".\DblEj\Communication\Http\Util::GetClientIpAddress();
        $headers[] = "X-PAYPAL-APPLICATION-ID: ". Paypal::$_applicationId;
		  $params = array(
			"returnUrl"=>$returnUrl,
			"cancelUrl"=>$cancelUrl,
			"actionType"=>"PAY",
			"requestEnvelope"=>array("errorLanguage"=>"en-US"),
			"currencyCode"=>"USD",
			"receiverList"=>array("receiver"=>array(array("email"=>$recipientEmail,"amount"=>number_format($amount,2)))));

			$paramsJson = \DblEj\Communication\JsonUtil::EncodeJson($params);
         $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_adaptiveApiUrl."Pay",true,$paramsJson,false,false,"","",false,null,null,$headers);
			return \DblEj\Communication\JsonUtil::DecodeJson($rawResponse);
     }

     /**
      * Validate a received IPN message.
      * @param string $postVars The HTTP POST variables sent by paypal.  If null then will read from standard input.
      * @return boolean True if this is a valid notification, otherwise false.
      */
    public static function ValidateIpn($postVars = null)
    {
        if ($postVars == null)
        {
            $raw_post_data = file_get_contents('php://input');
            $raw_post_array = explode('&', $raw_post_data);
            $postVars = array();
            foreach ($raw_post_array as $keyval) {
              $keyval = explode ('=', $keyval);
              if (count($keyval) == 2)
              {
                 $postVars[$keyval[0]] = urldecode($keyval[1]);
              }
            }
        }

        $IpnValidationResult = false;
        $queryString = "cmd=_notify-validate";
        foreach ($postVars as $key => $value) {
            $value = urlencode($value);
            $queryString .= "&$key=$value";
        }
        $IpnValidationResult = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_expressUrl, true, $queryString, true, true, null, null, true, null, null, array("Connection: Close"));
        $IpnValidationResult = ($IpnValidationResult == "VERIFIED");
        return $IpnValidationResult;
    }

	private static function GetOAuthAccess($clientid,$secret)
	{
		 if(!self::$_oauthToken)
		 {
			$headers = array(
				 "Content-Type: application/json",
				 "Accept-Language: en_US"
			);
         $rawResponse = \DblEj\Communication\Http\Util::SendRequest(Paypal::$_restApiUrl."oauth2/token", true, "grant_type=client_credentials",false,false,$clientid,$secret);
			$responseObject = \DblEj\Communication\JsonUtil::DecodeJson($rawResponse);
            if (isset($responseObject["error"]))
            {
                throw new \Exception("Error authorizing to paypal: ".$responseObject["error"].". ".$responseObject["error_description"]);
            }
			self::$_oauthTokenType=$responseObject["token_type"];
			self::$_oauthToken=$responseObject["access_token"];
		 }
	 }

	private static function ParseProcessResponse($httpResponse, $throwExceptionOnNoAck = true) {
        $httpParsedResponseAr = array();
        if ($httpResponse) {
            // Extract the response details.
            $httpResponseAr = explode("&", $httpResponse);

            foreach ($httpResponseAr as $i => $value) {
                $tmpAr = explode("=", $value);
                if (sizeof($tmpAr) > 1) {
                    $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
                }
            }

            if (sizeof($httpParsedResponseAr) == 0 || ($throwExceptionOnNoAck && !array_key_exists('ACK', $httpParsedResponseAr))) {
                throw new \Exception("Invalid httpResponse: $httpResponse This could indicate a mistake in your payapl login information.  This error will also occur if there is no connection to the internet.");
            }
        }

        return $httpParsedResponseAr;
    }
}