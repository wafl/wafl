<?php
namespace Wafl\SharedSdks\Paypal;

class PaypalDirectPayResponse
{
    private $_transactionId;
    private $_result;
    private $_amount;
    private $_timeStamp;
    private $_info;
    private $_correlationId;
    private $_version;
    private $_build;
    private $_currencyCode;
    private $_cvv2Match;
    private $_rawResponse;
    private $_status;
    private $_pendingReason;

    function __construct($paypalArray, $rawResponse)
    {
        $this->_rawResponse = $rawResponse;
        $this->_transactionId = isset($paypalArray["TRANSACTIONID"])?$paypalArray["TRANSACTIONID"]:$paypalArray["PAYMENTINFO_0_TRANSACTIONID"];
        $this->_result = $paypalArray["ACK"];
        if (isset($paypalArray["PAYMENTSTATUS"]))
        {
            $this->_status = $paypalArray["PAYMENTSTATUS"];
        }
        elseif (isset($paypalArray["PAYMENTINFO_0_PAYMENTSTATUS"]))
        {
            $this->_status = $paypalArray["PAYMENTINFO_0_PAYMENTSTATUS"];
        } else {
            $this->_status = $paypalArray["ACK"];
        }
        $this->_amount = isset($paypalArray["AMT"])?$paypalArray["AMT"]:$paypalArray["PAYMENTINFO_0_AMT"];
        if (isset($paypalArray["L_SHORTMESSAGE0"]))
        {
            $this->_info = $paypalArray["L_SHORTMESSAGE0"];
        }
        if (isset($paypalArray["L_SHORTMESSAGE0"]))
        {
            $this->_message = $paypalArray["L_LONGMESSAGE0"];
        }
        $this->_timeStamp = $paypalArray["TIMESTAMP"];
        if (isset($paypalArray["PENDINGREASON"]))
        {
            $this->_pendingReason = $paypalArray["PENDINGREASON"];
        }
        elseif (isset($paypalArray["PAYMENTINFO_0_PENDINGREASON"]))
        {
            $this->_pendingReason = $paypalArray["PAYMENTINFO_0_PENDINGREASON"];
        } else {
            $this->_pendingReason = "";
        }
        $this->_version = $paypalArray["VERSION"];
        $this->_build = $paypalArray["BUILD"];
        $this->_currencyCode = isset($paypalArray["CURRENCYCODE"])?$paypalArray["CURRENCYCODE"]:$paypalArray["PAYMENTINFO_0_CURRENCYCODE"];
        $this->_cvv2Match = isset($paypalArray["CVV2MATCH"])?$paypalArray["CVV2MATCH"]:null;
        if (!$this->_cvv2Match)
        {
            $this->_cvv2Match = isset($paypalArray["PAYMENTINFO_0_CVV2MATCH"])?$paypalArray["PAYMENTINFO_0_CVV2MATCH"]:null;
        }
        $this->_correlationId = isset($paypalArray["CORRELATIONID"])?$paypalArray["CORRELATIONID"]:null;
    }

    public function Get_RawResponse()
    {
        return $this->_rawResponse;
    }
    public function Get_TransactionId()
    {
        return $this->_transactionId;
    }
    public function Get_CorrelationId()
    {
        return $this->_correlationId;
    }
    public function Get_Result()
    {
        return $this->_result;
    }
    public function Get_Status()
    {
        return $this->_status;
    }
    public function Get_PendingReason()
    {
        return $this->_pendingReason;
    }
    public function Get_Amount()
    {
        return $this->_amount;
    }
    public function Get_Info()
    {
        return $this->_info;
    }
    public function Get_Message()
    {
        return $this->_message;
    }
    public function Get_TimeStamp()
    {
        return $this->_timeStamp;
    }
    public function Get_Cvv2Match()
    {
        return $this->_cvv2Match;
    }
    public function Get_Version()
    {
        return $this->_version;
    }
    public function Get_Build()
    {
        return $this->_build;
    }
}