<?php
namespace Wafl\SharedSdks\Paypal;

class PaypalRefundResponse
{
    private $_transactionId;
    private $_result;
    private $_amount;
    private $_timeStamp;
    private $_info;
    private $_correlationId;
    private $_version;
    private $_build;
    private $_currencyCode;
    function __construct($paypalArray)
    {
        $this->_transactionId = $paypalArray["REFUNDTRANSACTIONID"];
        $this->_result = $paypalArray["ACK"];
        $this->_amount = $paypalArray["GROSSREFUNDAMT"];
        if (isset($paypalArray["L_SHORTMESSAGE0"]))
        {
            $this->_info = $paypalArray["L_SHORTMESSAGE0"];
        }
        if (isset($paypalArray["L_SHORTMESSAGE0"]))
        {
            $this->_message = $paypalArray["L_LONGMESSAGE0"];
        }
        $this->_timeStamp = $paypalArray["TIMESTAMP"];
        $this->_version = $paypalArray["VERSION"];
        $this->_build = $paypalArray["BUILD"];
        $this->_currencyCode = $paypalArray["CURRENCYCODE"];
        $this->_cvv2Match = isset($paypalArray["CVV2MATCH"])?$paypalArray["CVV2MATCH"]:null;
        $this->_correlationId = isset($paypalArray["CORRELATIONID"])?$paypalArray["CORRELATIONID"]:null;
    }

    public function Get_TransactionId()
    {
        return $this->_transactionId;
    }
    public function Get_CorrelationId()
    {
        return $this->_correlationId;
    }
    public function Get_Result()
    {
        return $this->_result;
    }
    public function Get_Amount()
    {
        return $this->_amount;
    }
    public function Get_Info()
    {
        return $this->_info;
    }
    public function Get_Message()
    {
        return $this->_message;
    }
    public function Get_TimeStamp()
    {
        return $this->_timeStamp;
    }
    public function Get_Version()
    {
        return $this->_version;
    }
    public function Get_Build()
    {
        return $this->_build;
    }
}