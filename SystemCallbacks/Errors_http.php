<?php
use Wafl\Core;

/**
 *
 * @param int $errno
 * @param string $errstr
 * @param string $errfile
 * @param int $errline
 * @param mixed $errctx
 * @return boolean
 * @throws ErrorException
 * @todo rewrite this method to actually do something useful (it is using old stuff)
 */
function __activeWaflErrorHandler($errno, $errstr, $errfile, $errline, $errctx)
{
    $reptLevel = error_reporting();
    if ($reptLevel > 0)
    {
        if
        (
        $errno == E_WARNING || $errno == E_USER_WARNING ||
        $errno == E_NOTICE || $errno == E_USER_NOTICE ||
        $errno == E_STRICT ||
        $errno == E_DEPRECATED || $errno == E_USER_DEPRECATED
        )
        {
            //for recoverable errors, dont throw an exception.
            //let the app continue in case some other component
            //is testing for the error and wants to throw an informative exception.
            //If we handle the error, then they dont get a chance to catch it.
            error_log("Recoverable error suppressed: $errstr in " . $errfile . " at " . $errline);
            return false;
        }
        if (!Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_SuppressErrors())
        {
            throw new ErrorException($errstr . "(internal 3)", 0, $errno, $errfile, $errline, null);
        }
        else
        {
            return false; //error suppressed, let's let php handle it.
        }
    }
    else
    {
        return false; //$reptLevel 0 indicates the error was suppressed with an @, let's let php handle it.
    }
    return true; //tell php we handled it and to continue execution
}

function __debugActiveWaflErrorHandler($errno, $errstr, $errfile, $errline, $errctx)
{
    static $errorcount = null;
    $reptLevel         = error_reporting();
    if (isset(Core::$RUNNING_APPLICATION))
    {
        $suppressErrors = Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_SuppressErrors();
        if (!$errorcount)
        {
            $errorcount = 1;
        }
        else
        {
            $errorcount++;
        }
        if ($errorcount > 90)
        {
            $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 100);
            if (count($backtrace) > 90)
            {
                while (ob_get_level() > 0)
                {
                    ob_end_clean();
                }
                die("Deep nested errors indicates possible infinite recursion in an error trigger or error handler.<br><br>The current error is: <em>$errstr at $errline in $errfile</em>.");
            }
        }
        if (!$suppressErrors && $reptLevel == 0)
        {
            $suppressErrors = true;
        }
    }
    else
    {
        $suppressErrors = false;
    }

    if (!$suppressErrors)
    {
        switch ($errno)
        {
            case \E_STRICT:
                \Wafl\UserFeedback::AppendInfo("PHP Suggestion", "$errstr in " . $errfile . " at " . $errline);
                if (!defined("EXCEPTION_OUTPUT"))
                {
                    define("EXCEPTION_OUTPUT", true);
                }
                break;
            default:
                throw new \ErrorException($errstr, $errno, 0, $errfile, $errline, null);
        }
    }
    else
    {
        if ($reptLevel != 0)
        {
            error_log("Error suppressed: $errstr in " . $errfile . " at " . $errline);
        }
        \Wafl\Util\Debug::Set_LastSuppressedError($errstr);
        //		if (!defined("EXCEPTION_OUTPUT"))
        //		{
        //			define("EXCEPTION_OUTPUT", true);
        //		}
        return false; //$reptLevel 0 indicates the error was suppressed with an @, let's let php handle it.
    }

    return true; //tell php we handled it and to continue execution
}