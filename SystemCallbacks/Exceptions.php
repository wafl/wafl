<?php

use Wafl\Core;

function __activeWaflExceptionHandler($exception)
{
    error_log($exception->getMessage() . " in " . $exception->getFile() . " at " . $exception->getLine() . " (Global Exception Handler)");
    print("Framework Runtime Error: " . $exception->getMessage()."\n");
    print($exception->getTraceAsString()."\n");
    exit(1);
}

/**
 * @param Exception $exception
 */
function __activeWaflExceptionHandler2($exception)
{
    if (error_reporting() > 0)
    {
        try
        {
            if (((defined("AM_WEBPAGE") && AM_WEBPAGE) || isset($_SERVER["HTTP_HOST"])) && headers_sent())
            {
                error_log("There was an error while running the ActiveWafl application.  I couldn't display the error on-screen because the headers have already been sent.  Error details: " . $exception->getMessage() . " at line " . $exception->getLine() . " in file " . $exception->getFile());
                error_log($exception->getTraceAsString(), true);
            }

            while (ob_get_level() > 0)
            {
                ob_end_clean();
            }

            $printResult = "";

            $exceptions = [];
            $callStack  = [];

            $exceptionType = get_class($exception);
            switch ($exception->getCode())
            {
                case \E_WARNING:
                    $severityClass = "Warning";
                case \E_NOTICE:
                case \E_STRICT:
                    $severityClass = "Info";
                default:
                    $severityClass = "Error";
            }

            foreach ($exception->getTrace() as $traceLine)
            {
                $fileName     = isset($traceLine["file"]) ? $traceLine["file"] : "";
                $firstFile    = $fileName;
                $lineNumber   = isset($traceLine["line"]) ? $traceLine["line"] : "";
                $functionName = $traceLine["function"];
                $className    = isset($traceLine["class"]) ? $traceLine["class"] : "";
                $callChars    = isset($traceLine["type"]) ? $traceLine["type"] : "";
                $callArgs     = array();
                if (isset($traceLine["args"]))
                {
                    foreach ($traceLine["args"] as $cmdArg)
                    {
                        if (is_string($cmdArg) && trim($cmdArg) != "")
                        {
                            $callArgs[] = $cmdArg;
                        }
                    }
                }

                $callStack[] = array(
                    -1,
                    -1,
                    $className . $callChars . $functionName . "(" . implode(",", $callArgs) . ")",
                    $fileName,
                    $lineNumber);
            }
            $exceptions[] = ["ExceptionType" => $exceptionType,
                "File"          => $exception->getFile(),
                "Line"          => $exception->getLine(),
                "CallStack"     => $callStack,
                "Message"       => $exception->getMessage()];
            $prev         = $exception;
            while ($prev         = $prev->getPrevious())
            {
                $exceptionType = get_class($prev);
                foreach ($prev->getTrace() as $traceLine)
                {
                    $fileName     = isset($traceLine["file"]) ? $traceLine["file"] : "";
                    $firstFile    = $fileName;
                    $lineNumber   = isset($traceLine["line"]) ? $traceLine["line"] : "";
                    $functionName = $traceLine["function"];
                    $className    = isset($traceLine["class"]) ? $traceLine["class"] : "";
                    $callChars    = isset($traceLine["type"]) ? $traceLine["type"] : "";
                    $callArgs     = array();
                    if (isset($traceLine["args"]))
                    {
                        foreach ($traceLine["args"] as $cmdArg)
                        {
                            if (is_string($cmdArg) && trim($cmdArg) != "")
                            {
                                $callArgs[] = $cmdArg;
                            }
                        }
                    }

                    $callStack[] = array(
                        -1,
                        -1,
                        $className . $callChars . $functionName . "(" . implode(",", $callArgs) . ")",
                        $fileName,
                        $lineNumber);
                }
                $exceptions[] = ["ExceptionType" => $exceptionType,
                    "File"          => $prev->getFile(),
                    "Line"          => $prev->getLine(),
                    "CallStack"     => $callStack,
                    "Message"       => $prev->getMessage()];
            }
            $printResult = _getTextErrorOutput($exceptions);
            print($printResult);
        }
        catch (\Exception $ex)
        {
            print "There was an error while trying to output the previous error.\n";
            print "Initial Error: " . $exception->getMessage() . " in " . $exception->getFile() . " at " . $exception->getLine();
            print "Error printing error: " . $ex->getMessage() . " in " . $ex->getFile() . " at " . $ex->getLine();
        }
        if (!defined("EXCEPTION_OUTPUT"))
        {
            define("EXCEPTION_OUTPUT", true);
        }
        exit(1);
    }
    else
    {
        return false; //$reptLevel 0 indicates the error was suppressed with an @, let's let php handle it.
    }
}

function _getTextErrorOutput(array $exceptionInfos)
{
    $printResult        = "\n********** Execution halted due to " . count($exceptionInfos) . " unhandled exceptions **********\n";
    $exceptionIteration = 0;
    foreach ($exceptionInfos as $exceptionInfo)
    {
        $printResult .= "\n\t";
        $printResult.="Exception #" . ($exceptionIteration + 1) . " " . $exceptionInfo["ExceptionType"] . "\n\t------------\n\t" . $exceptionInfo["Message"] . "\nin file " . $exceptionInfo["File"] . "\nat line " . $exceptionInfo["Line"] . "\n";
        $printResult.="\t\t# Time\tMemory\tCall\n\t\t- -----\t------\t----\n";
        foreach ($exceptionInfo["CallStack"] as $callLabel => $callLine)
        {
            $printResult .= "\n\t\t" . $callLabel . " ";
            if (is_array($callLine))
            {
                foreach ($callLine as $cellIdx => $callLineCell)
                {
                    if ($callLineCell == "-1")
                    {
                        $callLineCell = "n/a";
                    }
                    if ($cellIdx == 0)
                    {
                        $printResult .= $callLineCell . "\t";
                    }
                    elseif ($cellIdx == 2)
                    {
                        $printResult .= $callLineCell . " in ";
                    }
                    elseif ($cellIdx == 3)
                    {
                        $printResult .= $callLineCell . " at line ";
                    }
                    else
                    {
                        $printResult .= $callLineCell . "\t";
                    }
                }
            }
            else
            {
                $printResult .= $callLine . "\t";
            }
            $printResult .= "\n";
        }
        $printResult .= "\n";
        $exceptionIteration++;
    }
    return $printResult;
}