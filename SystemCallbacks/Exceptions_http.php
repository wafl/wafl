<?php

use DblEj\Communication\Ajax\AjaxError,
    DblEj\Communication\Http\Request,
    DblEj\Communication\Http\Util,
    DblEj\Data\ArrayModel,
    DblEj\Presentation\RenderOptions,
    DblEj\UI\Skin,
    DblEj\UI\StyledFont,
    DblEj\Util\Debug,
    \DblEj\Util\Strings,
    Wafl\Core;

function __activeWaflExceptionHandler2($exception)
{
    //no longer using the ErrorException, which loses the backtrace
    //if (is_a($exception, "\Error"))
    //{
        //$exception = new \ErrorException($exception->getMessage(), $exception->getCode(), E_ERROR, $exception->getFile(), $exception->getLine());
    //}
    if (error_reporting() > 0)
    {
        try
        {
            error_log($exception->getMessage() . " at line " . $exception->getLine() . " in file " . $exception->getFile());
            error_log($exception->getTraceAsString());

            if (isset(Core::$RUNNING_APPLICATION))
            {
                $suppressErrors = Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_SuppressErrors();
                $isDebug = Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_DebugMode();
            } else {
                $suppressErrors = false;
                $isDebug = true;
            }

            $httpErrorCode = 0;
            if (is_a($exception, "DblEj\Communication\Http\Exception"))
            {
                if (AM_WEBPAGE)
                {
                    $headerSentByFile = null;
                    $headerSentByAtLine = null;
                    if (!headers_sent($headerSentByFile, $headerSentByAtLine))

                    {
                        header("HTTP/1.0 " . $exception->Get_HttpErrorCode() . " Application Runtime Error");
                    } else {
                        error_log("Could not send ".$exception->Get_HttpErrorCode()." because the headers were sent by $headerSentByFile at line $headerSentByAtLine");
                    }
                }
                $httpErrorCode = $exception->Get_HttpErrorCode();
            }
            else
            {
                if (AM_WEBPAGE)
                {
                    if (!headers_sent())
                    {
                        header("HTTP/1.0 500 Application Runtime Error");
                    }
                }
            }
            while (ob_get_level() > 0)
            {
                ob_end_clean();
            }
            $exceptionL = $exception;
            while ($exceptionL->getPrevious())
            {
                $exceptionL = $exceptionL->getPrevious();
            }
            switch ($exceptionL->getCode())
            {
                case \E_WARNING:
                    $severityClass = "Warning";
                    break;
                case \E_NOTICE:
                    //intentional follthrough
                case \E_STRICT:
                    $severityClass = "Info";
                    break;
                default:
                    $severityClass = "Error";
                    break;
            }

            $printResult = "";
            $isAjax = (Util::GetInput("HTTP_X_REQUESTED_WITH", Request::INPUT_SERVER) == "XMLHttpRequest") ? true : false;
            if ($isAjax)
            {
                $errorResponse = new AjaxError($exception, true, $isDebug && !$suppressErrors);
                $printResult   = $errorResponse->ToJson();
            }
            elseif (defined("AM_WEBPAGE") && !AM_WEBPAGE)
            {
                $printResult = _getTextErrorOutput($exception, $suppressErrors);
            }
            else
            {

                $requestedUrl = Util::GetInput("REQUEST_URI", Request::INPUT_SERVER);
                if (stristr($requestedUrl, "?"))
                {
                    $requestedUrl = substr($requestedUrl, 0, stripos($requestedUrl, "?"));
                }

                $isJs = Strings::EndsWith($requestedUrl, ".js");
                if ($isJs)
                {

                    $printResult = _getTextErrorOutput($exception, $suppressErrors);
                } else {
                    $printResult = _getViewEngineOutput($httpErrorCode, $exception, $exceptionL, $suppressErrors);
                }
            }
            print($printResult);
        }
        catch (\Throwable $ex)
        {
            print "There was an error while trying to render the error page for a previous error.\n";
            print "Initial Error: " . $exception->getMessage() . " in " . $exception->getFile() . " at " . $exception->getLine();
            print "Error printing error: " . $ex->getMessage() . " in " . $ex->getFile() . " at " . $ex->getLine();
        }
        if (!defined("EXCEPTION_OUTPUT"))
        {
            define("EXCEPTION_OUTPUT", true);
        }
        exit(1);
    }
    else
    {
        error_log("Error suppressed by php: " . $exception->getMessage() . " in " . $exception->getFile() . " at " . $exception->getLine());
        return false; //$reptLevel 0 indicates the error was suppressed with an @, let's let php handle it.
    }
}

function _getViewEngineOutput($httpErrorCode, $outermostException, $innerMostException, $suppressErrors = false)
{
    if (!$httpErrorCode)
    {
        $httpErrorCode = 500;
    }
    try
    {
        $errorDetails = _getHtmlErrorOutput($outermostException, $innerMostException, $suppressErrors);
        $viewEngine = \Wafl\Core::$RUNNING_APPLICATION->Get_TemplateRenderer();
        if ($viewEngine && $viewEngine->Get_IsReady())
        {
            if (!$viewEngine->GetGlobalData("SKIN"))
            {
                //set a default skin if none have been set yet, so we can render output
                $defaultSkin = new Skin("DefaultSkin", "DefaultSkin", new StyledFont());
                $viewEngine->SetGlobalData("SKIN", $defaultSkin);
                $viewEngine->SetGlobalData("SKINS", [$defaultSkin]);
                $viewEngine->SetGlobalData("CURRENT_SKIN_TITLE", $defaultSkin->Get_Title());
            }
            $page = null;
            if (\Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Debug()->Get_DebugMode() && !$suppressErrors)
            {
                $templateFolder = "Errors/Debug";
            } else {
                $templateFolder = "Errors";
            }
            if ($httpErrorCode)
            {
                $page = \Wafl\Core::$RUNNING_APPLICATION->GetSitePageByFilename("$templateFolder/$httpErrorCode");
            }
            if (!$page)
            {
                $page = \Wafl\Core::$RUNNING_APPLICATION->GetSitePageByFilename("$templateFolder/ServerError");
            }

            if ($page)
            {
                try
                {
                    $page->Set_Model(new ArrayModel(["ERROR_DETAILS"=>$errorDetails, "EXCEPTION"=>$outermostException, "ERROR_MESSAGE"=>$outermostException->getMessage()]));
                    \Wafl\Core::$RUNNING_APPLICATION->PreparePageForDisplay($page);
                    $printResult = @$page->Render($viewEngine, RenderOptions::GetDefaultInstance());
                } catch (\Throwable $ex) {
                    $printResult = $errorDetails;
                }
            }
            else
            {
                $printResult = $errorDetails;
            }
        } else {
            $printResult = $errorDetails;
        }
        return $printResult;
    }
    catch (\Throwable $ex)
    {
        $errorShowingError = $ex;
    }
    if ($errorShowingError)
    {
        $printResult = "<div style='text-align: left;'>"
        . "<h1>There was an error starting the Application</h1><h2>There was also an error displaying the error message.</h2>"
        . "<h3>Application Startup Error</h3>"
        . "<div>" . $outermostException->getMessage() . " in " . $outermostException->getFile() . " at " . $outermostException->getLine() . "</div>";
        $printResult .= "<div>" . nl2br($outermostException->getTraceAsString()) . "</div><br>";

        $exception = $outermostException;
        while ($exception->getPrevious())
        {
            $printResult .= ($exception->getPrevious() ? ("<div>Inner Exception:<br>" . $exception->getPrevious()->getMessage() . "<div>") : "");
            $exception = $exception->getPrevious();
        }

        $printResult .= "<h3>Could not render Error Page</h3>"
        . "<div>" . $errorShowingError->getMessage() . " in " . $errorShowingError->getFile() . " at " . $errorShowingError->getLine() . "</div>";
        $printResult .= "<div>" . nl2br($errorShowingError->getTraceAsString()) . "</div><br>";

        while ($errorShowingError->getPrevious())
        {
            $printResult .= ($errorShowingError->getPrevious() ? ("<div>Inner Exception:<br>" . $errorShowingError->getPrevious()->getMessage() . "<div>") : "");
            $errorShowingError = $errorShowingError->getPrevious();
        }


        $printResult .= "</div>";


        $printResult .= "<small>If you get an error about file write permissions, please check that your template compile and cache folders are writeable</small>";
    }
    return $printResult;
}

function _getHtmlErrorOutput($outermostException, $innerMostException, $suppressErrors = false)
{
    if (!$suppressErrors)
    {
        $printResult = "<section style='padding: 0 24px; font-family: monospace;'><h1>" . get_class($innerMostException) . "</h1>";
        $printResult .= "<h2 style='margin-bottom: 0;'>File: " . $innerMostException->getFile() . "</h2>";
        $printResult .= "<h2 style='margin-top: 0;'>Line: " . $innerMostException->getLine() . "</h2>";

        if ($innerMostException != $outermostException)
        {
            $innerTraceLines     = $innerMostException->getTrace();
            $isSmartyRenderError = false;

            foreach ($innerTraceLines as $innerTraceLine)
            {
                if (isset($innerTraceLine["file"]) && stristr(basename($innerTraceLine["file"]), "smarty_internal_templatebase.php"))
                {
                    $isSmartyRenderError = true;
                    break;
                }
            }
            if ($isSmartyRenderError)
            {
                if (substr($innerMostException->getMessage(), 0, 15) == "unable to write")
                {
                    $printResult .= "<p style='padding: 8px; background-color: #ED4337; color: #ffffff;font-size: 130%;'>There was an error while rendering the HTML template: cannot write to template compile folder (".substr($innerMostException->getMessage(), 21).").  Make sure the file is writable by the executing user.</p>";
                } else {
                    $printResult .= "<p style='padding: 8px; background-color: #ED4337; color: #ffffff;font-size: 130%;'>There was an error while rendering the HTML template.</p>";
                }
            }
            else
            {
                $printResult .= "<p style='padding: 8px; background-color: #ED4337; color: #ffffff;font-size: 130%;'>" . str_replace(". ", ".<br>", strip_tags($outermostException->getMessage())) . "</p>";
            }
            $printResult .= "<p style='border: solid 1px #999999; padding: 8px;'><b>Cause</b><br>" . str_replace(". ", ".<br>", $innerMostException->getMessage());
            $innerException = $outermostException->getPrevious();
            if ($innerException != $innerMostException)
            {
                $printResult .= "<p style='border: solid 1px #999999; padding: 8px;'><b>Intermediate Effect(s)</b><br>";
            }
            while ($innerException)
            {
                if ($innerException != $innerMostException)
                {
                    $printResult .= str_replace(". ", ".<br>", $innerException->getMessage()) . "<br>";
                }
                $innerException = $innerException->getPrevious();
            }
            $printResult .="</p>";
        }
        else
        {
            $printResult .= "<p style='padding: 8px; background-color: #ED4337; color: #ffffff;font-size: 130%;'>" . str_replace(". ", ".<br>", $innerMostException->getMessage()) . "</p>";
        }
        $printResult .= "<section style='border: dotted 1px #999999;'><h4 style='padding: 4px; background-color: #efefff;margin: 0 0 3px 0;'>Call Stack <span style='font-size: 90%;'>(in order of execution)</span></h4>";
        $smartyMutingFound = false;
        $currentAppLayer   = "Entry Point";
        $rowBgColor        = "#dfdfdf";
        $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
        foreach (array_reverse($innerMostException->getTrace()) as $callLabel => $callLine)
        {
            if (isset($callLine["class"]) && !$smartyMutingFound)
            {
                if (isset($callLine["file"]) && Strings::StartsWith($callLine["file"], "phar://") && (strstr($callLine["file"], "AppSupport.phar") !== false) && ($currentAppLayer != "Framework: Bootstrap and Run App"))
                {
                    $rowBgColor      = "#dfdfdf";
                    $currentAppLayer = "Framework: Bootstrap and Run App";
                    $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                }
                elseif (Strings::StartsWith($callLine["class"], "DblEj\\Communication") && Strings::EndsWith($callLine["function"], "GotoRoute") && $currentAppLayer != "Framework: Request Routing")
                {
                    $rowBgColor      = "#dfdfdf";
                    $currentAppLayer = "Framework: Request Routing";
                    $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                }
                elseif (Strings::StartsWith($callLine["class"], "Wafl") && Strings::EndsWith($callLine["function"], "FromServerToClient") && $currentAppLayer != "Framework: Render Response")
                {
                    $rowBgColor      = "#dfdfdf";
                    $currentAppLayer = "Framework: Render Response";
                    $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                }
                elseif ($callLine["class"] == "Smarty_Internal_TemplateBase" && $currentAppLayer != "Template Engine: Render Template")
                {
                    $rowBgColor      = "#dfdfdf";
                    $currentAppLayer = "Template Engine: Render Template";
                    $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                }
                elseif (strstr($callLine["class"], "\\FunctionalModel\\") && $currentAppLayer != "Application: Logic" && $currentAppLayer != "Template Model: Get Data")
                {
                    $rowBgColor      = "#dfefff";

                    if ($currentAppLayer == "Template Engine: Render Template")
                    {
                        $currentAppLayer = "Template Model: Get Data";
                    } else {
                        $currentAppLayer = "Application: Logic";
                    }
                    $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                }
                elseif (strstr($callLine["class"], "\\DataModel\\") && $currentAppLayer != "Application: Data Access")
                {
                    $rowBgColor      = "#dfefff";
                    $currentAppLayer = "Application: Data Access";
                    $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                }
                elseif (Strings::StartsWith($callLine["class"], "DblEj\\Data\\") && $currentAppLayer != "Framework: Data Access")
                {
                    $rowBgColor      = "#dfdfdf";
                    $currentAppLayer = "Framework: Data Access";
                    $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                }
                $printResult .= "<section style='padding: 6px; color: #333333; margin-bottom: 3px; background-color: $rowBgColor;'> <span style='color: #666666;'>
                                    " . str_replace(" ", "&nbsp;", (str_pad($callLabel, 3, " ", STR_PAD_LEFT))) . " <span style='color: #000000;'>" . (isset($callLine["file"]) ? $callLine["file"] : "") . "</span> <span style='color: #000000;'>" . (isset($callLine["line"]) ? $callLine["line"] : "dynamic call") . "</span>";
                if ($callLine["class"] == "Smarty" && $callLine["function"] == "mutingErrorHandler")
                {
                    $smartyMutingFound = true;
                    $printResult .= "<br>&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: blue;'>" . $outermostException->getMessage() . "</span>";
                }
                else
                {
                    $args          = $callLine["args"]??[];
                    $formattedArgs = [];
                    foreach ($args as $arg)
                    {
                        if (is_object($arg))
                        {
                            $argAsString     = Debug::GetVarString($arg, 2, false, false);
                            $jsonEncodedArg  = json_encode($argAsString);
                            $classString     = htmlspecialchars($jsonEncodedArg, ENT_QUOTES, 'utf-8');
                            $formattedArgs[] = "*<a href='' onclick=\"console.log($classString);alert('value sent to debug console');return false;\">" . get_class($arg) . "</a>";
                        }
                        elseif (is_string($arg))
                        {
                            if (strlen($arg) > 1000)
                            {
                                $argAsString = substr($arg, 0, 1000)."...";
                            } else {
                                $argAsString = $arg;
                            }
                            $formattedArgs[] = "\"$argAsString\"";
                        }
                        elseif (is_bool($arg))
                        {
                            $formattedArgs[] = $arg ? "true" : "false";
                        }
                        elseif (is_scalar($arg))
                        {
                            $formattedArgs[] = $arg;
                        }
                        else
                        {
                            $formattedArgs[] = "*" . gettype($arg);
                        }
                    }
                    $argsString = implode(",", $formattedArgs);
                    $printResult .= "<br>&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: blue;'>" . $callLine["class"] . "</span>" . $callLine["type"] . "<span style='color: blue;'>" . $callLine["function"] . "($argsString)</span>";
                }
                $printResult .= "</section>";
            }
            elseif (isset($callLine["file"]))
            {
                if (basename($callLine["file"]) != "Smarty.class.php")
                {
                    $gotoAppRequest = false;
                    if (Strings::StartsWith($callLine["function"], "call_user_func"))
                    {
                        if ($currentAppLayer == "Framework: Request Routing")
                        {
                            $rowBgColor      = "#dfdfdf";
                            $currentAppLayer = "Framework: Application Request";
                            $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                        }
                        elseif ($currentAppLayer == "Framework: Application Request")
                        {
                            $gotoAppRequest = true;
                        }
                    }
                    $printResult .= "<section style='padding: 6px; color: #333333; margin-bottom: 3px; background-color: $rowBgColor;'> <span style='color: #666666;'>
                                        " . str_replace(" ", "&nbsp;", (str_pad($callLabel, 3, " ", STR_PAD_LEFT))) . " <span style='color: #000000;'>" . $callLine["file"] . "</span>: <span style='color: #000000;'>" . $callLine["line"] . "</span>";
                    $printResult .= "<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<span style='color: blue;'>" . $callLine["function"] . "</span>
                                    </section>";
                    if ($gotoAppRequest)
                    {
                        $rowBgColor      = "#dfefff";
                        $currentAppLayer = "Application: Request Handler";
                        $printResult .= "<div style='border-top: dashed 1px #afafaf; margin-top: 8px; background-color: #ffffe0; color: #666666;'>$currentAppLayer</div>";
                    }
                }
                elseif (!$smartyMutingFound)
                {
                    $printResult .= "<section style='padding: 6px; color: #333333; margin-bottom: 3px; background-color: $rowBgColor;'> <span style='color: #666666;'>Call from inside HTML template</span></section>";
                }
            }
            elseif (!is_array($callLine))
            {
                $printResult .= "<section style='padding: 6px; color: #333333; margin-bottom: 3px; background-color: $rowBgColor;'> <span style='color: #666666;'>
                                    " . str_replace(" ", "&nbsp;", (str_pad($callLabel, 3, " ", STR_PAD_LEFT))) . " <span style='color: #000000;'>$callLine</span>
                                </section>";
            }
        }
        $printResult .= "</section></section>";
    } else {
        if (is_a($outermostException, "\\DblEj\\System\\Exception"))
        {
            $printResult = "Application halted: ".$outermostException->Get_PublicDetails();
        }
        elseif (is_a($innerMostException, "\\DblEj\\System\\Exception"))
        {
            $printResult = "Application halted: ".$innerMostException->Get_PublicDetails();
        } else {
            $printResult = "Application halted";
        }
    }
    return $printResult;
}

function _getTextErrorOutput(\Throwable $exception, $suppressErrors = false)
{
    if (!$suppressErrors)
    {
        $printResult        = "\n********** Execution halted due to an unhandled exceptions **********\n";
        $exceptionIteration = 0;
        $printResult .= "\n\t";
        $printResult.="Exception #" . ($exceptionIteration + 1) . " " . $exception->getMessage() . "\nin file " . $exception->getFile() . "\nat line " . $exception->getLine() . "\n";
        $printResult .= "\n\t\t" . $exception->getTraceAsString() . " ";
        $printResult .= "\n";
        $exceptionIteration++;
    } else {
        if (is_a($exception, "\\DblEj\\System\\Exception"))
        {
            $printResult = "Halted execution: ".$exception->Get_PublicDetails();
        }
        elseif (is_a($exception, "\\DblEj\\System\\Exception"))
        {
            $printResult = "Halted execution: ".$exception->Get_PublicDetails();
        } else {
            $printResult = "Halted execution";
        }
    }
    return $printResult;
}

function _getJsonErrorOutput($exceptionInfos, \Throwable $exception)
{
    $jsonResult = json_encode($exceptionInfos);
    return $jsonResult;
}