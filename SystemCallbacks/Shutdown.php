<?php

use Wafl\Core;

function __debugActiveWaflShutdownHandler()
{
    if (!defined("EXCEPTION_OUTPUT") || !EXCEPTION_OUTPUT)
    {
        if (error_reporting() > 0)
        {
            $lastError = error_get_last();
            $isAjax    = (\DblEj\Communication\Http\Util::GetInput("HTTP_X_REQUESTED_WITH", \DblEj\Communication\Http\Request::INPUT_SERVER) == "XMLHttpRequest") ? true : false;
            if ($lastError !== null && ($lastError["message"] != \Wafl\Util\Debug::Get_LastSuppressedError()))
            {
                while (ob_get_level() > 0)
                {
                    ob_end_clean();
                }
                print("**Application fatal error detected during shutdown**\n"
                . "---------------\n"
                . $lastError["message"] . "\n"
                . "in " . $lastError["file"] . " at line " . $lastError["line"]);
                exit(1);
            }
            elseif (Core::Get_RequestPending())
            {
                print("**Application Startup Warning**\n"
                . "-----------\n"
                . "The framework was successfully initialized and shutdown, however, no Application ran.\n"
                . "This indicates that your app never started, or that it exited prematurely *without any errors*.  "
                . "This could be caused by an error with your Wafl configuration.  "
                . "Note that if Application Auto Start is turned off, then you must start the application manually.");
                exit(1);
            }
        }
    }
}