<?php

use Wafl\Core;

function __debugActiveWaflShutdownHandler()
{
    if (!defined("EXCEPTION_OUTPUT") || !EXCEPTION_OUTPUT)
    {
        if (error_reporting() > 0)
        {
            $lastError = error_get_last();
            $isAjax    = (\DblEj\Communication\Http\Util::GetInput("HTTP_X_REQUESTED_WITH", \DblEj\Communication\Http\Request::INPUT_SERVER) == "XMLHttpRequest") ? true : false;
            if ($lastError !== null && ($lastError["message"] != \Wafl\Util\Debug::Get_LastSuppressedError()))
            {
                while (ob_get_level() > 0)
                {
                    ob_end_clean();
                }
                if ($isAjax)
                {
                    if (!headers_sent())
                    {
                        $errorResponse = new \DblEj\Communication\Ajax\AjaxError(new ErrorException($lastError["message"], $lastError["type"], E_ERROR, $lastError["file"], $lastError["line"]));
                        $responseText = $errorResponse->ToJson();
                        header("HTTP/1.0 500 Application error detected during shutdown(a) " . str_replace("\n", " ", str_replace("\r\n", " ", $lastError["message"])));
                        print $responseText;
                    } else {
                        error_log("There was a PHP shutdown error after output was already sent so I could not include it in the response content. File: ".$lastError["file"].", Line: ".$lastError["line"].": ".$lastError["message"]);
                    }
                }
                elseif ((!defined("AM_WEBPAGE") || AM_WEBPAGE))
                {
                    if (!headers_sent())
                    {
                        $errorMessage = str_replace("\r\n", " ", $lastError["message"]);
                        $errorMessage = str_replace("\n", " ", $errorMessage);

                        header("HTTP/1.0 500 Application error detected during shutdown(w) $errorMessage");
                        print("<html><head><title>Application error detected during shutdown</title><body style='padding: 0 32px; text-align: center;'><h2 style='color: red;'>Http 500</h2><h3 style='color: red;'>Application fatal error detected during shutdown</h3>"
                        . "<hr><p style='font-size: 20px;text-align: left;'>" . $lastError["message"] . "</p>"
                        . "<div style='text-align: left;'>in <b>" . $lastError["file"] . "</b> at <b>line " . $lastError["line"] . "</b></div>"
                        . "</body></html>");
                    }
                    else
                    {
                        print "<h2 style='color: red;'>Http 500</h2><h3 style='color: red;'>Application fatal error detected during shutdown</h3>"
                        . "<hr><p style='font-size: 20px;text-align: left;'>" . $lastError["message"] . "</p>"
                        . "<div style='text-align: left;'>in <b>" . $lastError["file"] . "</b> at <b>line " . $lastError["line"] . "</b></div>";
                        error_log("There was a PHP shutdown error after output was already sent so I could not include it in the response content. File: ".$lastError["file"].", Line: ".$lastError["line"].": ".$lastError["message"]);
                    }
                }
                else
                {
                    if (!headers_sent())
                    {
                        header("HTTP/1.0 500 Application error detected during shutdown(u) " . str_replace("\n", " ", str_replace("\r\n", " ", $lastError["message"])));
                        print("**Application error detected during shutdown**\n"
                        . "---------------\n"
                        . $lastError["message"] . "\n"
                        . "in " . $lastError["file"] . " at line " . $lastError["line"]);
                    } else {
                        print("**Application error detected during shutdown**\n"
                        . "---------------\n"
                        . $lastError["message"] . "\n"
                        . "in " . $lastError["file"] . " at line " . $lastError["line"]);
                        error_log("There was a PHP shutdown error after output was already sent so I could not include it in the response content. File: ".$lastError["file"].", Line: ".$lastError["line"].": ".$lastError["message"]);
                    }
                }
                exit(1);
            }
            elseif (Core::Get_RequestPending())
            {
                if ($isAjax)
                {
                    if (!headers_sent())
                    {
                        header("HTTP/1.0 500 Application Startup Warning");
                        print("There was a PHP shutdown error after output was already sent so I could not include it in the response content. The framework was initialized and shutdown but no application ran.");
                    } else {
                        error_log("There was a PHP shutdown error after output was already sent so I could not include it in the response content. The framework was initialized and shutdown but no application ran.");
                    }
                }
                elseif ((!defined("AM_WEBPAGE") || AM_WEBPAGE))
                {
                    if (!headers_sent())
                    {
                        header("HTTP/1.0 500 Application Startup Warning");
                        print("<html><head><title>Application Startup Warning</title><body style='padding: 0 32px; text-align: center;'><h2 style='color: orange;'>Http 500</h2><h3 style='color: orange;'>ActiveWAFL Application Startup Warning</h3>"
                        . "<hr><p style='font-size: 20px;text-align: left;'>The framework was successfully initialized and shutdown, however, no Application ran.</p>"
                        . "<div style='text-align: left;'>This indicates that your app never started, or that it exited prematurely *without any errors*.  This could be caused by an error with your Wafl configuration.  Note that if Application Auto Start is turned off, then you must start the application manually.</div>"
                        . "</body></html>");
                    } else {
                        error_log("There was a PHP shutdown error after output was already sent so I could not include it in the response content. The framework was initialized and shutdown but no application ran.");
                    }
                }
                else
                {
                    print("**Application Startup Warning**\n"
                    . "-----------\n"
                    . "The framework was successfully initialized and shutdown, however, no Application ran.\n"
                    . "This indicates that your app never started, or that it exited prematurely *without any errors*.  "
                    . "This could be caused by an error with your Wafl configuration.  "
                    . "Note that if Application Auto Start is turned off, then you must start the application manually.");
                }
                exit(1);
            }
        }
    }
}