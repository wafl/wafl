<?php

namespace Wafl;

use DblEj\Logging\Tracing;
use DblEj\Presentation\InfoDisplay;

class UserFeedback
{
    private static $_ERRORS = array();
    private static $_INFOS  = array();

    public static function InitSessionFeedback()
    {
        self::$_ERRORS = isset($_SESSION["feedback_errors"])?$_SESSION["feedback_errors"]:[];
        self::$_INFOS = isset($_SESSION["feedback_infos"])?$_SESSION["feedback_infos"]:[];
    }
    
    public static function AppendError($title, $description = null, $finePrint = null, $fileName = "", $lineNumber = 0)
    {
        self::$_ERRORS[] = new InfoDisplay($title, $description, $finePrint);
        Tracing::TraceError($title . ": " . $description, $fileName, $lineNumber);
        $_SESSION["feedback_errors"] = self::$_ERRORS;
    }

    public static function AppendInfo($title, $description = null, $finePrint = null, $fileName = "", $lineNumber = 0)
    {
        self::$_INFOS[] = new InfoDisplay($title, $description, $finePrint);
        Tracing::TraceInfo($title . ": " . $description, $fileName, $lineNumber);
        $_SESSION["feedback_infos"] = self::$_INFOS;
    }

    public static function GetGlobalErrors()
    {
        //once someone gets the errors, erase them from the session
        $_SESSION["feedback_errors"] = [];

        return self::$_ERRORS;
    }

    public static function GetGlobalInfos()
    {
        //once someone gets the infos, erase them from the session
        $_SESSION["feedback_infos"] = [];
        return self::$_INFOS;
    }

    public static function ClearSessionVariables()
    {
        $_SESSION["feedback_errors"] = [];
        $_SESSION["feedback_infos"] = [];
    }

    public static function ClearGlobalErrors()
    {
        $_SESSION["feedback_errors"] = [];
        self::$_ERRORS = array();
    }

    public static function ClearGlobalInfos()
    {
        $_SESSION["feedback_infos"] = [];
        self::$_INFOS = array();
    }
}