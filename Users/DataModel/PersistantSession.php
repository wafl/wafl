<?php
namespace Wafl\Users\DataModel;

abstract class PersistantSession
Extends \DblEj\Data\PersistableModel
implements \DblEj\Authentication\IUserSession
{
    protected $_sessionId;
    protected $_userId;
    protected $_startDate;
    protected $_lastActivityDate;
    public function Get_SessionId()
    {
        return $this->_sessionId;
    }
    public function Set_SessionId($sessionId)
    {
        $this->_sessionId = $sessionId;
    }

    public function Get_UserId()
    {
        return $this->_userId;
    }
    public function Set_UserId($userId)
    {
        $this->_userId = $userId;
    }

    public function Get_StartDate()
    {
        return $this->_startDate;
    }
    public function Set_StartDate($startDate)
    {
        $this->_startDate = $startDate;
    }

    public function Get_LastActivityDate()
    {
        return $this->_lastActivityDate;
    }
    public function Set_LastActivityDate($lastActivityDate)
    {
        $this->_lastActivityDate = $lastActivityDate;
    }
}