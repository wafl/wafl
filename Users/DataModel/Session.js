Namespace("Wafl.Users.DataModel");
Wafl.Users.DataModel.Session = DblEj.Data.Model.extend(
{
    init: function()
    {
        this._super();
        this.ServerObjectName = "\\Wafl\\Users\\FunctionalModel\\Session";
        this.SessionId=null;
        this.StartDate=null;
        this.LastActivityDate=null;
    },
    Get_ClientId: function()
    {
        return this.SessionId;
    },
    Get_SessionId: function()
    {
        return this.SessionId;
    },
    Set_SessionId: function(newValue)
    {
        if (this.SessionId != newValue)
        {
            this.SessionId = newValue;
            this.ModelChanged("SessionId");
        }
    },
    Get_StartDate: function()
    {
        return this.StartDate;
    },
    Set_StartDate: function(newValue)
    {
        if (this.StartDate != newValue)
        {
            this.StartDate = newValue;
            this.ModelChanged("StartDate");
        }
    },
    Get_LastActivityDate: function()
    {
        return this.LastActivityDate;
    },
    Set_LastActivityDate: function(newValue)
    {
        if (this.LastActivityDate != newValue)
        {
            this.LastActivityDate = newValue;
            this.ModelChanged("LastActivityDate");
        }
    }
})