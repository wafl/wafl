<?php
namespace Wafl\Users\FunctionalModel;

abstract class PersistantSession
extends \Wafl\Users\DataModel\PersistantSession
implements \DblEj\Authentication\IUserSession
{
    use \Wafl\Users\PhpSessionTrait;

    public function __construct($keyValue = null, array $objectData = null, \DblEj\Data\IDatabaseConnection $storageEngine = null, $dataGroup = null)
    {
        parent::__construct($keyValue, $objectData, $storageEngine, $dataGroup);
        if (!AM_WEBPAGE)
        {
            $this->_nonWebSessionVars = [];
        }
    }
    protected function onAfterEnd($permanent)
    {
        if ($permanent)
        {
            $this->Delete();
            $this->_sessionId = null;
        }
    }

    public function Set_StartDate($startDate)
    {
        parent::Set_StartDate($startDate);
        $this->Set_StartTime($startDate);
    }

    public function Get_StartDate()
    {
        return $this->Get_StartTime();
    }
}