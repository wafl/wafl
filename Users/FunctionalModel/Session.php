<?php
namespace Wafl\Users\FunctionalModel;

class Session
extends \Wafl\Users\DataModel\Session
implements \DblEj\Authentication\IUserSession
{
    use \Wafl\Users\PhpSessionTrait;

    private $_userAgent;

    public function Get_User()
    {
        return \Wafl\Core::$CURRENT_USER;
    }
    public function Set_UserAgent($userAgent)
    {
        $this->_userAgent = $userAgent;
    }
    public function Get_UserAgent()
    {
        return $this->_userAgent;
    }
    public static function CanOpen($sessionId = null)
    {
        return true;
    }
}