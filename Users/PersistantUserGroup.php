<?php

namespace Wafl\Users;

abstract class PersistantUserGroup
extends \DblEj\Data\PersistableModel
implements \DblEj\Authentication\IUserGroup
{
    use \DblEj\Resources\ActorTrait;
    
    protected static $_users         = array();
    protected static $_allUserGroups = array();
    protected $_title;
    protected $_userGroupId;

    public function AddUserToGroup(\DblEj\Authentication\IUserGroupUser $user)
    {
        self::$_users[$user->Get_Username()] = $user;
        $user->Set_UserGroup($this);
    }

    public function RemoveUserFromGroup(\DblEj\Authentication\IUserGroupUser $user)
    {
        if (isset(self::$_users[$user->Get_Username()]))
        {
            unset(self::$_users[$user->Get_Username()]);
            $user->Set_UserGroup(null);
        }
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Set_Title($newGroupName)
    {
        $this->_title = $newGroupName;
    }

    public function Get_UserGroupId()
    {
        return $this->_userGroupId;
    }

    public function Set_UserGroupId($newGroupId)
    {
        $this->_userGroupId = $newGroupId;
    }

    public function GetUsers()
    {
        return self::$_users;
    }

    public function Get_ActorId()
    {
        return $this->_userGroupId;
    }

    public function Get_ActorTypeId()
    {
        return \DblEj\Resources\Resource::RESOURCE_TYPE_PEOPLE;
    }

    public function Get_DisplayName()
    {
        return $this->_title;
    }

    public static function RegisterNewUserGroup($title, $userGroupId)
    {
        $newGroup               = new UserGroup();
        $newGroup->Set_Title($title);
        $newGroup->Set_UserGroupId($userGroupId);
        self::$_allUserGroups[] = $newGroup;
        return $newGroup;
    }

    public static function RegisterUserGroup(\DblEj\Authentication\IUserGroup $group)
    {
        self::$_allUserGroups[] = $group;
        return $group;
    }

    public static function GetGroupById($groupid)
    {
        $returnGroup = null;
        foreach (self::$_allUserGroups as $group)
        {
            if ($group->Get_UserGroupId() == $groupid)
            {
                $returnGroup = $group;
                break;
            }
        }
        return $returnGroup;
    }
}