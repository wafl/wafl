<?php
namespace Wafl\Users;

trait PhpSessionTrait
{
    protected $_remoteIp = null;
    protected $_dateStarted = null;
    protected $_nonWebSessionVars;
    protected $_cookieName;

    protected static $_instances  = [];

    public function Get_IpAddress()
    {
        return $this->_remoteIp;
    }
    public function Set_IpAddress($newIpAddress)
    {
        $this->_remoteIp = $newIpAddress;
        return $this;
    }
    public function Get_StartTime()
    {
        return $this->_dateStarted;
    }
    public function Set_StartTime($timeStamp)
    {
        $this->_dateStarted = $timeStamp;
    }
    public function Get_LastPingTime()
    {
        return $this->_lastActivityDate;
    }

    public function Set_CookieName($cookieName)
    {
        $this->_cookieName = $cookieName;
    }

    final public function End($permanent = true)
    {
        if ($this->onBeforeEnd($permanent) !== false)
        {
            if ($this->IsOpen())
            {
                if (AM_WEBPAGE)
                {
                    if (session_status() == PHP_SESSION_ACTIVE)
                    {
                        $params = session_get_cookie_params();
                        session_regenerate_id();
                        session_destroy();
                        setcookie($this->_cookieName, '', time() - 20, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
                    }
                }
            }
            if (isset($_SESSION))
            {
                $_SESSION = [];
            }
            $this->_cookieName = null;
            $this->_nonWebSessionVars = [];

            $this->onAfterEnd($permanent);
            $this->_sessionId = null;
        }
        return $this;
    }

    public function Ping()
    {
        if ($this->onActivity() !== false)
        {
            $this->_lastActivityDate = time();
        }
        return $this;
    }

    final public function Close()
    {
        if ($this->onClose() !== false)
        {
            if ($this->IsOpen())
            {
                if (AM_WEBPAGE)
                {
                    session_write_close();
                } else {
                    if (isset($this->_nonWebSessionVars) && $this->_sessionId)
                    {
                        //save anywhere?
                    }
                }
            }
        }
        return $this;
    }

    final public function SetData($dataName, $dataValue, $dataExpiration = NULL)
    {

        if ($this->onSetData($dataName, $dataValue) !== false)
        {
            if (AM_WEBPAGE)
            {
                if (isset($_SESSION) && is_array($_SESSION))
                {
                    $_SESSION[$dataName] = $dataValue;
                }
            } else {
                if (is_array($this->_nonWebSessionVars))
                {
                    $this->_nonWebSessionVars[$dataName] = $dataValue;
                } else {
                    throw new \Exception("Cannot store session data because the session storage is not ready or has been closed");
                }
            }

        }
        return $this;
    }

    final public function GetData($dataName, $defaultValue = null)
    {
        $dataArray = null;
        if ($this->onGetData($dataName) !== false)
        {
            if (AM_WEBPAGE)
            {
                $dataArray = isset($_SESSION)?$_SESSION:[];
            } else {
                if (!isset($this->_nonWebSessionVars))
                {
                    $this->_nonWebSessionVars = [];
                }
                $dataArray = $this->_nonWebSessionVars;
            }
        }
        return (isset($dataArray) && isset($dataArray[$dataName]))?$dataArray[$dataName]:$defaultValue;
    }

    public function HasData($dataName)
    {
        return isset($_SESSION) && isset($_SESSION[$dataName]);
    }

    final public function DeleteData($dataName)
    {
        if ($this->onDeleteData($dataName) !== false)
        {
            if (AM_WEBPAGE)
            {
                if (isset($_SESSION) && isset($_SESSION[$dataName]))
                {
                    unset($_SESSION[$dataName]);
                }
            } else {
                if (isset($this->_nonWebSessionVars) && isset($this->_nonWebSessionVars[$dataName]))
                {
                    unset($this->_nonWebSessionVars[$dataName]);
                }
            }
        }
        return $this;
    }

    final public function FlushAllData()
    {
        if ($this->IsOpen())
        {
            foreach (array_keys($_SESSION) as $sessionVar)
            {
                if ($this->onDeleteData($sessionVar) !== false)
                {
                    if (isset($_SESSION) && isset($_SESSION[$sessionVar]))
                    {
                        unsset($_SESSION[$sessionVar]);
                    }
                    if (isset($this->_nonWebSessionVars) && isset($this->_nonWebSessionVars[$sessionVar]))
                    {
                        unsset($this->_nonWebSessionVars[$sessionVar]);
                    }
                }
            }
        } else {
            $this->_nonWebSessionVars = null;
            if (isset($_SESSION))
            {
                $_SESSION = [];
            }
        }
        return $this;
    }

    final public function IsOpen()
    {
        return ($this->_sessionId && $this->_dateStarted) && (!AM_WEBPAGE || session_status() == PHP_SESSION_ACTIVE);
    }

    final public static function Open(\DblEj\Application\IApplication $app, $sessionId = null)
    {
        if ($sessionId && isset(self::$_instances[$sessionId]) && self::$_instances[$sessionId] && !self::$_instances[$sessionId]->IsOpen())
        {
            self::$_instances[$sessionId] = null;
        }

        if (!$sessionId || !isset(self::$_instances[$sessionId]) || !self::$_instances[$sessionId] || !self::$_instances[$sessionId]->IsOpen())
        {
            if ($sessionId)
            {
                self::$_instances[$sessionId] = null;
            }
            if (AM_WEBPAGE)
            {
                if (headers_sent())
                {
                    throw new \Exception("Cannot initialize session because the headers have been sent already");
                }
                if (session_status() == PHP_SESSION_ACTIVE)
                {
                    session_abort();
                }

                $webSettings = $app->Get_Settings()->Get_Web();
                if ($webSettings->Get_CookieDomainPrefix())
                {
                    session_set_cookie_params($webSettings->Get_CookieLifetime(), "/", $webSettings->Get_CookieDomainPrefix() . $webSettings->Get_DomainName());
                } else {
                    session_set_cookie_params($webSettings->Get_CookieLifetime(), "/");
                }

                $cookieName = $app->Get_Settings()->Get_Application()->GetSessionTagName();
                session_name($cookieName);
                header_remove("pragma");
                header_remove("expires");

                if ($sessionId)
                {
                    session_id($sessionId);
                }

                $sessionIdTrys = 0;
                \session_start();
                if (!$sessionId)
                {
                    $sessionId = session_id();
                }

                while (!static::CanOpen($sessionId))
                {
                    $sessionIdTrys++;
                    if ($sessionIdTrys > 10)
                    {
                        throw new \Exception("Cannot initialize user session");
                    }
                    \session_regenerate_id();
                    $sessionId = session_id();
                }
                $remoteIp = \DblEj\Communication\Http\Util::GetClientIpAddress();
                $useragent=isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:"";
            }
            elseif (defined("WAFL_SCRIPT_ID"))
            {
                $sessionId = md5(WAFL_SCRIPT_ID);
                $remoteIp = "cli-script";
                $useragent = "non-web-client";
                $cookieName = "nonwebsid";
            } else {
                $sessionId = md5(uniqid("", true));
                $remoteIp = "unknown";
                $useragent = "non-web-client";
                $cookieName = "nonwebsid";
            }
            $subclass = get_called_class();
            self::$_instances[$sessionId] = new $subclass($sessionId);
        }
        if ($sessionId && isset(self::$_instances[$sessionId]) && self::$_instances[$sessionId])
        {
            if (!self::$_instances[$sessionId]->Get_SessionId())
            {
                self::$_instances[$sessionId]->Set_SessionId($sessionId);
                self::$_instances[$sessionId]->Set_StartDate(time());
            }
            if (!self::$_instances[$sessionId]->Get_StartDate())
            {
                self::$_instances[$sessionId]->Set_StartDate(time());
            }
            self::$_instances[$sessionId]->Set_UserAgent($useragent);
            self::$_instances[$sessionId]->Set_IpAddress($remoteIp);
            self::$_instances[$sessionId]->Set_CookieName($cookieName);
            self::$_instances[$sessionId]->Ping();
        }
        return $sessionId?self::$_instances[$sessionId]:null;
    }

    final public function OverrideSessionId($sessionId)
    {
        if ($this->onSessionIdChange($this->_sessionId, $sessionId) !== false)
        {
            $this->_sessionId = $sessionId;
            if (session_status() == PHP_SESSION_ACTIVE)
            {
                session_id($sessionId);
            }
        }
        return $this;
    }

    public static function CanOpen($sessionId = null)
    {
        return true;
    }

    protected function onSessionIdChange($oldSessionId, $newSessionId)
    {
        return true;
    }
    protected function onActivity()
    {
        return true;
    }
    protected function onBeforeEnd($permanent)
    {
        return true;
    }
    protected function onAfterEnd($permanent)
    {
        return true;
    }
    protected function onAfterOpen()
    {
        return true;
    }
    protected function onClose()
    {
        return true;
    }
    protected function onSetData($dataName, $dataValue)
    {
        return true;
    }
    protected function onGetData($dataName)
    {
        return true;
    }
    protected function onDeleteData($dataName)
    {
        return true;
    }
}