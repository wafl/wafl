<?php
namespace Wafl\Users;

class User
extends \DblEj\Resources\Actor
implements \DblEj\Authentication\IUserGroupUser
{
    private $_username;
    private $_userid;
    private $_authorizationKey;

    /**
     *
     * @var \DblEj\Authentication\IUserGroup
     */
    private $_userGroup;
    protected $_isLoggedIn = false;
    private static $_allUsers;

    private function __construct($username, $password)
    {
        $this->_username         = $username;
        $this->_authorizationKey = $password;
    }

    public function Get_UserId()
    {
        return $this->_userid;
    }

    public function Get_Username()
    {
        return $this->_username;
    }

    public function Set_Username($newUsername)
    {
        $this->_username = $newUsername;
    }

    public function Get_UserGroup()
    {
        return $this->_userGroup;
    }

    public function Get_UserGroupId()
    {
        if ($this->_userGroup)
        {
            return $this->_userGroup->Get_UserGroupId();
        }
        else
        {
            return null;
        }
    }

    public function Set_UserGroup(\DblEj\Authentication\IUserGroup $newUserGroup)
    {
        $this->_userGroup = $newUserGroup;
    }

    public function Set_AuthorizationKey($newAuthorizationKey)
    {
        $this->_authorizationKey = $newAuthorizationKey;
    }

    public function GetIsAuthorized($authorizationKey)
    {
        return ($authorizationKey == $this->_authorizationKey);
    }

    public function GetIsLoggedIn()
    {
        return $this->_isLoggedIn;
    }

    public function Logout()
    {
        $this->_isLoggedIn        = false;
        \Wafl\Core::$CURRENT_USER = static::GetGuestUser();
    }

    public static function Login($username, $authorizationKey)
    {
        if (isset(self::$_allUsers[$username]) && self::$_allUsers[$username]->IsAuthorized($authorizationKey))
        {
            \Wafl\Core::$CURRENT_USER              = self::$_allUsers[$username];
            \Wafl\Core::$CURRENT_USER->_isLoggedIn = true;
            return self::$_allUsers[$username];
        }
        else
        {
            \Wafl\Core::$CURRENT_USER              = static::GetGuestUser();
            \Wafl\Core::$CURRENT_USER->_isLoggedIn = false;
            return null;
        }
    }

    public static function LoginBySession($sessionId = null)
    {
        if (!$sessionId)
        {
            $sessionId = session_id();
        }
        $subclass = get_called_class();
        $session  = Session::FilterFirst(" SessionId='$sessionId'");

        $user     = null;
        if ($session && $session->Get_UserId())
        {
            $user = new $subclass($session->Get_UserId());
        }
        if ($user)
        {
            $session->NotifyActivity();
            $user->_isloggedIn        = true;
            \Wafl\Core::$CURRENT_USER = $user;
        }
        return $user;
    }

    public static function RegisterNewUser($username, $authorizationKey)
    {
        if (!self::$_allUsers)
        {
            self::$_allUsers = array();
        }
        $subclass = get_called_class();
        self::$_allUsers[$username] = new $subclass($username, $authorizationKey);
        return self::$_allUsers[$username];
    }

    public static function GetGuestUser()
    {
        $subclass = get_called_class();
        return new $subclass("Guest User", "");
    }

    public function Get_ActorId()
    {
        return $this->_userid;
    }

    public function Get_ActorTypeId()
    {
        return \DblEj\Resources\Resource::RESOURCE_TYPE_PERSON;
    }

    public function Get_DisplayName()
    {
        return $this->_username;
    }

    public function GetParentActors()
    {
        return [$this->Get_UserGroup()];
    }
    public function GetContextualActor($context)
    {
        return [$this];
    }
}