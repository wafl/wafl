/*
 *@namespace Wafl.Util
 */
Namespace("Wafl.Util");

Wafl.Util.Api = Class.extend({});

/**
 * @method LoadModel
 * Load an existing instance of a Model from a server.
 *
 * @static
 *
 * @param {String} serverClassName The name of the server-side counterpart class that represents this Model.
 * @param {String} key The unique identifer of the model instance.
 * @param {Function} callbackMethod The method to notify when the model is received from the server.
 * If no callbackMethod is specified, then the request will be sent synchronously and will block
 * until a response is returned from the server, or until the request times out.
 * @default null
 *
 * @returns {Object|null} The returned instance, if no callback is specified.  Otherwise, null.
 */
Wafl.Util.Api.LoadModel = function (serverClassName, key, callbackMethod)
{
    var sendObject = {
        keyValue: key,
        serverClass: serverClassName
    };
    if (IsDefined(callbackMethod))
    {
        DblEj.Communication.Ajax.Utils.SendRequestAsync("DblEj.Data.Model.GetModelByKey", sendObject, "", callbackMethod);
        return null;
    } else {
        return DblEj.Communication.Ajax.Utils.SendRequest("DblEj.Data.Model.GetModelByKey", sendObject);
    }
};

/**
 * @function DeleteModel
 * Delete the specified model from a server.
 * 
 * @static
 * 
 * @param {String} serverClassName The name of the server-side class that represents this model.
 * @param {String} key The unique identifier used to find the model to be deleted.
 * @param {Function} callbackMethod The function to notify when the model has been deleted.
 * If no callbaclMethod is specified, then the request will be sent synchronously and will
 * block until the response is received, or until the request times out.
 * 
 * @returns {Object|null}
 */
Wafl.Util.Api.DeleteModel = function (serverClassName, key, callbackMethod)
{
    var sendObject = {
        keyValue: key,
        serverClass: serverClassName
    };
    if (IsDefined(callbackMethod))
    {
        DblEj.Communication.Ajax.Utils.SendRequestAsync("DblEj.Data.Model.DeleteModelByKey", sendObject, "", callbackMethod);
        return null;
    } else {
        return DblEj.Communication.Ajax.Utils.SendRequest("DblEj.Data.Model.DeleteModelByKey", sendObject);
    }
};
