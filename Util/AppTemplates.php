<?php

namespace Wafl\Util;

class AppTemplates
{

    public static function MakeAppTemplate($appType, $settings = null, $outFolderOrZipFilename = "AppTemplate.zip", $zipOutput = false)
    {
        $pharPath               = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Resources/AppTemplates.phar";
        $pharPath               = str_replace("\\", "/", $pharPath);
        $commonfiles            = self::_getTokenReplacedFiles("phar://$pharPath/Common", $settings);
        $commonfiles2           = null;

        if (!$settings)
        {
            $templateIniPath = "phar://$pharPath/Settings.ini";
            $settings = \Wafl\Util\AppTemplates::GetArrayFromIniFile($templateIniPath);
        }
        switch ($appType)
        {
            case "App":
                $appTemplateFolder = "phar://$pharPath/Application";
                break;
            case "WebApp":
                $commonfiles2      = self::_getTokenReplacedFiles("phar://$pharPath/CommonWeb", $settings);
                $appTemplateFolder = "phar://$pharPath/WebApplication";
                break;
            case "MvcWebApp":
                $commonfiles2      = self::_getTokenReplacedFiles("phar://$pharPath/CommonWeb", $settings);
                $appTemplateFolder = "phar://$pharPath/MvcWebApplication";
                break;
            case "ModularMvcWebApp":
                $commonfiles2      = self::_getTokenReplacedFiles("phar://$pharPath/CommonWeb", $settings);
                $appTemplateFolder = "phar://$pharPath/ModularMvcWebApplication";
                break;
        }

        $files = self::_getTokenReplacedFiles($appTemplateFolder, $settings);

        if ($zipOutput)
        {
            $outFolderOrZipFilename = realpath($outFolderOrZipFilename);
            if (file_exists($outFolderOrZipFilename))
            {
                throw new \Exception("The destination file already exists");
            }
            $zipFile = new \ZipArchive();
            $zipFile->open($outFolderOrZipFilename, \ZipArchive::CREATE);
            self::_addFilesToZip($commonfiles, $zipFile);
            self::_addFilesToZip($commonfiles2, $zipFile);
            self::_addFilesToZip($files, $zipFile);
        }
        else
        {
            if (!file_exists($outFolderOrZipFilename))
            {
                mkdir($outFolderOrZipFilename);
            }
            $outFolderOrZipFilename = realpath($outFolderOrZipFilename);
            self::_writeFiles($commonfiles, $outFolderOrZipFilename);
            if ($commonfiles2)
            {
                self::_writeFiles($commonfiles2, $outFolderOrZipFilename);
            }
            self::_writeFiles($files, $outFolderOrZipFilename);
            rename($outFolderOrZipFilename . DIRECTORY_SEPARATOR . "MyApp", $outFolderOrZipFilename . DIRECTORY_SEPARATOR . $settings["APPLICATION_NAMESPACE"]);
        }
    }

    public static function MakeControlTemplate($controlName, $phpNamespace, $jsNamespace, $outFolder)
    {
        $pharPath       = realpath(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Resources/AppTemplates.phar");
        $pharPath       = str_replace("\\", "/", $pharPath);
        $templateFolder = "phar://$pharPath/Control";

        $settings["CONTROL_NAME"]          = $controlName;
        $settings["CONTROLS_NAMESPACE"]    = $phpNamespace;
        $settings["CONTROLS_NAMESPACE_JS"] = $jsNamespace;

        $files        = self::_getTokenReplacedFiles($templateFolder, $settings);
        $renamedFiles = array();
        foreach ($files as $filename => $filecontents)
        {
            $newFilename                = str_replace("MyControl.", $controlName . ".", $filename);
            $renamedFiles[$newFilename] = $filecontents;
        }
        $outFolder = realpath($outFolder);
        $outFolder.=DIRECTORY_SEPARATOR . $controlName;
        if (!file_exists($outFolder))
        {
            mkdir($outFolder);
        }
        self::_writeFiles($renamedFiles, $outFolder);
    }

    public static function MakeExtensionTemplate($extensionName, $phpNamespace, $jsNamespace, $outFolder)
    {
        $pharPath       = realpath(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Resources/AppTemplates.phar");
        $pharPath       = str_replace("\\", "/", $pharPath);
        $templateFolder = "phar://$pharPath/Extension";

        $settings["EXTENSION_NAME"]          = $extensionName;
        $settings["EXTENSIONS_NAMESPACE"]    = $phpNamespace;
        $settings["EXTENSIONS_NAMESPACE_JS"] = $jsNamespace;

        $files        = self::_getTokenReplacedFiles($templateFolder, $settings);
        $renamedFiles = array();
        foreach ($files as $filename => $filecontents)
        {
            $newFilename                = str_replace("MyExtension.", $extensionName . ".", $filename);
            $renamedFiles[$newFilename] = $filecontents;
        }

        $outFolder = realpath($outFolder);
        $outFolder.=DIRECTORY_SEPARATOR . $extensionName;
        if (!file_exists($outFolder))
        {
            mkdir($outFolder);
        }
        self::_writeFiles($renamedFiles, $outFolder);
    }

    public static function MakeIni($outFolder)
    {
        if (file_exists($outFolder))
        {
            $pharPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Resources/AppTemplates.phar");
            $pharPath = str_replace("\\", "/", $pharPath);

            $lastChar = substr($outFolder, strlen($outFolder) - 1);
            if ($lastChar != "/" && $lastChar != "\\")
            {
                $outFile = $outFolder . "/" . "Settings.ini";
            }
            else
            {
                $outFile = $outFolder . "Settings.ini";
            }
            if (stristr(strtolower(php_uname("s")), "windows"))
            {
                $infile = "Settings-win.ini";
            } else {
                $infile = "Settings.ini";
            }

            if (file_exists($outFile))
            {
                unlink($outFile);
            }
            copy("phar://" . $pharPath . "/$infile", $outFile);
        }
        else
        {
            throw new \Exception("Invalid output directory: " . $outFolder);
        }
    }

    private static function _getTokenReplacedFiles($dir, $tokens)
    {
        $files  = array();
        $handle = opendir($dir);
        if ((substr($dir, strlen($dir) - 1) != "/") &&
        (substr($dir, strlen($dir) - 1) != "\\"))
        {
            $dir.=DIRECTORY_SEPARATOR;
        }
        if ($handle)
        {
            while (false !== ($entry = readdir($handle)))
            {
                if ($entry != "." && $entry != "..")
                {
                    $filePath = $dir . $entry;
                    if (is_file($filePath))
                    {
                        $replacedFileContents = file_get_contents($filePath);
                        foreach ($tokens as $tokenName => $tokenValue)
                        {
                            $replacedFileContents = str_replace("{" . $tokenName . "}", $tokenValue, $replacedFileContents);
                        }
                        $files[$entry] = $replacedFileContents;
                    }
                    elseif (is_dir($filePath))
                    {
                        $subFiles      = self::_getTokenReplacedFiles($filePath, $tokens);
                        $files[$entry] = $subFiles;
                    }
                }
            }
        }
        return $files;
    }

    private static function _writeFiles($files, $destinationDir)
    {
        if (!file_exists($destinationDir))
        {
            mkdir($destinationDir);
        }
        if ((substr($destinationDir, strlen($destinationDir) - 1) != "/") &&
        (substr($destinationDir, strlen($destinationDir) - 1) != "\\"))
        {
            $destinationDir.=DIRECTORY_SEPARATOR;
        }


        foreach ($files as $filename => $filecontents)
        {
            if (is_array($filecontents))
            {
                self::_writeFiles($filecontents, $destinationDir . $filename);
            }
            else
            {
                file_put_contents($destinationDir . $filename, $filecontents);
            }
        }
    }

    public static function GetArrayFromIniFile($settingsIniFile)
    {
        $settingsString = file_get_contents($settingsIniFile);
        $settingsArray = explode("\n", $settingsString);
        $compiledSettings = array();
        foreach ($settingsArray as $settingString)
        {
            $settingArray = explode("=", $settingString);
            if (count($settingArray)==2)
            {
                $compiledSettings[trim($settingArray[0])] = trim($settingArray[1]);
            }
        }
        return $compiledSettings;
    }
    private static function _addFilesToZip($files, \ZipArchive $zip, $parentFolderName = "")
    {
        if ($parentFolderName)
        {
            if ((substr($parentFolderName, strlen($parentFolderName) - 1) != "/") &&
            (substr($parentFolderName, strlen($parentFolderName) - 1) != "\\"))
            {
                $parentFolderName.=DIRECTORY_SEPARATOR;
            }
        }
        foreach ($files as $filename => $fileContents)
        {
            if (is_array($fileContents))
            {
                $zip->addEmptyDir($parentFolderName . $filename);
                self::_addFilesToZip($fileContents, $zip, $parentFolderName . $filename);
            }
            else
            {
                $zip->addFile($filename, $parentFolderName . $filename);
            }
        }
    }
}