<?php
namespace Wafl\Util;

class Captcha
{

    private static function GetCurrentCaptcha()
    {
        $app = \Wafl\Core::$RUNNING_APPLICATION;
        $captchaString = $app->GetSessionData("CaptchaString");

        if (!$captchaString)
        {
            self::ResetCaptcha();
            $captchaString = $app->GetSessionData("CaptchaString");
        }

        return $captchaString;
    }

    public static function ResetCaptcha($length = 5)
    {
        if ($length > 10)
        {
            $length = 10;
        }
        if ($length < 1)
        {
            $length = 1;
        }
        $disallowedChars = "iIoO01lL";
        $disallowedCharArray = str_split($disallowedChars);
        $captchaString = \DblEj\Util\Strings::GenerateRandomString($length, $disallowedCharArray);

        $app = \Wafl\Core::$RUNNING_APPLICATION;
        $app->StoreSessionData("CaptchaString", $captchaString);
    }

    public static function OutputCurrentCaptchaImage($crackDifficulty = 1, $fontPaths = null)
    {
        $string    = self::GetCurrentCaptcha();
        $letterColors   = array();
        $artifactColors = array();
        $letterAngles   = array();

        $letterAngleCount                    = $crackDifficulty;
        for ($letterAngleIdx = 1; $letterAngleIdx <= $letterAngleCount; $letterAngleIdx++)
        {
            $letterAngles[] = rand(-$crackDifficulty * 5, $crackDifficulty * 5);
        }

        //draw letters
        if (!$fontPaths || !is_array($fontPaths))
        {
            $fontPaths = array(12);
        }
        $letters = str_split($string);

        $stringWidth = 0;
        $stringHeight = 0;
        $smallestOffset = PHP_INT_MAX;
        foreach ($letters as $letter)
        {
            $font  = current($fontPaths);
            $angle = current($letterAngles);

            if ($font === false)
            {
                reset($fontPaths);
                $font = current($fontPaths);
            }
            if ($angle === false)
            {
                reset($letterAngles);
                $angle = current($letterAngles);
            }

            if (is_numeric($font))
            {
                $charWidth = imagefontwidth($font);
                $charHeight = imagefontheight($font);
                $offsetY = 0;
            }
            else
            {
                $meas      = imagettfbbox(32, $angle, $font, $letter);

                $charWidth = abs($meas[4] - $meas[0]);
                $charHeight = abs($meas[5] - $meas[1]);
                $offsetY = min([$meas[5], $meas[7]])/2;
            }
            $stringWidth += $charWidth;
            if ($charHeight > $stringHeight)
            {
                $stringHeight = $charHeight;
            }
            if ($offsetY < $smallestOffset)
            {
                $smallestOffset = $offsetY;
            }


            next($fontPaths);
            next($letterAngles);
        }


        $baseImageWidth = $stringWidth+$crackDifficulty;
        $baseImageHeight = $stringHeight+$crackDifficulty;
        $newWidth  = rand($baseImageWidth - $crackDifficulty, $baseImageWidth + $crackDifficulty);
        $newHeight = rand($baseImageHeight - $crackDifficulty, $baseImageHeight + $crackDifficulty) - $smallestOffset;
        if ($crackDifficulty < 1)
        {
            $crackDifficulty = 1;
        }
        if ($crackDifficulty > 10)
        {
            $crackDifficulty = 10;
        }
        $minimumContrast                     = 500 - ($crackDifficulty * 20);
        $letterColorCount                    = $crackDifficulty;
        $artifactColorCount                  = $crackDifficulty;
        $backgroundArtifectCount             = 10 * $crackDifficulty;
        $backgroundLineCount                 = 2 * $crackDifficulty;
        $foregroundArtifectCountPerCharacter = 2 * $crackDifficulty;

        $captcha = imagecreate($newWidth, $newHeight);
        $bgR     = rand(0, 255);
        $bgG     = rand(0, 255);
        $bgB     = rand(0, 255);
        $bgColor = imagecolorallocate($captcha, $bgR, $bgG, $bgB);

        $tryCt = 0;
        while (count($letterColors) < $letterColorCount)
        {
            $r = mt_rand(0, 255);
            $g = mt_rand(0, 255);
            $b = mt_rand(0, 255);

            if (\DblEj\Util\Colors::GetContrast($r, $g, $b, $bgR, $bgG, $bgB) > $minimumContrast)
            {
                $letterColors[] = imagecolorallocate($captcha, $r, $g, $b);
                $minimumContrast  = 500 - ($crackDifficulty * 20);
                $tryCt = 0;
            } else {
                $tryCt++;
            }
            if ($tryCt > 50 && $minimumContrast > 50)//only allow 50 attempts before reducing the contrast requirement
            {
                $tryCt = 0;
                $minimumContrast -= 20;
            }
        }
        for ($artifactColorIdx = 1; $artifactColorIdx <= $artifactColorCount; $artifactColorIdx++)
        {
            $artifactColors[] = imagecolorallocate($captcha, rand(0, 255), rand(0, 255), rand(0, 255));
        }

        imagefill($captcha, rand(0, $newWidth), rand(0, $newHeight), $bgColor);

        //background artifacts
        for ($artifactIdx = 0; $artifactIdx <= $backgroundLineCount; $artifactIdx++)
        {
            $color = current($artifactColors);
            if ($color === false)
            {
                reset($artifactColors);
                $color = current($artifactColors);
            }
            imageline($captcha, rand(0, $newWidth), rand(0, $newHeight), rand(0, $newWidth), rand(0, $newHeight), $color);
            next($artifactColors);
        }
        for ($artifactIdx = 0; $artifactIdx <= $backgroundArtifectCount; $artifactIdx++)
        {
            $color = current($artifactColors);
            if ($color === false)
            {
                reset($artifactColors);
                $color = current($artifactColors);
            }
            imagesetpixel($captcha, rand(0, $newWidth), rand(0, $newHeight), $color);
            next($artifactColors);
        }

        reset($fontPaths);
        reset($letterAngles);
        $outputWidth = 0;
        foreach ($letters as $letter)
        {
            $font        = current($fontPaths);
            $angle       = current($letterAngles);
            $letterColor = current($letterColors);

            if ($font === false)
            {
                reset($fontPaths);
                $font = current($fontPaths);
            }
            if ($angle === false)
            {
                reset($letterAngles);
                $angle = current($letterAngles);
            }
            if ($letterColor === false)
            {
                reset($letterColors);
                $letterColor = current($letterColors);
            }

            if (is_numeric($font))
            {
                $x  = (($newWidth / 2) - (ceil($stringWidth / 2))) + $outputWidth;
                imagestring($captcha, $font, $x, $newHeight/2 - $charHeight/2, $string, $letterColor);
                $charWidth = imagefontwidth($font);
                $charHeight = imagefontheight($font);
            }
            else
            {
                $meas      = imagettfbbox(32, $angle, $font, $letter);
                $x  = (($newWidth / 2) - (ceil($stringWidth / 2))) + $outputWidth;
                $charWidth = abs($meas[4] - $meas[0]);
                $charHeight = abs($meas[5] - $meas[1]);
                imagettftext($captcha, 32, $angle, $x, ($newHeight/2 + $charHeight/2 + max([$meas[5], $meas[7]])/2)+2, $letterColor, $font, $letter);
            }
            $outputWidth+=$charWidth;

            //foreground artifacts
            for ($artifactIdx = 0; $artifactIdx <= $foregroundArtifectCountPerCharacter; $artifactIdx++)
            {
                $artifactColor = current($artifactColors);
                if ($artifactColor === false)
                {
                    reset($artifactColors);
                    $artifactColor = current($artifactColors);
                }
                imagesetpixel($captcha, rand($x, $x + $charWidth), rand(0, $newHeight), $artifactColor);
                next($artifactColors);
            }

            next($fontPaths);
            next($letterAngles);
            next($letterColors);
        }
        header("Content-type: image/gif");
        header("Content-Transfer-Encoding: binary");
        \DblEj\Communication\Http\Util::DontAllowAnyCaching();
        header("Content-Disposition: inline; filename=\"captcha.gif\";");
        imagegif($captcha);
        imagedestroy($captcha);
    }

    public static function Authenticate($captcha, $autoReset = true)
    {
        $returnVal = false;
        if (strtolower($captcha) == strtolower(self::GetCurrentCaptcha()))
        {
            $returnVal = true;
        }
        if ($autoReset)
        {
            self::ResetCaptcha();
        }
        return $returnVal;
    }
}