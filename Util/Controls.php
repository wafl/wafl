<?php

namespace Wafl\Util;

use DblEj\Collections\TypedKeyedCollection,
    DblEj\Extension\IControl,
    ReflectionClass,
    Wafl\Syrup\Utils;

class Controls
{
    private static $_USED_CONTROLS         = null;
    public static $_PREREGISTERED_CONTROLS = array();

    public static function Initialize()
    {
        if (!self::$_USED_CONTROLS)
        {
            self::$_USED_CONTROLS = new TypedKeyedCollection(null, "\ReflectionClass");
        }
    }

    /**
     * Gets an instance of the specified control
     * @param string $controlName
     * @param string $controlsNamespace optional namespace that the Wafl Controls live in
     * @param string $namespace optional namespace/subfolder of this specific control (normall the same as the control name)
     * @return IControl
     */
    public static function GetControl($controlName, $controlsNamespace = "\\Wafl\\Controls", $namespace = null, $instanceId = null, $controlDirectory = null, $version = "0.0.1")
    {
        self::Initialize();
        if ($namespace == null)
        {
            $namespace = $controlName;
        }
        $returnControl = null;
        if ($controlDirectory)
        {
            $localControlsFolder = realpath($controlDirectory);
            if ($localControlsFolder === false)
            {
                throw new \Exception("Invalid controls folder specified: $controlDirectory");
            }
        }
        else
        {
            $localControlsFolder = realpath(\Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ControlsFolder());

        }
        $namespacePath = str_replace("\\", DIRECTORY_SEPARATOR, $namespace);

        $controlPath = $localControlsFolder . DIRECTORY_SEPARATOR . $namespacePath.DIRECTORY_SEPARATOR.$controlName.".php";
        //control isn't installed.  Try to find it in the public catalog.
        if (!file_exists($controlPath) && \Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Application()->Get_AutoDownloadExtensions())
        {
            $extensionUrl = "http://activewafl.com/Resources/Controls/$controlName/$controlName-$version.wctl";
            if (!file_exists($localControlsFolder))
            {
                mkdir($localControlsFolder, 0777, true);
            }
            if (!file_exists($localControlsFolder . DIRECTORY_SEPARATOR . $namespacePath))
            {
                mkdir($localControlsFolder . DIRECTORY_SEPARATOR . $namespacePath, 0775, true);
            }
            if (!file_exists($localControlsFolder . DIRECTORY_SEPARATOR . $namespacePath))
            {
                throw new \Exception("Cant create controls folder: ".$localControlsFolder . DIRECTORY_SEPARATOR . $namespacePath);
            }
            $destinationZipFile = sys_get_temp_dir().DIRECTORY_SEPARATOR.$controlName."-$version.wctl";
            if (file_exists($destinationZipFile))
            {
                unlink($destinationZipFile);
            }
            \DblEj\Communication\Http\Util::DownloadFile($extensionUrl, $destinationZipFile);
            if (file_exists($destinationZipFile))
            {
                $destinationZip = new \DblEj\Util\ZipArchive();
                $destinationZip->open($destinationZipFile);
                $destinationZip->extractTo($localControlsFolder . DIRECTORY_SEPARATOR . $namespacePath);
                $destinationZip->close();
                unlink($destinationZipFile);
            } else {
                throw new \Exception("Cannot get $namespacePath from auto update");
            }
        }

        if (file_exists($controlPath))
        {
            require_once($controlPath);
            $classInfo        = new ReflectionClass("$controlsNamespace\\$namespace\\$controlName");
            $templateRenderer = \Wafl\Util\Extensions::GetExtensionByType("\\DblEj\\Presentation\\Integration\\ITemplateRendererExtension");
            if ($templateRenderer === null)
            {
                throw new \Exception("Control can't be instantiated because there is no template renderer extension");
            }
            $returnControl    = $classInfo->newInstance($templateRenderer, $instanceId);
            self::$_USED_CONTROLS->AddItem($classInfo, $controlName);
        }
        return $returnControl;
    }

    public static function PreregisterControl($controlName, $namespace = "\\Wafl\\Controls", $minifyOutput = null, $version = "0.0.1")
    {
        self::Initialize();
        self::$_PREREGISTERED_CONTROLS[$controlName] = array(
            $controlName,
            $namespace,
            $minifyOutput,
            $version);
    }

    public static function PreRegisterControlsFromSyrup($controlsFile)
    {
        if (AM_WEBPAGE)
        {
            if (file_exists($controlsFile))
            {
                $controls = \Wafl\AppSupport\Application::ParseSyrupFileAsArray($controlsFile);
            }
            else
            {
                $controls = array();
            }
            foreach ($controls["PreRegisterControls"] as $controlId=>$control)
            {
                if (is_array($control)) //The control might have sub items which are options
                {
                    $controlName = $controlId;
                    $minify = isset($control["Minify"])?$control["Minify"]:null;
                    $namespace = isset($control["Namespace"])?$control["Namespace"]:"\\Wafl\\Controls";
                    $version = isset($control["Version"])?$control["Version"]:"0.0.1";
                } else {
                    $controlName = $control;
                    $minify = null;
                    $namespace = "\\Wafl\\Controls";
                    $version = "0.0.1";
                }
                self::PreregisterControl($controlName, $namespace, $minify, $version);
            }
        }
    }

    public static function GetPreregisteredControl($controlName)
    {
        self::Initialize();
        return isset(self::$_PREREGISTERED_CONTROLS[$controlName]) ? self::$_PREREGISTERED_CONTROLS[$controlName] : null;
    }

    public static function GetPreregisteredControls()
    {
        self::Initialize();
        return self::$_PREREGISTERED_CONTROLS;
    }

    public static function GetUsedControls()
    {
        return self::$_USED_CONTROLS;
    }

    public static function ClearUsedControls()
    {
        self::$_USED_CONTROLS = new TypedKeyedCollection(null, "\\ReflectionClass");
    }
}