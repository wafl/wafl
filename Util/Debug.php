<?php
namespace Wafl\Util;

class Debug
{
    private static $_LAST_SUPRESSED_ERROR;

    public static function Get_LastSuppressedError()
    {
        return self::$_LAST_SUPRESSED_ERROR;
    }

    public static function Set_LastSuppressedError($lastErrorString)
    {
        self::$_LAST_SUPRESSED_ERROR = $lastErrorString;
    }

    /**
     * Disables all of Wafl's error handlers and shutdown handlers
     * and sets up the environment to allow for you to just output text
     * and not have the framework complain about it.
     *
     * @param string $debugString The text you want to output
     */
    public static function PrintDebugAndExit($debugString)
    {
        if (!defined("EXCEPTION_OUTPUT"))
        {
            restore_error_handler();
            restore_exception_handler();
            define("EXCEPTION_OUTPUT", true);
            print $debugString;
            die();
        }
    }
}