<?php

namespace Wafl\Util;

use DblEj\EventHandling\EventInfo;
use DblEj\Extension\ExtensionException;
use DblEj\Extension\Dependency;
use DblEj\Extension\ExtensionBase;
use DblEj\Extension\IExtension;
use DblEj\Extension\MissingDependencyException;
use DblEj\SiteStructure\SiteArea;
use DblEj\Util\SystemEvents;
use ReflectionClass;
use Wafl\Core;

class Extensions
{
    private static $_LOADED_EXTENSIONS = array();
    private static $_ALL_EXTENSIONS = array();

    public static function InitializeExtensionsFromSyrup(\DblEj\Application\IApplication $app, $syrupFile, $callback = null)
    {
        if (file_exists($syrupFile))
        {
            $extensions = \Wafl\Util\Syrup::ParseSyrupFileAsArray($syrupFile);
        }
        else
        {
            $extensions = array();
        }
        foreach ($extensions as $extensionSyrpKey => $extensionSyrpVal)
        {
            if (!is_numeric($extensionSyrpKey) && is_array($extensionSyrpVal))
            {
                $extenionName = $extensionSyrpKey;
                $extensionProperties = $extensionSyrpVal;
            } else {
                $extenionName = $extensionSyrpVal;
                $extensionProperties = [];
            }
            if (isset($extensionProperties["Version"]))
            {
                $version = $extensionProperties["Version"];
            } else {
                $version = "0.0.1";
            }
            $extension = Extensions::AddExtension($extenionName, null, null, $version);
            if ($extension)
            {
                foreach ($extensionProperties as $extensionProperty => $propertyValue)
                {
                    if (is_string($propertyValue) && substr($propertyValue, 0, 1) == "{")
                    {
                        //$propValue = ("return " . substr($propertyValue, 1, strlen($propertyValue) - 2) . ";"); //todo need to define what dynamic vars/constanbt can be used and find nicer way to evaluate
                        $propValue = (eval("return " . substr($propertyValue, 1, strlen($propertyValue) - 2) . ";")); //todo need to define what dynamic vars/constanbt can be used and find nicer way to evaluate
                    }
                    else
                    {
                        $propValue = $propertyValue; //todo need to define what dynamic vars/constanbt can be used and find nicer way to evaluate
                    }
                    if ($propValue === null || $propValue === "")
                    {
                        $propValue = $extension->GetSettingDefault($extensionProperty);
                    }
                    if (is_scalar($propValue))
                    {
                        if (strtolower($propValue) == "true")
                        {
                            $propValue = true;
                        }
                        elseif (strtolower($propValue) == "false")
                        {
                            $propValue = false;
                        }
                        elseif (is_numeric($propValue))
                        {
                            $propValue = floatval($propValue);
                        }
                    }
                    $extension->Configure($extensionProperty, $propValue);
                }
            }
        }

        $extensions = Extensions::GetExtensions();

        //check extension dependencies
        foreach ($extensions as $extension)
        {
            $extClass = get_class($extension);
            foreach ($extClass::Get_Dependencies() as $dependency)
            {
                if ($dependency->Get_DependsOnType() == Dependency::TYPE_CONTROL)
                {
                    $depControl = Controls::GetPreregisteredControl($dependency->Get_DependsOn());
                    if (!$depControl)
                    {
                        throw new MissingDependencyException($dependency);
                    }
                }
                elseif ($dependency->Get_DependsOnType() == Dependency::TYPE_EXTENSION)
                {
                    $depExt = Extensions::GetExtension($dependency->Get_DependsOn());
                    if (!$depExt)
                    {
                        throw new MissingDependencyException($dependency);
                    }
                }
                elseif ($dependency->Get_DependsOnType() == Dependency::TYPE_EXTENSION_INTERFACE)
                {
                    $depExt = Extensions::GetExtensionByType($dependency->Get_DependsOn());
                    if (!$depExt)
                    {
                        throw new MissingDependencyException($dependency);
                    }
                }
            }
        }
        //run extension installation scripts where necessary
        foreach ($extensions as $extension)
        {
            if ($extension->Get_RequiresInstallation() === true)
            {
                $classInfo = new ReflectionClass($extension);
                $className = $classInfo->getName();
                $classNameShort = $classInfo->getShortName();
                $className::InstallData(Core::$STORAGE_ENGINE);
                Core::$STORAGE_ENGINE->UpdateStorageLocations();
            }
            $extension->Initialize($app);
            if ($callback && is_callable($callback))
            {
                $callback($extension);
            }
        }
    }

    public static function RegisterExtensionSitePages(\DblEj\Application\IApplication $app)
    {
        //register extension Site pages
        foreach (Extensions::GetExtensions() as $extension)
        {
            $extClassInfo = new ReflectionClass($extension);
            if (AM_WEBPAGE)
            {
                if ($extension->Get_SiteAreaId())
                {
                    $siteArea = $app->Get_SiteMap()->GetSiteArea($extension->Get_SiteAreaId());
                    if ($siteArea)
                    {
                        foreach ($extension->Get_SitePages() as $pageKey => $page)
                        {
                            $fullName = substr($extClassInfo->getName(), 16);
                            $fullName = substr($fullName, 0, strlen($fullName) - strlen($extClassInfo->getShortName()));
                            if (\DblEj\Util\Strings::EndsWith($fullName, "\\"))
                            {
                                $fullName = substr($fullName, 0, strlen($fullName)-1);
                            }
                            Extensions::AddExtensionPageToSiteArea($app, $fullName, $pageKey, $page->Get_DisplayOrder(), $siteArea);
                        }
                    }
                }
            }
        }
        SystemEvents::RaiseSystemEvent(new EventInfo(SystemEvents::AFTER_EXTENSION_INSTALL));
    }

    final public static function GetExtensionPageLink($extensionName, $pagename)
    {
        return "Extensions/$extensionName/$pagename";
    }

    final public static function FindExtensionByPath($path)
    {
        $path = str_replace("\\", DIRECTORY_SEPARATOR, $path);
        $path = str_replace("/", DIRECTORY_SEPARATOR, $path);

        if (substr($path, 0, 10) == "Extensions")
        {
            $path = substr($path, 11);
        }
        elseif (substr($path, 0, 11) == DIRECTORY_SEPARATOR."Extensions")
        {
            $path = substr($path, 12);
        }
        if (substr($path, 0, 1) == DIRECTORY_SEPARATOR)
        {
            $path = substr($path, 1);
        }
        $classpath = str_replace("/", "\\", $path);
        $classpath = "Wafl\\Extensions\\$classpath";
        $foundExtension = null;
        foreach (self::$_LOADED_EXTENSIONS as $extension)
        {
            $extensionClassName = get_class($extension);
            $extensionClassPath = str_replace("\\", DIRECTORY_SEPARATOR, $extensionClassName); //this allows us to use dirname on the classpath
            $extensionClassName = str_replace(DIRECTORY_SEPARATOR, "\\", dirname($extensionClassPath)) . "\\";
            $classnameLength = strlen($extensionClassName);
            if (substr($classpath, 0, $classnameLength) == $extensionClassName)
            {
                $foundExtension = $extension;
                break;
            }
        }
        return $foundExtension;
    }

    final public static function AddExtensionPageToSiteArea(\DblEj\Application\IMvcWebApplication $app, $extensionName, $pagename, $displayOrder, SiteArea $siteArea)
    {
        $ext = Extensions::GetExtension(basename(str_replace("\\", "/", $extensionName)));
        if ($ext)
        {
            $extClass       = new ReflectionClass($ext);
            $extClassName   = $extClass->getName();
            $extensionPages = $extClassName::Get_SitePages();

            $extPage = $extensionPages[$pagename];
            if ($extPage)
            {
                $extFileName = self::GetExtensionPageLink($extensionName, $pagename);
                $app->RegisterSitePage($extPage->Get_Title(), $extFileName, $extPage->Get_TemplateName(), $displayOrder, $siteArea, false, $extPage->Get_Description(), "", "", "", false, "Extension: $extensionName");
            }
            else
            {
                throw new ExtensionException("Trying to register a non-existent extension site page: $extensionName", 0, E_WARNING, __FILE__, __LINE__);
            }
        }
        else
        {
            throw new ExtensionException("Trying to register a site page for non-existent extension", 0, E_WARNING, __FILE__, __LINE__);
        }
    }

    /**
     * @param string $title The guid name of the extension
     * @return IExtension
     */
    public static function AddExtension($fqTitle, $extensionsNamespace = null, $extensionsFolder = null, $version = null)
    {
        $localExtensionFolder = null;
        if ($extensionsFolder)
        {
            $localExtensionFolder = realpath($extensionsFolder);
        }
        if (!$localExtensionFolder)
        {
            $app = \Wafl\Core::$RUNNING_APPLICATION;
            $localExtensionFolder = realpath($app->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder());
        }
        if (!$extensionsNamespace)
        {
            $extensionsNamespace = "\\Wafl\\Extensions";
        }
        if (!$version)
        {
            $version = "0.0.1";
        }
        $titleArray                = explode("\\", $fqTitle);
        $title                     = $titleArray[count($titleArray) - 1];
        $titleArray                = array_slice($titleArray, 0, count($titleArray) - 1);
        $titlePath                 = implode(DIRECTORY_SEPARATOR, $titleArray);
        $extensionFolder           = $localExtensionFolder . DIRECTORY_SEPARATOR . $titlePath . DIRECTORY_SEPARATOR . $title;
        $extensionPath             = $extensionFolder . DIRECTORY_SEPARATOR . "$title.php";

        //extension isn't installed.  Try to find it in the public catalog.
        if (!file_exists($extensionPath) && \Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Application()->Get_AutoDownloadExtensions())
        {
            $extensionUrl = "http://activewafl.com/Resources/Extensions/".str_replace("\\", "/", $fqTitle)."/$title-$version.wext";
            if (!file_exists($extensionFolder))
            {
                if (!@mkdir($extensionFolder, 0775, true))
                {
                    throw new \Exception("Cannot install extension: $title-$version because I cannot create the folder (probably because of permissions): $extensionFolder");
                } else {
                    chmod($extensionFolder, 0775);
                }
            }
            $destinationZipFile = sys_get_temp_dir().DIRECTORY_SEPARATOR.$title."-$version.wext";
            if (file_exists($destinationZipFile))
            {
                unlink($destinationZipFile);
            }
            \DblEj\Communication\Http\Util::DownloadFile($extensionUrl, $destinationZipFile);
            if (file_exists($destinationZipFile))
            {
                $destinationZip = new \DblEj\Util\ZipArchive();
                $destinationZip->open($destinationZipFile);
                $destinationZip->extractTo($extensionFolder);
                $destinationZip->close();
                unlink($destinationZipFile);
                $files = \DblEj\Util\Folder::GetAllChildObjects($extensionFolder, true);
                foreach ($files as $file)
                {
                    chmod($file, 0775);
                }
            } else {
                throw new \Exception("Cannot get $extensionPath from auto update at $extensionUrl");
            }
        }
        if (file_exists($extensionPath))
        {
            require_once($extensionPath);
        } else {
            throw new \Exception("Cannot find the specified extension file: $extensionPath");
        }
        $classInfo                 = new ReflectionClass("$extensionsNamespace\\$fqTitle\\$title");
        $extensionInstance = $classInfo->newInstance();
        self::$_ALL_EXTENSIONS[$title] = $extensionInstance;
        if (!$extensionInstance->Get_WebOnly() || AM_WEBPAGE)
        {
            self::$_LOADED_EXTENSIONS[$title] = $extensionInstance;
        } else {
            $extensionInstance = null;
        }
        return $extensionInstance;
    }

    /**
     * @param string $extensionTitle
     * @return IExtension
     */
    public static function GetExtension($extensionTitle)
    {
        if (!isset(self::$_LOADED_EXTENSIONS[$extensionTitle]))
        {
            throw new \Exception("the extension $extensionTitle cannot be found.  The enabled Extensions are ".  implode(",", array_keys(self::$_LOADED_EXTENSIONS)));
        }
        return self::$_LOADED_EXTENSIONS[$extensionTitle];
    }

    /**
     *
     * @param string $type
     * @return ExtensionBase
     */
    public static function GetExtensionByType($typeName)
    {
        $returnExtension = null;
        foreach (self::$_LOADED_EXTENSIONS as $extension)
        {
            if (is_a($extension, $typeName))
            {
                $returnExtension = $extension;
                break;
            }
        }
        return $returnExtension;
    }

    /**
     * Get all loaded extensions that match the given type
     * @param string $typeName
     * @return ExtensionBase[]
     */
    public static function GetExtensionsByType($typeName)
    {
        $returnExtensions = [];
        foreach (self::$_LOADED_EXTENSIONS as $extension)
        {
            if (is_a($extension, $typeName))
            {
                $returnExtensions[] = $extension;
            }
        }
        return $returnExtensions;
    }

    public static function FindExtensionClassByType($type, $namespace = "\\Wafl\\Extensions")
    {
        $returnExtension      = null;
        $localExtensionFolder = realpath(\Wafl\Core::$RUNNING_APPLICATION->Get_Settings()->Get_Paths()->Get_Wafl()->Get_ExtensionsFolder()) . DIRECTORY_SEPARATOR;
        $folders              = \DblEj\Util\Folder::GetAllChildObjects($localExtensionFolder);
        foreach ($folders as $folder)
        {
            try
            {
                if (DblEj\Util\Strings::StartsWith($type, "\\") || DblEj\Util\Strings::StartsWith($type, "/"))
                {
                    $type = substr($type, 1);
                }
                $folderName = basename($folder);
                $classPath  = "$namespace\\$folderName\\$folderName";
                $interfaces = class_implements($classPath);
                $subClasses = class_parents($classPath);
                if (in_array($type, $interfaces))
                {
                    $returnExtension = $classPath;
                }
                elseif (in_array($type, $subClasses))
                {
                    $returnExtension = $classPath;
                }
            }
            catch (\Exception $ex)
            {
                //try the next one
            }
        }
        return $returnExtension;
    }

    public static function GetExtensionByChildMethod($childMethodName)
    {
        $returnExtension = null;
        foreach (self::$_LOADED_EXTENSIONS as $extension)
        {
            if (method_exists($extension, $childMethodName))
            {
                $returnExtension = $extension;
                break;
            }
        }
        return $returnExtension;
    }

    public static function IsTableInstalledByExtension($tableName)
    {
        $isInstalled = false;
        foreach (self::$_ALL_EXTENSIONS as $extension)
        {
            if (array_search($tableName, $extension->Get_DatabaseInstalledTables()) !== false)
            {
                $isInstalled = true;
                break;
            }
        }
        return $isInstalled;
    }

    public static function GetExtensions()
    {
        return self::$_LOADED_EXTENSIONS;
    }
}