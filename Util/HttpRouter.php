<?php

namespace Wafl\Util;

use DblEj\Application\IApplication,
    DblEj\Communication\Http\Request,
    DblEj\Communication\Http\Response,
    DblEj\Communication\Http\Routing\IRouter,
    DblEj\Communication\Http\Util,
    DblEj\Communication\Http\Routing\IRoute,
    Exception,
    Wafl\Core,
    Wafl\Routers\HttpRouterChain,
    Wafl\Routers\RouterChain,
    Wafl\Routers\RoutingException;

/**
 * Handles the high-level routing of all http requests
 *
 * @since 770
 */
class HttpRouter
{
    /**
     *
     * @var RouterChain
     */
    private static $_inputRouterChain;

    private static $_urlRewriters;

    public static function Initialize()
    {
        if (self::$_inputRouterChain == null)
        {
            self::$_inputRouterChain = new HttpRouterChain();
        }
        if (self::$_urlRewriters == null)
        {
            self::$_urlRewriters = [];
        }
    }

    /**
     * Add a router to the input router chain.
     *
     * @param IRouter $router
     */
    public static function AddRouter(IRouter $router)
    {
        self::Initialize();
        self::$_inputRouterChain->AddRouter($router);
    }

    public static function AddUrlRewriter(\DblEj\Communication\Http\Routing\IUrlRewriter $urlReWriter)
    {
        self::Initialize();
        self::$_urlRewriters[] = $urlReWriter;
    }
    public static function RouteThruNonAppCalls($requestUri, IRouter &$routerUsed = null, &$rawOutput = "", &$outputHash = null, $requestMethod = null)
    {
        if (!$requestMethod)
        {
            $requestMethod = Util::GetInput("REQUEST_METHOD", \DblEj\Communication\Http\Request::INPUT_SERVER); //@todo do all http servers use this header?
        }
        if (!$requestMethod)
        {
            $requestMethod = "GET";
        }

        self::Initialize();
        $route = self::FromClientToServer($requestUri, null, $routerUsed, $requestMethod);
        if ($route)
        {
            $response = $route->GotoRoute(null);
            $outputHash = $response->Get_Etag();
            self::FromServerToClient($response, null, $rawOutput);
            Core::Set_RequestPending(false);
        }
    }

    /**
     * Routes HTTP Request to the appropriate handler and send the response back to the client
     *
     * @param string $requestUri
     * @param IApplication $app
     * @param IRouter $routerUsed
     * @return IRoute
     */
    public static function FromClientToServer($requestUri, IApplication $app = null, IRouter &$routerUsed = null, $requestMethod = null)
    {
        self::Initialize();

        if ($app)
        {
            //remove everything but the path and the request vars (@todo why not use native uri parser here?
            if (\DblEj\Util\Strings::StartsWith($requestUri, $app->Get_Settings()->Get_Web()->Get_DomainName()))
            {
                $domainLength = strlen($app->Get_Settings()->Get_Web()->Get_DomainName());
                $requestUri   = substr($requestUri, $domainLength);
            }
            if (trim($requestUri) == "")
            {
                $requestUri = "/";
            }
        }
        else
        {
            $requestUriParts = explode("/", parse_url($requestUri, PHP_URL_PATH));
            $requestUri      = implode("/", $requestUriParts); // "/".end($requestUriParts);
        }
        $rerouted = false;
        foreach (self::$_urlRewriters as $urlRewriter)
        {
            $reroutedUri = $urlRewriter->RewriteUrl($requestUri);
            if ($reroutedUri != $requestUri)
            {
                $requestUri = $reroutedUri;
                $rerouted = true;
                break;
            }
        }

        switch (strtoupper($requestMethod))
        {
            case "GET":
                $httpRequest = new Request($requestUri, Request::HTTP_GET);
                break;
            case "POST":
                $httpRequest = new Request($requestUri, Request::HTTP_POST);
                break;
            case "PUT":
                $httpRequest = new Request($requestUri, Request::HTTP_PUT);
                break;
            case "DELETE":
                $httpRequest = new Request($requestUri, Request::HTTP_DELETE);
                break;
            default:
                $httpRequest = new Request($requestUri, Request::HTTP_GET);
                break;
        }
        if ($rerouted && stristr($requestUri, "?"))
        {
            $getVarsString = substr($requestUri, stripos($requestUri, "?") + 1);
            $getVarsString = str_replace("&amp;", "&", $getVarsString);
            $getVarsStringPairs = explode("&", $getVarsString);
            foreach ($getVarsStringPairs as $getVarsStringPair)
            {
                $getVarPair = explode("=", $getVarsStringPair);
                if ($getVarPair && count($getVarPair) == 2)
                {
                    $httpRequest->SetInput($getVarPair[0], $getVarPair[1]);
                }
            }
        }
        $route = self::$_inputRouterChain->GetRoute($httpRequest, $app, $routerUsed);

        return $route;
    }

    public static function SendResponseHeaders(\DblEj\Communication\Http\Headers $headersObject, $responseCode = Response::HTTP_OK_200, $responseLength = null, IApplication $app = null)
    {
        $success = false;
        self::Initialize();
        try
        {
            self::_cleanObAndCloseAllObHandlers();

            http_response_code($responseCode);
            foreach ($headersObject->GetHeaders() as $headerValue)
            {
                header($headerValue);
            }

            //if the web server is not already handling it, then lets compress the output
            //this obviously doesnt cover all web servers... if we are not sure then assume server is handling it
            if ($responseCode != Response::HTTP_PARTIAL_206) //dont gzip partial content responses to "range" requests
            {
                if (function_exists("apache_get_modules"))
                {
                    $apacheMods = apache_get_modules();
                    $serverHasModDeflate = array_search("mod_deflate", $apacheMods) !== false;
                } elseif (file_exists("/etc/apache2/mods-enabled")) {
                    $apacheMods = \DblEj\Util\Folder::GetAllChildObjects("/etc/apache2/mods-enabled");
                    $serverHasModDeflate = array_search("mod_deflate", $apacheMods) !== false;
                    if (!$serverHasModDeflate)
                    {
                        foreach ($apacheMods as $apacheMod)
                        {
                            if (\DblEj\Util\Strings::EndsWith($apacheMod, "deflate.load"))
                            {
                                $serverHasModDeflate = true;
                                break;
                            }
                        }
                    }
                } else {
                   $serverHasModDeflate = true; //if we are not sure, assume server is handling it
                }

                if (!$serverHasModDeflate)
                {
                    Util::ZipOutputIfBrowserSupported();
                }
            }
            elseif ($responseLength && ($responseCode != Response::HTTP_FILE_NOT_CHANGED_304))
            {
                header("Content-Length: $responseLength");
            }
            $success = true;
        }
        catch (\Exception $ex)
        {
            $message = $ex->getMessage();
            $inner = $ex;
            while ($inner = $inner->getPrevious())
            {
                $message = $inner->getMessage() . ", ".$message;
            }
            throw new \Wafl\Exceptions\Exception("There was an error while preparing the response for the request: ".$message, E_ERROR, $ex, "Internal communication error");
        }
        return $success;
    }


    private static function _cleanObAndCloseAllObHandlers()
    {
            while (ob_get_level() > 0)
            {
                if (ob_get_length() > 0)
                {
                    error_log("There is unexpected output (length: "+ob_get_length()+") waiting in the output buffers.  This content will be deleted and not be shown.");
                }
                ob_end_clean();
            }
    }
    public static function FromServerToClient(Response &$response, IApplication $app = null, &$rawResult = "")
    {
        self::Initialize();

        $success = false;
        try
        {
            if (!$response->Get_HasBeenSent()) //@todo why would this even be an issue unless there was shitty code somehwere?
            {
                if (!$response->Get_HeadersSent())
                {
                    self::SendResponseHeaders($response->Get_Headers(), $response->Get_ResponseCode(), null, $app);
                }

                if ($response->Get_ResponseCode() != Response::HTTP_FILE_NOT_CHANGED_304)
                {
                    $rawResult = "";

                    switch ($response->Get_ContentType())
                    {
                        case Response::CONTENT_TYPE_PHP_SCRIPT:
                            ob_start();
                            include($response->Get_Content());
                            $rawResult = ob_get_clean();
                            break;
                        case Response::CONTENT_TYPE_FILE:
                            $rawResult = "STREAM_OUT_FILE";
                            $response->Set_ContentIsSymbolic(true);
                            break;
                        case Response::CONTENT_TYPE_RENDERABLE_OUTPUT:
                            $renderable    = $response->Get_Content();
                            $renderable->Set_TokenData($response->Get_SitePage()->Get_TokenData()); //not all responses have sitepages/models.  this needs to be smarter
                            $rawResult = $renderable->Render($app->Get_TemplateRenderer(), $response->Get_Options());
                            break;
                        default:
                            $rawResult = $response->Get_Content();
                            break;
                    }

                    if (ob_get_level() > 0)
                    {
                        print ($rawResult);
                        $bufferContents = "";
                        $bufferSize     = 0;

                        $bufferSize = ob_get_length();
                        $bufferContents = ob_get_clean();
                        $rawResult = $bufferContents;
                    }

                    $outputStartedFile = null;
                    $outputStartedLine = null;

                    if ($rawResult == "STREAM_OUT_FILE")
                    {
                        $fileNamesString = $response->Get_Content();
                        $filenames = explode(";", $fileNamesString);
                        if (count($filenames) > 1)
                        {
                            $response->Set_RawResponse("Filestream: ".print_r($filenames, true));
                        } else {
                            $response->Set_RawResponse("Filestream: $fileNamesString");
                        }
                        foreach ($filenames as $filename)
                        {
                            //usually, we'll have sent the headers by now except in cases where we explicitly don't like in a 206 partial or file download where we want to send a content-length.
                            if (count($filenames) == 1 && !headers_sent($outputStartedFile, $outputStartedLine) && file_exists($filename))
                            {
                                $filesize = filesize($filename);
                                header("Content-Length: $filesize");
                            }
                            readfile($filename);
                        }
                    } else {
                        $response->Set_RawResponse($rawResult);
                        //usually, we'll have sent the headers by now except in cases where we explicitly don't like in a 206 partial or file download where we want to send a content-length.
                        if (!headers_sent($outputStartedFile, $outputStartedLine))
                        {
                            $filesize = strlen($rawResult);
                            header("Content-Length: $filesize");
                        }
                        print ($rawResult);
                    }
                }
                $success = true;
                $response->Set_HeadersSent(true);
                $response->Set_HasBeenSent($success);
                Core::Set_HasOutputBeenSent(true);
            }
        }
        catch (\Exception $ex)
        {
            $message = $ex->getMessage();
            $inner = $ex;
            while ($inner = $inner->getPrevious())
            {
                $message = $inner->getMessage() . ", ".$message;
            }
            throw new RoutingException("There was an error while routing the request: ".$message, E_ERROR, $ex);
        }
        return $success;
    }
}