<?php

namespace Wafl\Util
{
    class Phar
    {

        public static function CreatePhar($appName, $sourceFolder, $destFolder, $entryPointFile = null, $externalFiles = array(), $outputTracing = false, $compress = false, $overwriteExisting = true, $ignoreVcs = true, $ignoreNonPhp = false)
        {
            $phar = null;
            try
            {
                ini_set("phar.readonly", "Off");
                $pharFilename = $destFolder . "$appName.phar";
                if (file_exists($pharFilename) && $overwriteExisting)
                {
                    unlink($pharFilename);
                }
                $phar       = new \Phar($pharFilename, \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::KEY_AS_FILENAME, "$appName.phar");
                $files      = \DblEj\Util\Folder::GetAllChildObjects($sourceFolder, true);
                $filesAdded = 0;
                foreach ($files as $file)
                {
                    if (!$ignoreVcs ||
                    (!strstr($file, ".git") && !strstr($file, ".hg") && !strstr($file, ".svn")))
                    {
                        if (is_file($file) && (!$ignoreNonPhp || \DblEj\Util\Strings::EndsWith($file, ".php")))
                        {
                            if (substr($file, 0, strlen($sourceFolder)) == $sourceFolder)
                            {
                                $relativeFile = substr($file, strlen($sourceFolder));
                            }
                            else
                            {
                                $relativeFile = $file;
                            }
                            $relativeFile = str_replace($sourceFolder, "", $file);
                            if ($outputTracing)
                            {
                                print "\tAdding file\n$relativeFile...\n";
                            }
                            $phar[$relativeFile] = \file_get_contents($file);
                            $filesAdded++;
                        }
                    }
                }
                if ($entryPointFile)
                {
                    if ($outputTracing)
                    {
                        print "\tSetting entry point to $entryPointFile...";
                    }
                    $phar->setStub($phar->createDefaultStub($entryPointFile));
                    if ($outputTracing)
                    {
                        print "done\n";
                    }
                }
                else
                {
                    if ($outputTracing)
                    {
                        print "\tSetting default entry point...";
                    }
                    $phar->setStub("<?php __HALT_COMPILER();");
                    if ($outputTracing)
                    {
                        print "done\n";
                    }
                }

                if ($outputTracing)
                {
                    print "\tCopying external files...";
                }
                foreach ($externalFiles as $externalFile)
                {
                    \copy($sourceFolder . $externalFile, $destFolder . $externalFile);
                }
                if ($outputTracing)
                {
                    print "done\n\n$filesAdded Files Added to $pharFilename";
                }
                if ($compress)
                {
                    if ($outputTracing)
                    {
                        print "\tCompressing Phar...";
                    }
                    $phar->compress(\Phar::GZ);
                    if ($outputTracing)
                    {
                        print "done\n";
                    }
                }
            }
            catch (\Exception $ex)
            {
                throw new \Exception("There was an unexpected error while generating the Phar file", $ex->getCode(), $ex);
            }
            return $phar;
        }
    }
}