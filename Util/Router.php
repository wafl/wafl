<?php
namespace Wafl\Util;

use DblEj\Application\IApplication,
    DblEj\Communication\IRoute,
    DblEj\Communication\IRouter,
    DblEj\Communication\Request,
    DblEj\Communication\Response,
    Exception,
    Wafl\Core,
    Wafl\Routers\RouterChain,
    Wafl\Routers\RoutingException;

/**
 * Handles the high-level routing of all requests
 *
 * @since 1405
 */
class Router
{
    /**
     *
     * @var RouterChain
     */
    private static $_inputRouterChain;

    public static function Initialize()
    {
        if (self::$_inputRouterChain == null)
        {
            self::$_inputRouterChain = new RouterChain();
            if (!defined("AM_WEBPAGE"))
            {
                define('AM_WEBPAGE', false);
            }
        }
    }

    public static function AddRouter(IRouter $router)
    {
        self::Initialize();
        self::$_inputRouterChain->AddRouter($router);
    }

    /**
     * Routes Request to the appropriate handler and send the response back to the client
     *
     * @param string $requestString
     * @param IApplication $app
     * @param IRouter $routerUsed
     * @return IRoute
     */
    public static function FromClientToServer($requestString, IApplication $app = null, IRouter &$routerUsed = null, $requestMethod = null)
    {
        self::Initialize();
        $request = new Request($requestString);
        $route   = self::$_inputRouterChain->GetRoute($request, $app, $routerUsed);
        if (!$route)
        {
            throw new \Exception("No route was found for the given request.");
        }
        return $route;
    }

    public static function FromServerToClient(Response &$response, IApplication $app = null)
    {
        self::Initialize();
        $success = false;
        try
        {
            if (!$response->Get_HasBeenSent()) //@todo why would this even be an issue unless there was shitty code somehwere?
            {
                if (ob_get_level() > 0)
                {
                    print ($response->Get_Response());
                    $bufferContents = "";
                    while (ob_get_level() > 0)
                    {
                        $bufferContents .= ob_get_clean();
                    }
                    print $bufferContents;
                }
                else
                {
                    print $response->Get_Response();
                }
                $success = true;
                $response->Set_HasBeenSent($success);
                Core::Set_HasOutputBeenSent(true);
            }
        }
        catch (\Exception $ex)
        {
            throw new RoutingException("There was an error while routing the request", E_ERROR, $ex);
        }
        return $success;
    }
}