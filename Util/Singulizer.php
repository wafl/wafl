<?php

namespace Wafl\Util;

class Singulizer
{

    public static function Singulize($pluarlString)
    {
        $singularString = $pluarlString;
        $length         = strlen($pluarlString);
        if (substr($pluarlString, $length - 3) == "ies")
        {
            $singularString = substr($pluarlString, 0, $length - 3) . "y";
        }
        elseif (($length > 5) && (substr($pluarlString, $length - 4) == "sses"))
        {
            $singularString = substr($pluarlString, 0, $length - 2);
        }
        elseif (substr($pluarlString, $length - 2) == "es")
        {
            $singularString = substr($pluarlString, 0, $length - 1);
        }
        elseif (substr($pluarlString, $length - 2) == "ss")
        {
            $singularString = $pluarlString;
        }
        elseif (substr($pluarlString, $length - 1) == "s")
        {
            $singularString = substr($pluarlString, 0, $length - 1);
        }
        else
        {
            $singularString = $pluarlString;
        }
        return $singularString;
    }

    public static function Pluarlize($singularString)
    {
        $length       = strlen($singularString);
        if (substr($singularString, $length - 1) == "y")
        {
            $pluarlString = substr($singularString, 0, $length - 1) . "ies";
        }
        elseif (substr($singularString, $length - 1) == "s")
        {
            $pluarlString = $singularString . "es";
        }
        elseif (substr($singularString, $length - 1) == "z")
        {
            $pluarlString = $singularString . "es";
        }
        elseif (substr($singularString, $length - 1) == "x")
        {
            $pluarlString = $singularString . "es";
        }
        else
        {
            $pluarlString = $singularString . "s";
        }
        return $pluarlString;
    }
}