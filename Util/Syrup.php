<?php

namespace Wafl\Util;

class Syrup
{
    private static $_syrupParser;

    public static function Initialize($syrupParser)
    {
        self::$_syrupParser = $syrupParser;
    }

    public static function ParseSyrupFile($syrupFile)
    {
        $parser = self::$_syrupParser;
        return $parser::ParseFile($syrupFile);
    }


    public static function InvalidateSyrupCache($syrupFile)
    {
        $cacheFolder = sys_get_temp_dir().DIRECTORY_SEPARATOR."SyrupCache";
        $cacheFile = $cacheFolder.DIRECTORY_SEPARATOR.md5($syrupFile).".syrupcache";
        if (file_exists($cacheFile))
        {
            unlink($cacheFile);
        }
    }

    public static function PurgeSyrupCache()
    {
        $cacheFolder = sys_get_temp_dir().DIRECTORY_SEPARATOR."SyrupCache";
        if (file_exists($cacheFolder))
        {
            unlink($cacheFolder);
        }
    }

    public static function ParseSyrupFileAsArray($syrupFile, $useCache = true)
    {
        if ($useCache && is_writable(sys_get_temp_dir()))
        {
            $cacheFolder = sys_get_temp_dir().DIRECTORY_SEPARATOR."SyrupCache";
            if (!file_exists($cacheFolder))
            {
                mkdir($cacheFolder, 0777, true);
                chmod($cacheFolder, 0777);
            }
        } else {
            $useCache = false;
        }
        if ($useCache)
        {
            $cacheFile = $cacheFolder.DIRECTORY_SEPARATOR.md5($syrupFile).".syrupcache"; //we use a hash because we want to capture the uniqueness based on entire path

            //if the syrup file is newer than the cache, then refresh the cache
            if (file_exists($cacheFile) && (filemtime($syrupFile) > filemtime($cacheFile)))
            {
                unlink($cacheFile);
            }
            if (file_exists($cacheFile))
            {
                $syrup = unserialize(file_get_contents($cacheFile));
            } else {
                $parser = self::$_syrupParser;
                $syrup = $parser::ParseFileAsArray($syrupFile);
                file_put_contents($cacheFile, serialize($syrup));
            }
        } else {
            $parser = self::$_syrupParser;
            $syrup = $parser::ParseFileAsArray($syrupFile);
        }
        return $syrup;
    }

    public static function ParseSyrupString($syrupFile)
    {
        $parser = self::$_syrupParser;
        return $parser::ParseString($syrupFile);
    }

    public static function ParseSyrupStringAsArray($syrupFile)
    {
        $parser = self::$_syrupParser;
        return $parser::ParseStringAsArray($syrupFile);
    }
}