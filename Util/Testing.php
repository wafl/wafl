<?php

namespace Wafl\Util;

use DblEj\UnitTesting\ITester;

class Testing
{
    private static $_UNIT_TESTING_ENGINE;
    private static $_INTEGRATION_TESTING_ENGINE;

    /**
     * @return ITester
     */
    public static function GetIntegrationTestingEngine()
    {
        return self::$_INTEGRATION_TESTING_ENGINE;
    }

    public static function SetIntegrationTestingEngine(\DblEj\AutomatedTesting\Integration\ITesterExtension $tester)
    {
        self::$_INTEGRATION_TESTING_ENGINE = $tester;
    }

    /**
     * @return ITester
     */
    public static function GetUnitTestingEngine()
    {
        return self::$_UNIT_TESTING_ENGINE;
    }

    public static function SetUnitTestingEngine(\DblEj\AutomatedTesting\Integration\ITesterExtension $tester)
    {
        self::$_UNIT_TESTING_ENGINE = $tester;
    }
}